<?php

Yii::import("application.extensions.SocialNetworkAuth.Providers.*");
Yii::import("application.extensions.SocialNetworkAuth.SNAProvider");


class SocialNetworkAuthService extends CWidget {

    public $providers;
    public $linkCssClass;
    static protected $_identity;
    static protected $rememberMe;

    /**
     * @var SNAProvider[]
     */
    private $SNAProvidersList = array();

    public function init(){
        /**
         * Заполнение массива провайдеров
         */
        foreach($this->providers as $provider_name){
            $serviceProvider = self::getServiceProvider($provider_name);
            $this->SNAProvidersList[$provider_name] = $serviceProvider;

        }
        parent::init();
    }

    /**
     * @provider SNAProvider
     */
    public function run(){
        foreach($this->SNAProvidersList as $provider){
            echo $provider->getAuthUrl($this->linkCssClass);
        }
        parent::run();
    }

    /**
     * @param $className
     * @return SNAProvider
     */
    public static function getServiceProvider($className){
        $className = "SNAProvider".$className;
        return new $className();
    }

    public static function login($provider_name, $uid){
        if(self::$_identity===null)
        {
            self::$_identity=new UserIdentity(null,null,$provider_name,$uid);
            self::$_identity->authenticate();
        }
        if(self::$_identity->errorCode===UserIdentity::ERROR_NONE)
        {
            Yii::app()->user->login(self::$_identity);
            return true;
        }
        else
            return false;
    }


}


