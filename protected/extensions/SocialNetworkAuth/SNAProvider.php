<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 13.08.15
 * Time: 12:31
 */

abstract class SNAProvider {

    public $providerName ;

    protected $params ;
    protected $userParams ;

    protected $authUrl ;
    protected $accessTokenUrl ;
    protected $getUserInfoUrl ;
    protected $linkHtml = '';
    protected $accessToken = '';
    protected $userID = '';

    /**
     * Вернуть кнопку для аутентификации
     * @return string
     */
    public function getAuthUrl($class){
        $class = (isset($class)) ? "class=\"$class\"" : "";
        return '<p><a '.$class.' id="Provider_'.$this->providerName.'" href="' . $this->authUrl . '?' . urldecode(http_build_query($this->params)) . '"></a></p>';
    }

    /**
     * Вернуть URL для получения Access Token
     * @return string
     */
    public function getAccessTokenUrl(){
        return str_replace("&amp;", "&", $this->accessTokenUrl . '?' . urldecode(http_build_query($this->params))) ;
    }

    /**
     * Получить информацию о пользователе по запросу на сервер
     * @return User
     */
    abstract public function getUserInfo();

    /**
     * Добавить параметр в массив $params
     * @param $key
     * @param $value
     */
    public function addParam($key, $value){
        $this->params[$key] = $value;
    }

    public function removeParam($key)
    {
        if(isset($this->params[$key]))
            unset($this->params[$key]);
    }

    /**
     * Заполнить массив $userParams
     * @param array $array
     */
    public function addUserInfoParams(Array $array){
        foreach($array as $key => $value){
            $this->userParams[$key] = $value;
        }
    }

    /**
     * Получить Access Token по URL
     * @param $code
     * @return mixed
     */
    public abstract function getAccessTokenByRequest($code);


}