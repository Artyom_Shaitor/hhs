<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 13.08.15
 * Time: 12:30
 */


class SNAProviderFacebook extends SNAProvider{

    protected $params = array(
        'client_id' => '1542185586078109',
        'client_secret' => 'fc2b1a1b3342ca969073b275dbe707f2',
        'public_key'    => '1744c21b3c820ed56db78d5856cfbdc1',
        'redirect_uri' => "http://hhs.by/site/sociallogin?provider=Facebook",
        'response_type' => 'code',
        'scope' => 'email,publish_actions,user_birthday,user_about_me,user_location,user_hometown,user_photos'

    );

    protected  $userParams = array(
        'fields'       => 'uid,first_name,last_name,screen_name,sex,city,country,bdate,photo_big,email,contacts,city,country',
    );

    public $providerName = "facebook";

    protected $authUrl = 'https://www.facebook.com/dialog/oauth';
    protected $accessTokenUrl = 'https://graph.facebook.com/oauth/access_token';
    protected $linkHtml = 'Facebook';
    protected $getUserInfoUrl = 'https://graph.facebook.com/me';
    // me?fields=id,name,email,about,bio,cover,location,hometown


    /**
     * Получить Access Token по URL
     * @param $code
     * @return mixed
     */
    public function getAccessTokenByRequest($code){
        $this->addParam('code', $code);
        $this->addParam('grant_type', 'authorization_code');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->accessTokenUrl); // url, куда будет отправлен запрос
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($this->params))); // передаём параметры
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        parse_str($result, $array);

        $this->accessToken = $array['access_token'];

        $token = array();
        $token['access_token'] = $array['access_token'];

        return $token;
    }

    /**
     * Получить информацию о пользователе по запросу на сервер
     * @return User
     */
    public function getUserInfo()
    {
        if ($this->accessToken != "") {

            $params = array(
                'me' => array(
                    'access_token' => $this->accessToken,
                    'fields' => "id,name,email,about,bio,cover,location,hometown",
                ),
                'picture' => array(
                    'access_token' => $this->accessToken,
                    'type' => 'large',
                )
            );



            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->getUserInfoUrl . '/picture'); // url, куда будет отправлен запрос
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params['picture']))); // передаём параметры
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);

            $userInfo['picture'] = json_decode($result, true);
            $userInfo['me'] = json_decode(file_get_contents($this->getUserInfoUrl . '?' . urldecode(http_build_query($params['me']))), true);
            var_dump($userInfo);


            $user = new User;

            $user->username = $userInfo['me']['name'];
//            $user->avatar_url = (isset($userInfo['pic1024x768']))? $userInfo['pic1024x768'] : "";
            $user->created = time();
            $user->email = (isset($userInfo['me']['email']))? $userInfo['me']['email'] : "";
            $user->about = (isset($userInfo['me']['bio']))? $userInfo['me']['bio'] : "";
            $user->url = "";

            $country = Country::model()->findByPk(1);

            $user->country_id = $country->id;

            if(isset($userInfo['location']))
                if($userInfo['location']['countryName'] == $country->name) {
                    $city = City::model()->find("name=:n", array(
                        ":n" => $userInfo['location']['city'],
                    ));
                    $user->city_id = $city->id;
                    $user->region_id = $city->region->id;
                }

            $user->role = 1;
            $user->ban = 0;
            $user->is_confirmed = 1;
            $user->provider = $this->providerName;
            $user->uid = $userInfo['me']['id'];
            return $user;
        }

        return null;

    }
}