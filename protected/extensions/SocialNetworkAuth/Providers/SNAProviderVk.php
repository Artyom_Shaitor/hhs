<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 13.08.15
 * Time: 12:30
 */


class SNAProviderVk extends SNAProvider{

    protected $params = array(
        'client_id' => '5055016',
        'client_secret' => '7MrxD0gMnRvGX21t6gsb',
        'redirect_uri' => "http://hhs.by/site/sociallogin?provider=Vk",
        'response_type' => 'code',
        'scope' => 'email,photos,offline'

    );

    protected  $userParams = array(
        'fields'       => 'uid,first_name,last_name,screen_name,about,sex,city,country,photo_big,email,contacts,city,country',
    );

    public $providerName = "vk";

    protected $authUrl = 'http://oauth.vk.com/authorize';
    protected $accessTokenUrl = 'https://oauth.vk.com/access_token';
    protected $linkHtml = 'VK';
    protected $getUserInfoUrl = 'https://api.vk.com/method/users.get';


    /**
     * Получить Access Token по URL
     * @param $code
     * @return mixed
     */
    public function getAccessTokenByRequest($code){
        $this->addParam('code', $code);
        $array = json_decode(file_get_contents($this->getAccessTokenUrl()), true);

        $this->accessToken = $array['access_token'];
        $this->userID = $array['user_id'];


        $token = array();
        $token['access_token'] = $array['access_token'];
        $token['user_id'] = $array['user_id'];
        $token['expires_in'] = $array['expires_in'];

        return $token;
    }

    /**
     * Получить информацию о пользователе по запросу на сервер
     * @return User
     */
    public function getUserInfo()
    {
        $params = array(
            'uids'         => $this->userID,
            'fields'       => $this->userParams['fields'],
            'access_token' => $this->accessToken,
        );

        $url = $this->getUserInfoUrl . '?' . urldecode(http_build_query($params));

        $userInfo = json_decode(file_get_contents($url), true);

        if (isset($userInfo['response'][0]['uid'])) {

            $userInfo = $userInfo['response'][0];

            $user = new User;

            $user->username = $userInfo['first_name'] . " " .$userInfo['last_name'];
            $user->avatar_url = (isset($userInfo['photo_big']))? $userInfo['photo_big'] : "";
            $user->phone = (isset($userInfo['contacts']['mobile_phone']))? $userInfo['contacts']['mobile_phone'] : (isset($userInfo['contacts']['home_phone']))? $userInfo['contacts']['home_phone'] : null;
            $user->created = time();
            $user->about = (isset($userInfo['about']))? $userInfo['about'] : "";
            $user->url = (isset($userInfo['screen_name']))? $userInfo['screen_name'] : "";

            $country = Country::model()->findByPk(1);

            if(isset($userInfo['country']))
                if($userInfo['country']['title'] == $country->name) {
                    $user->country_id = $country->id;
                    $city = City::model()->find("name=:n", array(
                        ":n" => $userInfo['city']['title'],
                    ));
                    $user->city_id = $city->id;
                    $user->region_id = $city->region->id;
                }

            $user->role = 1;
            $user->ban = 0;
            $user->is_confirmed = 1;
            $user->provider = $this->providerName;
            $user->uid = $userInfo['uid'];
            return $user;
        }

        return null;

    }
}