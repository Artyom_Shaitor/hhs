<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 13.08.15
 * Time: 12:30
 */


class SNAProviderOk extends SNAProvider{

    protected $params = array(
        'client_id' => '1246735360',
        'client_secret' => 'E8BF9DA0867BABDF20860D48',
        'public_key'    => 'CBANKQELEBABABABA',
        'redirect_uri' => "http://hhs.by/site/sociallogin?provider=Ok",
        'response_type' => 'code',
        'scope' => 'email,photos,offline'

    );

    protected  $userParams = array(
        'fields'       => 'uid,first_name,last_name,screen_name,sex,city,country,bdate,photo_big,email,contacts,city,country',
    );

    public $providerName = "ok";

    protected $authUrl = 'https://connect.ok.ru/oauth/authorize';
    protected $accessTokenUrl = 'https://api.ok.ru/oauth/token.do';
    protected $linkHtml = 'OK';
    protected $getUserInfoUrl = 'http://api.ok.ru/fb.do';


    /**
     * Получить Access Token по URL
     * @param $code
     * @return mixed
     */
    public function getAccessTokenByRequest($code){
        $this->addParam('code', $code);
        $this->addParam('grant_type', 'authorization_code');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->accessTokenUrl); // url, куда будет отправлен запрос
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($this->params))); // передаём параметры
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

//        $array = json_decode(file_get_contents($this->getAccessTokenUrl()), true);
        $array = json_decode($result, true);

        $this->accessToken = $array['access_token'];
//        $this->userID = $array['user_id'];

        $token = array();
        $token['access_token'] = $array['access_token'];
//        $token['user_id'] = $array['user_id'];
//        $token['expires_in'] = $array['expires_in'];

        return $token;
    }

    /**
     * Получить информацию о пользователе по запросу на сервер
     * @return User
     */
    public function getUserInfo()
    {
        if($this->accessToken != "" && isset($this->params['public_key'])){
            $fields = 'name,uid,location,pic1024x768';
            $sign = md5("application_key={$this->params['public_key']}fields={$fields}format=jsonmethod=users.getCurrentUser" . md5("{$this->accessToken}{$this->params['client_secret']}"));

            $params = array(
                'method'          => 'users.getCurrentUser',
                'access_token'    => $this->accessToken,
                'application_key' => $this->params['public_key'],
                'fields'           => $fields,
                'format'          => 'json',
                'sig'             => $sign,
            );

            $userInfo = json_decode(file_get_contents($this->getUserInfoUrl . '?' . urldecode(http_build_query($params))), true);

            $user = new User;

            $user->username = $userInfo['name'];
            $user->avatar_url = (isset($userInfo['pic1024x768']))? $userInfo['pic1024x768'] : "";
            $user->created = time();
            $user->about = "";
            $user->url = "";

            $country = Country::model()->findByPk(1);

            if(isset($userInfo['location']))
                if($userInfo['location']['countryName'] == $country->name) {
                    $user->country_id = $country->id;
                    $city = City::model()->find("name=:n", array(
                        ":n" => $userInfo['location']['city'],
                    ));
                    $user->city_id = $city->id;
                    $user->region_id = $city->region->id;
                }

            $user->role = 1;
            $user->ban = 0;
            $user->is_confirmed = 1;
            $user->provider = $this->providerName;
            $user->uid = $userInfo['uid'];
            return $user;
        }

        return null;

    }
}