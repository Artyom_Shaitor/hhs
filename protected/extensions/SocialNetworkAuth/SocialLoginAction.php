<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 13.08.15
 * Time: 13:52
 */

Yii::import("application.extensions.SocialNetworkAuth.*");

class SocialLoginAction extends CAction{

    private $controller;
    private $id;

    public function __construct($controller, $id){
        parent::__construct($controller, $id);

        $this->controller = $controller;
        $this->id = $id;

    }

    public function run(){
        $provider = $_GET['provider'];
        $code = $_GET['code'];
        $_provider = SocialNetworkAuthService::getServiceProvider($provider);

        $token = $_provider->getAccessTokenByRequest($code);
        $user = null;

        if( isset($token['access_token'])){
            $user = $_provider->getUserInfo();

            $userExist = User::model()->find("uid=:uid AND provider=:provider", array("uid" => $user->uid, ":provider" => $user->provider));

            $isNew = false;
            if(!isset($userExist)) {
                $user->save();
                $isNew = true;
            }

            if(SocialNetworkAuthService::login($_provider->providerName, $user->uid)){
                if($isNew)
                    $this->controller->redirect("/profile/settings");
                else
                    $this->controller->redirect("/profile");
            }

        }

    }

}