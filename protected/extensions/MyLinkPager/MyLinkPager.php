<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 22.03.16
 * Time: 10:42
 */

class MyLinkPager extends CLinkPager {

    public function init()
    {
        $this->lastPageLabel = (string)$this->pageCount;
        parent::init();
    }

    public function run()
    {
        parent::run();
    }

}