<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.08.15
 * Time: 21:00
 */

class loginUser extends CValidator{

    public $compareAttribute;

    protected function validateAttribute($object, $attribute)
    {
        $compareAttribute = $this->compareAttribute;
        $password = PasswordService::encode($object->$compareAttribute);

        if($object->$attribute != "" && $object->$compareAttribute != "") {
            $user = User::model()->find("email=:email AND password=:password", array(
                ":email" => $object->$attribute,
                ":password" => $password
            ));

            if (!isset($user))
                $this->addError($object, $attribute, "Неправильные E-mail / Пароль");
        }
    }


    public function clientValidateAttribute($object,$attribute){

        return "
if(jQuery.trim(value) != '' && jQuery('#LoginForm_password').val() != ''){
    $.ajax({
        url : '".Yii::app()->getBaseUrl(true)."/core/AjaxGetUserByEmailAndPassword',
        method : 'POST',
        async : false,
        data : { email : value, password : jQuery('#LoginForm_password').val() },
        success : function(data){
            if(data == 'false') messages.push(".CJSON::encode("Неправильные E-mail / Пароль").");
        }
    })
}";

    }


}