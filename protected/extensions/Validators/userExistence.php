<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.08.15
 * Time: 21:00
 */

class userExistence extends CValidator{


    protected function validateAttribute($object, $attribute)
    {
        $user = User::model()->find("email=:email", array(":email" => $object->$attribute));

        if(isset($user)){
            $this->addError($object, $attribute, "Пользователь с таким e-mail уже существует");
        }

    }

    public function clientValidateAttribute($object,$attribute){

        return "
if(jQuery.trim(value) != ''){
    $.ajax({
        url : '".Yii::app()->getBaseUrl(true)."/core/AjaxGetUserByEmail',
        method : 'POST',
        async : false,
        data : { email : value },
        success : function(data){
            if(data == 'true') messages.push(".CJSON::encode("Пользователь с таким e-mail уже существует").");
        }
    })
}";



    }
}