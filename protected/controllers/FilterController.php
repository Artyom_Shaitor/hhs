<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.08.15
 * Time: 21:27
 */

class FilterController extends Controller{

    public function actionGetTable(){


        if(isset($_POST['category_id']) && isset($_POST['sub_category_id'])){
            $attributes = FilterService::performInputsInTable($_POST['category_id'], $_POST['sub_category_id']);

            if(count($attributes) > 0) {

                $result = "<li>Дополнительные параметры</li><div><table>";

                foreach ($attributes as $title => $input) {
                    $result .= "<tr>";

                    $result .= "<td>" . $title . "</td>";
                    $result .= "<td>" . $input . "</td>";

                    $result .= "</tr>";
                }

                $result .= "</table></div>";
                echo $result;
            }

        }else echo "false";

    }

}