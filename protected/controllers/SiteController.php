<?php

class SiteController extends Controller
{


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','catalog', 'catalogMini', 'logout', 'login', 'registration',
					'error', 'coreConfirmRegistrationEmail','sociallogin', 'getCityList',
					'getRegionList', 'getCountryList', 'catalogRegistration', 'passwordrecovery', 'ViewUserServicesByUrl', 'ViewUserServices', 'gensitemap'),

				'users'=>array('*'),
			),
			array('allow',
				'actions' => array('usersettings'),
				'roles'=>array('1', '2', '3')

			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionGenSiteMap()
	{
		$sitemap = new SiteMap();

		$sitemap->addUrl("/", SiteMap::NEVER, 0.95);
		$sitemap->addUrl(Catalog::getAllCatalogUrls(), SiteMap::DAILY, 0.9);
		$sitemap->addModule(User::model()->published()->findAll(), SiteMap::MONTHLY, 0.6);
		$sitemap->addModule(Service::model()->published()->findAll(), SiteMap::MONTHLY, 0.75);

		$sitemap->render();
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
//
			'sociallogin' => array(
				'class' => 'ext.SocialNetworkAuth.SocialLoginAction'
			)
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		Yii::import('application.components.IKeyWordsAdapter', true);

		KeyWordsProvider::putData("og:title", "Организация праздников и проведение свадеб в Беларуси");
		KeyWordsProvider::putData("title", "Организация праздников и проведение свадеб в Беларуси");

		$this->setPageTitle("Организация праздников и проведение свадеб в Беларуси");

		KeyWordsProvider::putData("og:image", Yii::app()->getBaseUrl(true)."/images/social.jpg");
		KeyWordsProvider::putData("image", Yii::app()->getBaseUrl(true)."/images/social.jpg");

		KeyWordsProvider::putData("og:description", Yii::app()->params['description']);
		KeyWordsProvider::putData("description", Yii::app()->params['description']);

		KeyWordsProvider::putData("og:url", Yii::app()->getBaseUrl(true));
		KeyWordsProvider::putData("url", Yii::app()->getBaseUrl(true));

		$this->render('index');
	}

	public function actionCreate()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('create');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	public function actionAjaxValidate(){
		$model = new User('registration');

		if(isset($_POST['ajax'])) {
			if ($_POST['ajax']=='form') {
				echo CActiveForm::validate($model);

			}
			if(!$model->validate())
				Yii::app()->end();
		}

	}


	public function actionUserSettings(){

	}

	/**
	 * Регистрация подрядчика
	 */
	public function actionCatalogRegistration($success = false){

		if(!Yii::app()->user->isGuest) {
			$this->redirect("/profile");
		}

		$model = new User('catalogRegistration');

		$this->setPageTitle("Регистрация подрядчика". " - ".Yii::app()->name );

		if(isset($_POST['User']))
		{

			$model->attributes=$_POST['User'];

			$user = User::model()->find("email = :email", array(":email" => $model->email)) ;

			/**
			 * Если такого пользователя не существует
			 */

			if(!isset($user)) {
				$model->role = 2;
				$model->is_confirmed = 0;
				$model->ban = 0;
				$model->phone = PhoneService::formatToInt($model->phone);
				$model->pro = 0;
				$model->country_id = 1;

				if ($model->save()) {


					if( $this->sendActivationCodeEMail($model->id, $model->email, $model->username) ) {
//							Yii::app()->user->setFlash('message', 'Вы были успешно зарегистрированы. Зайди на Вашу почту и активируйте аккаунт');
						echo CJSON::encode(array(
							'status' => 'success',
						));
//						Yii::app()->end();
					}

				}else {
					$error = CActiveForm::validate($model);
					if($error!='[]')
						echo $error;
//					Yii::app()->end();
				}
			}

			return;

		};

		if($success == true){
			$message = "Письмо с подтверждением отправлено Вам на e-mail<br>";

			$this->render('catalogRegistration', array(
				"message" => $message
			));
			return;
		}

		$this->render('catalogRegistration', array(
			"model" => $model
		));

	}

	/**
	 * Регистрация пользователя
	 */
	public function actionRegistration($success = false){


		if(!in_array($_SERVER['REMOTE_ADDR'], array("178.120.77.21", "::1", "192.168.1.1", "192.168.1.2"))) {
			$this->redirect('/registration/catalog');
			return;
		}

		if(!Yii::app()->user->isGuest) {
			$this->redirect("/profile");
		}

		/**
		 *
		 */

		$model = new User('registration');

		$this->performAjaxValidation($model);


		/**
		 * При нажатии на "Зарегистрироваться" или "Далее"
		 */
		if(isset($_POST['User']))
		{

			$model->attributes=$_POST['User'];

			$user = User::model()->find("email = :email", array(":email" => $model->email)) ;

			/**
			 * Если такого пользователя не существует
			 */
			if(!isset($user)) {
				$model->role = 1;
				$model->is_confirmed = 0;
				$model->ban = 0;
				$model->pro = 0;

				if ($model->save()) {

					$this->sendActivationCodeEMail($model->id, $model->email, $model->username);
					Yii::app()->user->setFlash('activation_code', $this->generateActivationCode($model->id, $model->email));
					echo CJSON::encode(array(
						'status' => 'success',
					));
					Yii::app()->end();

				}else {
					$error = CActiveForm::validate($model);
					if($error!='[]')
						echo $error;
					Yii::app()->end();
				}
			}

		};

		if($success == true){
			$message = "Письмо с подтверждением отправлено Вам на e-mail<br>";

			$this->render('registration', array(
				"message" => $message
			));
			return;
		}

		$this->render('registration', array(
			"model" => $model
		));


	}


	/**
	 * Проверка валидности имэил и кода активации.
	 * @param $activation_code
	 * @param $email
	 */
	public function actionCoreConfirmRegistrationEmail($activation_code, $email){
		/**
		 * @type $user User
		 */
		$user = User::model()->find("email = :email", array(":email" => $email));

		$code = $this->generateActivationCode($user->id, $email);

		if($activation_code == $code){
			$user->is_confirmed = 1;
			if($user->save()){
				Yii::app()->user->setFlash('confirm_success', 'Вы успешно подтвердили свой аккаунт. <br>Выполните вход');
				$this->redirect(CHtml::normalizeUrl("/profile/settings"));
			}
		}

	}

	/**
	 * Генерация кода активации
	 * @param $id
	 * @param $email
	 * @return string
	 */
	private function generateActivationCode($id, $email){
		$id = substr(md5($id),0, 16);
		$email = substr(md5($email),16, 16);

		return $id.$email;
	}

	/**
	 * Функция отправки сообщения-активации
	 * @param $id
	 * @param $email
	 * @return bool
	 */
	private function sendActivationCodeEMail($id, $email, $username){

		$activateUrl = Yii::app()->request->hostInfo."/activate/".$this->generateActivationCode($id, $email)."/email/".$email;

		$name='=?UTF-8?B?'.base64_encode("Подтверждение регистрации").'?=';
		$subject='=?UTF-8?B?'.base64_encode("Подтверждение регистрации").'?=';

		$message = "
<html>
	<head>
		<title>Подтверждение регистрации</title>
	</head>
	<body>
		Уважаемый {$username}!<br><br>
		Вы получили это письмо, потому что этот адрес электронной почты был указан при регистрации на свадебном сервисе hhs.by. <br/>

Для подтверждения электронного адреса, пожалуйста, перейдите по ссылке:<a href=\"{$activateUrl}\">{$activateUrl}</a> <br/>
Если данное письмо отправлено вам по ошибке, то просто проигнорируйте его. <br>
Пожалуйста, не отвечайте на это письмо, так как оно было выслано автоматически. <br/><br/>

С уважением Happy Holiday Service! <br/><br/>

Тел.: <a href=\"tel:+375292441199\">+375(29)244-11-99</a> <br>
		E-mail: <a href=\"mailto:info@hhs.by\">info@hhs.by</a> <br>
		<a href=\"http://hhs.by\">http://hhs.by</a> <br>

	</body>";


		$from = "noreply@hhs.by";

		$headers="From: Happy Holiday Service <noreply@hhs.by>\r\n".
			"Reply-To: noreply@hhs.by\r\n".
			"MIME-Version: 1.0\r\n".
			"Date:".date("Y-n-d")."\r\n".
			"Content-Type: text/html; charset=UTF-8";

		return mail($email,$subject,$message,$headers, "-f$from");
	}


	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		if(isset($_GET['confirmed'])){
			if ($_GET['confirmed'] == 'true'){
				echo 'Типа страница настроек';
			}
		}else {

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form'){
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm'])){
				$model->attributes=$_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid

				if($model->validate() && $model->login()) {
					$this->redirect((Yii::app()->user->returnUrl == "/")? "/profile" : Yii::app()->user->returnUrl);
				}


			}
			// display the login form
			$this->render('login',array('model'=>$model));
		}

	}

	public function actionPasswordRecovery(){
		if(isset($_POST['email'])){

			$email = htmlspecialchars($_POST['email']);

			if(trim($email) == ""){
				echo json_encode(array(
					"result" => false,
					"message" => 'Вы не ввели e-mail'
				));
				return;
			}

			/**
			 * @type $user User
			 */
			$user = User::model()->find("email=:email", array( ":email" => $email ));

			if($user == null){
				echo json_encode(array(
					"result" => false,
					"message" => 'Пользователя с таким e-mail не существует в системе'
				));
				return;
			}




			$password = PasswordService::generatePassword();

			$name='=?UTF-8?B?'.base64_encode("Восстановление пароля для личного кабинета").'?=';
			$subject='=?UTF-8?B?'.base64_encode("Восстановление пароля для личного кабинета").'?=';

			$message = "
<html>
	<head>
		<title>Восстановление пароля для личного кабинета</title>
	</head>
	<body>
		Уважаемый {$user->username}!<br><br/>
		Вы получили это письмо, так как вы послали запрос на восcтановление пароля к личному кабинету. <br/>
		Вход в личный кабинет: <a href=\"http://hhs.by/login/\">http://hhs.by/login/</a><br/>
		Ваш новый пароль: <b>{$password}</b>. <br/>
		Если вы не отправляли запрос на востановление пароля проигнорируйте данное сообщение. <br/><br/>

		С уважением Happy Holiday Service! <br/><br/>

		Тел.: <a href=\"tel:+375292441199\">+375(29)244-11-99</a> <br>
		E-mail: <a href=\"mailto:info@hhs.by\">info@hhs.by</a> <br>
		<a href=\"http://hhs.by\">http://hhs.by</a> <br>

	</body>
	";

			$from = "noreply@hhs.by";

			$headers="From: Happy Holiday Service <noreply@hhs.by>\r\n".
				"Reply-To: noreply@hhs.by\r\n".
				"MIME-Version: 1.0\r\n".
				"Content-Type: text/html; charset=UTF-8";

			if(mail($email,$subject,$message,$headers, "-f$from")){

				/**
				 * @var $user User
				 */

				$user->password = PasswordService::encode($password);
				$user->save(true, array('password'));
				echo json_encode(array(
					"result" => true,
					"message" => 'Новый пароль был выслан на указанный e-mail'
				));

			}
			else
				echo json_encode(array(
					"result" => false,
					"message" => 'Ошибка при отправке запроса. Извините за неудобства'
				));

		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}


	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionGetCityList(){

		$user = new User;

		echo CHtml::tag('option',
			array('value'=>''), CHtml::encode( $user->getAttributeLabel('city_id')),true);

		if($_POST['id'] != null ){
			$id = $_POST['id'];

			$cities=City::model()->findAll( array(
				'condition' => "regionid=".$id,
				'order' => "name"
			)  );

			$data = CHtml::listData($cities,'id','name');
			foreach($data as $value=>$name)
			{
				echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
			}

			return $data;
		}


		return array();
	}


	private function getServicesIdArrayByCondition($condition){
		$cri = new CDbCriteria();
		$cri->select = 't.id';
		$cri->addCondition($condition);
		$s = Service::model()->with('metadata')->findAll($cri);

		return  $this->getServicesIdsArray($s);
	}

	/**
	 *
	 * @param $models Service[]
	 * @return array
	 */
	private function getServicesIdsArray($models){
		$array = array();
		$result = array();
		foreach($models as $model){
			if(!isset($array[$model->id])){
				$array[$model->id] = 0;
				$result[] = $model->id;
			}
		}

		return $result;
	}

	public function actionViewUserServicesByUrl($url)
	{
		$user = User::model()->find("url=:u", array(
			":u" => $url,
		));
		$this->viewUserServices($user);
	}

	public function actionViewUserServices($id)
	{
		$user = User::model()->findByPk($id);
		$this->viewUserServices($user);
	}

	/**
	 * @param $user User
	 */
	private function viewUserServices($user)
	{
		$criteria = new CDbCriteria();
		$criteria->compare('author_id', $user->id);
		$criteria->compare('is_created', 1);

		$count=Service::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->applyLimit($criteria);

		$models = Service::model()->findAll($criteria);

		$this->setPageTitle("Услуги ".$user->username." - ".Yii::app()->name);

		$this->render('userServices', array(
			"model" => $user,
			"models" => $models,
			"pages" => $pages,
		));
	}

	public function actionCatalogMini($holiday_id, $category = null, $subCategory = null, $view = 'catalogMini')
	{
		if(!isset($holiday_id))
			throw new CException("Не верно указан holiday_id");
		$this->actionCatalog($category, $subCategory, $view, $holiday_id);
	}

	public function actionCatalog($category = null, $subCategory = null, $view = 'catalog', $holiday_id = null){

		$this->layout = "/layouts/catalogLayout";

		if(strpos(Yii::app()->request->urlReferrer, "/profile/holiday/view") != false && isset($_GET['holiday_id'])){
			SessionService::set("holiday_id", $_GET['holiday_id']);
			SessionService::set("holiday_url", Yii::app()->request->urlReferrer);
		}

		$search = new Search();
		$pageSize = 20;

		$errorMessage = "";

		$criteria = new CDbCriteria();
		$criteria->compare('is_created', 1);
		$criteria->order = "t.occupancy DESC, t.is_working_around ASC, t.created DESC";

		$services = Service::model()->findAll($criteria);

		$category = str_replace("%20", " ", $category);
		$subCategory = str_replace("%20", " ", $subCategory);

		$category = str_replace("+", " ", $category);
		$subCategory = str_replace("+", " ", $subCategory);

		$categoryId = 0;


		if($category != null){

			$category = Category::model()->find("name=:n", array(
				":n" => $category
			));

			$categoryId = $category->id;

			if($category != null){
				$search->category_id = $category->id;
				$criteria->compare('category_id', $search->category_id);
				$category = str_replace(" ", "+", $category->name);
			}
			else
				$errorMessage = "Извините, но такой категории не существует";
		}

		if($subCategory != null && $category != null && $errorMessage == ""){

			$subCategory = Category::model()->find("name=:n AND parent_cat_id=:cid", array(
				":n" => $subCategory,
				":cid" => $categoryId
			));

			if($subCategory != null){
				$search->sub_category_id = $subCategory->id;
				$criteria->compare('sub_category_id', $search->sub_category_id);
				$subCategory = str_replace(" ", "+", $subCategory->name);
			}
			else
				$errorMessage = "Извините, но такой подкатегории не существует";
		}

		if(isset($_GET['Search'])){

			$search->currency_id = ApplicationSettings::getSettings()->default_currency;

			foreach($_GET['Search'] as $field => $value) {
				$search->$field = $value;

				if($field != 'region_id' && $field != 'currency_id' && $field != 'old_category_id' && $field != "price_value" &&  $field != 'max_price_value' && $field != 'date' && $field != 'city_id')
					$criteria->compare($field, $value);
			}

			if(isset($search->max_price_value) && $search->max_price_value != null && $search->max_price_value != 0){
				$search->max_price_value = preg_replace("/\D/", "", $search->max_price_value);
				$priceService = new PriceService();

				$fromCurrency = Currencies::model()->findByPk($search->currency_id)->name;
				$currenciesIdArray = $priceService->getCurrenciesIDArray();

				$prices = array();
				foreach($priceService->getCurrenciesArray() as $key => $value){
					$prices[$currenciesIdArray[$key]] = $priceService->convertToCurrencyNumber($search->max_price_value, $fromCurrency, $key);
				}

				$pricesQuery = array();
				foreach($prices as $currency_id => $value){
					$pricesQuery[] = "(       (   (price_value<=$value AND price_value != 0) OR ($value>=min_price_value AND min_price_value != 0)   )     AND     currency_id=$currency_id    )";
				}

				$pricesQuery = implode(" OR ", $pricesQuery);

				$criteria->addCondition($pricesQuery);

			}



			if(isset($search->date)){

				$day_number = date("N",strtotime($search->date));
				$month_number = date("n", strtotime($search->date));
				$year = date("Y", strtotime($search->date));


				$weekends = Weekend::model()->findAll("day_number=:d AND (month_number=:m OR month_number=0) AND year=:y", array(
					":d" => $day_number,
					":m" => $month_number,
					":y" => $year,
				));

				$authorsIdArray = array();
				if(count($weekends) > 0){
					foreach($weekends as $weekend){
						$authorsIdArray[] = $weekend->author_id;
					}

				}

				$date = $search->refactorDate($search->date);

				$calendarDays = CalendarDay::model()->findAll("date=:d", array(
					":d" => $date,
				));

				foreach ($calendarDays as $day) {
					if($day->weekend == 1)
						$authorsIdArray[] = $day->author_id;
					else if($day->weekend == 0){
						foreach($authorsIdArray as $index => $author_id){
							if($day->author_id == $author_id)
								unset($authorsIdArray[$index]);
						}
					}
				}

				$criteria->addNotInCondition("author_id", $authorsIdArray);

			}


			if(isset($search->city_id) && $search->city_id != null){

				/**
				 * @type $cities CityServiceAssignment[]
				 */
				$cities = CityServiceAssignment::model()->findAll("city_id=:cid", array(
					":cid" => $search->city_id,
				));

				$tempCondition = "";

				if(count($cities) > 0) {
					$servicesId = array();
					foreach($cities as $city)
						$servicesId[] = $city->service_id;

					$tempCondition .= '(t.id IN ('.implode(',',$servicesId).')';
				}else
					$tempCondition .= '(city_id = 0';

				$tempCondition .= ") OR ( is_working_around = 1 )";
				$criteria->addCondition($tempCondition);

			}

			$conditionsSet = array();
			$servicesId = array();

			if(isset($_GET['MetaData'])){

				$count = count($_GET['MetaData']);

				foreach($_GET['MetaData'] as $attribute_id => $expr){
					foreach($expr as $type => $value){
						switch($type) {
							case 'number' :

								if(is_array($value)){
									if(isset($value['from']) && isset($value['to']) && !empty($value['from']) && !empty($value['to'])) {
										$s = $this->getServicesIdArrayByCondition("((value >= ".$value['from']. " AND value <= ".$value['to'].") OR value >= ".$value['to'].") AND attribute_id=".$attribute_id);
										$servicesId = array_merge($servicesId, $s);
									}
									else {
										if (isset($value['from']) && !empty($value['from'])) {
											$s = $this->getServicesIdArrayByCondition('(value>=' . $value['from'] . " AND attribute_id=" . $attribute_id.")");
											$servicesId = array_merge($servicesId, $s);


										} else if (isset($value['to']) && !empty($value['to'])) {
											$s = $this->getServicesIdArrayByCondition('(value>=' . $value['to'] . " AND attribute_id=" . $attribute_id.")");
											$servicesId = array_merge($servicesId, $s);
										}
									}

								} else {
									$s = $this->getServicesIdArrayByCondition('(value='.$value.")");
									$servicesId = array_merge($servicesId, $s);
								}
							break;
							case 'checkbox' :
								$query = array();
								foreach($value as $index => $id)
									$query[] = "value LIKE \"%$id%\"";
								$query = '(('.implode(' OR ', $query).') AND attribute_id='.$attribute_id.")";
								$s = $this->getServicesIdArrayByCondition($query);
								$servicesId = array_merge($servicesId, $s);

							break;
							case 'select' :
								if(!empty($value)) {
									$s = $this->getServicesIdArrayByCondition('(value=' . $value . " AND attribute_id=" . $attribute_id.")");
									$servicesId = array_merge($servicesId, $s);
								}
							break;

						}
					}
				}

				$resultServicesId = array();
				$existArray = array();

				if(count($servicesId) != 0) {

					$attributeCount = $count;
					$count = count($servicesId);

					for ($i = 0; $i < $count; $i++) {
						$repeatNumber = 0;
						for ($j = 0; $j < $count; $j++) {
							if ($servicesId[$i] == $servicesId[$j])
								$repeatNumber++;
						}


						if ($repeatNumber == $attributeCount && !isset($existArray[$servicesId[$i]])) {
							$resultServicesId[] = $servicesId[$i];
							$existArray[$servicesId[$i]] = 0;
						}
					}

					$criteria->addInCondition('t.id', $resultServicesId);

				}
				else $criteria->compare('t.id', 0);


			}


			$count = Service::model()->with('metadata')->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize = $pageSize;
			$pages->applyLimit($criteria);


		}
		else {
			$count = Service::model()->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize = $pageSize;
			$pages->applyLimit($criteria);

		}

		$dependency = new CDbCacheDependency(Service::DEPENDENCY_QUERY);

		$services = Service::model()->cache(Service::CACHE_DURATION, $dependency, 2)->with('metadata')->findAll($criteria);
		$servicesCount = $count;

		$holidays = Holiday::model()->findAll("author_id=:uid AND is_created=1", array(
			":uid" => Yii::app()->user->id,
		));

		$cityName = (!empty($search->city_id))? City::model()->findByPk($search->city_id)->name : "";
		$cityTitle = ($search->city_id != null && $search->city_id != 0)? ", ".$cityName : "";
		$category = ($category != "")? " ".str_replace("+", " ", $category) : "";


		$subCategory = ($subCategory != "" && $subCategory != "Другое")? ", ".str_replace("+", " ", $subCategory) : "";

		$pageTitle = "Свадебный каталог{$category}{$subCategory}{$cityTitle}";
		$this->setPageTitle($pageTitle);

		KeyWordsProvider::putData('og:title', $pageTitle);
		KeyWordsProvider::putData('title', $pageTitle);

		KeyWordsProvider::putData("og:image", Yii::app()->getBaseUrl(true)."/images/social.jpg");
		KeyWordsProvider::putData("image", Yii::app()->getBaseUrl(true)."/images/social.jpg");

		KeyWordsProvider::putData("og:description", Yii::app()->params['description']);
		KeyWordsProvider::putData("description", Yii::app()->params['description']);

		KeyWordsProvider::putData("og:url", Yii::app()->getBaseUrl(true)."/catalog/{$category}");
		KeyWordsProvider::putData("url", Yii::app()->getBaseUrl(true)."/catalog/{$category}");

		if($view == 'catalog')
			$this->render('catalog', array(
				"services" => $services,
				"model" => $search,
				"pages" => $pages,
				"errorMessage" => $errorMessage,
				"cityTitle" => $cityName,
				"holiday_id" => "",
				"holidays" => $holidays,
				"servicesCount" => $servicesCount,
			));
		else
			$this->renderPartial('catalogMini', array(
				"services" => $services,
				"model" => $search,
				"pages" => $pages,
				"errorMessage" => $errorMessage,
				"cityTitle" => $cityName,
				"holiday_id" => $holiday_id."/"
			));
	}

	public function actionGetRegionList(){

		$user = new User;
		echo CHtml::tag('option',
			array('value'=>''), CHtml::encode( $user->getAttributeLabel('region_id')),true);

		if($_POST['id'] != null ){
			$id = $_POST['id'];

			$regions=Region::model()->findAll( array(
				'condition' => "country_id=".$id,
				'order' => "name"
			)  );

			$data = CHtml::listData($regions,'id','name');
			foreach($data as $value=>$name)
			{
				echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
			}

			return $data;
		}



		return array();
	}

}