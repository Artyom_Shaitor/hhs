<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.08.15
 * Time: 21:27
 */

class CoreController extends Controller{



    public function actionAjaxGetUserByEmail(){

        if($_POST['email']) {

            $user = User::model()->find("email=:email", array(":email" => $_POST['email']));

            if(isset($user))
                echo "true";
            else
                echo "false";
        }

    }

    public function actionAjaxSetDayState(){
        if(isset($_POST['day'])){

            $day = $_POST['day'];
            $month = $_POST['month'];
            $year = $_POST['year'];
            $state = (int) $_POST['state'];

            $day = ($day < 10)? "0".$day : $day;
            $month = ($month < 10)? "0".$month : $month;

            $date = date("Y-m-d", strtotime($year."-".$month."-".$day));

            $isGlobalWeekend = Weekend::model()->isGlobalWeekend($date);

            CalendarDay::model()->deleteAllByAttributes(array(
                "date" => $date,
                "author_id" => Yii::app()->user->id,
            ));

            if($isGlobalWeekend){

                if($state == 0){
                    $model = new CalendarDay;

                    $model->date = $date;
                    $model->service_id = 0;
                    $model->author_id = Yii::app()->user->id;
                    $model->reserved = $model->request = 0;
                    $model->weekend = 0;

                    if($model->save())
                        echo "workday";

                }else{
                    echo "weekend";
                }


            }else{

                if($state == 1){
                    $model = new CalendarDay;

                    $model->date = $date;
                    $model->service_id = 0;
                    $model->author_id = Yii::app()->user->id;
                    $model->reserved = $model->request = 0;
                    $model->weekend = 1;

                    if($model->save())
                        echo "weekend";

                }else{
                    echo "workday";
                }

            }

        }
    }


    public function actionAjaxGetAllUsersCalendarDaysWithWeekends(){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
            $month = $_POST['month'] + 1;
            $year = $_POST['year'];
            $isYC = $_POST['isYC'];


            $monthBlock = floor($month/4);

            $today = date("Y-n-j");
            $today_stamp = strtotime($today);

                $first_month = 3*$monthBlock+1;
                $last_month = 3*$monthBlock+3;

                $first_month_zero = ($first_month < 10)? "0".$first_month : $first_month;
                $last_month_zero = ($last_month < 10)? "0".$last_month : $last_month;


            $min_time_stamp = strtotime($year."-".$first_month_zero."-01");
//            echo $monthBlock."|".$year;
            $max_time_stamp = strtotime($year."-".$last_month_zero."-".cal_days_in_month(CAL_GREGORIAN, $last_month, $year));

            $min_time_stamp = ($today_stamp > $min_time_stamp)? $today_stamp : $min_time_stamp;

            $min_date = date("Y-n-j", $min_time_stamp);
            $max_date = date("Y-n-j", $max_time_stamp);

            $model = CalendarDay::model()->findAll("author_id=:id AND (date>=:min AND date<=:max)", array(
                ":id" => $id,
                ":min" => $min_date,
                ":max" => $max_date,
            ));

            $result = array();
            $result["fullWeekendsList"] = DateService::datesToFullWeekendsList($model, $id);
            $result["manageListCount"] = 0;

            if($isYC == 'true') {
                $user = User::model()->findByPk(Yii::app()->user->id);
                $array = DateService::performWeekendDays($user, $year, true, true);
                $result["weekendsManageList"] = $array;
                $result['manageListCount'] = count($array);
            }

            echo json_encode($result);

        }
    }

    public function actionAjaxGetUserByEmailAndPassword(){

        if($_POST['email']){

            $password = PasswordService::encode($_POST['password']);
            $email = $_POST['email'];

            $user = User::model()->find("email=:email AND password=:password", array(
                ":email" => $email,
                ":password" => $password
            ));

            if(isset($user))
                echo 'true';
            else
                echo 'false';

        }else echo 'false';
    }


    public function actionAjaxlogin(){
        $model=new LoginForm;


        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
//				$this->redirect(Yii::app()->user->returnUrl);
				echo "true";
            }else
                echo"false";
        }
        else
            echo"false";

    }

    public function actionAjaxGetServicesDataInCalendar(){
        if(isset($_POST['day'])){

            $day = $_POST['day'];
            $month = $_POST['month'];
            $year = $_POST['year'];

            $day = ($day < 10)? "0".$day : $day;
            $month = ($month < 10)? "0".$month : $month;

            $date = date("Y-m-d", strtotime($year."-".$month."-".$day));

            $model = CalendarDay::model()->findAllByAttributes(array(
                "author_id" => Yii::app()->user->id,
                "date" => $date
            ));

            $resultArray = array();

            if(count($model) > 0){

                /**
                 * @var $calendarDay CalendarDay
                 */
                foreach($model as $calendarDay){

                    if(!isset($resultArray[$calendarDay->service_id]))
                        $resultArray[$calendarDay->service_id] = array();

                    $resultArray[$calendarDay->service_id]["reserved"] = (int) $calendarDay->reserved;
                    $resultArray[$calendarDay->service_id]["request"] = (int) $calendarDay->request;

                }

            }

            echo json_encode($resultArray);

        }

    }

    public function actionAjaxSetServiceState(){
        if(isset($_POST['state'])){
            $state = $_POST['state'];
            $stateType = $_POST['state_type'];
            $serviceID = $_POST['serviceID'];

            $day = $_POST['day'];
            $month = $_POST['month'];
            $year = $_POST['year'];


            $day = ($day < 10)? "0".$day : $day;
            $month = ($month < 10)? "0".$month : $month;

            $date = date("Y-m-d", strtotime($year."-".$month."-".$day));

            $model = CalendarDay::model()->find("author_id=:aid AND date=:d AND service_id=:sid", array(
                ":aid" => Yii::app()->user->id,
                ":sid" => $serviceID,
                ":d" => $date
            ));


            if($model == null){
                $model = new CalendarDay;
                $model->date = $date;
                $model->service_id = $serviceID;
                $model->author_id = Yii::app()->user->id;
                $model->request = $model->reserved = $model->weekend = 0;

            }

            $state = ( (int) $state == 1)? 0 : 1;
            $model->$stateType = $state;

            $resultArray = array();

            if($model->request == 0 && $model->reserved == 0 && $model->weekend == 0){

                if($model->delete()){
                    $resultArray["service_id"] = (int) $serviceID;
                    $resultArray["state_type"] = $stateType;
                    $resultArray["state"]      = (int) $state;

                    echo json_encode($resultArray);
                    return;
                }

            }

            if($model->save()) {
                $resultArray["service_id"] = (int) $serviceID;
                $resultArray["state_type"] = $stateType;
                $resultArray["state"]      = (int) $state;

                echo json_encode($resultArray);
            }
            else
                echo false;

        }
    }

    public function actionAjaxGetChildrenCategories(){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
            $model = Category::model()->findAll("parent_cat_id=:id ORDER BY name ASC", array(":id" => $id));
            Category::refactorSubCategoriesList($model);

            $listData = CHtml::listData($model, 'id', 'name');

            $htmlOptions = array();

            if(isset($_GET['withPrompt'])){
                    $htmlOptions = array('encode'=>true);
            }else
                $htmlOptions = array('prompt'=>'Выберите подкатегорию','encode'=>true);

            $return = CHtml::listOptions('0', $listData, $htmlOptions);

            echo $return;

        }
    }

    public function actionAjaxGetChildrenRegions(){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
            $model = Region::model()->findAll("country_id=:id", array(":id" => $id));

            $listData = CHtml::listData($model, 'id', 'name');

            $htmlOptions = array('prompt'=>'Выберите регион','encode'=>true);
            $return = CHtml::listOptions('0', $listData, $htmlOptions);

            echo $return;

        }
    }

    public function actionAjaxGetChildrenСities(){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
            $model = City::model()->findAll("regionid=:id", array(":id" => $id));

            $listData = CHtml::listData($model, 'id', 'name');

            $htmlOptions = array('encode'=>true);
                if(count($listData) == 0) $htmlOptions = array_merge(array('prompt' => 'Выберите город'), $htmlOptions);

            $return = CHtml::listOptions('0', $listData, $htmlOptions);

            echo $return;

        }
    }


}