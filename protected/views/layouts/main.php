<?php /* @var $this Controller */
	date_default_timezone_set("Europe/Minsk");
	$baseUrl = Yii::app()->request->baseUrl;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml"
	lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="ru">
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/stylesheet.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/header.css"/>


	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/gallery.css"/>


<!--	--><?php //if(CheckPage::isPages(array("site/index", "site/registration", 'site/catalogregistration', "site/login"))) : ?>
<!--		<link rel="stylesheet" type="text/css" href="--><?php //echo $baseUrl; ?><!--/css/main.css"/>-->
<!--	--><?php //endif; ?>

	<?php if(CheckPage::isPage("site/catalog")) : ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/catalog.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/service.css"/>
	<? endif; ?>


	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<!--	<script src="--><?php //echo $baseUrl; ?><!--/scripts/jquery-2.1.4.min.js"></script>-->
	<?Yii::app()->getClientScript()->registerCoreScript('jquery');?>
	<script src="<?php echo $baseUrl; ?>/scripts/header.js"></script>
	<script src="<?php echo $baseUrl; ?>/scripts/controller.js"></script>
	<script src="<?php echo $baseUrl; ?>/scripts/gallery.js"></script>
	<script src="<?php echo $baseUrl; ?>/scripts/retina.min.js"></script>

</head>

<body>

<!--<div id="content">-->
	<header>

		<div class="container">
			<div class="logo">
				<a href="<? echo CHtml::normalizeUrl("/")?>"><img src="<?php echo $baseUrl; ?>/images/logo.svg" alt="HHS"/></a>
			</div>
			<div class="menu">
				<?php $this->widget("zii.widgets.CMenu", array(
					"activeCssClass" => "active-menu",
					"items" => array(
						array('label'=>'Регистрация в каталоге', 'url'=>array('/registration/catalog'), "visible" => Yii::app()->user->isGuest, 'linkOptions' => array('class' => 'create-holiday'), 'active' => false),
						array('label'=>'Каталог', 'url'=>array('/catalog'), "active" => CheckPage::isPage("site/catalog")),
						array('label'=>'Кабинет', 'url'=>array('/profile/'), "visible" => !Yii::app()->user->isGuest, 'itemOptions' => array('id' => 'profile')),
						array('label'=>'Вход', 'url'=>array('/login'), "visible" => (Yii::app()->user->isGuest), 'active' => CheckPage::isPages(array('site/login', 'site/registration'))),
					),
					"htmlOptions" => array("class" => "header-menu")
				)) ?>
			</div>
		</div>

	</header>

	<? if(Yii::app()->user->isGuest && !CheckPage::isPages(array("site/registration", "site/login"))) : ?>

	<div class="popup-fullsize">

		<div id="log-in" class="popup-content">

			<a class="close-pop-up" href=""></a>

			<!--					--><?php //$this->widget('application.extensions.SocialNetworkAuth.SocialNetworkAuthService', array(
//						'providers' => array('Vk'),
//						'linkCssClass' => 'social-link'
//					)); ?>



		</div>
	</div>

	<? endif; ?>

	<div class="header-padding"></div>

		<?php echo $content?>


<!--</div>-->

<? $this->renderPartial('application.views.site._footer'); ?>

</body>
</html>

