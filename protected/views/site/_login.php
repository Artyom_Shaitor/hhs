
<?
/**
 * @var $model LoginForm
 * @var $this Controller
 * @var $form CActiveForm
 */

    if(isset($id))
        $id = ($id != "" || $id != null)? $id = 'id="'.$id.'"' : "";

    $actionUri = null;

    if(isset($action))
        $actionUri = $action;
    else
        $actionUri = "/login";

?>

<div class="form" <?=$id?> >

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <div class="row">
        <?php echo $form->textField($model,'email', array("class"=>"input-enter", "placeholder" => $model->getAttributeLabel('email'))); ?>
        <div class="error-message"><?php echo $form->error($model,'email'); ?></div>
    </div>

    <div class="row">
        <div>
            <?php echo $form->passwordField($model,'password', array("class"=>"input-enter", "placeholder" => $model->getAttributeLabel('password'))); ?>
            <a class="show-password" href="/"></a>
            <div class="forgot-title">
                <a href="/">?</a>
            </div>
            <div class="forgot-block">

                <h5 class="recover-success"></h5>
                <h3>Забыли пароль?</h3>
                <span class="h4">Введите Ваш e-mail и получите новый</span>
                <input type="email" class="forgot-email" name="email"/>
                <input type="button" class="red button-recover" value="ОТПРАВИТЬ"/>
            </div>
            <div class="transparent-fullscreen-block"></div>
        </div>
        <div class="error-message"><?php echo $form->error($model,'password'); ?></div>
    </div>

    <div class="row rememberMe">
        <?php echo $form->checkBox($model,'rememberMe'); ?>
        <?php echo $form->label($model,'rememberMe', array("style" => 'color:white')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('ВОЙТИ'); ?>
    </div>

    <a class="red-button" href="/registration/catalog">РЕГИСТРАЦИЯ</a>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script src="/scripts/is.min.js"></script>
<script>
    if(is.safari() || is.firefox()){
        $("a.show-password").css("margin-top", "12px");
    }
</script>

<style>
    .error, .error .error-message {
        opacity: 1;
        /*margin-top: -80px !important;*/
    }

    .error-message {
        margin-left: 330px;
        margin-top: -37px;
    }

    .errorMessage {
        display: block;
    }

    .red-button {
        color: #ff5c00 !important;
        border: 1px solid #ff5c00 !important;
    }

    .red-button:hover {
        background-color: #ff5c00 !important;
        color:white !important;
    }

    .transparent-fullscreen-block {
        position: fixed;
        top:0;
        left:0;
        width:100%;
        height:100%;
        background-color: rgba(0,0,0,0.4);
        display: none;
        z-index: 995;
    }

    .recover-success {
        font-weight: normal;
        font-size: 14px;
        position: absolute;
        right: 0;
        left: 0;
        top: -65px;
        color: white;
        display: none;
    }

    .recover-failed {
        font-weight: normal;
        font-size: 14px;
        position: absolute;
        right: 0;
        left: 0;
        top: -65px;
        color: rgb(255, 160, 160);
        display: none;
    }

    .red {
        font-size: 15px;
        width: 200px;
        background-color: transparent;
        box-sizing: border-box;
        padding: 9px 0 6px 0;
        margin-top: 14px;
    }


</style>