<?php
$categories = Category::model()->findAll("parent_cat_id IS NULL OR parent_cat_id=0");
if(!CheckPage::isPages(array("site/registration", 'site/login', 'site/catalogregistration'))) : ?>
    <footer style="color:white; box-sizing: border-box;">
        <div class="container1200" style="margin:0 auto;padding: 0 25px;">
            <table>
                <tr>
                    <td>
                        <h4>Каталог</h4>
<!--                        <br/>-->
<!--                        --><?// echo Category::getAllChildrenCategoriesInUL($categories[0]); ?>
                        <br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[1]); ?>
                    </td>
                    <td>
                        <br/><br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[2]); ?>
                        <br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[3]); ?>
                        <br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[4]); ?>
                    </td>
                    <td>
                        <br/><br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[5]); ?>
                        <br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[6], 0, 11); ?>
                    </td>
                    <td>
                        <br/><br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[6], 12); ?>
                        <br/>
                        <? echo Category::getAllChildrenCategoriesInUL($categories[7]); ?>
                    </td>
                    <td style="text-align: right;">
                        <br/><br/>
<!--                        <a class="right-links" href="">О компании</a><br/>-->
                        <a class="right-links" href="/registration/catalog">Стать специалистом</a><br/>
<!--                        <a class="right-links" href="">Справка и помощь</a><br/>-->
<!--                        <a class="right-links" href="">Контакты</a><br/>-->

                        <div class="policy">
                            <a href="/user-agreement.pdf" target="_blank">Пользовательское соглашение</a>
                        </div>

                        <div class="address">
                            <a href="tel:+375292441199">+375292441199</a><br>
                            По всем вопросам: <a href="mailto:info@hhs.by">info@hhs.by</a><br/>
                            Тех. поддержка: <a href="mailto:support@hhs.by">support@hhs.by</a>
                        </div>

                    </td>
                </tr>
            </table>

            <div class="under">
                <table>
                    <tr>
                        <td style="padding-left: 20px"><img src="/images/icons/copyright.svg" style="width: 15px;height: 15px;margin-top: -2px; margin-left:-20px; position: absolute;" alt="hhs.by"/>2015, HHS.BY, УНП 291266292</td>
                        <td><a class="footer-link" href="https://vk.com/hhservice"><i class="vk-icon"></i>Группа Вконтакте</a></td>
                        <td>Разработка: Moon Design</td>
                    </tr>
                </table>
            </div>

        </div>
    </footer>
<?php endif; ?>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'fMPtC7mBwG';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74351313-1', 'auto');
    ga('send', 'pageview');

</script>