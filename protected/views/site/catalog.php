<?php
/**
 * @var $this SiteController
 * @var $services Service[]
 * @var $model Search
 * @var $pages CPagination
 * @var $form CActiveForm
 * @var $holidays Holiday[]
 * @var $servicesCount integer
 */

$priceService = new PriceService();

function getActionUrl()
{
	$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
	return'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
}

/**
 * Получение get параметров из ссылки
 * @return array
 */
function getParamsArray()
{
	$pattern = '/%5B(\w+)%5D/';
	$replacement = "[$1]";

	$subject = preg_replace($pattern, $replacement, substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "?")+1));
	$params = explode('&', $subject);
	$result = [];

	foreach($params as $value){
		$value = explode('=', $value);
		if(count($value) > 1)
			$result[$value[0]] = $value[1];
	}

	return $result;
}

/**
 * Удаляет из массива элементы с ключом MetaData
 * @param $array
 */
function removeMetaData(&$array)
{
	$pattern = '/^MetaData/';
	foreach($array as $key => $value){
		if(preg_match($pattern, $key) == 1)
			unset($array[$key]);
	}
}

$params = getParamsArray();
removeMetaData($params);

$params = (empty($params))? "" : "?".http_build_query($params);

?>

<style>

	header {
		left:0;
		z-index: 1000;
		box-shadow: 0 -4px 10px black;
	}

	.header-padding {
		background-color: #eaeaea;
	}

	#content.container960 {
		margin-top: 10px !important;
	}

	ul.yiiPager li a {
		display: inline-block;
		padding: 7px !important;
		font-size: 16px;
		border: 0 !important;
	}

	ul.yiiPager .page a {
		line-height: 16px;
		width:16px;
	}

	ul.yiiPager li {
		margin:0;
		display: inline !important;

	}

	ul.yiiPager .selected a {
		background: #ff5c00;
		border-radius: 100%;
		color: #FFFFFF !important;
		font-weight: bold;
	}

	ul.yiiPager a:link, ul.yiiPager a:visited {
		color: rgb(70,70,70);
	}




</style>

<? if(!SessionService::isEmpty("holiday_id")) : ?>
<div class="choose-holiday-deny-block">
	Выберите услугу для своего праздника <a id="deny-adding-holiday" class="no" href="/">Отмена</a>
</div>

<style>
	.choose-holiday-deny-block {
		position: fixed;
		top: 0;
		z-index: 3000;
		font-size: 18px;
		font-weight: 600;
		width: 500px;
		text-align: center;
		left: 50%;
		margin-left: -250px;
		margin-top: 19px;
		color: rgba(0,0,0,0.7)
	}

	a.no {
		text-decoration: none;
		color: #d6224a;
		border: 1px solid #d6224a;
		padding: 7px 20px 5px 20px;
		border-radius: 40px;
		margin-left: 10px;
	}

	a.no:hover {
		background-color: #d6224a;
		color: white;
	}
</style>

<script>
	$(document).on("click", "#deny-adding-holiday", function(){
		var xhr = new XMLHttpRequest();

		xhr.onload = function(){
			var response = JSON.parse(xhr.response);
			var url = response['url'];
			if(url == undefined || url == '')
				location.reload();
			else
				window.location = response['url'];
		};

		xhr.open("POST", "/profile/holiday/clearSession");
		xhr.send();

		return false;
	})
</script>
<? endif; ?>

<script src="/scripts/pickmeup/jquery.pickmeup.min.js"></script>
<script src="/scripts/autoNumeric-min.js"></script>
<link rel="stylesheet" href="/css/pickmeup/pickmeup.css"/>


<h1 class="main-h1">

	<? if($model->category_id == null || $model->category_id == 0) : ?>
		Каталог
	<? elseif($model->sub_category_id == null || $model->sub_category_id == 0) : ?>
		<? echo Category::model()->findByPk($model->category_id)->name ?>
	<? else : ?>
		<? echo Category::model()->findByPk($model->sub_category_id)->name ?>
	<? endif; ?>

	<? if($model->city_id != null && $model->city_id != 0) : ?>
		<span class="dot">&#149;</span> <? echo $cityTitle; ?>
	<? endif; ?>
</h1>

<style>
	.dot {
		font-size: 22px;
		margin: 0 10px;
	}
</style>

<div id="content" class="container960">

	<div class="col-span-3">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'filter-form',
		'enableAjaxValidation'=>false,
		'method' => "GET",
		'action' => getActionUrl(),
	)); ?>

	<div class="filter-title">Категория</div>
	<div class="filter-block">
		<? if(isset($model->category_id) && $model->category_id != null) : ?>
			<a class="remove-from-filter" data-field="category_id" href="/catalog"></a>
		<? endif ?>

		<?php
			$array = Category::getAllParentCategories(array(1));
			foreach($array as $key => $value) :	?>

<!--				<input type="button" name="Search[category_id]" value="--><?// echo $value?><!--" data-id="--><?// echo $key?><!--" class="category-buttons --><?// if($model->category_id == $key) echo "active-category" ?><!--"/>-->
				<a href="/catalog/<? echo str_replace(" ", "+", $value) ?>" class="category-buttons <? if($model->category_id == $key) echo "active-category" ?>"><? echo $value ?></a>
		<?  endforeach ?>

	</div>

	<? if(isset($model->category_id) && $model->category_id != null) : ?>
			<? $categoryTitle = str_replace(" ", "+", Category::model()->findByPk($model->category_id)->name); ?>
			<div class="filter-title">Подкатегория</div>
			<div class="filter-block">
				<? if(isset($model->sub_category_id) && $model->sub_category_id != null) : ?>
					<a class="remove-from-filter" data-field="sub_category_id" href="/catalog/<? echo $categoryTitle ?> "></a>
				<? endif ?>
<!--				--><?// echo $form->radioButtonList($model, 'sub_category_id', Category::getAllChildrenCategories($model->category_id)) ?>

				<?
					$subCategories = Category::getAllChildrenCategories($model->category_id);

					foreach($subCategories as $key => $title){
						$value = str_replace(" ", "+", $title);
						$active = ($model->sub_category_id == $key)? "active" : "";
						echo "<a class='sub-categories-link {$active}' href='/catalog/{$categoryTitle}/{$value}{$params}'>{$title}</a>";
					}
				?>

			</div>

			<div class="filter-title">Дата</div>
			<div class="filter-block" data-name="date">
				<? if(isset($model->date) && $model->date != null) : ?>
					<a class="remove-from-filter" data-field="date" href="/"></a>
				<? endif ?>
				<? echo $form->textField($model, 'date', array("style" => "text-align:center; width:97%;")) ?>
			</div>

			<div class="filter-title">Город</div>
			<div class="filter-block">
				<? if(isset($model->city_id) && $model->city_id != null) : ?>
					<a class="remove-from-filter" data-field="city_id" href="/"></a>
				<? endif ?>
				<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'class' => 'item-region') ) ?>
				<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'class' => 'item-city') ) ?>
			</div>

			<div class="filter-title">Цена</div>
			<div class="filter-block" data-name="price">
				<? if((isset($model->max_price_value) && $model->max_price_value != null) || (isset($model->price_type) && $model->price_type != null)) : ?>
					<a class="remove-from-filter" data-field="max_price_value" href="/"></a>
				<? endif ?>
				до <?php echo $form->textField($model, 'max_price_value', array("style" => "margin:10px 0 10px 7px"))?>
				<div class="price-types-currencies">
					<?php echo $form->radioButtonList($model, 'currency_id', Currencies::getAllCurrencies())?>
					<hr/>
					<? echo $form->radioButtonList($model, 'price_type', PriceService::$priceTypesTitlesForCatalog ) ?>
				</div>
			</div>

			<? if(isset($model->sub_category_id)) { ?>
				<div class="filter-title">Дополнительные параметры</div>
				<div class="filter-block" id="additional-params">
					<? if(isset($_GET['MetaData'])) : ?>
						<a class="remove-from-filter" data-field="meta-data-input" href="/"></a>
					<? endif ?>
					<? echo FilterService::performInputsForFilterCatalog($model); ?>
				</div>
			<? } ?>


		<a href="" id="submit-anchor"></a>
		<input type="submit" value="Обновить" />

	<? endif; ?>

		<input type="submit" value="Обновить" style="display: none;" />

		<input type="hidden" name="Search[category_id]" value="<? echo $model->category_id; ?>"/>
		<input type="hidden" name="Search[old_category_id]" value="<? echo $model->old_category_id; ?>"/>
	<?php $this->endWidget(); ?>

</div><div class="col-span-9">
		<?
		if(count($services) > 0 && $errorMessage == "") {

			echo '<div style="text-align: center; position: relative; margin-bottom:20px;">';
			$this->widget('application.extensions.mylinkpager.MyLinkPager', array(
				'pages' => $pages,
				'prevPageLabel' => '&laquo;',
				'nextPageLabel' => '&raquo;',
				"header" => "",
				"previousPageCssClass" => "first",
				"nextPageCssClass" => "last",
				'firstPageLabel' => '1',
				"firstPageCssClass" => "my-first",
				"lastPageCssClass" => "my-last",
				"maxButtonCount" => 5,
			));

//			$foundedValue = "";
//			switch($servicesCount){
//				case ( $servicesCount%100 > 10 && $servicesCount%100 < 20) : $foundedValue = "Найдено <b>{$servicesCount}</b> услуг"; break;
//				case ( $servicesCount%10 > 1 && $servicesCount%10 < 5) : $foundedValue = "Найдено <b>{$servicesCount}</b> услуги"; break;
//				case 1 : $foundedValue = "Найдена <b>1</b> услуга"; break;
//				default : $foundedValue = "Найдено <b>{$servicesCount}</b> услуг"; break;
//			}
//
//			?>
<!--					<div class="founded left">--><?// echo $foundedValue ?><!--</div>-->
<!--					<div class="founded right">Показано --><?// echo ($pages->getOffset()+1)."-".(($pages->getOffset()+$pages->pageSize > $servicesCount)? $servicesCount : $pages->getOffset()+$pages->pageSize) ?><!-- услуг</div>-->
<!--				</div>-->
<!--			--><?//

			echo "</div>";

			foreach ($services as $service) {
				$this->renderPartial('application.modules.profile.views.default._service_list_item', array(
					"model" => $service,
					"priceService" => $priceService,
				));
			}

			echo '<div style="text-align: center">';
			$this->widget('application.extensions.mylinkpager.MyLinkPager', array(
				'pages' => $pages,
				'prevPageLabel' => '&laquo;',
				'nextPageLabel' => '&raquo;',
				"header" => "",
				"previousPageCssClass" => "first",
				"nextPageCssClass" => "last",
				'firstPageLabel' => '1',
				"firstPageCssClass" => "my-first",
				"lastPageCssClass" => "my-last",
				"maxButtonCount" => 5,
			));
			echo '</div>';
		}

		else {
			echo "<h4 class='no-services-match'>";
			echo ($errorMessage != "") ? $errorMessage : "Извините, но по Вашему запросу не найдено ни одной услуги";
			echo "</h4>";
		}
		?>
</div>


</div>

<?
	if(in_array(Yii::app()->user->id, array(6, 7, 8, 12)))
		$this->renderPartial('_choose_holiday', array(
			"holidays" => $holidays
		))
?>



<style>

	.founded {
		position: absolute;
		/*right: 0;*/
		top: 6px;
	}

	.founded.left {
		left: 0;
	}

	.founded.right {
		right: 0;
	}

	.no-services-match {
		margin-top: 0;
	}

	.col-span-9 {
		padding-top: 44px;
	}

	input[type='submit'] {
		-webkit-appearance: none;
		border: 1px solid #ff5c00;
		box-sizing: border-box;
		width: 240px;
		position: absolute;
		padding: 10px 0px 8px 0;
		font-size: 14px;
		background-color: #ff5c00;
		color:rgba(255,255,255,0.95);
		text-transform: uppercase;
		font-family: "Officina";
		font-weight: 600;
		outline: none;

	}

	input[type='submit']:hover {
		cursor: pointer;
		color: #ff5c00;
		background-color: white;
	}

	form#filter-form {
		padding-bottom: 45px;
	}

	.submit-fixed {
		position: fixed !important;
		bottom: 0;
	}

</style>

<? if(in_array(Yii::app()->user->id, array(6,7,8,12))) : ?>
	<script>



	</script>
<? endif; ?>


<? Yii::app()->clientScript->registerScript(
	'pickmeup', '

		var windowScroll = function(){
			if($("#submit-anchor").length > 0){
				var yOffset = window.pageYOffset;
				var anchorPositionBottom = $("#submit-anchor").offset().top+36;
				var windowHeight = document.documentElement.clientHeight;

				if(yOffset >= anchorPositionBottom - windowHeight )
					$("input[type=\'submit\']").removeClass("submit-fixed");
				else
					$("input[type=\'submit\']").addClass("submit-fixed");
			}
		}

		windowScroll();


		window.onscroll = function(){
			windowScroll();
		}


		$("#Search_max_price_value").autoNumeric(\'init\', {
			aSep: \' \',
			mRound: \'D\',
			mDec: 0
		});

		$("#Search_date").pickmeup({
			format : "d-m-Y",
			position: "bottom",
			hide_on_select : true,
			min : new Date
		});



		$(".filter-block input:not([type=\'text\']):not([name=\'Search[currency_id]\']):not([type=\'number\']):not([type=\'checkbox\'])").click(function(){
			console.log(true);
			if( $(this).attr("name") == "Search[category_id]" ){
				$("input[type=\'hidden\'][name=\'Search[category_id]\']").attr("value", $(this).attr("data-id"));
				$("#additional-params input, #additional-params select, .meta-data-input, input[name=\'Search[price_type]\'], input[name=\'Search[sub_category_id]\']").prop(\'disabled\', true);
			};
			if( $(this).attr("name") == "Search[sub_category_id]" ){
				$(".meta-data-input").prop(\'disabled\', true);
			};
			$("input[type=\'submit\']").click();
		});

		$(".filter-block #Search_date, .filter-block #Search_city_id").change(function(){
			$("input[type=\'submit\']").click();
		})

		$("input[type=\'submit\']").click(function(){

			$("#additional-params input, #additional-params select, #additional-params [type=\'checkbox\'], #additional-params [type=\'radio\']").each(function(index1, index2){
				if($(this).attr("type") == "checkbox" && $(this).prop("checked") == false ) $(this).prop("disabled", true);
				if($(this).val() == 0 || $(this).val() == "" && $(this).val() == null)
					$(this).prop("disabled", true);
			});
			return true;
		});

		$(".remove-from-filter").click(function(){

			var dataField = $(this).attr("data-field");

			if(dataField == "category_id" || dataField == "sub_category_id")
				return true;

			switch(dataField){
//				case "category_id" :
//					$(".filter-block input, .filter-block select").prop("disabled", true);
//				break;
//
//				case "sub_category_id" :
//					$(".filter-block input:not([name=\'Search[category_id]\']), .filter-block select").prop("disabled", true);
//				break;

				case "city_id" :
					$("input[name=\'Search[city_id]\'], select[name=\'Search[city_id]\']").prop("disabled", true);
					$("input[name=\'Search[region_id]\'], select[name=\'Search[region_id]\']").prop("disabled", true);
				break;

				case "max_price_value" :
					$("input[name=\'Search[max_price_value]\'], select[name=\'Search[max_price_value]\']").prop("disabled", true);
					$("input[name=\'Search[price_type]\'], select[name=\'Search[price_type]\']").prop("disabled", true);
				break;

				case "meta-data-input":
					$("input.meta-data-input, select.meta-data-input").prop("disabled", true);
				break;

				default:
					$("input[name=\'Search["+dataField+"]\'], select[name=\'Search["+dataField+"]\']").prop("disabled", true);
				break;

			}

			$("input[type=\'submit\']").click();
			return false;
		})

		$("body,html").on("change", "#Search_region_id", function(){
			var value = $(this).val();

			var xhr = new XMLHttpRequest();
			var params = "id="+value;
			var obj = this;

			xhr.open("POST", "'.CController::createUrl("/core/ajaxGetChildrenСities").'", true);

			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

			xhr.onload = function () {
				$(obj).parent().children("#Search_city_id").children().remove();
				$(obj).parent().children("#Search_city_id").append(xhr.response);
				$("#Search_city_id").click();
			};

			xhr.send(params);

		});

		function splitNums(delimiter, str) {
    		str = str.replace(/(\d+)(\.\d+)?/g,
          		function(c,b,a){return b.replace(/(\d)(?=(\d{3})+$)/g, \'$1\'+delimiter) + (a ? a : \'\')} );

    		return str;
		}


	', CClientScript::POS_READY
); ?>

