<?php
/**
 * @var $form CActiveForm
 * @var $model User
 * @var $this Controller
 */

?>

<?php
    if(isset($_GET['success'])) :
?>
        <div class="container960" style="color: white; font-weight: normal; font-size: 20px;">

            <h4 style="text-align: center"><? echo $message ?></h4>

        </div>


<? else : ?>

<div class="form" id="registration-form">


    <?php
        $this->renderPartial('_registration_step1', array(
            "model" => $model
        ))
    ?>

    <div class="providers-list">

        <?php
            $providers = $this->widget('application.extensions.SocialNetworkAuth.SocialNetworkAuthService', array(
                'providers' => array("Vk", "Facebook", "Ok"),
                'linkCssClass' => 'network-link'
            ));

        ?>
        <h5>РЕГИСТРАЦИЯ ЧЕРЕЗ СОЦИАЛЬНЫЕ СЕТИ</h5>
    </div>


</div><!-- form -->
        <div id="registration-in-catalog">
            <a href="/registration/catalog">РАЗМЕЩЕНИЕ В КАТАЛОГЕ</a>
        </div>
        
<? endif; ?>


<style>
    body, html {
        background: url(../../../images/background.jpg) no-repeat center center;
        background-size: cover;
        height:100%;
    }

    #content .form {
        width:300px;
    }

    #content .form input{
        margin-bottom: 6px;
    }

    #content .form [type=submit] {
        margin-left: 0;
        position: relative;
        margin-top: 0px;
    }

    #registration-submit-button {
        margin-top: 34px !important;
    }

    #registration-form {
        position: absolute;
        top: 50%;
        margin-top: -135px;
        left: 0;
        right: 0;
    }

    #registration-in-catalog {
        text-align: center;
        display: block;
        position: absolute;
        width: 100%;
        bottom: 10px;
    }

    #registration-in-catalog a{
        color:white;
        font-size: 14px;
    }

    #content h4 {
        color: white;
        font-weight: normal;
        display: block;
        position: absolute;
        left: 0;
        right: 0;
        top: 180px;
        font-size: 15px;
    }

    .row.error .error-message {
        margin-left: 400px;
        opacity: 1;
    }

    .errorMessage{
        display: block !important;
    }

    .error-message.small {
        margin-top: -44px;
    }

    .terms {
        height:44px;
        font-size: 14px;
        line-height: 43px;
        text-align: center;
    }

    h1 {
        color:white;
    }
</style>

<script>

//    $("input").on("change", function(){
//        if($(this).val() != "") if( $(this).parent().hasClass("error") ) $(this).parent().removeClass("error");
//    })

</script>
