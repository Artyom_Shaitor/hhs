<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $pages CPagination
 * @var $models Service[]
 * @var $serviceCount integer
 */

$priceService = new PriceService();

?>
<link rel="stylesheet" href="/css/service.css"/>
<h1 class="main-h1"><? echo $model->username ?></h1>

<div id="content" class="container960">

<div class="col-span-3">

	<? $this->renderPartial('_leftMenu', array(
		"active" => "services",
		"model" => $model
	)); ?>

</div><div class="col-span-9">

	<div class="services-list">

		<?foreach($models as $model):?>
			<? $this->renderPartial('application.modules.profile.views.default._service_list_item', array(
				"model" => $model,
				"priceService" => $priceService
			)) ?>
		<?endforeach?>

	</div>


	<?$this->widget('CLinkPager', array(
		'pages' => $pages,
		'internalPageCssClass' => '',
		'id' => 'pagination',
		'pageSize' => 10,
		'header' => '',
		'selectedPageCssClass' => 'active',
		'hiddenPageCssClass' => 'disabled',
		'nextPageLabel' => false,         // »
		'prevPageLabel' => false,         // «
		'lastPageLabel' => '...',  // »» &raquo;
		'firstPageLabel' => '...', // «« &laquo;
		'htmlOptions' => array('class' => 'pagination'),
	))?>

</div>
</div>

<script src="/scripts/is.min.js"></script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:35%;
	}

	.information table td+td {
		width:65%;
	}

</style>

<script>


	$(".confirm-block-wrapper").click(function() {
		$(".confirm-block-wrapper, .confirm-block").css("display", "none");
		$(".confirm-block").removeClass("bounceIn");
		$("footer, #content, .main-h1, header").removeClass("blur");
	});

	$(".manage-button.trash").click(function(){
		$(".confirm-block-wrapper, .confirm-block").css("display", "block");
		$(".confirm-block").addClass("bounceIn");
		$("footer, #content, .main-h1, header").addClass("blur");
		$("#yes").attr("href", $(this).attr("href"));
		return false;
	});

	$("#yes").click(function(){
		return true;
	});

	$("#no").click(function(){
		$(".confirm-block-wrapper").click();
		return false;
	});

</script>