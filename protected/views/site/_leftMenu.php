<?
/**
 * @var $model User
 */
    $userMenuArray = array(
        "profile" => array(
            "link" => "/profile",
            "title" => "СТЕНА"
        ),
        "calendar" => array(
            "link" => "/profile/calendar",
            "title" => "КАЛЕНДАРЬ"
        ),
        "requests" => array(
            "link" => "/profile/requests",
            "title" => "ЗАЯВКИ",
        ),
        "services" => array(
            "link" => "/profile/services",
            "title" => "МОИ УСЛУГИ",
        ),
        "holidays" => array(
            "link" => "/profile/holidays",
            "title" => "МОИ ПРАЗДНИКИ",
        ),

        "settings" => array(
            "link" => "/profile/settings",
            "title" => "РЕДАКТИРОВАТЬ ПРОФИЛЬ"
        ),
        "logout" => array(
            "link" => "/logout",
            "title" => "ВЫЙТИ"
        )
    );

    $customerMenuArray = array(
        "profile" => array(
            "link" => "/profile",
            "title" => "СТЕНА"
        ),
//        "holidays" => array(
//            "link" => "/profile/holidays",
//            "title" => "МОИ ПРАЗДНИКИ",
//        ),
        "settings" => array(
            "link" => "/profile/settings",
            "title" => "РЕДАКТИРОВАТЬ ПРОФИЛЬ"
        ),
        "logout" => array(
            "link" => "/logout",
            "title" => "ВЫЙТИ"
        )
    );

    $guestMenuArray = array(
        "wall" => array(
            "link" => UrlService::performUserUrl($model),
            "title" => "СТЕНА"
        ),
        "services" => array(
            "link" => UrlService::performUserUrl($model)."/services",
            "title" => "УСЛУГИ"
        )
    );

    function buildMenu($userMenuArray, $active)
    {
        $baseUrl = Yii::app()->getBaseUrl(true);
        foreach($userMenuArray as $index => $params){
            if(( $index != "holidays" && $index != "requests") || in_array(Yii::app()->user->id, array(6,7,8,12))) {

                if($index == "requests") {
                    $requestCount = Yii::app()->user->getNewRequests();
                    $underline = "";
                    if($requestCount > 0){
                        $requestCount = "<span>+{$requestCount}</span>";
                        $underline = "underline";
                    }else{
                        $requestCount = "";
                        $underline = "";
                    }
                    if ($index == $active) {
                        echo "<li class='active {$underline}'><a href='{$baseUrl}{$params["link"]}'>{$params["title"]}{$requestCount}</a>";
                    } else {
                        echo "<li class='{$underline}'><a href='{$baseUrl}{$params["link"]}'>{$params["title"]}{$requestCount}</a>";
                    }
                    continue;
                }

                if ($index == $active) {
                    echo "<li class='active'><a href='{$baseUrl}{$params["link"]}'>{$params["title"]}</a>";
                } else {
                    echo "<li><a href='{$baseUrl}{$params["link"]}'>{$params["title"]}</a>";
                }
            }
        }
    }
?>

<? if(CheckPage::isPages(array("site/viewuserservices", "user/default/view", "service/default/view", "user/default/viewbyusername", "service/default/viewbyusername", 'site/viewuserservices', 'site/viewuserservicesbyurl'))) :  ?>
    <img id="avatar" src="<?php echo ImageService::performImageUrl($model->avatar_url, ImageType::AVATAR); ?>" alt="<? echo "hhs.by - ".$model->username ?>"/>
    <div class="left-menu">
        <ul>
            <? buildMenu($guestMenuArray, $active) ?>
        </ul>
    </div>
<? else : ?>
    <? if(!Yii::app()->user->isGuest) : ?>
        <img id="avatar" src="<?php echo ImageService::performImageUrl(Yii::app()->user->getAvatarUrl(), ImageType::AVATAR); ?>" alt="<? echo "hhs.by - ".$model->username ?>"/>
        <div class="left-menu">
            <ul>
                <?php
                    if(Yii::app()->user->getRole() == 1)
                        buildMenu($customerMenuArray, $active);
                    elseif(Yii::app()->user->getRole() > 1)
                        buildMenu($userMenuArray, $active);
                ?>

            </ul>
        </div>
    <? else : ?>
        <a href="/registration/catalog" id="ad-registration">
            <div id="background">
                <div class="my-ad-block">
                    <div class="my-ad-title">Регистрируйся</div>
                    <div class="my-ad-text">в каталоге!</div>
                </div>
                <div class="my-ad-block">
                    <div class="my-ad-title">Размещай</div>
                    <div class="my-ad-text">свои услуги!</div>
                </div>
                <div class="my-ad-block">
                    <div class="my-ad-title">Принимай</div>
                    <div class="my-ad-text">новые заказы!</div>
                </div>
            </div>
        </a>

    <? endif; ?>

<? endif; ?>