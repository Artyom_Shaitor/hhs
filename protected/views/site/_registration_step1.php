<?php
/**
 * @var $form CActiveForm
 */


 $form=$this->beginWidget('CActiveForm', array(
    'htmlOptions' => array("class" => 'form'),
    'enableClientValidation'=> true,
    'enableAjaxValidation' => false,
    'action'=>$this->createUrl('/registration'),
    'clientOptions'=>array(
//            'validateOnSubmit'=> true,
//            'validateOnChange' => true,
//            'afterValidate' => "js: function(form, data, hasError){ if(hasError) console.log(hasError); else alert('Success'); }"
    ),
)); ?>


    <div class="row">
        <?php echo $form->textField($model,'username', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("username"))); ?>
        <div id="regUser" class="error-message"><?php echo $form->error($model,'username'); ?></div>
    </div>

    <div class="row">
        <?php echo $form->textField($model,'email', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("email"))); ?>
        <div class="error-message small"><?php echo $form->error($model,'email'); ?></div>
    </div>

    <div class="row">
        <?php echo $form->passwordField($model,'password', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("password"))); ?>
        <div class="error-message small"><?php echo $form->error($model,'password'); ?></div>
    </div>

    <div class="row terms">
        <?php echo CHtml::activeCheckBox($model,'terms'); ?>
        <?php echo CHtml::activeLabel($model,'terms', array("style" => "color:white")); ?>
        <div class="error-message small"><?php echo $form->error($model,'terms'); ?></div>
    </div>

    <div class="row buttons">
        <!--        --><?php //echo CHtml::submitButton('Зарегистрироваться'); ?>
        <?php echo CHtml::ajaxSubmitButton('ЗАРЕГИСТРИРОВАТЬСЯ',CHtml::normalizeUrl('/registration'),
            array(
                'dataType'=>'json',
                'type'=>'post',
                'success'=>'function(data) {
                        if(data.status=="success"){
                            window.location = "'.Yii::app()->getBaseUrl(true).'/registration?success=true";
                        }else{
                            $.each(data, function(key, val) {
                                $("#registration-form #"+key+"_em_").parent().parent().addClass("error");
                                $("#registration-form #"+key+"_em_").html(val);
                            });
                        }}'
            )); ?>
    </div>

<?php $this->endWidget(); ?>