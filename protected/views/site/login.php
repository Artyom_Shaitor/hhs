<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';

$action = "/login";

?>

<? if( Yii::app()->user->hasFlash('confirm_success') ) : ?>
<h4 style="text-align: center"><? echo Yii::app()->user->getFlash('confirm_success') ?></h4>
	<?
		$action .= "?confirmed=true"; // Типа страница настроек, второй этап регистрациии
	?>
<? endif; ?>

<?
	$model = new LoginForm;
?>


<? $this->renderPartial("_login", array(
	"model" => $model,
	"id" => "log-in-main",
	"action" => $action
)) ?>


<style>

	body, html {
		background: url(../../../images/background.jpg) no-repeat center center;
		background-size: cover;
		height:100%;
	}

	h1 {
		color:white;
	}

	#content h4 {
		color: white;
		font-weight: normal;
		display: block;
		position: absolute;
		left: 0;
		right: 0;
		top: 80px;
	}

	#log-in-main {
		position: absolute;
		margin-top: -90px;
		top: 50%;
		left: 0;
		right: 0;
	}


</style>

<script>

	$(".forgot-title a").click(function(){
		if( $(".forgot-block").hasClass("active") ){
			$(".forgot-block, .forgot-title a").removeClass("active");
			$(".transparent-fullscreen-block").css("display", "none");
		}else {
			$(".forgot-block, .forgot-title a").addClass("active");
			$(".transparent-fullscreen-block").css("display", "block");
		}
		return false;
	});

	$(".show-password").click(function(){
		if( $(this).hasClass("active") ){
			$(this).removeClass("active");
			$("input[name='LoginForm[password]'], input[name='User[password]']").attr("type", "password");
		}else {
			$(this).addClass("active");
			$("input[name='LoginForm[password]'], input[name='User[password]']").attr("type", "text");
		}
		return false;
	});

	$(".transparent-fullscreen-block").click(function(){
//		$(".forgot-block, .forgot-title a").removeClass("active");
		$(".forgot-title a").click();
		$(this).css("display", "none");
	});

	$(".button-recover").click(function(){

		var email = $(".forgot-email").val();
		var xhr = new XMLHttpRequest();
		var params = "email="+email;
		var fd = new FormData;
		fd.append('email', email);

		xhr.open('post', '/site/passwordrecovery', true);

		xhr.onload = function(){
			var resp = JSON.parse(xhr.response);

			$(".forgot-block h5").removeClass("recover-failed").removeClass("recover-success").css("display", "none");
			var classCss = (resp.result == true)? "recover-success" : "recover-failed";
			console.log(classCss);

			$(".forgot-block h5").addClass(classCss).html(resp.message).css("display", "block");

			if(resp.result == true)
				setTimeout(function(){

					$(".forgot-title a").click();
					$(".forgot-block h5").removeClass("recover-failed").removeClass("recover-success").css("display", "none");
					$(".forgot-email").val("");

				}, 3000);


		};

		xhr.send(fd);

	})

</script>