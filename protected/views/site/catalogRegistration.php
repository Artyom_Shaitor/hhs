<?php
/**
 * @var $form CActiveForm
 * @var $model User
 * @var $this Controller
 */
?>

<?php
if(isset($_GET['success'])) :
    ?>
    <div class="container960" style="color: white; font-weight: normal; font-size: 20px;">

<!--        <h4 style="text-align: center">Вы были успешно зарегистрированы! Пожалуйста, зайдите на Вашу почту и активируйте аккаунт</h4>-->
    <? $this->renderPartial('registrationSuccess'); ?>

    </div>


<? else : ?>



<div class="form" id="registration-form">
    <h2>Регистрация в каталоге</h2>
<? $form=$this->beginWidget('CActiveForm', array(
    'htmlOptions' => array("class" => 'form'),
    'enableClientValidation'=> true,
    'enableAjaxValidation' => false, // НЕ ВКЛЮЧАТЬ ИНАЧЕ ВСЁ ПОЛОМАЕШЬ!!!
//    'action'=>$this->createUrl('/registration/step2'),
    'clientOptions'=>array(
//            'validateOnSubmit'=> true,
//            'validateOnChange' => true,
//            'afterValidate' => "js: function(form, data, hasError){ if(hasError) console.log(hasError); else alert('Success'); }"
    ),
)); ?>


    <div class="row">
        <?php echo $form->textField($model,'username', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("username"))); ?>
        <div  class="error-message" style="margin-top: -41px;"><?php echo $form->error($model,'username'); ?></div>
    </div>

    <div class="row">
        <?php echo $form->textField($model,'email', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("email"))); ?>
        <div class="error-message small"><?php echo $form->error($model,'email'); ?></div>
    </div>

    <div class="row">
        <?php echo $form->passwordField($model,'password', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("password"))); ?>
        <div class="error-message small"><?php echo $form->error($model,'password'); ?></div>
        <a class="show-password" href="/"></a>
    </div>


    <div class="row">

        <div class="input-enter" id="region_div"><?=$model->getAttributeLabel("region_id")?></div>

        <?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array('class' => 'input-enter', "for" => "region_div", "placeholder" => $model->getAttributeLabel("region_id"), 'prompt' => $model->getAttributeLabel("region_id"), 'ajax' => array(
            'type' => 'post',
            'url' => CController::createUrl("site/getCityList"),
            "data" => array('id' => 'js:this.value'),
            'update' => '#User_city_id',
            'success' => 'function(data){
                $("#User_city_id").children().remove();
                $("#User_city_id").append(data);
                $("#city_div").html( $("#User_city_id option:selected").html() ) }'
        ))) ?>
        <div class="error-message small"><?php echo $form->error($model,'region_id'); ?></div>
    </div>

    <div class="row">
        <div class="input-enter" id="city_div"><?=$model->getAttributeLabel("city_id")?></div>

        <?php echo $form->dropDownList($model, 'city_id', array(), array('class' => 'input-enter', "for" => "city_div", "placeholder" => $model->getAttributeLabel("city_id"), 'prompt' => $model->getAttributeLabel("city_id"))) ?>
        <div class="error-message small"><?php echo $form->error($model,'city_id'); ?></div>
    </div>

    <div class="row">
        <?php echo $form->textField($model,'phone', array('class' => 'input-enter', "placeholder" => $model->getAttributeLabel("phone"),)); ?>
        <div class="error-message small"><?php echo $form->error($model,'phone'); ?></div>
    </div>

    <div class="row terms">
        <?php echo CHtml::activeCheckBox($model,'terms'); ?>
        <?php echo CHtml::activeLabel($model,'terms', array("style" => "color:white")); ?>
        <div class="error-message small"><?php echo $form->error($model,'terms'); ?></div>
    </div>


    <div class="row buttons">
        <!--        --><?php //echo CHtml::submitButton('Зарегистрироваться'); ?>
        <?php echo CHtml::ajaxSubmitButton('ЗАРЕГИСТРИРОВАТЬСЯ', CHtml::normalizeUrl('/registration/catalog'),
            array(
                'dataType'=>'json',
                'async' => false,
                'type'=>'post',
                'success'=>'function(data) {
                        console.log(data);
                        if(data.status == "success"){
                            window.location = "'.Yii::app()->getBaseUrl(true).'/registration/catalog?success=true";
                        }else{
                            $.each(data, function(key, val) {
                                $("#registration-form #"+key+"_em_").parent().parent().addClass("error");
                                $("#registration-form #"+key+"_em_").html(val);
                            });
                        }}'
            ), array(
                "id" => 'registration-submit-button',
                "class" => 'red'
            )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<? endif; ?>

<script src="/scripts/is.min.js"></script>
<script>
    if(is.safari()){
        $("a.show-password").css("margin-top", "12px");
    }
</script>

<style>

    body, html {
        background: url(../../../images/background.jpg) no-repeat center center;
        background-size: cover;
        height:100%;
    }

    #content .form {
        width:300px;
    }

    #content .form input{
        margin-bottom: 6px;
    }

    #content .form [type=submit] {
        margin-left: 0;
        position: relative;
        margin-top: 0px;
    }

    #registration-submit-button {
        margin-top: 34px !important;
    }

    #registration-form {
        position: absolute;
        top: 50%;
        margin-top: -240px;
        left: 0;
        right: 0;
    }

    #registration-in-catalog {
        text-align: center;
        display: block;
        position: absolute;
        width: 100%;
        bottom: 10px;
    }

    #registration-in-catalog a{
        color:white;
        font-size: 14px;
    }

    #content h4 {
        color: white;
        font-weight: normal;
        display: block;
        position: absolute;
        left: 0;
        right: 0;
        top: 180px;
        font-size: 15px;
    }

    .row.error .error-message {
        margin-left: 400px;
        opacity: 1;
    }

    .errorMessage{
        display: block !important;
    }

    .error-message.small {
        margin-top: -44px;
    }

    .terms {
        height:44px;
        font-size: 14px;
        line-height: 43px;
        text-align: center;
    }

    h1 {
        color:white;
    }

    h2 {
        text-align: center;
        color: white;
    }

    #region_div, #city_div, #country_div {
        position: absolute;
        z-index: 0;
        color:rgba(255,255,255,0.6);
        border-bottom: 0 !important;
    }

</style>

<script>

    $(".form select").change(function(){


        var _for = "#"+$(this).attr("for");
        var id = $(this).attr("id");
        $(_for).html( $("#"+id+" option:selected").html() )
    });


    $(".show-password").click(function(){
        if( $(this).hasClass("active") ){
            $(this).removeClass("active");
            $("input[name='LoginForm[password]'], input[name='User[password]']").attr("type", "password");
        }else {
            $(this).addClass("active");
            $("input[name='LoginForm[password]'], input[name='User[password]']").attr("type", "text");
        }
        return false;
    });

</script>
