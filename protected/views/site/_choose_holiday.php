<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 21.03.16
 * Time: 15:08
 * @var $holidays Holiday[]
 */

?>

<link rel="stylesheet" href="/css/animate.css"/>
<div class="confirm-block-wrapper v2"></div>
<div class="confirm-block animated v2">
    <div class="confirm-title">Выберите, в какой праздник Вы хотите добавить услугу</div>
    <div class="confirm-title success">Услуга успешно добавлена</div>
    <!--	<div class="confirm-sub-title">Удалённую услугу невозможно будет восстановить</div>-->

    <div class="holiday-mini-list">
        <ul>
            <?
            if(count($holidays) > 0) {
                foreach ($holidays as $holiday) : ?>
                    <li>
                        <a class='choose-holiday-link' href="/"><? echo $holiday->title?> </a>
                        <div class="message-block">
                            <textarea></textarea><br/>
                            <input data-service-id="" data-service-author-id="" data-holiday-id="<? echo $holiday->id ?>" type="button" value="Добавить"/>
                        </div>
                    </li>
                <? endforeach;
                echo "<hr/>";
            }
            ?>

            <li><a class='choose-holiday-link' href="/profile/holidays">+ Создать новый праздник</a></li>
        </ul>
    </div>

    <a id="no" class="v2" href="/">Отмена</a>

    <script>

        $(".confirm-block-wrapper.v2").click(function() {
            $(".confirm-block-wrapper.v2, .confirm-block.v2").css("display", "none");
            $(".confirm-block.v2").removeClass("bounceIn");

            $(".holiday-mini-list .choose-holiday-link.active").removeClass("active");
            $(".v2 .confirm-title").css("display", "block");
            $(".holiday-mini-list").css("display", "inline-block");
            $(".v2 .confirm-title.success").css("display", "none");
            $("#no.v2").html("Отмена");

            $("footer, #content, .main-h1, header").removeClass("blur");
        });

        $("#no.v2").click(function(){
            $(".confirm-block-wrapper.v2").click();
            return false;
        });

        $(document).on("click", ".message-block input", function(){
            var serviceId = $(this).attr("data-service-id");
            var serviceAuthorId = $(this).attr("data-service-author-id");
            var holidayId = $(this).attr("data-holiday-id");

            var xhr = new XMLHttpRequest();
            var fd = new FormData();

            fd.append("service_id", serviceId);
            fd.append("service_author_id", serviceAuthorId);
            fd.append("holiday_id", holidayId);

            xhr.onload = function(){
                var response = JSON.parse(xhr.response);

                if(response['result'] == false){
                    alert(response['message']);
                    return false;
                }

                $(".v2 .confirm-title, .holiday-mini-list").css("display", "none");
                $(".v2 .confirm-title.success").css("display", "block");
                $("#no.v2").html("Закрыть");

            };

            xhr.open("POST", "/profile/holiday/addServiceToHoliday");
            xhr.send(fd);
        });

        $(document).on("click", ".manage-button.catalog-add-holiday-button", function(){
            var self = this;
            if($(this).attr("data-holiday-id") != "false"){

                var serviceId = $(this).attr("data-service-id");
                var serviceAuthorId = $(this).attr("data-service-author-id");
                var holidayId = $(this).attr("data-holiday-id");

                var xhr = new XMLHttpRequest();
                var fd = new FormData();

                fd.append("service_id", serviceId);
                fd.append("service_author_id", serviceAuthorId);
                fd.append("holiday_id", holidayId);

                xhr.onload = function(){
                    var response = JSON.parse(xhr.response);
                    if(response['result'] == false){
                        alert(response['message']);
                        return false;
                    }
                    location.href = $(self).attr("href");
                };

                xhr.open("POST", "/profile/holiday/addServiceToHoliday");
                xhr.send(fd);

                return false;
            }

            $(".holiday-mini-list input").each(function(index, elem){
                $(elem).attr("data-service-id", $(self).attr("data-service-id"));
                $(elem).attr("data-service-author-id", $(self).attr("data-service-author-id"));
            });

            $(".confirm-block-wrapper.v2, .confirm-block.v2").css("display", "block");
            $(".confirm-block.v2").addClass("bounceIn");
            $("footer, #content, .main-h1, header").addClass("blur");
            $("#yes").attr("data-model-id", $(this).attr("data-model-id"));
            return false;
        });

        $(".holiday-mini-list .choose-holiday-link").click(function(){
            $(".choose-holiday-link.active").removeClass("active");
            $(this).addClass("active");
            return false;
        })

    </script>

    <style>

        .confirm-block {
            margin-top: -180px;
        }

        .confirm-title.success {
            display: none;
        }

        .holiday-mini-list {
            width:80%;
            display: inline-block;
            background-color: rgba(245,245,245, 1);
            padding: 10px 0;
            border: 2px solid rgb(190,190,190);
            margin-top: 15px;
        }

        .holiday-mini-list ul {
            list-style: none;
            float: none;
            padding: 0;
            margin: 0;
        }

        .holiday-mini-list .message-block {
            background-color: rgb(220,220,220);
            height:0;
            padding: 0;
            overflow: hidden;

            transition: height 0.2s ease-in-out;
            -o-transition: height 0.2s ease-in-out;
            -moz-transition: height 0.2s ease-in-out;
            -webkit-transition: height 0.2s ease-in-out;
        }

        /*.holiday-mini-list .message-block.hidden {*/
        /*height:0;*/
        /*padding: 0;*/
        /*}*/

        .holiday-mini-list .message-block textarea {
            width: 220px;
            border: 0;
            resize: none;
            height: 70px;
            outline: none;
            padding: 5px;
        }

        .holiday-mini-list hr {
            border-bottom: 1px solid rgb(190,190,190);
            width: 70%;
        }

        .holiday-mini-list ul li {
            list-style: none;
            float: none;
            padding: 0;
            margin: 0;
        }

        .holiday-mini-list ul .choose-holiday-link {
            display: block;
            text-decoration: none;
            padding: 8px 0 7px 0;
            color: rgb(125,125,125);
        }

        .holiday-mini-list ul .choose-holiday-link.active+.message-block {
            height: 103px;
            padding: 10px 0;
        }

        .holiday-mini-list ul .choose-holiday-link:hover,
        .holiday-mini-list ul .choose-holiday-link.active,
        .holiday-mini-list ul .choose-holiday-link:focus {
            display: block;
            text-decoration: none;
            background-color: rgb(190,190,190);
            color: rgb(75,75,75);
        }

    </style>


</div>