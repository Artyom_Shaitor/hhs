<?php
/* @var $this SiteController */

$model = new LoginForm;

?>

<style>

    #content {
        max-width:1200px;
        width:100%;
        min-width: 960px;
    }

    #home {
        height:610px;
        background: url(/images/homePage/001_hhs_home@2x.jpg) no-repeat bottom center;
        background-size: cover;
    }

    #home h1 {
        position: absolute;
        text-align: center;
        white-space: pre-line;
        width: 900px;
        color: white;
        font-size: 48px;
        text-transform: uppercase;
        font-weight: bold;
        margin-top: 240px;
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
    }

    #home h2 {
        position: absolute;
        text-align: center;
        white-space: pre-line;
        width: 500px;
        color: white;
        font-size: 23px;
        /*text-transform: uppercase;*/
        font-weight: bold;
        margin-top: 340px;
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
    }

    #home .logo {
        width: 340px;
        margin-top: 105px;
    }

    #content-home {
        max-width:1200px;
        min-width: 960px;
        margin:30px auto;
    }

    #registration-button-block {
        /*background-color: #ed7634;*/
        background: url(/images/homePage/002_hhs_home@2x.jpg) no-repeat center center;
        background-size: cover;
        box-sizing: border-box;
        width: 100%;
        padding: 35px 0px;
        color: white;
        height: 200px;
    }

    #registration-button-block a {
        text-transform: uppercase;
        color: white;
        text-decoration: none;
        border: 1px solid white;
        padding: 10px 30px 8px 30px;
        border-radius: 21px;
        display: block;
        width: 200px;
        text-align: center;
        margin: 30px auto 0 auto;
        font-weight: 900;
        font-size: 1.1em;
    }

    #registration-button-block a:hover {
        cursor: pointer !important;
        background-color: white;
        color: #ed7634;
    }

    #about-block {
        background-color: white;
        box-sizing: border-box;
        padding: 80px 140px;
        font-size: 23px;
        text-align: center;
        line-height: 38px;
        color:rgb(40,40,40);
        margin: 0;
    }

    #about-block b {
        font-weight: 900;
        color:black;
    }

    .home-block-title {
        position: absolute;
        background-color: rgba(255,91,0, 0.6);
        width: 100%;
        max-width: 1200px;
        text-align: center;
        color: white;
        text-transform: uppercase;
        font-size: 31px;
        box-sizing: border-box;
        padding: 20px;
        height:110px;
    }

    #macbook {
        height:700px;
        background: url(/images/homePage/003_hhs_home@2x.jpg) no-repeat top center;
        background-size: cover;
        font-weight: bold;
    }

    #iphone {
        height:700px;
        background: url(/images/homePage/004_hhs_home@2x.jpg) no-repeat top center;
        background-size: cover;
        font-weight: bold;
    }

    #ipad {
        height:700px;
        background: url(/images/homePage/005_hhs_home@2x.jpg) no-repeat top center;
        background-size: cover;
        font-weight: bold;
    }

    #bottom-registration-block {
        padding: 60px;
        background-color: white;
        box-sizing: border-box;
    }

    #bottom-registration-block a {
        text-transform: uppercase;
        color: #ff5b00;
        text-decoration: none;
        border: 2px solid #ff5b00;
        padding: 11px 0px 5px 0px;
        border-radius: 60px;
        display: block;
        width: 500px;
        font-size: 30px;
        font-weight: bold;
        text-align: center;
        margin: 0 auto;
    }

    #bottom-registration-block a:hover {
        color: white;
        background-color: #ff5b00;
    }
</style>

<div id="content-home">

    <div class="container1200" id="home">

        <h1>Организация праздников и проведение свадеб в Беларуси</h1>
        <div class="container1200" style="text-align: center;">
            <img class="logo" src="/images/logo_white.svg" alt="hhs.by"/>
        </div>

<!--        <img src="/images/homePage/001_hhs_home.jpg" alt="hhs.by"/>-->
    </div>

    <div class="container1200" id="registration-button-block">
        <div style="text-transform: uppercase; font-weight: bold; font-size: 36px; text-align: center">Открыта регистрация в каталоге</div>
        <div style="text-transform: uppercase; font-size: 18px; font-weight: 100; text-align: center">Добавляйте ваши услуги и получайте новые заказы</div>
        <a href="/registration/catalog">Зарегистрироваться</a>
    </div>

    <p class="container1200" id="about-block">
        <b>HHS.by</b> – это уникальный сервис для организации праздников, в котором учтены все тонкости этого бизнеса. В разработке  сайта приняли участие профессионалы из разных сфер праздничной индустрии.  Удобный  интерфейс и ряд уникальных функций позволят сэкономить время и повысить эффективность Ваших услуг. Сервис поможет привлечь к себе внимание и найти новых клиентов.

    </p>

    <div class="container1200" id="macbook">
        <div class="home-block-title" style="margin-top:590px">Календарь занятости поможет спланировать <br>рабочий процесс и ни о чем не забыть</div>
    </div>

    <div class="container1200" id="iphone">
        <div class="home-block-title" style="margin-top:590px; line-height: 73px;">Получайте SMS и E-mail уведомления о новых заказах</div>
    </div>

    <div class="container1200" id="ipad">
        <div class="home-block-title" style="margin-top:590px">В личном кабинете Вы можете добавить свои услуги и товары,<br>продвигать их и отслеживать новые заказы</div>
    </div>

    <div class="container1200" id="bottom-registration-block">
        <a href="/registration/catalog">Регистрация в каталоге</a>
    </div>

</div>

