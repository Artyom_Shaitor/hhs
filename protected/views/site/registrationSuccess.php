
<div id="logo">
</div>

<div id="text">
    Вы были успешно зарегистрированы!<br>
    Пожалуйста, зайдите на Вашу почту и активируйте аккаунт.<br><br>
    Есть вероятность, что письмо придёт в СПАМ.
</div>


<style>

    #logo {
        margin: 100px auto 0 auto;
        width: 400px;
        height: 85px;
        background: url(/images/logo_white.svg) no-repeat center center;
        background-size: contain;
    }

    #text {
        max-width: 450px;
        text-align: center;
        font-size: 18px;
        margin: 50px auto 0 auto;
    }


</style>

