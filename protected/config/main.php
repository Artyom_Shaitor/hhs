<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Happy Holiday Service - Удобный сервис для ведения и продвижения Вашего праздничного бизнеса Беларусь',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'l0g0sclub',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'admin',
		'service',
		'user',
		'profile'

	),

	// application components
	'components'=>array(

		'user'=>array(
			'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true
		),
		'cache'=>array(
			'class'=>'system.caching.CDbCache',
		),

		'authManager' => array(
			// Будем использовать свой менеджер авторизации
			'class' => 'PhpAuthManager',
			// Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
			'defaultRoles' => array('guest'),
		),

//		'authManager'=>array(
//			'class'=>'CDbAuthManager',
//			'connectionID'=>'db',
//		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				array('site/gensitemap', 'pattern'=>'/gensitemap', 'urlSuffix'=>''),
				'activate/<activation_code>/email/<email>' => 'site/coreConfirmRegistrationEmail',
				'registration/catalog' => 'site/catalogregistration',
				'registration' => 'site/registration',
				'login' => 'site/login',
				'logout' => 'site/logout',
				'catalog' => 'site/catalog',
				'catalog/<category>' => 'site/catalog',
				'catalog/<category>/<subCategory>' => 'site/catalog',

				'catalogMini/<holiday_id>' => 'site/catalogMini',
				'catalogMini/<holiday_id>/<category>' => 'site/catalogMini',
				'catalogMini/<holiday_id>/<category>/<subCategory>' => 'site/catalogMini',

				'user/view/<id>/services' => 'site/viewuserservices',
				'user/<action>' => 'user/default/<action>/',
				'user/<action>/<id>' => 'user/default/<action>/',

				'service/edit/deleteprice' => 'service/default/deletePrice',
				'service/edit/deletecity' => 'service/default/deleteCity',
				'service/<action>' => 'service/default/<action>',
				'service/<action>/<id>' => 'service/default/<action>',

				'profile/add-service' => 'profile/service/addService/',
				'profile/service/loadImage' => 'profile/service/loadImage/',
				'profile/service/loadAvatar' => 'profile/service/loadAvatar/',
				'profile/service/delete/' => 'profile/service/delete/',
				'profile/service/deleteGalleryImage/<id>' => 'profile/service/deleteGalleryImage/',

				'profile/holidays' => 'profile/holiday/holidays',
				'profile/holiday/<action>' => 'profile/holiday/<action>',
				'profile/holiday/<action>/<id>' => 'profile/holiday/<action>',

				'profile/requests' => 'profile/request/requests',
				'profile/request/<action>' => 'profile/request/<action>',
				'profile/request/<action>/<id>' => 'profile/request/<action>',

				'profile/add-holiday' => 'profile/holiday/addHoliday',
				'profile/add-holiday/<type>' => 'profile/holiday/addHoliday',

				'profile' => 'profile/default/index',
				'profile/<action>' => 'profile/default/<action>/',
				'profile/<action>/<id>' => 'profile/default/<action>/',

				'u/<url>/services' => 'site/viewuserservicesbyurl',
				'u/<url>/<service_id>' => 'service/default/viewbyusername',
				'u/<url>' => 'user/default/viewbyusername',


				'<controller:\w+>/<	id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

			),
			"showScriptName" => false
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'info@hhs.by',
		'description' => "Организация праздников и проведение свадеб в Беларуси",
	),
);
