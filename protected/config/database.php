<?php

// This is the database connection configuration.
return array(
//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database

	/**
	 * LOCALHOST - MAX OS X
	 */

	'connectionString' => 'mysql:host=localhost;dbname=hhs_db',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',

    /**
     * LOCALHOST - WINDOWS 8
     */

//    'connectionString' => 'mysql:host=localhost;dbname=hhs_db',
//    'emulatePrepare' => true,
//    'username' => 'root',
//    'password' => '',
//    'charset' => 'utf8',



	/**
	 * HOSTINGER.RU
	 */

//	'connectionString' => 'mysql:host=localhost;dbname=u333817651_boyar',
//	'emulatePrepare' => true,
//	'username' => 'u333817651_admin',
//	'password' => 'l0g0sclub',
//	'charset' => 'utf8',



	/**
	 * HHS.BY
	 */

//	'connectionString' => 'mysql:host=localhost;dbname=admin_hhs_by',
//	'emulatePrepare' => true,
//	'username' => 'admin_root',
//	'password' => 'smpqTwTFvQ',
//	'charset' => 'utf8',

);