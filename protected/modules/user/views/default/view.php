<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $model->author User
 * @var $services Service[]
 */

$priceService = new PriceService();

$isEditable = (Yii::app()->user->id == $model->id)? true : false;
?>

<h1 class="main-h1"><a href="<? echo UrlService::performUserUrl($model); ?>"><? echo $model->username; ?></a></h1>

<?php if (count($services) > 0 && Yii::app()->user->id == $services[0]->author_id) : ?>
<link rel="stylesheet" href="/css/animate.css"/>
<div class="confirm-block-wrapper"></div>
<div class="confirm-block animated ">
	<div class="confirm-title">Вы действительно хотите удалить услугу?</div>
	<div class="confirm-sub-title">Удалённую услугу невозможно будет восстановить</div>

	<a id="yes" href="/">Да</a><a id="no" href="/">Нет</a>
</div>
<?php endif; ?>

<div id="content" class="container960">
<div class="col-span-3">

	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => "profile",
		"model" => $model,
	)); ?>

</div><div class="col-span-9">


	<div class="information block">
		<div class="title-grey"><span class="title-black">ИНФОРМАЦИЯ</span> <? if($isEditable) echo '<a href="/profile/settings">Редактировать</a>'; ?></div>
		<ul>
			<table>
				<tr>
					<td>
						<li class="info-title">Регион:</li>
						<li><?php echo RegionService::performRegionAddressForUser($model); ?></li>
					</td>
					<td>
						<li class="info-title">Номер телефона:</li>
						<li><a href="tel:<?php echo PhoneService::formatToStr($model->phone); ?>"><?php echo PhoneService::formatToStr($model->phone); ?></a></li>
					</td>
				</tr>
				<tr>
					<td>
						<?php if($model->email != "") : ?>
							<li class="info-title">E-mail:</li>
							<li><a href="mailto:<?php echo $model->email; ?>"><?php echo $model->email; ?></a></li>
						<?php endif; ?>
					</td>
					<td>
						<?php if($model->website != "") : ?>
						<li class="info-title">Веб-сайт:</li>
<!--						<li class="link"><a href="http://--><?php //echo $model->website ?><!--" target="_blank">--><?php //echo $model->website?><!--</a> </li>-->
						<li class="link"><a href="<? echo $model->website; ?>" target="_blank"><? echo $model->website; ?></a> </li>
						<?php endif; ?>
					</td>
				</tr>


			</table>
		</ul>
	</div>

	<div class="description block">
		<div class="title-grey"><span class="title-black">ОБО МНЕ</span> <? if($isEditable) echo '<a href="/profile/settings">Редактировать</a>'; ?> </div>
		<?php if(trim($model->about) != "") : ?>
			<?php echo $model->about; ?>
		<?php elseif($model->id == Yii::app()->user->id) : ?>
			<? if($isEditable) echo '<a class="big-orange-add-button" href="/profile/settings">Добавить описание</a>'; ?>
		<?php endif; ?>
	</div>

    <? if($model->id == Yii::app()->user->id && Yii::app()->user->getRole() >= 2) : ?>
        <div class="block">
            <div class="title-grey"><span class="title-black">КАЛЕНДАРЬ</span> <a href="/profile/calendar">Отметить даты</a> </div>

            <div id="calendar-container">
                <div id="calendar-year">
                    <a class="button left disabled" for="year" href=""><img src="/images/arrow_black_left.svg" alt="hhs.by"/></a>
                    <span id="year"></span>
                    <a class="button right" for="year" href=""><img src="/images/arrow_black_right.svg" alt="hhs.by"/></a>
                </div>
                <div id="change-months-container">
                    <a class="button left disabled" href="" for="months"><img src="/images/arrow_black_left.svg" alt="hhs.by"/></a>
                    <a class="button right" href="" for="months"><img src="/images/arrow_black_right.svg" alt="hhs.by"/></a>
                </div>
                <div class="calendar-container"></div>
    <!--			<div id="explication">-->
    <!--				<div id="request" class="block"><div class="explication-circle"></div> - Заявка</div>-->
    <!--				<div id="reserve" class="block"><div class="explication-circle"></div> - Заказанная услуга</div>-->
    <!--				<div id="weekend" class="block"><span>21</span>/<span>21</span> - Выходной день</div>-->
    <!--			</div>-->
            </div>

        </div>
    <? endif; ?>

<!--	--><?// if($model->id == Yii::app()->user->id && Yii::app()->user->getRole() >= 2) : ?>
	<? if($model->role >= 2) : ?>
	<div class="services block">
		<div class="title-grey"><span class="title-black">УСЛУГИ</span> <? if($isEditable) echo '<a href="/profile/settings">Редактировать</a>'; ?></div>

		<?php if(count($services) > 0) : ?>
			<?php
			foreach($services as $model) : ?>

				<?php $this->renderPartial('application.modules.profile.views.default._service_list_item', array(
					"model" => $model,
					"priceService" => $priceService
				)) ?>

			<? endforeach; ?>
		<?php elseif($model->id == Yii::app()->user->id) : ?>
			<? if($isEditable) echo '<a class="big-orange-add-button" href="/profile/add-service">Добавить услугу</a>'; ?>
		<?php endif; ?>
	</div>
	<? endif; ?>

</div>
</div>

<script src="/scripts/is.min.js"></script>
<script>
	var date = new Date();
	var year = date.getFullYear();


	const MIN_YEAR = year;
	var month = date.getMonth();
	const MIN_MONTH = month;
	const MIN_DATE = new Date(MIN_YEAR, MIN_MONTH);
	var id = "<? echo Yii::app()->user->id; ?>";


	var calendarService = new CalendarService(year, month, "#calendar-container");

	$(document).ready(function(){

		if(is.firefox()){
			$(".weekend table input[type=submit]").css({
				"font-size" : "14px"
			})
		}

		calendarService.render().loadData(id);

		$(document).on("click", "td:not(.disabled)", function(){

			var day = $(this).attr("data-day");
			var month = $(this).attr("data-month");
			var year = $(this).attr("data-year");


			if(typeof(day) != "undefined" && typeof(month) != "undefined" && typeof(year) != "undefined")
				window.location = "/profile/calendar?day="+day+"&month="+month+"&year="+year;
		});

		$(".confirm-block-wrapper").click(function() {
			$(".confirm-block-wrapper, .confirm-block").css("display", "none");
			$(".confirm-block").removeClass("bounceIn");
			$("footer, #content, .main-h1, header").removeClass("blur");
		});

		$(".manage-button.trash").click(function(){
			$(".confirm-block-wrapper, .confirm-block").css("display", "block");
			$(".confirm-block").addClass("bounceIn");
			$("footer, #content, .main-h1, header").addClass("blur");
			$("#yes").attr("href", $(this).attr("href"));
			return false;
		});

		$("#yes").click(function(){
			return true;
		});

		$("#no").click(function(){
			$(".confirm-block-wrapper").click();
			return false;
		});

	});
</script>

<script src="/scripts/calendar/main.js"></script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	tr:not(:last-child) td {
		padding-bottom: 10px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-bottom: 20px;
	}

	.block li a {
		color:black;
		text-decoration: none;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:50%;
	}

	.information table td+td {
		width:50%;
	}

	.calendar-container {
		height: 250px !important;
	}


</style>
