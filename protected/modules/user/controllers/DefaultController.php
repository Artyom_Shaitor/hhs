<?php

class DefaultController extends Controller
{

	public $layout = "userLayout";

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionView($id, $_model=null)
	{
        /**
         * @var $model User
         */
		$model = ($_model != null)? $_model : User::model()->findByPk($id);
		if(!isset($model)) {
			parent::redirectError();
			return;
		}

		KeyWordsProvider::put($model);

		$services = Service::loadServicesByAuthorId($model->id);

		$this->setPageTitle($model->username. " - Happy Holiday Service");
//        KeyWordsProvider::put($model);
    
		$this->render('view', array(
			"model" => $model,
			"services" => $services,
			"id" => $id,
		));
	}

	public function actionViewByUserName($url)
	{
		try{
			$model = User::model()->find("url=:u", array(
				":u" => $url
			));
			if($model)
				$this->actionView(null, $model);

		}catch (Exception $e){

		}
	}

	public function actionLoadAvatar()
	{
		$model = User::model()->findByPk($_POST['model_id']);

		if($model){
			$filename = $model->upload($_FILES['avatar']);
			echo $filename;
			return true;
		}
		return false;
	}
}