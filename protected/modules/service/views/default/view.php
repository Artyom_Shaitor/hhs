<?php
/**
 * @var $this DefaultController
 * @var $model Service
 * @var $model->author User
 * @var $services Service[]
 * @var $galleryItem Gallery
 * @var $model->prices Price[]
 */

	$priceService = new PriceService();
	$shareButtonManager = new ShareButtonsManager();

?>



<h1 class="main-h1"><? echo $model->title; ?> <a href="<? echo UrlService::performUserUrl($model->author) ?>" class="more">Просмотреть профиль</a></h1>

<? if(!SessionService::isEmpty("holiday_id")) : ?>
	<div class="choose-holiday-deny-block">
		Выберите услугу для своего праздника <a id="deny-adding-holiday" class="no" href="/">Отмена</a>
	</div>

	<style>
		.choose-holiday-deny-block {
			position: fixed;
			top: 0;
			z-index: 3000;
			font-size: 18px;
			font-weight: 600;
			width: 500px;
			text-align: center;
			left: 50%;
			margin-left: -250px;
			margin-top: 19px;
			color: rgba(0,0,0,0.7)
		}

		a.no {
			text-decoration: none;
			color: #d6224a;
			border: 1px solid #d6224a;
			padding: 7px 20px 5px 20px;
			border-radius: 40px;
			margin-left: 10px;
		}

		a.no:hover {
			background-color: #d6224a;
			color: white;
		}
	</style>

	<script>
		$(document).on("click", "#deny-adding-holiday", function(){
			var xhr = new XMLHttpRequest();

			xhr.onload = function(){
				location.reload();
			};

			xhr.open("POST", "/profile/holiday/clearSession");
			xhr.send();

			return false;
		})
	</script>
<? endif; ?>

<link rel="stylesheet" href="/css/animate.css"/>
<div class="confirm-block-wrapper v1"></div>
<div class="confirm-block animated v1">
	<div class="confirm-title">Вы действительно хотите удалить услугу?</div>
	<div class="confirm-sub-title">Удалённую услугу невозможно будет восстановить</div>

	<a id="yes" href="/">Да</a><a id="no" href="/">Нет</a>
</div>

<div class="wrapper"></div>
<div class="image-container"></div>



<div id="content" class="container960">
	<? if(preg_match("/catalog/", Yii::app()->request->urlReferrer)) : ?><a class="button-back" href="<? echo Yii::app()->request->urlReferrer ?>">Вернуться в каталог</a><? endif; ?>
<div class="col-span-3">

	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => null,
		"model" => $model->author,
	)); ?>


</div><div class="col-span-9">

		<div class="manage-button-list">
			<?php if(Yii::app()->user->id == $model->author_id) : ?>
				<a class="manage-button edit" data-alt="Редактировать услугу" href="<? echo UrlService::performEditServiceUrl($model) ?>"></a>
				<a class="manage-button trash" data-alt="Удалить услугу" href="<? echo UrlService::performDeleteServiceUrl($model) ?>"></a>
			<?php endif; ?>
			<? if(in_array(Yii::app()->user->id, array(6,7,8,12))) : ?>
				<a class="manage-button catalog-add-holiday-button" data-service-id="<? echo $model->id ?>" data-holiday-id="<? echo (!SessionService::isEmpty("holiday_id"))? SessionService::get("holiday_id") : "false" ?>" data-alt="Добавить услугу в праздник" href="<? echo (!SessionService::isEmpty("holiday_id"))? "/profile/holiday/view/".SessionService::get("holiday_id") : "/" ?>"></a>
			<? endif; ?>
		</div>

		<div id="photo-video">

		<div id="tabs">
			<? if(count($model->gallery) > 0 && count($model->video) > 0) : ?>
				<a class="<? echo ($model->sub_category_id != 33)? "active" : "" ?>" id="photo-tab" data-type="photo" href="">Фото</a>
				<a class="<? echo ($model->sub_category_id == 33)? "active" : "" ?>" id="video-tab" data-type="video" href="">Видео</a>
			<? endif; ?>
			<? if (count($model->gallery) > 0 && count($model->video) == 0) : ?>
				<a class="active" id="photo-tab" data-type="photo" href="">Фото</a>
			<? elseif (count($model->video) > 0 && count($model->gallery) == 0) : ?>
				<a class="active" id="video-tab" data-type="video" href="">Видео</a>
			<? endif; ?>
		</div>

		<?php if(count($model->gallery) > 0 && count($model->video) > 0) : ?>

			<div id="photo-gallery" style="<? echo ($model->sub_category_id != 33)? "display: block" : "display: none;" ?>">
				<div id="main-image" style="background-image: url(<? echo ImageService::performImageUrl($model->gallery[0]->image_url, ImageType::GALLERY_IMAGE); ?>)" data-image="<? echo ImageService::performImageUrl($model->gallery[0]->image_url, ImageType::GALLERY_IMAGE); ?>" data-image-index="0"></div>
				<div id="image-list">
					<?php foreach($model->gallery as $key => $galleryItem) : ?>
						<div class="image <? if($key == 0) echo 'active' ?>" style="background-image: url(<? echo ImageService::performImageUrl($galleryItem->image_url, ImageType::GALLERY_IMAGE); ?>)" data-image="<? echo ImageService::performImageUrl($galleryItem->image_url, ImageType::GALLERY_IMAGE)?>" data-image-index="<?=$key;?>"></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div id="video-gallery" style="<? echo ($model->sub_category_id == 33)? "display: block" : "display: none;" ?>">
				<div id="main-video">
					<? echo VideoService::getVideoPreview($model->video[0]); ?>
				</div>

				<div id="video-list">
					<?php foreach($model->video as $key => $video) : ?>
						<div class="video active" style="background-image: url(<? echo VideoService::getVideoThumbnail($video); ?>)"  data-content='<? echo VideoService::getVideoPreview($video) ?>'></div>
					<?php endforeach; ?>
				</div>

			</div>

		<?php elseif(count($model->gallery) > 0) : ?>
			<div id="photo-gallery" style="display: block">
				<div id="main-image" style="background-image: url(<? echo ImageService::performImageUrl($model->gallery[0]->image_url, ImageType::GALLERY_IMAGE); ?>)" data-image="<? echo ImageService::performImageUrl($model->gallery[0]->image_url, ImageType::GALLERY_IMAGE); ?>" data-image-index="0"></div>
				<div id="image-list">
					<?php foreach($model->gallery as $key => $galleryItem) : ?>
						<div class="image <? if($key == 0) echo 'active' ?>" style="background-image: url(<? echo ImageService::performImageUrl($galleryItem->image_url, ImageType::GALLERY_IMAGE); ?>)" data-image="<? echo ImageService::performImageUrl($galleryItem->image_url, ImageType::GALLERY_IMAGE)?>" data-image-index="<?=$key;?>"></div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php elseif(count($model->video) > 0) :  ?>
			<div id="video-gallery" style="display: block">
				<div id="main-video">
					<? echo VideoService::getVideoPreview($model->video[0]); ?>
				</div>
				<div id="video-list">
					<?php foreach($model->video as $key => $video) : ?>
						<div class="video active" style="background-image: url(<? echo VideoService::getVideoThumbnail($video); ?>)"  data-content='<? echo VideoService::getVideoPreview($video) ?>'></div>
					<?php endforeach; ?>
				</div>

			</div>
		<? endif; ?>
	</div>



	<div class="block" style="float: right;width: auto;">
        <span class="info-title"><a style="color:rgb(70,70,70);" href="<?php echo UrlService::performUserUrl($model->author)?>">Подробнее о "<? echo $model->author->username; ?>"</a></span><br/>
    </div>

	<div class="block" style="width: 297px;
    right: 0;
    margin-top: 76px;
    position: absolute;
    padding-right: 26px;
    text-align: left;">
		<span class="info-title">Контакты:</span><br/>
		<div><a href="tel:<? echo PhoneService::formatToStr($model->author->phone) ?>" style="font-size: 14px; text-decoration: none; font-weight: 600; color: rgb(70,70,70)"><? echo PhoneService::formatToStr($model->author->phone) ?></a></div>
		<div><a href="mailto:<? echo $model->author->email ?>" style="font-size: 14px; text-decoration: none; font-weight: 600; color: rgb(70,70,70)"><? echo $model->author->email ?></a></div>
	</div>

	<div class="service-title block">
		<span class="info-title">Услуга:</span><br/>
		<span class="black"><?php echo $model->subCategory->name; ?></span>
	</div>

	<div class="block">
		<span class="info-title">Регион работы:</span><br/>
		<span class="black" style="width: 330px;display: inline-block;"><?php echo RegionService::performRegionAddressForService($model)?></span>
	</div>

	<? if(trim($model->description) != "") : ?>

		<div class="description block">
			<span class="info-title" style="margin-bottom: -14px;display: block;">Описание:</span><br/>
			<?php echo $model->description; ?>
		</div>

	<? endif; ?>

	<div class="block">
		<span class="info-title">Поделиться услугой:</span>
		<div class="links">
			<? $shareButtonManager->buildButtons($model); ?>
		</div>
	</div>

	<div class="prices-block block">

		<div class="additional-prices-block">
			<div class="additional-price" <? if(count($model->prices) == 0) echo 'style="border-bottom:0;"' ?> >
				<div class="additional-price-description">Цена за услугу</div>
				<div class="additional-price-value">

					<?php
						$array = $priceService->getCurrenciesArray();

						foreach ($array as $key => $currency) {
							if($model->price_value != 0 && $model->price_value != null) {
								echo '<span class="additional-prices">' . $priceService->convertToCurrency($model, $key) . '</span>';
							}else{
								echo '<span class="additional-prices">' . $priceService->performPricesFromTo($model, $key) . '</span>';
							}

						}
					?>

				</div>
			</div>

<!--			--><?php //if( count($model->prices) > 0 ) : ?>
<!---->
<!--				--><?php //foreach( $model->prices as $index => $price ) : ?>
<!---->
<!--					<div class="additional-price">-->
<!--						<div class="additional-price-description">--><?php //echo $price->description ?><!--</div>-->
<!--						<div class="additional-price-value">-->
<!---->
<!--							--><?php
//							$array = $priceService->getCurrenciesArray();
//							foreach($array as $key => $currency){
//								echo '<span class="additional-prices">'.$priceService->convertToCurrencyWithAdditionalPrices($model, $index, $key, "").'</span>';
//							}
//
//							?>
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--				--><?php //endforeach;?>
<!---->
<!--			--><?php //endif; ?>
		</div>

		<? if (count($model->prices) > 0) : ?>

			<div class="additional-prices-list1">

				<div class="additional-price-main-title1">Пакеты услуг</div>

				<? foreach($model->prices as $index => $price) : ?>

					<? $array = $priceService->getCurrenciesArray(); ?>

					<div class="additional-prices-item1">
						<div class="additional-price-item-title1"><? echo $price->description ?></div>
						<hr/>
						<? 	foreach($array as $key => $currency){
								echo '<span class="additional-prices-value '.$key.'">'.$priceService->convertToCurrencyWithAdditionalPrices($model, $index, $key, "").'</span>';
							}
						?>
					</div>

				<? endforeach; ?>


			</div>

		<? endif ?>

	</div>



</div>
</div>

<?
if(in_array(Yii::app()->user->id, array(6, 7, 8, 12)) || !SessionService::isEmpty("holiday_id"))
	$this->renderPartial('application.views.site._choose_holiday', array(
		"holidays" => $holidays
	))
?>

<?php Yii::app()->clientScript->registerScript(
	"tabs-switch", '

		$(document).ready(function(){

			var isset = function(element){
				if(element.length>0) return true;
				return false;
			};

			var getImgByIndex = function(index){
				return $("#photo-gallery #image-list div[data-image-index=\'"+index+"\']");
			};

//			$(document).on("click", ".catalog-add-holiday-button", function(){
//				var serviceId = $(this).attr("data-service-id");
//				var holidayId = '.$model->id.';
//
//				var xhr = new XMLHttpRequest();
//				var fd = new FormData();
//				fd.append("service_id", serviceId);
//				fd.append("holiday_id", holidayId);
//
//				xhr.onload = function(){
//					var response = JSON.parse(xhr.response);
//
//					if(response[\'result\'] == false){
//						alert(response[\'message\']);
//						return;
//					}
//
//					var buttonDom = ".holiday-list input[data-category-id=\'"+response[\'category_id\']+"\'][data-subcategory-id=\'"+response[\'sub_category_id\']+"\']";
//					var holidayDom = ".service-holiday[data-category-id=\'"+response[\'category_id\']+"\'][data-subcategory-id=\'"+response[\'sub_category_id\']+"\']";
//					if($(buttonDom).length > 0) {
//						$(response[\'render\']).insertAfter(buttonDom);
//						$(buttonDom).remove();
//
//					}
//					else if($(holidayDom).length > 0){
//						$(response[\'render\']).insertAfter($(holidayDom)[$(holidayDom).length - 1]);
//						$(buttonDom).remove();
//					}
//					else
//						$(".holiday-list").append(response[\'render\']);
//
//					changeSummary();
//
//					$(".bg-black, .catalog-mini").addClass("hide");
//					$("body").css("overflow", "auto");
//
//				};

//				xhr.open("POST", "/profile/holiday/addServiceToHoliday");
//				xhr.send(fd);
//
//				return false;
//			})

			$("#tabs a").click(function(){

				var clickedTab = $(this).attr("data-type");
				var activeTab = $("#tabs .active").attr("data-type");

				$("#tabs .active").removeClass("active");
				$("#tabs #"+clickedTab+"-tab").addClass("active");

				console.log("#tabs #"+activeTab+"-gallery");

				$("#"+activeTab+"-gallery").css("display", "none");
				$("#"+clickedTab+"-gallery").css("display", "block");

				return false;
			});

			$("#video-list .video").click(function(){

				var data = $(this).attr("data-content");
				$("#main-video").html(data);

				$(".video.active").removeClass("active");
				$(this).addClass("active");

			});


			 $("#photo-gallery .image").click(function(){
				var image = $(this).css("background-image");
				$("#photo-gallery #main-image").css("background-image", image).attr("data-image", $(this).attr("data-image")).attr("data-image-index", $(this).attr("data-image-index"));
				$(".image-container").attr("data-image-index", $(this).attr("data-image-index"));

				$(".image.active").removeClass("active");
				$(this).addClass("active");
			 });


			$("#main-image").click(function(){
				$(".wrapper").css("display", "block");
				$(".image-container").css("display", "block").css("background-image", "url("+$(this).attr("data-image")+")").attr("data-image-index", $(this).attr("data-image-index"));
			});

			$(".wrapper").click(function(){
				$(this).css("display", "none");
				$(".image-container").css("display", "none");
			});


			$(".image-container").click(function(){

				var dataIndex = parseInt( $(".image-container").attr("data-image-index"));

				var _this = this;
				var nextImg = getImgByIndex(dataIndex+1);

				if(!isset(nextImg)){
					nextImg = getImgByIndex(0);
					dataIndex = -1;
				}

				var url = nextImg.attr("data-image");
				$(this).css("background-image", "url("+url+")");
				$(this).attr("data-image-index", dataIndex+1 );

			});


			$(".confirm-block-wrapper.v1").click(function() {
				$(".confirm-block-wrapper.v1, .confirm-block.v1").css("display", "none");
				$(".confirm-block.v1").removeClass("bounceIn");
				$("footer, #content, .main-h1, header").removeClass("blur");
			});

			$(".manage-button.trash").click(function(){
				$(".confirm-block-wrapper.v1, .confirm-block.v1").css("display", "block");
				$(".confirm-block.v1").addClass("bounceIn");
				$("footer, #content, .main-h1, header").addClass("blur");
				$("#yes").attr("href", $(this).attr("href"));
				return false;
			});

			$("#yes").click(function(){
				return true;
			});

			$("#no").click(function(){
				$(".confirm-block-wrapper").click();
				return false;
			});


		});

        ', CClientScript::POS_READY
)?>

<script>
	<?php echo $priceService->initJavaScript();?>
</script>

<style>

	.additional-prices-list1 {
		font-size: 0;
		padding-top: 10px;
	}

	.additional-prices-item1 {
		display: inline-block;
		vertical-align: top;
		width: 22.4%;
		font-size: 16px;
		background-color: rgb(230,230,230);
		box-sizing: border-box;
		margin: 14px 8px;
		padding-bottom: 30px;

		transition: all 0.15s ease-in-out;
		-o-transition: all 0.15s ease-in-out;
		-moz-transition: all 0.15s ease-in-out;
		-webkit-transition: all 0.15s ease-in-out;

	}

	.working-around {
		text-align: left;
		font-size: 12px;
		margin-top: 2px;
	}

	.additional-price-main-title1 {
		font-size: 16px;
		font-weight: bold;
		margin-top: 25px;
		margin-left: 8px;
	}

	.additional-price-item-title1 {
		text-align: center;
		padding-top: 30px;
		font-size: 13px;
	}

	.additional-prices-item1 hr {
		border-bottom: 1px solid rgb(170,170,170) !important;
		margin: 27px 47px !important;
	}

	span.additional-prices-value {
		width: 100%;
		display: block;
		text-align: center;
	}

	.additional-prices-item1 .BLR {
		margin-bottom: 8px;
	}

	.additional-prices-item1 .BLR .price-value  {
		font-size: 14px;
	}

	.additional-prices-item1 .BLR .price-currency,
	.additional-prices-item1 .BLR .price-type  {
		font-size: 10px;
		font-weight: 600;
	}

	.additional-prices-item1 .USD {
		margin-top: 8px;
		color:rgb(80,80,80);
	}

	.additional-prices-item1 .USD   {
		font-size: 14px;
	}

	header {
		box-shadow: 0 -4px 10px black;
	}

	tr:not(:last-child) td {
 		padding-bottom: 10px;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.manage-button-list {
		right: 15px;
		top: 10px;
	}

	.manage-button-list .manage-button {
		margin: 0 1px;
	}

	.col-span-9 { position: relative; }

	.information table {
		width:100%;
	}

	.information table td {
		width:35%;
	}

	.information table td+td {
		width:65%;
	}


</style>