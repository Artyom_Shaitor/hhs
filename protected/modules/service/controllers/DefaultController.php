<?php

class DefaultController extends Controller
{
	public $layout = "serviceLayout";

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('view', 'viewbyusername', 'help'),
				'roles'=>array('guest')
			),
			array('allow',
				'actions' => array('help'),
				'roles'=>array('1', '2', '3')
			),
			array('allow',
				'actions' => array('edit', 'deleteprice', 'deletecity', 'delete'),
				'roles'=>array('2', '3')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id = 1, $_model = null)
	{
		/**
		 * @var $model Service
		 */
		$model = ($_model == null)? Service::model()->findByPk($id) : $_model;

		if($model == null || !$model->isCreated()) return true;

		$services = Service::loadServicesByAuthorId($model->author->id);

		$holidays = Holiday::model()->findAll("author_id=:uid", array(
			":uid" => Yii::app()->user->id,
		)) ;

		KeyWordsProvider::put($model);

		$subCategory = ($model->subCategory->name != "Другое")? $model->subCategory->name : $model->category->name;

		$this->setPageTitle($model->title." ". $subCategory);

		$this->render('view', array(
			"model" => $model,
			"services" => $services,
			"id" => $id,
			"holidays" => $holidays
		));
	}

	public function actionViewByUsername($url, $service_id)
	{
		$model = Service::model()->findByPk($service_id);
		if($model != null && $model->author->url == $url)
			$this->actionView(null, $model);
	}

	public function actionDeletePrice(){

		$count = Price::model()->deleteByPk($_POST['id']);

		if ($count > 0) return true;
		return false;
	}

	public function actionDeleteCity(){
		if(isset($_POST['id']) && isset($_POST['service_id'] ) ){
			if($_POST['service_id'] != "") {

				$id = $_POST['id'];
				$service_id = $_POST['service_id'];

				$service = Service::model()->findByPk($service_id);
				if ($service->author_id == Yii::app()->user->id) {
					$count = CityServiceAssignment::model()->deleteAll("city_id=:cid AND service_id=:sid", array(
						":sid" => $service_id,
						":cid" => $id,
					));

					if ($count > 0) {
						echo CJavaScript::encode(true);
						return true;
					}
					echo CJavaScript::encode(false);
					return false;
				}
				echo CJavaScript::encode(false);
				return false;
			}else{ echo CJavaScript::encode(true); return true;}

		}elseif(isset($_POST['id'])) { echo CJavaScript::encode(true); return true;}
		echo CJavaScript::encode(false);
		return false;
	}

	public function actionEdit($id){

		/**
		 * @type $model Service
		 */
		$model = Service::loadModel($id);
		$model->setScenario('sc_editService');

		$this->setPageTitle(Yii::app()->name." - Редактировать услугу");
		/**
		 * @type $prices Price[]
		 */
		$prices = $model->prices;
		$user = User::model()->findByPk(Yii::app()->user->id);

		$price_type = 1;

		if($model->max_price_value != 0 && $model->max_price_value != null) $price_type = 2;
		if($model->min_price_value != 0 && $model->min_price_value != null) $price_type = 2;

		if($model->region_id == null || $model->region_id == 0) $type = 1;
		if(count($model->city) > 0 || isset($_POST['Service']['Job_Region'][3]) ) $type = 3;

		if(isset($_POST['Service']))
		{

			$model->attributes=$_POST['Service'];
			$model->author_id = Yii::app()->user->id;
			$model->is_working_around = $_POST['Service']['is_working_around'];

			$model->description = nl2br($_POST['Service']['description']);

			$post = (isset($_POST['Price']))? $_POST['Price'] : array();

			foreach( $post as $i => $postPrice ){
				if(isset($prices[$i])){
					$prices[$i]->attributes = $postPrice;
				}
				else{
					$price = new Price();
					$price->attributes = $postPrice;
					$prices[] = $price;
				}
			}

			$price_type = $_POST['price-radio'];

			if($price_type == 1){
				$model->min_price_value = $model->max_price_value = 0;
			}elseif($price_type == 2) {
				$model->price_value = 0;
			}

			if(isset($_POST['Service']['Job_Region']["city"]) && count($_POST['Service']['Job_Region']["city"]) > 0){
				foreach($_POST['Service']['Job_Region']['city'] as $index => $city){
					if($city['city_id'] != null) {
						$csa = new CityServiceAssignment;

						$csa->city_id = $city['city_id'];
						$csa->service_id = $model->id;
						$csa->save();
					}
				}
			}
			else {
				$model->addError('city', 'Вы должны выбрать хотя бы один город');
			}

			$model->country_id = 1;

			$model->avatar_url = $_POST['avatar'];

			foreach($prices as $price)
				$price->validate();


			if (isset($_POST['Video'])) {

				$deletedVideo = Video::model()->deleteAll("author_id=:id && service_id=:sid", array(
					":id" => Yii::app()->user->id,
					":sid" => $model->id,
				));

				$maxVideos = ApplicationSettings::getSettings()->max_videos;
				$videoCount = 0;
				foreach ($_POST['Video'] as $video_link) {
					if (!empty($video_link)) {
						$video = new Video;
						$video->video_link = $video_link;
						$video->service_id = $model->id;
						$video->author_id = Yii::app()->user->id;
						$video->video_provider = VideoService::getVideoProvider($video);

						if($video->save())
							$videoCount++;
						if($videoCount >= $maxVideos) break;
					}
				}

			}

			$model->setOccupancy();
			if($model->validate('city', false))
			{

				if ($model->save()) {


					$metaDataIds = array();
					foreach ($model->metadata as $metaData) {
						$metaDataIds[] = $metaData->id;
					}


					if(isset($_POST['MetaData']))
						if(count($_POST['MetaData']))
							foreach ($_POST['MetaData'] as $attribute_id => $value) {

								$isExist = false;

								foreach ($model->metadata as $index => $metaData) {

									if ($metaData->attribute_id == $attribute_id) {
										$isExist = true;

										if (is_array($value))
											$value = implode(',', $value);
										$metaData->value = $value;
										if ($metaData->save())
											unset($metaDataIds[$index]);

									}

								}

								if (!$isExist) {
									$md = new MetaData();
									$md->attribute_id = $attribute_id;
									if (is_array($value))
										$value = implode(',', $value);
									$md->service_id = $model->id;
									$md->value = $value;
									$md->save();
								}


							}

					MetaData::model()->deleteAllByAttributes(array(
						'id' => $metaDataIds,
					));

					foreach ($prices as $p) {
						if ($p->isNewRecord) $p->service_id = $model->id;
						$p->save();
					}


					Yii::app()->user->setFlash('success', 'Услуга изменена');
					$this->redirect('/profile/services');
				}
			}

		}

		$galleries = Gallery::model()->findAll("author_id=:id AND service_id=:sid ", array(":id" => Yii::app()->user->id , ":sid" => $id));

		$this->setPageTitle("Редактировать услугу - ". Yii::app()->name);

		$this->render('application.modules.profile.views.default.service', array(
			"model" => $model,
			"prices" => $prices,
			"user" => $user,
			"galleries" => $galleries,
			"type" => $type,
			"id" => $id,
			"price_type" => $price_type,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id){

		$model = Service::model()->findByPk($id);
		if(Yii::app()->user->id == $model->author_id){

			if($model->delete()){
				Yii::app()->user->setFlash('delete-result', 'Услуга успешно удалена');
			}

			$this->redirect('/profile/services');

		}

	}

}