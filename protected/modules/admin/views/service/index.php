<?php
/* @var $this ServiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Services',
);

$this->menu=array(
	array('label'=>'Create Service', 'url'=>array('create')),
	array('label'=>'Manage Service', 'url'=>array('admin')),
);
?>

<h1>Услуги</h1>

<a href="/admin">Главная</a>
<a href="/admin/service/refreshRate">Пересчитать рейтинг</a>

<? $this->renderPartial('_search', array(
	"model" => $dataProvider->model,
)) ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'columns' => array(
			'id',
			array(
				'class' => 'CLinkColumn',
				'header' => 'Название услуги',
				'urlExpression' => 'UrlService::performServiceUrl($data)',
				'labelExpression' => '$data->title'
			),
			array(
				'name' => 'category_id',
				'value'=> '$data->category->name',
			),
			array(
				'name' => 'sub_category_id',
				'value'=> '$data->subCategory->name',
			),
			'author_id',
			array(
				'header' => 'Имя автора',
				'class' => 'CLinkColumn',
				'labelExpression' => '$data->author->username',
				'urlExpression' => 'Yii::app()->createUrl("admin/user", array("User[id]" => $data->author->id))',
			),
			array(            // display a column with "view", "update" and "delete" buttons
				'class'=>'CButtonColumn',
			),

		)

)); ?>

<style>
	#content {
		padding: 10px 20px;
	}
</style>
