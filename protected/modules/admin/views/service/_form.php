<?php
/* @var $this ServiceController */
/* @var $model Service */
/* @var $form CActiveForm */
?>

<div class="update-service-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'service-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<a href="/admin/service">К списку услуг</a><br/><br/>

	<?php echo $form->errorSummary($model); ?>

	<table>
		<tr>
			<td><?php echo $form->labelEx($model,'title'); ?></td>
			<td>
				<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
				<?php echo $form->error($model,'title'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'description'); ?></td>
			<td>
				<?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>1500)); ?>
				<?php echo $form->error($model,'description'); ?>
			</td>
		</tr>
	</table>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<style>
	#content {
		padding: 10px 20px;
	}

	table td:first-child {
		padding-right: 20px;
	}
</style>