<?php
/* @var $this DefaultController */

?>
<h1>Панель администрирования HHS</h1>

<a href="/admin/user/">Пользователи</a>
<a href="/admin/service">Услуги</a>

<style>
    #content {
        padding: 10px 20px;
    }

    #content a {
        text-decoration: none;
        color:black;
        font-weight: 100;
        margin:0 10px;
    }

    #content a:hover {
        text-decoration: underline;
    }
</style>