<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1>Пользователи</h1>

<a href="/admin">Главная</a>

<style>

	.settings {
		font-size: 0;
		border-bottom:1px solid rgba(0,0,0,0.1);
	}

	.left, .right {
		display: inline-block;
		vertical-align: top;
		width:50%;
		box-sizing: border-box;
		font-size: 16px;
		padding: 0 25px 25px 25px;

	}

	.left {
		border-right:1px solid rgba(0,0,0,0.1);
	}

	.form {
		margin: 0;
	}

</style>

<div class="settings">
	<div class="left">
		<h3>Поиск:</h3>
		<? $this->renderPartial('_search', array(
			"model" => $dataProvider->model,
		)) ?>
	</div>

	<div class="right">
		<h3>Рассылка</h3>
		<input type="text" placeholder="Тема" name="subject" id="subject"/> <br/>
		<textarea name="mail" id="mail" cols="30" rows="10" placeholder="Текст письма"></textarea><br/>

		<? echo CHtml::ajaxButton('Отправить', $this->createUrl('/admin/default/sendEmail'), array(
			'type' => "POST",
			'data' => 'js:{ids : $.fn.yiiGridView.getChecked("users-list", "checked_users").toString(), "mail" : $("#mail").val(), "subject" : $("#subject").val()}',
			'success' => 'function(data){ data = JSON.parse(data); if(data["result"] == false) alert(data["message"]); else alert(data["message"]); }'
		)) ?>

		<?
		Yii::app()->clientScript->registerScript('searchTalent', "
	$('#users-search-form').submit(function(){
		$.fn.yiiGridView.update('users-list', {
			data: $(this).serialize()
		});
		return false;
	});
	");
		?>
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'users-list',
	'dataProvider'=>$dataProvider,
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'value' => '$data->id',
			'id' => 'checked_users',
			'selectableRows' => '100',
		),
		'id',
		array(
			'name' => 'Имя пользователя',
			'value' => '$data->username',
			'htmlOptions' => array(
				'width' => 200
			)
		),
		array(
			'name' => 'Дата регистрации',
			'value' => 'date("d-m-Y", $data->created)',
			'htmlOptions' => array(
				'width' => 200
			)
		),
		array(
			'name' => 'Роль',
			'value' => 'RoleService::getRoleName($data->role)'
		),
		array(
			'name' => 'Телефон',
			'value' => 'PhoneService::formatToStr($data->phone)',
			'htmlOptions' => array(
				'width' => 150
			)
		),
		array(
			'name' => 'Бан',
			'value' => '($data->ban == 1)? "Да" : "Нет"',
			'htmlOptions' => array(
				'width' => 90
			)
		),
		"email",
		array(
			'name' => 'Подтвержден',
			'value' => '($data->is_confirmed == 1)? "Да" : "Нет"',
			'htmlOptions' => array(
				'width' => 30
			)
		),
		'provider',
		array(
			'name' => 'Pro-аккаунт',
			'value' => '($data->pro == 1)? "Да" : "Нет"',
			'htmlOptions' => array(
				'width' => 0
			)
		),
		array(
			'class' => "CLinkColumn",
			'header' => 'Услуги',
			'urlExpression' => 'Yii::app()->createUrl("admin/service", array("Service[author_id]" => $data->id))',
			'label' => 'Все услуги'
		),
		array(            // display a column with "view", "update" and "delete" buttons
			'class'=>'CButtonColumn',
		),
	),
	'pager'=> array(
		'prevPageLabel' => '&laquo; назад',
		'nextPageLabel' => 'далее &raquo;',
	),
)); ?>

<style>

	::-webkit-input-placeholder {color: grey;}
	::-moz-placeholder          {color: grey;}/* Firefox 19+ */
	:-moz-placeholder           {color: grey;}/* Firefox 18- */
	:-ms-input-placeholder      {color: grey;}

</style>




<a href="/admin/user/create">Создать пользователя</a>

<style>
	#content {
		padding: 10px 20px;
	}
</style>
