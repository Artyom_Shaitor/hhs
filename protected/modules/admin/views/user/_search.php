<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'users-search-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'role'); ?>
		<?php echo $form->dropDownList($model, 'role', RoleService::getListData(), array("prompt" => "Все пользователи")); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ban'); ?>
		<?php echo $form->dropDownList($model, 'ban', array("1" => 'Забаненные', '0' => 'Незабаненные'), array("prompt" => "Все пользователи")); ?>
		<?php echo $form->error($model,'ban'); ?>
	</div>

	<div class="row">
		<label for="User_pro">PRO-аккаунт</label>
		<?php echo $form->dropDownList($model, 'pro', array("1" => 'Да', '0' => 'Нет'), array("prompt" => "Все пользователи")); ?>
		<?php echo $form->error($model,'pro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Provider'); ?>
		<?php echo $form->dropDownList($model, 'provider', array("vk" => 'Вконтакте', 'ok' => 'Одноклассники', 'facebook' => 'Facebook'), array('prompt' => '')); ?>
	</div>

	<div class="row">
		<label for="User_is_confirmed">Подтверждение</label>
		<?php echo $form->dropDownList($model, 'is_confirmed', array("1" => 'Подтвержденные', '0' => 'Ожидают подтверждения'), array("prompt" => "Все пользователи")); ?>
		<?php echo $form->error($model,'is_confirmed'); ?>
	</div>

	<div class="row">
		<label for="User_phone">Телефон</label>
		<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->