<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="user-update-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<a href="/admin/user">Вернуться к списку пользователей</a><br/><br/>

	<?php echo $form->errorSummary($model); ?>

	<table>
		<tr>
			<td><?php echo $form->labelEx($model,'username'); ?></td>
			<td>
				<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'username'); ?>
			</td>
		</tr>
		<tr>
			<td>Телефон (37529XXXYYZZ)</td>
			<td>
				<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'phone'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'about'); ?></td>
			<td>
				<?php echo $form->textArea($model,'about',array('size'=>160,'maxlength'=>400)); ?>
				<?php echo $form->error($model,'about'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'email'); ?></td>
			<td>
				<?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>45)); ?>
				<?php echo $form->error($model,'email'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'ban'); ?></td>
			<td>
				<?php echo $form->dropDownList($model, 'ban', array("0" => 'Разбанить', '1' => 'Забанить')); ?>
				<?php echo $form->error($model,'role'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'pro'); ?></td>
			<td>
				<?php echo $form->dropDownList($model, 'pro', array("0" => 'Нет', '1' => 'Да')); ?>
				<?php echo $form->error($model,'pro'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'is_confirmed'); ?></td>
			<td>
				<?php echo $form->dropDownList($model, 'is_confirmed', array("0" => 'Нет', '1' => 'Да')); ?>
				<?php echo $form->error($model,'is_confirmed'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'role'); ?></td>
			<td>
				<?php echo $form->dropDownList($model, 'role', RoleService::getListData()); ?>
				<?php echo $form->error($model,'role'); ?>
			</td>
		</tr>
	</table>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<style>
	#content {
		padding: 10px 20px;
	}

	table td:first-child {
		padding-right: 20px;
	}
</style>