<?php

class DefaultController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'sendEmail'),
				'roles'=>array('3'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionSendEmail()
	{
		if(Yii::app()->user->getRole() == 3) {
			if($_POST['ids'] == null){
				$result = array(
					'result' => false,
					'message'=> 'Выберите как минимум одного пользователя',
				);
				echo json_encode($result);
				return true;
			}

			if(trim($_POST['subject']) == null){
				$result = array(
					'result' => false,
					'message'=> 'Необходимо указать тему письма',
				);
				echo json_encode($result);
				return true;
			}

			if(trim($_POST['mail']) == null){
				$result = array(
					'result' => false,
					'message'=> 'Необходимо написать текст письма',
				);
				echo json_encode($result);
				return true;
			}

			$name='=?UTF-8?B?'.base64_encode(trim($_POST['subject'])).'?=';
			$subject='=?UTF-8?B?'.base64_encode(trim($_POST['subject'])).'?=';
			$message = trim($_POST['mail']);

			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', explode(',', $_POST['ids']));
			/**
			 * @type $users User[]
			 */
			$users = User::model()->findAll($criteria);

			$result = array(
				'result' => true,
				'message' => 'Письма успешно разосланы',
				'data' => [],
			);

			foreach($users as $user){

				$email = $user->email;

				$result['data'][] = array(
					"user_id" => $user->id,
					"result" => SendMail::send($email, $_POST['subject'], $_POST['mail'], $user)
				);
			}

			echo json_encode($result);
			return true;

		}
	}
}