<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'update', 'index','view', 'create'),
				'roles'=>array('3'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User('adminCreate');

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$genPassword = PasswordService::generatePassword();
			$model->password = $genPassword;
			$model->is_confirmed = 1;

			if($model->save()) {

				$name='=?UTF-8?B?'.base64_encode("Регистрация").'?=';
				$subject='=?UTF-8?B?'.base64_encode("Регистрация").'?=';

				$message = "
<html>
	<head>
		<title>Регистрация</title>
	</head>
	<body>
		Уважаемый {$model->username}!<br><br>
		Вы получили это письмо, потому что этот адрес электронной почты был указан при регистрации на свадебном сервисе hhs.by. <br/>
Логин для входа: <b>{$model->email}</b> <br/>
Пароль для входа: <b>{$genPassword}</b> <br/>
Если данное письмо отправлено вам по ошибке, то просто проигнорируйте его. <br>
Пожалуйста, не отвечайте на это письмо, так как оно было выслано автоматически. <br/><br/>

С уважением Happy Holiday Service! <br/><br/>

Тел.: <a href=\"tel:+375292441199\">+375(29)244-11-99</a> <br>
		E-mail: <a href=\"mailto:info@hhs.by\">info@hhs.by</a> <br>
		<a href=\"http://hhs.by\">http://hhs.by</a> <br>

	</body>";

				$from = "noreply@hhs.by";

				$headers="From: Happy Holiday Service <noreply@hhs.by>\r\n".
					"Reply-To: noreply@hhs.by\r\n".
					"MIME-Version: 1.0\r\n".
					"Date:".date("Y-n-d")."\r\n".
					"Content-Type: text/html; charset=UTF-8";

				mail($model->email,$subject,$message,$headers, "-f$from");

				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
//			$model->is_confirmed = $_POST['User']['is_confirmed'];
			if($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');

		if(isset($_GET['User'])){
			$model = new User('search');
			$model->attributes = $_GET['User'];
			$dataProvider = $model->search();
		}

		$dataProvider->pagination->pageSize = 30;
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
