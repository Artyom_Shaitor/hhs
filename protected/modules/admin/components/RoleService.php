<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 18.12.15
 * Time: 18:16
 */

class RoleService {

    private static $ROLES = array(
        '2' => 'Подр.',
        '1' => 'Клиент',
        '3' => 'Админ.'
    );

    public static function getRoleName($id){
        if(isset(self::$ROLES[$id])) return self::$ROLES[$id];
        return null;
    }

    public static function getListData(){
        return self::$ROLES;
    }

}