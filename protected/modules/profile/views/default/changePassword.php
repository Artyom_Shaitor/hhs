<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $form CActiveForm
 */

$priceService = new PriceService();

?>
<style>
    .form {
        width:100%;
    }

	[data-for="data-textarea"] {
		color: rgb(20,235,0);
		vertical-align: top;
		margin-left: 10px;
	}

	.save-button {
		width:100% !important;
		margin-top: 30px;
	}

</style>


<h1 class="main-h1"><? echo $model->username; ?></h1>

<div id="content" class="container960">

<div class="col-span-3">

	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => "settings",
		"model" => $model
	)); ?>

</div><div class="col-span-9">


	<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'service-form',
		'enableAjaxValidation'=>false,
		'errorMessageCssClass' => "add-service-error-message",
	)); ?>

        <ol class="params-list">

			<!-- Имя пользователя   -->
			<li>Смена пароля
				<?php
					if($model->hasErrors('old_password') || $model->hasErrors('new_password') || $model->hasErrors('repeat_password') ){
						if($model->hasErrors('old_password')) { echo $form->error($model, 'old_password');  }
						if($model->hasErrors('new_password')) { echo $form->error($model, 'new_password');  }
						if($model->hasErrors('repeat_password')) { echo $form->error($model, 'repeat_password');  }
					}
				?>
			</li>
			<div>
				<div class="sub-title">Введите старый пароль</div>
				<?php echo $form->passwordField($model,'old_password',array('size'=>60,'maxlength'=>32)); ?>

				<br/><br/>
				<div class="sub-title">Введите новый пароль</div>
				<?php echo $form->passwordField($model,'new_password',array('size'=>60,'maxlength'=>32)); ?>

				<div class="sub-title">Повторите пароль</div>
				<?php echo $form->passwordField($model,'repeat_password',array('size'=>60,'maxlength'=>32)); ?>
			</div>



        </ol>



	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить', array("class" => "add-service save-button")); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->

</div>
</div>

<script src="/scripts/gallerymanager.min.js"></script>

<script src="/scripts/is.min.js"></script>

<script>


</script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	#User_country_id {
		width:124px;
	}

	#User_region_id {
		width:200px;
	}

	#User_city_id {
		width:190px;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:35%;
	}

	.information table td+td {
		width:65%;
	}


	 .change-password-button{
		 display: block;
		 background-color: rgb(225,225,225) !important;
		 border: 0 !important;
		 width: 261px !important;
		 margin-top: 12px !important;
		 color:rgb(130,130,130) !important;
	 }

	.change-password-button:hover{
		color:rgb(170,170,170) !important;
	}

	.password-change-block {
		background-color: rgb(240,240,240);
		margin-top: 30px;
		box-sizing: border-box;
		padding: 30px 60px;
		display: inline-block;
	}

	.password-change-title {
		text-transform: uppercase;
		font-weight: 600;
		color: rgba(70,70,70,0.8);
		margin-bottom: 10px;
	}

	.password-change-block  input[type=password]:focus {
		border:1px solid rgba(0,0,0,0.35)
	}


</style>