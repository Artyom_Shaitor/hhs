<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $form CActiveForm
 */

$priceService = new PriceService();

?>
<style>
    .form {
        width:100%;
    }

	[data-for="data-textarea"] {
		color: rgb(20,235,0);
		vertical-align: top;
		margin-left: 10px;
	}

	.save-button {
		width:100% !important;
		margin-top: 30px;
	}

</style>

<?php if(Yii::app()->user->hasFlash('success')) : ?>
	<div class="flash-container">
		<div class="success-flash"><? echo Yii::app()->user->getFlash('success')?></div>
		<script>
			setTimeout(function(){
				$(".success-flash").animate({opacity : 0}, 600, function(){
					$(".success-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<?php if($model->hasErrors()) : ?>
	<div class="flash-container">
		<div class="failed-flash">Заполните все неоходимые поля</div>
		<script>
			setTimeout(function(){
				$(".failed-flash").animate({opacity : 0}, 600, function(){
					$(".failed-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<h1 class="main-h1"><? echo (empty($model->username))? 'Настройки' : $model->username ?></h1>

<div id="content" class="container960">

<div class="col-span-3">

	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => "settings",
		"model" => $model
	)); ?>

</div><div class="col-span-9">

	<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'service-form',
		'enableAjaxValidation'=>false,
		'errorMessageCssClass' => "add-service-error-message",
	)); ?>

        <ol class="params-list" style="margin-top:0;">

			<div class="row buttons">
				<?php echo CHtml::submitButton('Сохранить', array("class" => "add-service save-button", "style" => "margin-top:0;")); ?>
			</div>

			<!-- Имя пользователя   -->
			<li><?php echo $form->label($model,'username'); ?> <?php echo $form->error($model,'username'); ?></li>
			<div>
				<div class="sub-title">Ваше имя, фамилия, либо же название организации</div>
				<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
			</div>

			<li>Адрес страницы <?php echo $form->error($model, 'url')?></li>
			<div>
				hhs.by/u/ <? echo $form->textField($model, 'url'); ?>
			</div>

			<li><?php echo $form->label($model,'avatar_url'); ?> <?php echo $form->error($model,'avatar_url'); ?></li>
			<div>
				<div class="sub-title">Выберите фотографию профиля</div>
<!--				--><?php
//				$this->widget('ext.xphoto.XPhoto',array(
//					'model'=>$model,
//					'attribute'=>'avatar_url',
//					'upload' => true,
//					'uploadButtonTitle' => 'Загрузить',
//					'width' => 200,
//					'height' => 200,
//					'capture' => false,
//
//				));
//				?>
<!--				<input type="hidden" name="old_image" id="old_image" value="--><?// echo $model->avatar_url ?><!--"/>-->

				<div id="avatarUpload" <? if($model->avatar_url != "") echo "style=\"background-image: url(".ImageService::performImageUrl($model->avatar_url, ImageType::AVATAR).");\""; ?> >
					<div class="bg-black">
						<div id="percent">0%</div>
						<a id="discard" href="">Отмена</a>
					</div>
				</div>
				<? echo $form->fileField($model, 'avatar_url', array("accept" => "image/*", "style" => "display:none;")); ?>
				<input type="button" name="avatarUploadButton" value="Загрузить"/>
				<input type="hidden" name="avatar" id="avatarHidden" value="<?=$model->avatar_url;?>"/>
				<input type="hidden" name="model_id" value="<?=$model->id; ?>"/>

			</div>

			<!-- Обо мне   -->
			<li><?php echo $form->label($model,'about'); ?> <?php echo $form->error($model,'about'); ?></li>
			<div>
				<div class="sub-title">Максимально подробно опишите инофрмацию о себе.</div>
				<?php echo $form->textArea($model,'about',array('size'=>60,'maxlength'=>1500, "id" => "desc-textarea", "lang" => 'ru', 'value' => str_replace("<br />","",$model->about))); ?>

				<span id="max-count" data-max-count="1500" data-for="data-textarea">1500</span>

			</div>

			<!-- E-mail   -->
			<li><?php echo $form->label($model,'email'); ?> <?php echo $form->error($model,'email'); ?></li>
			<div>
				<?php echo $form->emailField($model,'email',array('size'=>60,'maxlength'=>45, 'disabled' => !Yii::app()->user->getRole() == 1)); ?>
			</div>

			<!-- Город -->
			<?php if( $model->hasErrors("region_id") || $model->hasErrors("city_id") ) : $error = '<div class="'.$form->errorMessageCssClass.'"> Необходимо заполнить поле ' ?>
				<?php if( $model->hasErrors("region_id") ) $error .= $model->getError("region_id")." и поле "; ?>
				<?php if( $model->hasErrors("city_id") ) $error .= $model->getError("city_id"); ?>
				<li>Место жительства <? echo $error.".</div>" ?> </li>
			<?php else : ?>
				<li>Место жительства</li>
			<?php endif; ?>

			<div>
				<div class="sub-title">Выберите место жительства</div>

				<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион",
					"ajax" => array(
						'type' => 'post',
						'beforeSend' => 'function(){ $("#city-loading.loading").css("display", "block"); }',
						'url' => CController::createUrl("/core/ajaxGetChildrenСities"),
						"data" => array('id' => 'js:this.value'),
						'update' => '#User_city_id',
						'success' => 'function(data){ $("#User_city_id").children().remove();  $("#User_city_id").append($(data)); $("#city-loading.loading").css("display", "none"); }'
					)
					) ) ?>
				<div class="loading" id="region-loading">...</div>

				<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город") ) ?>
				<div class="loading" id="city-loading">...</div>


				<style>
					.loading {
						position: absolute;
						margin-left: 460px;
						margin-top: -30px;
						color: rgb(200,200,255);
						display: none;
					}

					#region-loading {
						margin-left: 300px;
					}

					#city-loading {
						margin-left: 495px;
					}
				</style>
			</div>


			<!-- Номер   -->
			<li>Номер телефона <?php echo $form->error($model,'phone'); ?></li>
			<div>
				<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>20, "disabled" => !Yii::app()->user->getRole() == 1, "value" => ($model->phone == "")? '375' : $model->phone)); ?>
			</div>

			<!-- website   -->
			<li><?php echo $form->label($model,'website'); ?> <?php echo $form->error($model,'website'); ?></li>
			<div>
				<?php echo $form->textField($model,'website',array('size'=>60,'maxlength'=>45)); ?>
			</div>


			<li>Сменить пароль</li>
			<a class="change-password" target="_blank" href="/profile/changePassword">Сменить пароль</a>

        </ol>



	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить', array("class" => "add-service save-button")); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->

</div>
</div>

<script src="/scripts/XMLHttpRequest.js"></script>
<script src="/scripts/gallerymanager.min.js"></script>
<script src="/scripts/service.js"></script>
<script src="/scripts/is.min.js"></script>
<script src="/scripts/avatarUploading.js"></script>

<script>

	$(".save-button").click(function(){

		if( $(this).val() == "Сохранить" )
			$(this).val("Сохранение... Пожалуйста, не закрывайте страницу");

		return true;
	});

    $("#User_url").keyup(function(e){
        if(e.keyCode == 32)
            replaceSpaces(this);
    });

    $("#User_url").change(function(){
        replaceSpaces(this);
    });

    var replaceSpaces = function(elem)
    {
        $(elem).val($(elem).val().replace(/\s+/g, "_"))
    }

</script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	#User_country_id {
		width:124px;
	}

	#User_region_id {
		width:200px;
	}

	#User_city_id {
		width:190px;
	}

	input[type='submit']:disabled {
		opacity: 0.4;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:35%;
	}

	.information table td+td {
		width:65%;
	}

	.error-title {
		position: absolute;
		width: 100%;
		margin-top: -35px;
		text-align: center;
		color: rgb(255,40,40);
		text-transform: uppercase;
	}

	a.change-password {
		display: inline-block;
		text-decoration: none;
		border: 1px solid #ff5c00;
		color: #ff5c00;
		padding: 7px 0 4px 0;
		width: 260px;
		text-align: center;
		text-transform: uppercase;
		margin-left: 30px;
		font-weight: 600;
	}

	a.change-password:hover {
		color: white;
		background-color: #ff5c00;
	}

	 .change-password-button{
		 display: block;
		 background-color: rgb(225,225,225) !important;
		 border: 0 !important;
		 width: 261px !important;
		 margin-top: 12px !important;
		 color:rgb(130,130,130) !important;
	 }

	.change-password-button:hover{
		color:rgb(170,170,170) !important;
	}

	.password-change-block {
		background-color: rgb(240,240,240);
		margin-top: 30px;
		box-sizing: border-box;
		padding: 30px 60px;
		display: inline-block;
	}

	.password-change-title {
		text-transform: uppercase;
		font-weight: 600;
		color: rgba(70,70,70,0.8);
		margin-bottom: 10px;
	}

	.password-change-block  input[type=password]:focus {
		border:1px solid rgba(0,0,0,0.35)
	}


</style>