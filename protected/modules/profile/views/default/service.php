<?php
/**
 * @var $this DefaultController
 * @var $user User
 * @var $model Service
 * @var $galleries Gallery[]
 * @var $form CActiveForm
 * @var $videos Video[]
 * @var $prices Price[]
 */

$priceService = new PriceService();


?>


<style>
    .form {
        width:100%;
    }

	[data-for="data-textarea"] {
		color: rgb(20,235,0);
		vertical-align: top;
		margin-left: 10px;
	}

	.add-service {
		width:100% !important;
		margin-top: 30px;
	}

	#gallery-widget {
		font-size: 0;
	}

	#gallery-list {
		display: inline-block;
	}

	.gallery-item {
		width:110px;
		height:110px;
		box-sizing: border-box;
		margin:4px 8px 4px 0;
		display: inline-block;
		border:1px solid rgba(0,0,0,0.2);
		background: no-repeat top center;
		background-size: cover;
	}

	.gallery-item a {
		font-size: 14px;
		position: absolute;
		width: 108px;
		height: 108px;
		border: 0 !important;
		box-sizing: border-box;
		text-align: center;
		line-height: 110px !important;
		background-color: rgba(0,0,0,0.2);
		color: white !important;
		text-shadow: 0px 0px 12px black;
	}

	#gallery-widget [name='load_gallery_pic'] {
		-webkit-appearance: none;
		vertical-align: top;
		border: 1px solid rgba(0,0,0,0.2);
		background-color: white;
		height: 110px;
		width: 110px;
		color: rgba(120,120,120,0.7);
		font-size: 12px;
		margin:4px 0;
	}

	#gallery-widget [name='load_gallery_pic']:disabled,
	#gallery-widget [name='load_gallery_pic']:disabled:hover {
		border: 1px solid rgba(0,0,0,0.1);
		color: rgba(120,120,120,0.4);
		cursor:wait;
	}

	#gallery-widget [name='load_gallery_pic']:hover {
		border: 1px solid rgba(0,0,0,0.5);
		color: rgba(70,70,70,0.9);
		cursor: pointer;

	}

	.delete-additional-price, .delete-city{
		-webkit-appearance: none;
		border: 1px solid rgba(0,0,0,0.2);
		background-color: white;
		width: 110px;
		font-size: 14px;
		color: rgba(120,120,120,0.7);
		text-decoration: none;
		padding: 11px 10px 12px 10px;
		margin-left: 27px;
	}

	.delete-additional-price:before, .delete-city:before {
		content: '';
		display: inline-block;
		height: 1px;
		border-top: 1px solid rgba(0,0,0,0.2);
		width: 31px;
		position: absolute;
		margin-left: -41px;
		margin-top: 18px;
	}

	.add-button[disabled] {
		opacity: 1;
	}

	.add-button[disabled]:hover {
		border: 1px solid rgba(0,150,0,0.4);
		color: rgba(0,150,0,0.6);
		cursor: auto;
	}

	.add-button {
		-webkit-appearance: none;
		border: 1px solid rgba(0,150,0,0.4);
		background-color: white;
		width: 300px;
		display: block;
		margin: 10px 0;
		position: relative;
		text-align: center;
		font-size: 14px;
		color: rgba(0,150,0,0.6);
		text-decoration: none;
		padding: 11px 10px 12px 10px;
	}

	.delete-additional-price:hover {
		border: 1px solid rgba(0,0,0,0.5);
		color: rgba(70,70,70,0.9);
		cursor: pointer;
	}

	.add-button:hover {
		border: 1px solid rgba(0,145,0,0.8);
		color: rgba(0,145,0,1);
		cursor: pointer;
	}

	.additional-price-item {
		margin-bottom: 15px;
	}

	#video-list input {
		width:400px;
		display: block;
		margin-bottom: 5px;
	}

	#job-region, #price-radio {
		width:100%;
	}

	#job-region td+td:not(.checked), #price-radio td+td:not(.checked) {
		opacity: 0.5;
	}

</style>

<script>
	var isErrors = <? echo ($model->hasErrors())? "true" : "false"; ?>;
</script>

<?php if($model->hasErrors()) : ?>
	<div class="flash-container">
		<div class="failed-flash">Заполните все неоходимые поля</div>
		<script>
			setTimeout(function(){
				$(".failed-flash").animate({opacity : 0}, 600, function(){
					$(".failed-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<h1 class="main-h1"><? echo $user->username; ?></h1>

<div id="content" class="container960">
<div class="col-span-3">
	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => null,
		"model" => $model->author
	)); ?>
</div><div class="col-span-9">

	<?php if( $model->isNewRecord || $model->author_id == Yii::app()->user->id) : ?>

			<div class="form">

				<?php $additionalPrice = new Price(); ?>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'service-form',
				'enableAjaxValidation'=>false,
				'errorMessageCssClass' => "add-service-error-message",
				"htmlOptions" => array(
					"encoding" => "multipart/form-data",
				)
			)); ?>

				<ol class="params-list" style="margin-top:0;">

					<div class="row buttons">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить услугу' : 'Сохранить', array("class" => "add-service add-service-button", 'id' => '', 'style'=>'margin-top:0;')); ?>
					</div>

					<!-- Основная Категория и Подкатегория-->
					<?php if( $model->hasErrors("category_id") || $model->hasErrors("sub_category_id") ) : $error = '<div class="'.$form->errorMessageCssClass.'"> Необходимо заполнить поле ' ?>
						<?php if( $model->hasErrors("category_id") ) $error .= $model->getError("category_id")." и поле " ?>
						<?php if( $model->hasErrors("sub_category_id") ) $error .= $model->getError("sub_category_id"); ?>
						<li>Категория и подкатегория услуги <? echo $error."."; echo "<a class='error-anchor'></a>"; ?> </li>
					<?php else : ?>
						<li>Категория и подкатегория услуги</li>
					<?php endif; ?>
					<div>
						<div class="sub-title">Выберите категорию и подкатегорию, в которые входит предоставляемая Вами услуга.</div>
						<?php echo $form->dropDownList($model, 'category_id', Category::getAllParentCategories(array(1)), array( "prompt" => 'Выберите категорию',
							"ajax" => array(
								'type' => 'post',
								'beforeSend' => 'function(){ $(".loading").css("display", "block"); }',
								'url' => CController::createUrl("/core/ajaxGetChildrenCategories"),
								"data" => array('id' => 'js:this.value'),
								'update' => '#Service_sub_category_id',
								'success' => 'function(data){ $("#Service_sub_category_id").children().remove();  $("#Service_sub_category_id").append($(data)); $(".loading").css("display", "none"); }'
							)
						)) ?>
						<?php echo $form->dropDownList($model, 'sub_category_id', Category::getAllChildrenCategories($model->category_id), array("prompt" => "Выберите подкатегорию",
							"ajax" => array(
								'type' => 'post',
								'url' => CController::createUrl('/filter/getTable'),
								'data' => array('category_id' => 'js:$("#Service_category_id option:selected").val()', 'sub_category_id' => 'js:$("#Service_sub_category_id option:selected").val()'),
								'success' => 'function(data){ $(".filters").children().remove(); $(".filters").append(data); }'
							)
						) ) ?>
						<div class="loading">...</div>

						<style>
							.loading {
								position: absolute;
								margin-left: 460px;
								margin-top: -30px;
								color: rgb(200,200,255);
								display: none;
							}

							.filters table td:first-child {
								width:40%;
							}

						</style>
					</div>

					<div class="filters">
						<?
						$attributes = FilterService::performInputsInTable($model->category_id, $model->sub_category_id, $model);

						if(count($attributes) > 0) {

							$result = "<li>Дополнительные параметры</li><div><table>";

							foreach ($attributes as $title => $input) {
								$result .= "<tr>";

								$result .= "<td>" . $title . "</td>";
								$result .= "<td>" . $input . "</td>";

								$result .= "</tr>";
							}

							$result .= "</table></div>";
							echo $result;
						}
						?>
					</div>

					<li>Название услуги <?php echo $form->error($model, 'title'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<div class="sub-title">Не используйте в заголовке услуги названия вашей категории или подкатегории.</div>
						<? echo $form->textField($model, 'title'); ?>
					</div>


					<li><?php echo $form->label($model,'avatar_url'); ?> <?php echo $form->error($model,'avatar_url'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<div class="sub-title">Выберите главную фотографию. Для корректного отображения используйте квадратное изображение. Максимально допустимый размер - <b>5 Мб</b></div>

						<div id="avatarUpload" <? if($model->avatar_url != "") echo "style=\"background-image: url(/images/avatars/{$model->avatar_url});\""; ?> >
							<div class="bg-black">
								<div id="percent">0%</div>
								<a id="discard" href="">Отмена</a>
							</div>
						</div>
							<? echo $form->fileField($model, 'avatar_url', array("accept" => "image/*", "style" => "display:none;")); ?>
						<input type="button" name="avatarUploadButton" value="Загрузить"/>
						<input type="hidden" name="avatar" id="avatarHidden" value="<?=$model->avatar_url;?>"/>


					</div>

					<!-- Описание Услуги   -->
					<li><?php echo $form->label($model,'description'); ?> <?php echo $form->error($model,'description'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<div class="sub-title">Недостаток информации уменьшает интерес и расположение клиента к вашим услугам. Напишите об услуге более подробно - это сделает вашу страницу привлекательнее для клиентов.</div>
						<?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>1500, "id" => "desc-textarea", 'value' => str_replace("<br />","",$model->description))); ?>


						<span id="max-count" data-max-count="1500" data-for="data-textarea">1500</span>

					</div>



					<!-- РЕГИОН РАБОТЫ	-->
		<!--			<li>Регион работы --><?// if(Yii::app()->user->hasFlash('city')) : ?><!-- <div class="add-service-error-message">--><?// echo Yii::app()->user->getFlash('city'); ?><!--</div>	--><?// endif; ?><!--</li>-->
					<li>Регион работы <? echo $form->error($model, 'city'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<table id="job-region">

							<tr>

								<td >
									<div class="city-list">
										Отдельные города

											<? if(count($model->city) == 0) : ?>
												<div class="city">
													<?php echo $form->dropDownList($model, '[Job_Region]region_id[]', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'style' => 'width:160px', 'class' => 'item-region') ) ?>
													<div class="loading" id="region-loading">...</div>

													<?php echo $form->dropDownList($model, '[Job_Region][city][0]city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'style' => 'width:140px', 'class' => 'item-city') ) ?>
													<div class="loading" id="city-loading">...</div>
												</div>

											<? else : ?>

												<? foreach($model->city as $index => $city) : ?>

													<div class="city">

														<?php echo $form->dropDownList($city->region, '[Job_Region]id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", "name"=>'Service[Job_Region][region_id]', 'style' => 'width:160px', 'class' => 'item-region') ) ?>
														<div class="loading" id="region-loading">...</div>

														<?php echo $form->dropDownList($city, '[Job_Region][city]['.$index.']id', City::getCityListByRegionId($city->region->id), array("name" => 'Service[Job_Region][city]['.$index.'][city_id]', 'style' => 'width:140px', 'class' => 'item-city') ) ?>
														<? if($index != 0) : ?><a class="delete-city" data-index="1" href="" data-record-id="<? echo $city->id; ?>">Удалить</a><?php endif;?>
														<div class="loading" id="city-loading">...</div>

													</div>

												<? endforeach ?>

											<?php endif; ?>

									</div>
<!--									<input type="checkbox" name="Service[Job_Region][isWorkingAround]" data-scenario-name="sc_addCountry" /> Работаю на выезд-->
									<?php echo $form->checkBox($model, 'is_working_around', array("data-scenario-name" => 'sc_addCountry'))?> Работаю на выезд
									<input type="button" id="add-additional-city" class="add-button" value="Добавить город"  >
								</td>


							</tr>
							<input type="hidden" name="scenario" value="sc_addCountry" />
						</table>
					</div>



					<style>

						.city {
							margin-bottom: 10px;
						}

						#job-region td, #price-radio td {
							vertical-align: top;
							padding-bottom: 15px;
						}

						#job-region td+td, #price-radio td+td {
							box-sizing: border-box;
							padding-left: 15px;
						}

					</style>



					<!-- ЦЕНА   -->
					<li>Основная цена <?php echo $form->error($model, 'price_value')?> <?echo $form->error($model, 'price_type'); echo "<a class='error-anchor'></a>";?></li>
					<div>
						<div class="sub-title">Цена будет использоваться в каталоге при фильтрации. Стоимость будет рассчитываться исходя из из курса <a class="external-link" href="http://12.com">НБРБ</a></div>
						<table id="price-radio">
							<tr>
								<td><input type="radio" name="price-radio" value="1" <? if($price_type == 1) echo 'checked' ?>/></td>
								<td <? if($price_type == 1) echo 'class="checked"' ?>>Фиксированная цена:<br>
									<?php echo $form->dropDownList($model,'currency_id', Currencies::getAllCurrencies(), array("disabled" => $price_type != 1)); ?>
									<?php echo $form->textField($model,'price_value', array("placeholder" => "Цена"), array("disabled" => $price_type != 1, "value" => ($model->price_value == 0 || $model->price_value == null)? "" : $model->price_value)); ?>
									<?php echo $form->dropDownList($model,'price_type', $priceService->getPriceTypes(true), array('prompt' => 'Выберите тип цены', "disabled" => $price_type != 1)) ?>
								</td>
							</tr>
							<tr>
								<td><input type="radio" name="price-radio" value="2" <? if($price_type == 2) echo 'checked' ?>/></td>
								<td <? if($price_type == 2) echo 'class="checked"' ?>>Цена от и до:<br>
									<?php echo $form->dropDownList($model,'currency_id', Currencies::getAllCurrencies(), array("disabled" => $price_type != 2)); ?>
									<?php echo $form->textField($model,'min_price_value', array("placeholder" => "Минимальная цена", "style" => "width:120px", "disabled" => $price_type != 2, "value" => ($model->min_price_value == 0 || $model->min_price_value == null)? "" : $model->min_price_value)); ?>
									<?php echo $form->textField($model,'max_price_value', array("placeholder" => "Максимальная цена", "style" => "width:120px", "class" => "max-price-input", "disabled" => $price_type != 2, "value" => ($model->max_price_value == 0 || $model->max_price_value == null)? "" : $model->max_price_value)); ?><span class="before-max-price-line"></span>
									<?php echo $form->dropDownList($model,'price_type', $priceService->getPriceTypes(true), array('prompt' => 'Выберите тип цены', "disabled" => $price_type != 2)) ?>
								</td>
							</tr>
						</table>
						<input type="hidden" name="price_type" value="<? echo $price_type; ?>"/>
					</div>

					<style>

						.max-price-input {
							margin-left: 36px;
						}

						.before-max-price-line:before {
							content: '';
							display: inline-block;
							height: 1px;
							border-top: 1px solid rgba(0,0,0,0.2);
							width: 40px;
							position: absolute;
							margin-left: -182px;
							margin-top: 18px;
						}

						#price-radio td{
							vertical-align: top;
						}

						#price-radio td+td {
							padding-left: 19px;
						}

					</style>


					<li>Пакеты услуги </li>
					<div>
						<div class="sub-title">Для удобства клиентов добавьте пакеты услуги. Стоимость будет рассчитываться исходя из из курса <a class="external-link" href="http://12.com">НБРБ</a></div>

						<div id="additional-price-items-list">
						<?php foreach($prices as $i => $price) : ?>
							<div class="additional-price-item" data-index="<? echo $i;?>">
								<?php echo $form->textField($price,'['.$i.']description', array("placeholder" => "Название пакета", "class" => 'price_description', 'data-index' => $i)); ?>
								<?php echo $form->dropDownList($price,'['.$i.']currency_id', Currencies::getAllCurrencies(), array( "class" => 'price_currency', 'data-index' => $i )); ?>
								<?php echo $form->textField($price,'['.$i.']price_value', array("placeholder" => "Цена", 'style' => "width:85px !important;", "class" => 'price_value', 'data-index' => $i)); ?>
								<?php echo $form->dropDownList($price,'['.$i.']price_type', $priceService->getPriceTypes(true), array( "class" => 'price_type', 'data-index' => $i )); ?>
								<?php echo $form->hiddenField($price,'['.$i.']id')?>
								<a class="delete-additional-price" data-index="<? echo $price->id;?>" href="">Удалить</a>
							</div>

						<?php endforeach; ?>
						</div>

						<a id="add-additional-price" class="add-button" href="">Добавить пакет</a>

					</div>

					<li>Фотографии галереи</li>
					<div>

						<div class="sub-title">Выберите фотографии для галереи. Максимально допустимый размер - <b>2 Мб</b></div>

						<div id="gallery-widget">

							<div id="images-count">Осталось изображений: <span></span></div>
							<div id="gallery-list">


								<?php foreach($galleries as $gallery) : ?>
									<div class="gallery-item" style="background-image: url(<? echo ImageService::performImageUrl($gallery->image_url, ImageType::GALLERY_IMAGE) ?>)">
										<a href="#" name="delete" id="<? echo $gallery->id?>">Удалить</a>
									</div>
								<?php endforeach; ?>

								<input type="button" name="load_gallery_pic" value="+ Добавить фото" />
								<input type="file" name="gallery_file[]" accept="image/jpeg,image/png" style="display: none;" multiple/>

								<input type="hidden" name="model_id" value="<? echo $model->id ?>" />

							</div>

						</div>

					</div>

					<li>Видео</li>
					<div>
						<div class="sub-title">Добавьте ссылки на видео из Youtube или Vimeo</div>

						<div id="video-widget">

							<div id="video-list">

								<? if($model->isNewRecord) : ?>
									<input class="video-item" type="text" name="Video[]" value="" />
								<? else :
									$count = 0;
									foreach($model->video as $video) {
										echo $form->textField($video, 'video_link', array("name" => "Video[]", 'class' => 'video-item', "value" => $video->video_link));
										$count++;
									}
									if($count < 10) echo '<input class="video-item" type="text" name="Video[]" value="" />';
								   endif;
								?>
							</div>

						</div>

					</div>

				</ol>



			<div class="row buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить услугу' : 'Сохранить', array("class" => "add-service add-service-button", 'id' => '')); ?>
			</div>

			<?php $this->endWidget(); ?>

	<? else : ?>

			<h1>Ошибка прав доступа</h1>

	<? endif; ?>

</div><!-- form -->

</div>
</div>

<script src="/scripts/XMLHttpRequest.js"></script>
<script src="/scripts/service.js"></script>
<script src="/scripts/gallerymanager.js"></script>
<script src="/scripts/is.min.js"></script>
<script src="/scripts/avatarUploading.js"></script>


<script>

    var extractDomain = function (url) {
        var domain;
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];

        return domain;
    };

    var domainsArray = ["www.youtube.com", "youtube.com", "www.vimeo.com",
                        "vimeo.com", "www.youtu.be", "youtu.be"];

	$('body').on('change', ".video-item", function(){

        var domain = extractDomain($(this).val());

        if(domainsArray.indexOf(domain) == -1) {
            $(this).val("");
        }

		var notNullItemsCount = 0;
		var inputsCount = $(".video-item").length;
		$(".video-item").each(function(index, item){
			if($(item).val() != "") notNullItemsCount++;
		});

		if(notNullItemsCount == inputsCount  && inputsCount < 10){
			var input = $('<input type="text" class="video-item" name="Video[]" >');
			$("#video-list").append(input);
		}

		if(notNullItemsCount < inputsCount){
			$(".video-item").each(function(index, item){
				if($(item).val() == "") $(item).remove();
			});
			var input = $('<input type="text" class="video-item" name="Video[]" >');
			$("#video-list").append(input);
		}

	});

	<? if($model->isNewRecord) :  ?>
		$(document).on("click", ".delete-additional-price", function(){
			var index = $(this).attr("data-index");
			$(this).parent().remove();
			return false;
		});
	<? else : ?>

		$(document).on("click", ".delete-additional-price", function(){
			var index = $(this).attr("data-index");
			var deletebutton = this;
			if(typeof (index) != 'undefined') {
				var xhr = new XMLHttpRequest();
				var params = 'id='+index;
				xhr.open('POST', '/service/edit/deleteprice', true);

				xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

				xhr.onload = function () {
					console.log(true);
					$(deletebutton).parent().remove();
				};

				xhr.send(params);
			}else
				$(this).parent().remove();
			return false;
		});

	<? endif; ?>

</script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.error-title {
		position: absolute;
		width: 100%;
		margin-top: -35px;
		text-align: center;
		color: rgb(255,40,40);
		text-transform: uppercase;
	}

	#images-count {
		opacity: 0;
		font-size: 12px;
		color:rgb(90,90,90);
	}

	input[type='submit']:disabled {
		opacity: 0.4;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:50%;
	}

	.information table td+td {
		width:50%;
	}

	.abort_image_loading {
		font-size: 14px;
		color:black;
		position: absolute;
		width:110px;
		height:110px;
		line-height:110px;
		text-align: center;
	}

	.abort_image_loading:hover {
		cursor: pointer;
	}

	.percent {
		position: absolute;
		left: 0;
		right: 0;
		margin-top: 31px;
	}

</style>

<?php Yii::app()->clientScript->registerScript(
	"changeRadio", '
				$(".add-service-button").click(function(){
					if($("input[name=\'old_image\']").val() != "" && $("input[name=\'old_image\']").val() != "true" ) {
//						$("input[name=\'Service[avatar_url]\']").prop("disabled", true);
					}
					if( $(this).val() == "Добавить услугу" )
						$(".add-service-button").val("Добавление услуги... Пожалуйста, не закрывайте страницу");
					else if( $(this).val() == "Сохранить" )
						$(".add-service-button").val("Сохранение... Пожалуйста, не закрывайте страницу");

					return true;
				});

				$("#price-radio input[type=radio]").change(function(){
					$("#price-radio .checked input, #price-radio .checked select").attr("disabled", "disabled");
					$("#price-radio .checked").removeClass("checked");
					$(this).parent().next().addClass("checked");
					$("#price-radio .checked input, #price-radio .checked select").removeAttr("disabled");
				});

				$("#job-region #add-additional-city").click(function(){
					var addCityCount = $(".city").length;

					var cityBlock = $("<div class=\'city\'></div>");
					var region = $(\'<select style="width:160px" class="item-region" name="Service[Job_Region][region_id]" id="Service_Job_Region_3_\'+addCityCount+\'_region_id">'.Region::getRegionListToOptions(true).'</select>\');
					var city = $(\'<select style="width:140px" class="item-city" name="Service[Job_Region][city][\'+addCityCount+\'][city_id]" id="Service_Job_Region_3_\'+addCityCount+\'_city_id"><option value="">Выберите город</option></select>\');
					var deleteButton = $(\'<a class="delete-city" href="">Удалить</a>\');

					cityBlock.append(region, " ", city, " ", deleteButton);
					$(".city-list").append(cityBlock);
				});

				$("input[type=\'submit\']").click(function(){
					$(".item-city").each(function(){
						if($(this).val() == "") $(this).prop("disabled", true);
					});
				});

				$("body,html").on("change", "#job-region .city .item-country", function(){
					var value = $(this).val();

					var xhr = new XMLHttpRequest();
					var params = "id="+value;
					var obj = this;
					xhr.open("POST", "'.CController::createUrl("/core/ajaxGetChildrenRegions").'", true);

					xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

					xhr.onload = function () {
						$(obj).parent().children(".item-region").children().remove();
						$(obj).parent().children(".item-region").append(xhr.response);
					};

					xhr.send(params);

				});

				$("body,html").on("change", "#job-region .city .item-region", function(){
					var value = $(this).val();

					var xhr = new XMLHttpRequest();
					var params = "id="+value;
					var obj = this;

					xhr.open("POST", "'.CController::createUrl("/core/ajaxGetChildrenСities").'", true);

					xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

					xhr.onload = function () {
						$(obj).parent().children(".item-city").children().remove();
						$(obj).parent().children(".item-city").append(xhr.response);
					};

					xhr.send(params);

				});



				$("#add-additional-price").click(function(){
					var addPricesCount = $(".additional-price-item").length;
					var add_price_item = $(\'<div class="additional-price-item" data-index="\'+addPricesCount+\'"></div>\');
					var desc = $("<input placeholder=\'Название пакета\' class=\'price_description\' data-index=\'"+addPricesCount+"\' name=\'Price["+addPricesCount+"][description]\' id=\'Price_"+addPricesCount+"_description\' type=\'text\' maxlength=\'45\'>");
					var currency = $(\'<select class="price_currency" data-index="\'+addPricesCount+\'" name="Price[\'+addPricesCount+\'][currency_id]" id="Price_\'+addPricesCount+\'_currency_id">'.Currencies::optionsInHtml().'</select>\');
					var value = $(\'<input placeholder="Цена" class="price_value" data-index="\'+addPricesCount+\'" name="Price[\'+addPricesCount+\'][price_value]" id="Price_\'+addPricesCount+\'_price_value" type="text">\');
					var type = $(\'<select class="price_type" data-index="\'+addPricesCount+\'" name="Price[\'+addPricesCount+\'][price_type]" id="Price_\'+addPricesCount+\'_price_type">'.$priceService->optionsInHtml(true).'</select>\');
					var deleteButton = $(\'<a class="delete-additional-price" href="">Удалить</a>\');

					add_price_item.append(desc, " ", currency, " ", value, " ", type, " ", deleteButton);


					$("#additional-price-items-list").append(add_price_item);
					return false;
            	});

				$(document).on("click", ".delete-city", function(){
					var index = $(this).attr("data-record-id");
					var deletebutton = this;

					if(typeof(index) != "undefined") {
						var xhr = new XMLHttpRequest();
						var params = "id="+index+"&service_id='.$id.'";
						xhr.open("POST", "/service/edit/deletecity", true);

						xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

						xhr.onload = function () {
							if(xhr.response == "true")
								$(deletebutton).parent().remove();
						};

						xhr.send(params);
					}else{
						$(this).parent().remove();
					}
					return false;
				});

			$(document).on("mouseenter", ".abort_image_loading", function() {
				$(this).html(\'Отмена\');
			});

			$(document).on("mouseleave", ".abort_image_loading", function() {
				$(this).html(\'<img src="/images/icons/loading_spinner.gif" width="50" height="50" alt=""/><div class="percent"></div>\');
			});

//			$(document).ready(function(){
//				if(isErrors){
//					var firstErrorElement = $($(".add-service-error-message")[0]);
//					var anchor = firstErrorElement.parent("li").children($("a.error-anchor"));
//					window.scrollBy(0, anchor.offset().top - 80);
//					alert(2);
//				}
//			});

        ', CClientScript::POS_READY
);?>

