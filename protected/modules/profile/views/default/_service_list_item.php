<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 30.09.15
 * Time: 15:17
 * @var $model Service
 */

    $priceService = new PriceService();

//    $additionalUrl = (CheckPage::isPages(array("site/catalog", "site/catalogMini")))? "?returnUrl=".urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") : "";

?>

<div class="service">



    <div class="manage-button-list">
        <?php if(Yii::app()->user->id == $model->author_id && !CheckPage::isPages(array('site/catalog', 'site/catalogMini'))) : ?>
            <a class="manage-button edit" data-alt="Редактировать услугу" href="<? echo UrlService::performEditServiceUrl($model) ?>"></a>
            <a class="manage-button trash" data-alt="Удалить услугу" href="<? echo UrlService::performDeleteServiceUrl($model) ?>"></a>
        <?php endif; ?>
        <? if(in_array(Yii::app()->user->id, array(6,7,8,12))) : ?>
            <a class="manage-button catalog-add-holiday-button"
               data-service-id="<? echo $model->id ?>"
               data-service-author-id="<? echo $model->author_id ?>"
               data-holiday-id="<? echo (!SessionService::isEmpty("holiday_id"))? SessionService::get("holiday_id") : "false"; ?>"
               data-alt="Добавить услугу в праздник"
               href="<? echo (!SessionService::isEmpty("holiday_id"))? "/profile/holiday/view/".SessionService::get("holiday_id") : "/" ?>"></a>
        <? endif; ?>
    </div>



    <? //TODO :  performServiceUrl($model).$additionalUrl ?>
    <a class="avatar" href="<? echo Yii::app()->request->baseUrl.UrlService::performServiceUrl($model) ?>"><? echo ImageService::appendImage($model->avatar_url, ImageType::AVATAR, 210, 210)?></a>
    <div class="info">
        <img class="category-icon" src="<? echo CategoryService::getCategoryIcon($model); ?>" alt="<? echo $model->category->name ?>"/>
        <h2><a class="h2" href="<? echo Yii::app()->request->baseUrl.UrlService::performServiceUrl($model) ?>"><? echo DescriptionService::performUsername($model->title); ?></a></h2>
        <h3><? echo $model->subCategory->name; ?></h3>
        <div class="all-info">
            <div class="desc"><?php echo DescriptionService::performText($model->description); ?></div>

            <div class="region-and-comments-block">
                <a class="region-button" href="" onclick="javascript: return false;">Регион работы</a>
                <div class="regions">
                    <? echo RegionService::performRegionAddressForService($model, true); ?>
                </div>

            </div>

        </div>

    </div>
    <div class="price-block-new">
        <div class="price-for-service">Цена за услугу</div>
        <div class="price-in-byr">
            <? if(!empty($model->min_price_value) || !empty($model->max_price_value) ) : ?>
                <? echo $priceService->performPricesFromTo($model) ?>
            <? else : ?>
                <? echo $priceService->convertToCurrency($model) ?>
            <? endif; ?>
        </div>
        <div class="price-in-usd">
            <? if(!empty($model->min_price_value) || !empty($model->max_price_value) ) : ?>
                <? echo $priceService->performPricesFromTo($model, "USD") ?>
            <? else : ?>
                <? echo $priceService->convertToCurrency($model, "USD") ?>
            <? endif; ?>
        </div>
    </div>



</div>