<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $galleryItem Gallery
 * @var calendarDay CalendarDay
 * @var array $services services sorted by category
 * @var Service $service
 * @var Weekend[] $weekends
 */

?>
<?php if(Yii::app()->user->hasFlash('success')) : ?>
	<div class="flash-container">
	<div class="success-flash"><? echo Yii::app()->user->getFlash('success')?></div>
		<script>
			setTimeout(function(){
				$(".success-flash").animate({opacity : 0}, 600, function(){
					$(".success-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('failed')) : ?>
    <div class="flash-container">
        <div class="failed-flash"><? echo Yii::app()->user->getFlash('failed')?></div>
        <script>
            setTimeout(function(){
                $(".failed-flash").animate({opacity : 0}, 600, function(){
                    $(".failed-flash").css("display", "none");
                    $('.flash-container').remove();
                })
            }, 4000)
        </script>
    </div>
<?php endif; ?>

<h1 class="main-h1"><? echo $model->username ?></h1>

<div class="confirm-block-wrapper"></div>
<div class="weekends-management-block">
	<div id="weekend-info-block">Добавить день недели как выходной на месяц/год (<span class="year"></span>)</div>

	<form action="saveWeekend" method="post">
		<select name="day_number" id="">
			<option value="1">Понедельник</option>
			<option value="2">Вторник</option>
			<option value="3">Среда</option>
			<option value="4">Четверг</option>
			<option value="5">Пятница</option>
			<option value="6">Суббота</option>
			<option value="7">Воскресенье</option>
		</select>
		<select name="month" id="">
			<option value="0">Весь год</option>
			<option value="1">Январь</option>
			<option value="2">Февраль</option>
			<option value="3">Март</option>
			<option value="4">Апрель</option>
			<option value="5">Май</option>
			<option value="6">Июнь</option>
			<option value="7">Июль</option>
			<option value="8">Август</option>
			<option value="9">Сентябрь</option>
			<option value="10">Октябрь</option>
			<option value="11">Ноябрь</option>
			<option value="12">Декабрь</option>
		</select>
		<input type="hidden" name="year" value=""/>
		<input type="submit" value="Подтвердить"/>
	</form>
	<br/>

	<div class="padding-block">

		<div class="weekends-list">

			<? $weekends = Weekend::model()->findAll("author_id=:id ORDER BY year ASC, day_number ASC, month_number ASC", array(":id" => Yii::app()->user->id));

			$weekends_titled_array = DateService::performWeekendDays($model, date("Y"), false);

			$iter = -1;

			if(!empty($weekends_titled_array)) : ?>
				<table>
					<? foreach($weekends_titled_array as $weekendDay => $array) : ?>
					<tr>
						<td> <?=$weekendDay;?></td>
						<td>
							<? foreach($array as $month) : ?>
								<? $iter++; ?>
								<?=$month;?><form class="inline-form" action="deleteWeekend" method="post">
								<input type="hidden" name="month_number" value="<?=$weekends[$iter]->month_number?>"/>
								<input type="hidden" name="day_number" value="<?=$weekends[$iter]->day_number?>"/>
								<input type="hidden" name="year" value="<?=$weekends[$iter]->year;?>"/>
								<input type="submit" name="submit" value="&#10005;"/></form>

							<?  endforeach; ?>
						</td>
						<td>
							<form class="inline-form" action="deleteWeekend" method="post">
								<input type="hidden" name="month_number" value="all"/>
								<input type="hidden" name="day_number" value="<?=$weekends[$iter]->day_number?>"/>
								<input type="hidden" name="year" value="<?=$weekends[$iter]->year;?>"/>
								<input type="submit" name="submit" value="&#10005;"/>
							</form></td>

						<? endforeach; ?>
					</tr>
				</table>
			<? endif; ?>


		</div>
	</div>





	<div class="padding-block">
		<div id="day-state">

			<div class="day-state-title">Состояние дня и его услуг (<span id="active-day-string"></span>)</div>

			<div class="big-red-button" id="day-state-selector" data-value=""></div>

		</div>

	</div>

</div>

<div class="confirm-block-wrapper"></div>
<div class="confirm-block animated ">
	<div class="confirm-title">Вы действительно хотите сделать день выходным?</div>
	<div class="confirm-sub-title">Все заказы на этот день будут удалены</div>

	<a id="yes" href="/">Да</a><a id="no" href="/">Нет</a>
</div>

<div id="content" class="container960">
<div class="col-span-3">

	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => "calendar",
		"model" => $model
	)); ?>

</div><div class="col-span-9">

	<!--	<div class="black-title">Выбирайте дни и отмечайте состояние услуг</div>-->

	<div id="calendar-container">
		<div id="calendar-year">
			<a class="button left disabled" for="year" href=""><img src="/images/arrow_black_left.svg" alt="hhs.by"/></a>
			<span id="year"></span>
			<a class="button right" for="year" href=""><img src="/images/arrow_black_right.svg" alt="hhs.by"/></a>
		</div>
		<div id="change-months-container">
			<a class="button left disabled" href="" for="months"><img src="/images/arrow_black_left.svg" alt="hhs.by"/></a>
			<a class="button right" href="" for="months"><img src="/images/arrow_black_right.svg" alt="hhs.by"/></a>
		</div>
		<div class="calendar-container"></div>
		<div id="explication">
<!--			<div id="request" class="block"><div class="explication-circle"></div> - Заявка</div>-->
			<div id="reserve" class="block"><div class="explication-circle"></div> - Заказанная услуга</div>
			<div id="weekend" class="block"><span>21</span>/<span>21</span> - Выходной день</div>
		</div>
	</div>


		<div class="calendar-settings">
			<div class="day-and-month-year">
				<div class="calendar-title">День</div>
				<div id="day-state-work" class="day-state-button" data-value="0">Рабочий</div>
				<div id="day-state-weekend" class="day-state-button" data-value="1">Выходной</div>

				<div class="calendar-title" style="margin-top: 20px">Месяц / Год</div>
				<a href="" class="weekends-button">Отметить выходные</a>

			</div>
			<div id="services">
				<div class="calendar-title">Услуги</div>
					<div id="services-container">

						<div id="services-list">


							<?	foreach($services as $key => $value) : ?>

<!--								<div class="category-title">--><?// echo $key ?><!--</div>-->

								<?	foreach($services[$key] as $service) : ?>
<!--									<div class="service" id="service_--><?// echo $service->id;?><!--">-->
<!--										--><?// echo ImageService::appendImage($service->avatar_url, ImageType::AVATAR, 210, 210, array("class" => "avatar"))?>
<!--										<div class="content">-->
<!--											<div class="sub-category">--><?// echo $service->subCategory->name; ?><!--</div>-->
<!--											<a href="" class="state-button reserved-button" data-state-type="reserved" data-state="0" data-service-id="--><?//=$service->id?><!--">Заказанная</a>-->
<!--											<a href="" class="state-button request-button" data-state-type="request" data-state="0" data-service-id="--><?//=$service->id?><!--	"></a>-->
<!--										</div>-->
<!--									</div>-->

							<div class="service" id="service_<? echo $service->id;?>" style="background-image: url(/images/avatars/<? echo $service->avatar_url ?>)">
							<div class="black-bckgr">
								<div class="title"><? echo $service->title ?></div>
								<div class="sub-title"><? echo $service->subCategory->name ?></div>
							</div>
							<a href="" class="state-button reserved-button" data-state-type="reserved" data-state="0" data-service-id="<?=$service->id?>">Заказанная</a>
							</div>

								<? endforeach; ?>

							<? endforeach; ?>



						</div>


					</div>
			</div>
		</div>




</div>
</div>

<style>

	#explication {
		display: none;
	}

	.calendar-settings {
		font-size: 0;
		box-sizing: border-box;
		padding: 0 40px;
	}

	.day-state-button {
		background-color: rgb(120,120,120);
		text-align: center;
		margin: 10px 0;
		padding: 7px 0 3px 0;
		text-transform: uppercase;
		color: white;
		font-size: 15px;
	}

	.day-state-button:hover {
		cursor: pointer;
	}

	.day-state-button.active {
		background-color: #ff5c00;
	}

	.calendar-title {
		color: rgba(0,0,0,0.8);
		box-sizing: border-box;
		padding: 10px 0px 1px 0px;
		margin: 0;
		font-size: 18px;
		font-weight: bold;
		border-bottom: 1px solid rgb(120,120,120);
		text-transform: uppercase;
	}

	.day-and-month-year, #services {
		display: inline-block;
		vertical-align: top;
		box-sizing: border-box;
		font-size: 16px;
	}

	.day-and-month-year {
		width:35%;
		padding-right: 15px;

	}

	#services {
		width:65%;
		padding-left: 15px;
	}

	.avatar {
		height:100px !important;
		width:100px !important;
	}

	.sub-title {
		color: rgba(140,140,140, 0.8);
		font-weight: 100;
		font-size: 12px;
		margin: 2px 0;
		font-style: italic;
		letter-spacing: 0.15px;
		box-sizing: border-box;
		padding: 0 30px;
	}

	#weekend-info-block, .day-state-title {
		padding: 10px 10px 8px 10px;
	}

	.service {
		height:100px;
		background: no-repeat center center;
		background-size: cover;
	}

	#services-list {
		text-align: center;
	}

	.black-bckgr {
		position: absolute;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
	}

	.black-bckgr .title {
		color: white;
		font-size: 17px;
		margin-top: 35px;
		text-transform: uppercase;
	}

	.black-bckgr .sub-title {
		font-size: 14px;
		text-align: center;
		font-weight: 100;
		font-style: italic;
		color: rgb(240,240,240);
	}

	a.weekends-button {
		display: block;
		text-decoration: none;
		border: 1px solid #ff5c00;
		text-align: center;
		color: #ff5c00;
		padding: 6px 0 2px 0;
		text-transform: uppercase;
		margin-top: 10px;
		font-size: 14px;
	}

	.confirm-block-wrapper {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(255,255,255,0.65);
		box-sizing: border-box;
		z-index: 2300;
		display: none;
	}

	.weekends-management-block {
		z-index: 2400;
		position: fixed;
		left: 50%;
		margin-left: -255px;
		/*width: 510px;*/
		/*height: 125px;*/
		text-align: center;
		margin-top: -80px;
		top: 50%;
		box-sizing: border-box;
		display: none;
		background-color: white;
		padding-bottom: 12px;
		box-shadow: 0px 3px 10px rgba(0,0,0,0.2);
	}

	.confirm-title {
		font-weight: bold;
		font-size: 27px;
	}

</style>
<link rel="stylesheet" href="/css/animate.css"/>
<script src="/scripts/calendar/main.js"></script>
<script src="/scripts/is.min.js"></script>
<script>

	var date = new Date();

	<?php if(isset($_GET['year']) && isset($_GET['month'])) : ?>

var year = <? echo (int)$_GET['year']; ?>;
var month = <? echo (int)$_GET['month'] - 1; ?>;

	<?php else : ?>

var month = date.getMonth();
var year = date.getFullYear();

	<?php endif; ?>

	const MIN_YEAR = year;

	const MIN_MONTH = month;
	const MIN_DATE = new Date(MIN_YEAR, MIN_MONTH);
	var id = "<? echo Yii::app()->user->id; ?>";

	var calendarService = new CalendarService(year, month, "#calendar-container");

	calendarService.render().loadData(id);

	$(document).ready(function(){

		if(is.firefox()){
			$(".weekend table input[type=submit]").css({
				"font-size" : "14px"
			})
		}

		var td = $(".calendar-container td[data-day='"+date.getDate()+"'][data-month='"+( (date.getMonth()+1 < 10)? "0"+(date.getMonth()+1) : date.getMonth()+1  )+"'][data-year='"+date.getFullYear()+"']");

		$("a.weekends-button").click(function(){
			$(".confirm-block-wrapper, .weekends-management-block").css("display", "block");
			return false;
		});

        $("body").on("click", ".calendar-container td:not(.disabled)", function(){
			if($(this).hasClass("active-day")) {
//				$(".active-day").removeClass("active-day");
//				calendarService.activeDay.setNull();
//				calendarService.activeDayStateRender();
				return;
			}
			$(".active-day").removeClass("active-day");
            $(this).addClass("active-day");
			var day = $(this).attr('data-day');
			var month = $(this).attr('data-month');
			var year = $(this).attr('data-year');

			var state = ($(this).hasClass("disabled-button"))? 1 : 0;

			calendarService.activeDay.setActiveDay(day, month, year, state);
			calendarService.activeDayStateRender().loadServicesData();

        });

		td.click();


		$(".confirm-block-wrapper").click(function() {
			$(".confirm-block-wrapper, .confirm-block, .weekends-management-block").css("display", "none");
			$(".confirm-block").removeClass("bounceIn");
			$("footer, #content, .main-h1, header").removeClass("blur");
		});


		$("#yes").click(function(){
			$(".confirm-block-wrapper").click();
			var object = $(".day-state-button:not(.active)");
			changeDayState(object);
			return false;
		});

		$("#no").click(function(){
			$(".confirm-block-wrapper").click();
			return false;
		});


		<?php if(isset($_GET['year']) && isset($_GET['month'])) : ?>

			var day = <? echo $_GET['day']; ?>;
			var year = <? echo $_GET['year']; ?>;
			var month = <? echo $_GET['month']; ?>;

			var td = selectTd(day, month, year);
			td.click();

		<?php endif; ?>

	});
</script>



<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.information table {
		width:100%;
	}

	.information table td {
		width:35%;
	}

	.information table td+td {
		width:65%;
	}


</style>