<?php /* @var $this Controller */
	date_default_timezone_set("Europe/Minsk");
	$baseUrl = Yii::app()->request->baseUrl;
	$model = new LoginForm;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:og="http://ogp.me/ns#"
	  xmlns:fb="http://www.facebook.com/2008/fbml"
	  lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="ru">
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/stylesheet.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/header.css"/>

	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/service.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/calendar.css"/>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?Yii::app()->getClientScript()->registerCoreScript('jquery');?>
	<script src="<?php echo $baseUrl; ?>/scripts/header.js"></script>
	<script src="<?php echo $baseUrl; ?>/scripts/controller.js"></script>
	<script src="<?php echo $baseUrl; ?>/scripts/calendar/calendar.js"></script>

</head>

<body>

	<header>

		<div class="container">
			<div class="logo">
				<a href="<? echo CHtml::normalizeUrl("/")?>"><img src="<?php echo $baseUrl; ?>/images/logo.svg" alt=""/></a>
			</div>
			<div class="menu">
				<?php $this->widget("zii.widgets.CMenu", array(
					"activeCssClass" => "active-menu",
					"items" => array(
						array('label'=>'Создать праздник', 'url'=>array('/site/create'), 'linkOptions' => array('class' => 'create-holiday'), 'active' => false, 'visible' => false),
						array('label'=>'Каталог', 'url'=>array('/site/catalog')),
						array('label'=>'Кабинет', 'url'=>array('/profile/'), "active" => true, "visible" => !Yii::app()->user->isGuest, 'itemOptions' => array('id' => 'profile')),
						array('label'=>'Вход', 'url'=>array('/login'), "visible" => (Yii::app()->user->isGuest), 'active' => CheckPage::isPages(array('site/login', 'site/registration'))),
					),
					"htmlOptions" => array("class" => "header-menu")
				)) ?>
			</div>
		</div>

	</header>

	<? if(Yii::app()->user->isGuest && !CheckPage::isPages(array("site/registration", "site/login"))) : ?>

	<div class="popup-fullsize">

		<div id="log-in" class="popup-content">

			<a class="close-pop-up" href=""></a>

			<!--					--><?php //$this->widget('application.extensions.SocialNetworkAuth.SocialNetworkAuthService', array(
//						'providers' => array('Vk'),
//						'linkCssClass' => 'social-link'
//					)); ?>



		</div>
	</div>

	<? endif; ?>

	<div class="header-padding"></div>

		<?php echo $content?>



	<? $this->renderPartial('application.views.site._footer'); ?>

</body>
</html>

