<?php
/**
 * @var $model Request
 * @var $status integer
 */
?>
<div class="request status-<? echo $model->status ?>" data-id="<? echo $model->id ?>">
    <div class="avatar" style="background-image: url(<? echo ImageService::performImageUrl($model->author->avatar_url, ImageType::AVATAR) ?>)" ></div>
    <div class="request-information">
        <div class="request-title">
            <span class="author-name"><? echo $model->author->username ?></span>
            <a class="phone-number" href="tel:+<? echo $model->phone ?>"><? echo PhoneService::formatToStr($model->phone) ?></a>
        </div>
        <div style="padding-left: 12px; padding-top: 12px; box-sizing: border-box">
            <div class="request-message">
                <? echo $model->message; ?>
            </div>
            <div class="request-info">
                <div class="request-left">
                    <div class="param"><? echo HolidayManager::getTypeStringRU($model->holiday->type) ?></div>
                    <div class="param"><? echo RegionService::performRegionAddressForRequest($model) ?></div>
                    <div class="param"><? echo date("d.m.Y", $model->holiday->date) ?></div>
                    <?
                        switch($model->status){
                            case Request::NEW_REQUEST :
                                ?>
                                <a href="/profile/request/accept"
                                   class="request-accept"
                                   data-request-id="<? echo $model->id ?>"
                                   data-service-id="<? echo $model->service_id ?>"
                                   data-holiday-id="<? echo $model->holiday_id ?>" >Да</a>
                                <a href="/profile/request/deny"
                                   class="request-deny"
                                   data-request-id="<? echo $model->id ?>"
                                   data-service-id="<? echo $model->service_id ?>"
                                   data-holiday-id="<? echo $model->holiday_id ?>" >Нет</a>
                                <?
                                break;
                            case Request::WAITING_REQUEST :
                                ?>
                                <a href="/profile/request/accept"
                                   class="request-accept"
                                   data-request-id="<? echo $model->id ?>"
                                   data-service-id="<? echo $model->service_id ?>"
                                   data-holiday-id="<? echo $model->holiday_id ?>" >Да</a>
                                <a href="/profile/request/deny"
                                   class="request-deny"
                                   data-request-id="<? echo $model->id ?>"
                                   data-service-id="<? echo $model->service_id ?>"
                                   data-holiday-id="<? echo $model->holiday_id ?>" >Нет</a>
                                <?
                                break;
                            case Request::ACCEPTED_REQUEST : break;
                        }
                    ?>
                </div>
                <div class="request-right"></div>
            </div>
        </div>
    </div>
</div>