<?php
/* @var $this RequestController
 * @var $model User
 * @var $requests Request[]
 * @var $newRequests Request[]
 * @var $waitingRequests Request[]
 */

$priceService = new PriceService();

?>

<link rel="stylesheet" href="/css/service.css"/>
<link rel="stylesheet" href="/css/request.css"/>

<?php if(Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-container">
        <div class="success-flash"><? echo Yii::app()->user->getFlash('success')?></div>
        <script>
            setTimeout(function(){
                $(".success-flash").animate({opacity : 0}, 600, function(){
                    $(".success-flash").css("display", "none");
                    $('.flash-container').remove();
                })
            }, 4000)
        </script>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('delete-result')) : ?>
    <div class="flash-container">
        <div class="success-flash"><? echo Yii::app()->user->getFlash('delete-result')?></div>
        <script>
            setTimeout(function(){
                $(".success-flash").animate({opacity : 0}, 600, function(){
                    $(".success-flash").css("display", "none");
                    $('.flash-container').remove();
                })
            }, 4000)
        </script>
    </div>
<?php endif; ?>

<link rel="stylesheet" href="/css/animate.css"/>

<h1 class="main-h1"><? echo $model->username ?></h1>

<div id="content" class="container960">

    <div class="col-span-3">

        <? $this->renderPartial('application.views.site._leftMenu', array(
            "active" => "requests",
            "model" => $model
        )); ?>

    </div><div class="col-span-9">

        <?
            if(count($newRequests) > 0) {
                ?> <div class="requests-list status-<? echo Request::NEW_REQUEST ?>"> <?
                echo '<div class="title-grey"><span class="title-orange">НОВЫЕ ЗАЯВКИ</span></div>';
                foreach($newRequests as $r)
                    $this->renderPartial('partial/requestLoop', array(
                        "model" => $r,
                        "status" => Request::NEW_REQUEST
                    ))
            ?> </div> <? } ?>
        <?
            if(count($waitingRequests) > 0) {
                ?> <div class="requests-list status-<? echo Request::WAITING_REQUEST ?>"> <?
                echo '<div class="title-grey"><span class="title-orange" style="width: 210px;">В ОЖИДАНИИ ПОДТВЕРЖДЕНИЯ</span></div>';
                foreach($waitingRequests as $r)
                    $this->renderPartial('partial/requestLoop', array(
                        "model" => $r,
                        "status" => Request::WAITING_REQUEST
                    ))
            ?> </div> <? } ?>
        <?
            if(count($requests) > 0) {
                ?> <div class="requests-list status-<? echo Request::ACCEPTED_REQUEST ?>"> <?
                echo '<div class="title-grey"><span class="title-black">ЗАЯВКИ</span></div>';
                foreach($requests as $r)
                    $this->renderPartial('partial/requestLoop', array(
                        "model" => $r,
                        "status" => Request::ACCEPTED_REQUEST
                    ))
            ?> </div> <? } ?>

    </div>
</div>

<script src="/scripts/is.min.js"></script>

<style>


    header {
        box-shadow: 0 -4px 10px black;
    }

    .block .info-title {
        font-size: 14px;
    }

    .block{
        float: none;
        display: block;
        width:100%;
        margin-top: 20px;
    }

    .information table {
        width:100%;
    }

    .information table td {
        width:35%;
    }

    .information table td+td {
        width:65%;
    }

</style>

<script>

    $(document).ready(function(){

        $(document).on('click', '.request-accept, .request-deny', function(){

            var postUrl = $(this).attr("href");
            var serviceId = $(this).attr("data-service-id");
            var holidayId = $(this).attr("data-holiday-id");
            var requestId = $(this).attr("data-request-id");

            var xhr = new XMLHttpRequest();
            var fd = new FormData();

            fd.append("service_id", serviceId);
            fd.append("holiday_id", holidayId);
            fd.append("request_id", requestId);

            xhr.onload = function()
            {
                var response = JSON.parse(xhr.response);
                console.log(response['action']);
//                eval(response['action']);
            };

            xhr.open("POST", postUrl, true);
            xhr.send(fd);

            return false;
        })

    });

</script>