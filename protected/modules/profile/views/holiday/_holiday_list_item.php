<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 30.09.15
 * Time: 15:17
 * @var $model Holiday
 */

//    $priceService = new PriceService();

//    $additionalUrl = (CheckPage::isPage("site/catalog"))? "?returnUrl=".urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") : "";

?>

<div class="holiday holiday-wedding">


    <?php if(Yii::app()->user->id == $model->author_id && !CheckPage::isPage('site/catalog')) : ?>
    <div class="manage-button-list">
        <a class="manage-button edit" data-alt="Редактировать праздник" href="<? echo UrlService::performHolidayEditUrl($model) ?>"></a>
        <a class="manage-button trash" data-alt="Удалить праздник" href="<? echo UrlService::performHolidayDeleteUrl($model) ?>"></a>
    </div>
    <?php endif; ?>

    <a class="holiday-item <? echo HolidayManager::getTypeString($model->type) ?>" href="<? echo UrlService::performHolidayViewUrl($model) ?>">
        <div class="type"><? echo HolidayManager::getTypeStringRU($model->type) ?></div>
        <div class="title"><? echo $model->title; ?></div>
        <div class="date"><? echo date("d.m.Y", $model->date) ?></div>
        <div class="region"><? echo RegionService::performRegionAddressForHoliday($model) ?></div>
    </a>

</div>

