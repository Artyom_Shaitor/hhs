<?php
/**
 * @var $this DefaultController
 * @var $model User
 * @var $pages CPagination
 * @var $models Holiday[]
 * @var $serviceCount integer
 */

$priceService = new PriceService();

?>


<?php if(Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-container">
        <div class="success-flash"><? echo Yii::app()->user->getFlash('success')?></div>
        <script>
            setTimeout(function(){
                $(".success-flash").animate({opacity : 0}, 600, function(){
                    $(".success-flash").css("display", "none");
                    $('.flash-container').remove();
                })
            }, 4000)
        </script>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('delete-result')) : ?>
    <div class="flash-container">
        <div class="success-flash"><? echo Yii::app()->user->getFlash('delete-result')?></div>
        <script>
            setTimeout(function(){
                $(".success-flash").animate({opacity : 0}, 600, function(){
                    $(".success-flash").css("display", "none");
                    $('.flash-container').remove();
                })
            }, 4000)
        </script>
    </div>
<?php endif; ?>

<link rel="stylesheet" href="/css/animate.css"/>

<h1 class="main-h1"><? echo $model->username ?></h1>

<?php if(count($models) > 0 && Yii::app()->user->id == $models[0]->author_id) : ?>
    <div class="confirm-block-wrapper"></div>
    <div class="confirm-block animated ">
        <div class="confirm-title">Вы действительно хотите удалить праздник?</div>
        <div class="confirm-sub-title">Удалённый прадник невозможно будет восстановить</div>

        <a id="yes" href="/">Да</a><a id="no" href="/">Нет</a>
    </div>
<?php endif; ?>


<div id="content" class="container960">

    <div class="col-span-3">

        <? $this->renderPartial('application.views.site._leftMenu', array(
            "active" => "holidays",
            "model" => $model
        )); ?>

    </div><div class="col-span-9">

        <style>

            .manage-button.trash { border-color: white; }

        </style>

            <a class="big-orange-add-button" style="line-height:70px;" href="/profile/add-holiday">Создать праздник</a>

        <div class="holiday-types-list">
            <ul>
                <li><a href="/profile/add-holiday/wedding">Свадьба</a></li>
                <li><a href="/profile/add-holiday/corporate">Корпоратив</a></li>
                <li><a href="/profile/add-holiday/birthday">День рождения</a></li>
                <li><a href="/profile/add-holiday">Другой праздник</a></li>
            </ul>
        </div>

        <div class="holidays-list">

            <?foreach($models as $model):?>
                <? $this->renderPartial('_holiday_list_item', array(
                    "model" => $model,
                )) ?>
            <?endforeach?>

        </div>


<!--        --><?//$this->widget('CLinkPager', array(
//            'pages' => $pages,
//            'internalPageCssClass' => '',
//            'id' => 'pagination',
//            'pageSize' => 10,
//            'header' => '',
//            'selectedPageCssClass' => 'active',
//            'hiddenPageCssClass' => 'disabled',
//            'nextPageLabel' => false,         // »
//            'prevPageLabel' => false,         // «
//            'lastPageLabel' => '...',  // »» &raquo;
//            'firstPageLabel' => '...', // «« &laquo;
//            'htmlOptions' => array('class' => 'pagination'),
//        ))?>

    </div>
</div>

<script src="/scripts/is.min.js"></script>

<style>


    header {
        box-shadow: 0 -4px 10px black;
    }

    .block .info-title {
        font-size: 14px;
    }

    .block{
        float: none;
        display: block;
        width:100%;
        margin-top: 20px;
    }

    .information table {
        width:100%;
    }

    .information table td {
        width:35%;
    }

    .information table td+td {
        width:65%;
    }




</style>

<script>


    $(".confirm-block-wrapper").click(function() {
        $(".confirm-block-wrapper, .confirm-block").css("display", "none");
        $(".confirm-block").removeClass("bounceIn");
        $("footer, #content, .main-h1, header").removeClass("blur");
    });

    $(".manage-button.trash").click(function(){
        $(".confirm-block-wrapper, .confirm-block").css("display", "block");
        $(".confirm-block").addClass("bounceIn");
        $("footer, #content, .main-h1, header").addClass("blur");
        $("#yes").attr("href", $(this).attr("href"));
        return false;
    });

    $("#yes").click(function(){
        return true;
    });

    $("#no").click(function(){
        $(".confirm-block-wrapper").click();
        return false;
    });

    $(".big-orange-add-button").click(function()
    {
        $(".holiday-types-list").addClass("open");
        return false;
    });

</script>