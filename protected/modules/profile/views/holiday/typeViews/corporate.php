<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.03.16
 * Time: 10:54
 * @var $model Holiday
 */
?>

<div id="main-info" class="col-2">
    <table>
        <tr>
            <td><div class="avatar" style="background-image: url(<? echo ImageService::performHolidayAvatarImage($model->photo_1, ImageType::HOLIDAY_IMAGE) ?>)"></div></td>
            <td class="va-top">
                <div id="center-text">
                    <h3><? echo $model->title ?></h3>

                    <div id="date">
                        <? echo date("d.m.Y", $model->date) ?>
                    </div>

                    <div id="region">
                        <? echo RegionService::performRegionAddressForHoliday($model); ?>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</div>