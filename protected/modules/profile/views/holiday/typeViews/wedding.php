<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.03.16
 * Time: 10:54
 * @var $model Holiday
 */
?>

<div id="main-info" class="col-3">
    <table>
        <tr>
            <td><div class="avatar" style="background-image: url(<? echo ImageService::performHolidayAvatarImage($model->photo_1, ImageType::HOLIDAY_IMAGE) ?>)"></div></td>
            <td class="va-top">

                <div id="center-text">
                    <? if(mb_strlen($model->name_1) + mb_strlen($model->name_2) < 25 ) : ?>
                        <h3><? echo $model->name_1; ?></h3>
                        &
                        <h3><? echo $model->name_2; ?></h3>
                    <? else : ?>
                        <h3><? echo $model->name_1; ?></h3><br/>
                        &<br/>
                        <h3><? echo $model->name_2; ?></h3>
                    <? endif; ?>

                    <div id="date">
                        <? echo date("d.m.Y", $model->date) ?>
                    </div>

                    <div id="region">
                        <? echo RegionService::performRegionAddressForHoliday($model); ?>
                    </div>
                </div>
            </td>
            <td><div class="avatar" style="background-image: url(<? echo ImageService::performHolidayAvatarImage($model->photo_2, ImageType::HOLIDAY_IMAGE) ?>)"></div></td>
        </tr>
    </table>
</div>