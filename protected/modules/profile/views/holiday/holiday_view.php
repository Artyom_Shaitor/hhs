<?php
/**
 * @var $this DefaultController
 * @var $user User
 * @var $model Holiday
 * @var $holidayServices HolidayService[]
 * @var $form CActiveForm
 * @var $type string
 * @var $baseSubCategoriesList Category[]
 * @var $baseCategoriesList Category[]
 * @var $servicesArray array
 */

$priceService = new PriceService();

?>


<style>
    .form {
        width:100%;
    }

	.add-service {
		width:100% !important;
		margin-top: 30px;
	}

	.add-button[disabled] {
		opacity: 1;
	}

	.add-button[disabled]:hover {
		border: 1px solid rgba(0,150,0,0.4);
		color: rgba(0,150,0,0.6);
		cursor: auto;
	}

	.add-button {
		-webkit-appearance: none;
		border: 1px solid rgba(0,150,0,0.4);
		background-color: white;
		width: 300px;
		display: block;
		margin: 10px 0;
		position: relative;
		text-align: center;
		font-size: 14px;
		color: rgba(0,150,0,0.6);
		text-decoration: none;
		padding: 11px 10px 12px 10px;
	}

	.add-button:hover {
		border: 1px solid rgba(0,145,0,0.8);
		color: rgba(0,145,0,1);
		cursor: pointer;
	}

	.manage-button-list {
		right: 10px;
		top: 3px;
	}

	.manage-button-list .manage-button {
		margin: 0 1px;
	}

	.col-span-9 {
		padding: 26px 26px 26px 36px;
	}

	.price-value {
		padding: 4px 5px;
		border-radius: 4px;
		outline: none;
		display: inline-block;
		font-size: 16px;
		border: 0;
		background-color: transparent;
		font-family: "Officina";
		text-align: center;
	}

	.add {
		display: block;
	}

	.price-summary {
		background-color: #ff5c00;
		color: white;
		box-sizing: border-box;
		padding: 9px 17px;
		font-size: 18px;
		margin-top: 15px;
		width: 800px;
		margin-left: 80px;
	}

	.price-summary span {
		font-size: 16px;
	}

	.price-summary span b{
		font-size: 18px;
	}

	#prices {
		float: right;
	}

    .add-link {
        display: block;
		/*opacity: 0.6;*/
    }

	.manage-button.trash:after {
		margin-left: -47px;
		width: 100px;
	}

	.specialist-list .manage-button.trash:after {
		margin-left: -52px !important;
		width: 75px !important;
	}


</style>

<link rel="stylesheet" href="/css/pickmeup/pickmeup.css"/>
<link rel="stylesheet" href="/css/holiday.css"/>

<?php if($model->hasErrors()) : ?>
	<div class="flash-container">
		<div class="failed-flash">Заполните все неоходимые поля</div>
		<script>
			setTimeout(function(){
				$(".failed-flash").animate({opacity : 0}, 600, function(){
					$(".failed-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<div class="bg-black hide"></div>
<div class="catalog-mini hide">
    <a href="/" class="close-catalog-mini">✕</a>
	<iframe src="" id="catalog-mini-content" frameborder="0"></iframe>
</div>

<h1 class="main-h1">
	<? echo $user->username; ?>
	<div class="manage-button-list">
		<a class="manage-button edit" data-alt="Редактировать праздник" href="<? echo UrlService::performHolidayEditUrl($model) ?>"></a>
	</div>
</h1>

<link rel="stylesheet" href="/css/animate.css"/>
<div class="confirm-block-wrapper v1"></div>
<div class="confirm-block animated v1">
	<div class="confirm-title">Вы действительно хотите удалить услугу из праздника?</div>
	<div class="confirm-sub-title">Удалённую услугу невозможно будет восстановить</div>

	<a id="yes" href="/">Да</a><a id="no" href="/">Нет</a>
</div>


<div class="confirm-block-wrapper v2"></div>
<div class="confirm-block animated v2">
	<div class="confirm-title">Введите сообщение и номер телефона</div>

	<div class="request-block">
		<textarea class="message" name="" id="" cols="" rows="" placeholder="Текст сообщения"><? echo Request::DEFAULT_MESSAGE ?></textarea>
		<input class="phone-number" type="text" placeholder="Номер телефона" value="<? if($user->phone != "") echo PhoneService::formatToStr($user->phone) ?>"/>

	</div>

	<a id="yes2" href="/">Отправить</a><a id="no2" href="/">Отмена</a>
</div>

<div id="content" class="container960">
	<a class="button-back" href="/profile/holidays">Вернуться к праздникам</a>
<div class="col-span-12" style="position: relative">

	<?php if( $model->author_id == Yii::app()->user->id) : ?>

		<?
			switch($type) {
				case "wedding" :
					$this->renderPartial('typeViews/wedding', array(
						"model" => $model
					));
					break;
				case "corporate" :
					$this->renderPartial('typeViews/corporate', array(
						"model" => $model
					));
					break;
				case "birthday" :
					$this->renderPartial('typeViews/birthday', array(
						"model" => $model
					));
					break;
				default :
					$this->renderPartial('typeViews/other', array(
						"model" => $model
					));
					break;
			}

		?>

		<div class="holiday-list">
			<?

			$priceSummaryUsd = 0;
			$priceSummaryBlr = 0;
			$currentCategoryLoop = "";
			$currentCategoryLoopCount = 0;

			$userServicesIdArray = array();

            /**
             * @var $services Service[]
             */
			foreach($baseCategoriesList as $baseCategory){
				$subCategoriesCount = 0;
//				$modelServices = [];
				$modelHolidayServices = [];
				$tempSubCategoriesArray = [];
				foreach($baseSubCategoriesList as $baseSubCategory){
					if($baseSubCategory->getParentCategory()->tag == $baseCategory->tag) {
						$subCategoriesCount++;
						$tempSubCategoriesArray[$baseSubCategory->name] = $baseSubCategory;
					}
				}

				foreach($holidayServices as $holidayService){
					$service = $holidayService->service;
					if($service->category->tag == $baseCategory->tag) {
						$modelHolidayServices[] = $holidayService;
						unset($tempSubCategoriesArray[$service->subCategory->name]);
						$subCategoriesCount++;
					}
				}

				if($subCategoriesCount > 0) {
					$currentCategoryLoop = $baseCategory->tag;
					$currentCategoryLoopCount = 0;

					foreach($modelHolidayServices as $holidayService) {

						$priceSummaryBlr += (int)$priceService->convertToCurrencyNumber($holidayService->price, $holidayService->currency->name, "BLR");
						$priceSummaryUsd += (int)$priceService->convertToCurrencyNumber($holidayService->price, $holidayService->currency->name, "USD");

						$this->renderPartial('partial/services_loop', array(
							"priceService" => $priceService,
							"service" => $holidayService->service,
							"holidayService" => $holidayService,
							"currentCategoryLoop" => $currentCategoryLoop,
							"currentCategoryLoopCount" => $currentCategoryLoopCount,
						));
						$currentCategoryLoopCount++;
					}


					foreach($tempSubCategoriesArray as $category){
						$this->renderPartial('partial/add_link_render', array(
							"category" => $category,
							"model" => $model,
							"priceService" => $priceService,
							"currentCategoryLoop" => $currentCategoryLoop,
							"currentCategoryLoopCount" => $currentCategoryLoopCount,
						));
						$currentCategoryLoopCount++;
					}

				}
			}


			?>
		</div>

		<div class="category-seperator" data-type="specialists" style="display: <? echo (count($model->specialists) > 0) ? "block" : "none" ?>;">Свои специалисты</div>

		<div class="specialist-list">
			<?
			foreach($model->specialists as $specialist){
				$priceSummaryBlr += (int)$priceService->convertToCurrencyNumber($specialist->price, $specialist->currency->name, "BLR");
				$priceSummaryUsd += (int)$priceService->convertToCurrencyNumber($specialist->price, $specialist->currency->name, "USD");
				$this->renderPartial('partial/specialist_loop', array(
					"priceService" => $priceService,
					"specialist" => $specialist,
				));
			}

			?>
		</div>

		<div class="add-buttons-block">
			<? echo CHtml::link("Добавить услугу из каталога", "/catalog/?holiday_id=".$model->id, array("class" => "big-orange-add-button-catalog")); ?>
			<? echo CHtml::link("Добавить своего специалиста", "/", array("class" => "big-orange-add-button-catalog", "id" => "add-specialist")); ?>
		</div>


		<div class="price-summary">
			Итого: <div id="prices"><span class="blr"><? echo "<b>".number_format($priceSummaryBlr, 0, '.', ' ')."</b> ".$priceService->getCurrencyTitle("BLR") ?></span>&nbsp;~&nbsp;<span class="usd"><? echo "<b>".number_format($priceSummaryUsd, 0, '.', ' ')."</b> ".$priceService->getCurrencyTitle("USD") ?></span></div>
		</div>

	<? else : ?>

			<h1>Ошибка прав доступа</h1>

	<? endif; ?>

</div><!-- form -->

</div>
</div>

<script src="/scripts/XMLHttpRequest.js"></script>
<script src="/scripts/service.js"></script>
<script src="/scripts/autoNumeric-min.js"></script>
<script src="/scripts/is.min.js"></script>
<script src="/scripts/pickmeup/jquery.pickmeup.min.js"></script>

<style>


	.specialist-list .service-info > input:hover {
		background-color: rgba(0,0,0,0.1);
	}

	.specialist-list .service-info > input:focus {
		background-color: white;
		font-weight: 100;
		text-shadow: none;
		font-style: normal;
		color: black;
		text-transform: none;
	}


</style>

<script>

	$("#Holiday_date").pickmeup({
		format : "d-m-Y",
		position: "bottom",
		hide_on_select : true,
		min : new Date
	});

</script>

<script>

	function refreshCategoryIcons()
	{
		var currentCategory = $(".category-menu:not(.except)").first();
		var currentCategoryName = currentCategory.attr("data-category");

		currentCategory.children().css("opacity", 1);

		$(".category-menu:not(.except)").each(function(index, elem){
			if(index != 0){
				var elemCategoryName = $(elem).attr("data-category");
				if(elemCategoryName == currentCategoryName)
					$(elem).children().css("opacity", 0);
				else {
					currentCategoryName = elemCategoryName;
				}
			}
		})
	}

	function encodeData(data) {
		return Object.keys(data).map(function(key) {
			return [key, data[key]].map(encodeURIComponent).join("=");
		}).join("&");
	}

	$(".catalog-mini iframe").load(function(){

		$(this).contents().find('.catalog-add-holiday-button').click(function(){
			var serviceId = $(this).attr("data-service-id");
			var holidayId = <? echo $model->id ?>;

			var xhr = new XMLHttpRequest();
			var fd = new FormData();
			fd.append("service_id", serviceId);
			fd.append("holiday_id", holidayId);

			xhr.onload = function(){
				var response = JSON.parse(xhr.response);

				if(response['result'] == false){
					alert(response['message']);
					return;
				}

				var buttonDom = ".holiday-list input[data-category-id='"+response['category_id']+"'][data-subcategory-id='"+response['sub_category_id']+"']";
				var holidayDom = ".service-holiday[data-category-id='"+response['category_id']+"'][data-subcategory-id='"+response['sub_category_id']+"']";
				if($(buttonDom).length > 0) {
					$(response['render']).insertAfter(buttonDom);
					$(buttonDom).remove();

				}
				else if($(holidayDom).length > 0){
					$(response['render']).insertAfter($(holidayDom)[$(holidayDom).length - 1]);
					$(buttonDom).remove();
				}
				else
					$(".holiday-list").append(response['render']);

				changeSummary();

				$(".bg-black, .catalog-mini").addClass("hide");
				$("body").css("overflow", "auto");

			};

			xhr.open("POST", "/profile/holiday/addServiceToHoliday");
			xhr.send(fd);

			return false;
		})
	});

	$(document).on("click", ".add", function(){

		var params = {
			'Search[city_id]' : <? echo $model->city_id ?>,
			'Search[region_id]' : <? echo $model->city->regionid ?>,
			'Search[date]' : '<? echo date("d-m-Y", $model->date) ?>'
		};

		var category = $(this).attr("data-category-name");
		var subcategory = $(this).attr("data-subcategory-name");

		var categoryFilter = (category == "" && subcategory == "")? "" : category+"/"+subcategory;

		var self = ".catalog-mini iframe";
		var src = "/catalogMini/<? echo $model->id ?>/"+categoryFilter+"?"+encodeData(params);
		$(self).attr("src", src);


		$(".bg-black, .catalog-mini").removeClass("hide");
		$("body").css("overflow", "hidden");
	});

	$(".bg-black, .close-catalog-mini").click(function(){
		$(".bg-black, .catalog-mini").addClass("hide");
		$("body").css("overflow", "auto");
        return false;
	});

	$(document).on("focusout", "textarea[name='note']", function(){
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		var note = $(this).val();
		var modelId = $(this).attr("data-model-id");
		var modelType = $(this).attr("data-model-type");

		var self = this;

		fd.append("note", note);
		fd.append("model_id", modelId);
		fd.append("type", modelType);

		xhr.onload = function(){
			var response = JSON.parse(xhr.response);
		};

		xhr.open("POST", "/profile/holiday/changeNote");
		xhr.send(fd);
	});

	$(document).on("keydown", ".service-content .price-value, .service-holiday[data-model-type='Specialist'] .service-content .service-info > input", function(e){
		if(e.keyCode == 13) {
			e.preventDefault();
			$(this).blur();
			return false;
		}
	});

	$(document).on('keyup', '.service-content .service-info > input', function(){
		var regexp = /[a-zа-я]+/ig;
		var oldPhone = $(this).val();
		var newPhone = $(this).val().replace(regexp, "");

		if(oldPhone != newPhone)
			$(this).val(newPhone);
	});

	$(document).on('keyup', ".service-content .price-value", function(e){
		var regexp = /[\D(&nbsp;)]+/ig;
		var number = $(this).val().replace(regexp, "");

		if(parseInt(number) > Math.pow(2, 31) - 1)
			number = (Math.pow(2, 31) - 1).toString();

		regexp = /\d{1,3}/ig;
		number = number.reverse().replace(regexp, function(s){
			return s + " "
		}).reverse().trim();

		$(this).val(number);
		resizeInput($(this));

	});

	function resizeInput(elem)
	{
		$(elem).attr('size', $(elem).val().length+1);
	}

	String.prototype.reverse = function()
	{
		return this.split("").reverse().join("");
	};

	$(document).on("click", ".change-price-title", function(){
		$(this).parent().children(".blr").children(" .price-value").focus();
	});

	$(document).on("click", "#add-specialist", function(){
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		fd.append("model_id", <? echo $model->id ?>);

		xhr.onload = function(){
			var response = JSON.parse(xhr.response);

			if(response['result'] == true){
				var count = $(".service-holiday[data-model-type='Specialist']").length;
				if(count > 0){
					$(response['render']).insertAfter($(".service-holiday[data-model-type='Specialist']")[count-1])
				}else {
					$(".category-seperator[data-type='specialists']").css("display", "block");
					$(response['render']).insertAfter($(".category-seperator[data-type='specialists']"));
				}
			}
		};

		xhr.open("POST", "/profile/holiday/addSpecialist");
		xhr.send(fd);

		return false;
	});

	$(document).on("focusout", ".service-holiday[data-model-type='Specialist'] .service-content .service-info > input", function(){
		var parent = $(this).parent();
		var username = parent.children(".title").val();
		var category = parent.children(".subCategory").val();
		var phone = parent.children(".author-number").val();
		var modelId = parent.attr("data-model-id");

		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		fd.append('username', username);
		fd.append('category', category);
		fd.append('phone', phone);
		fd.append('model_id', modelId);

		xhr.open("POST", '/profile/holiday/changeSpecialistData');
		xhr.send(fd);
	});

	$(document).on("focusout", ".service-content .price-value", function(){
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		var priceValue = parseInt($(this).val().replace(/\s+/g, ''));
		var newPrice = (priceValue > Math.pow(2, 31) - 1)? Math.pow(2, 31) - 1 : priceValue;
		var modelId = $(this).parent().attr("data-model-id");
		var modelType = $(this).parent().attr("data-model-type");
		var currencyString = $(this).parent().attr("class");

		var self = this;

		fd.append("price", newPrice);
		fd.append("model_id", modelId);
		fd.append("type", modelType);
		fd.append("currency", currencyString);

		xhr.onload = function(){
			var response = JSON.parse(xhr.response);

			$(self).parent().parent().children(".blr").children(".price-value").val(response.blr);
			$(self).parent().parent().children(".usd").children(".price-value").val(response.usd);

			changeSummary();
		};

		xhr.open("POST", "/profile/holiday/changePrice");
		xhr.send(fd);

	});

	var changeSummary = function(){
		var blrSummary = 0;
		var usdSummary = 0;

		$(".blr .price-value").each(function(index, elem){

			var blrValue = parseInt($(elem).val().replace(/\s+/g, ''));
			var usdValue = parseInt($($(".usd .price-value")[index]).val().replace(/\s+/g, ''));

			blrSummary += (isNaN(blrValue))? 0 : blrValue;
			usdSummary += (isNaN(usdValue))? 0 : usdValue;

			resizeInput($(elem));
			resizeInput($($(".usd .price-value")[index]));

		});

		$(".price-summary .blr b").html(number_format(blrSummary, 0, '.', ' '));
		$(".price-summary .usd b").html(number_format(usdSummary, 0, '.', ' '));
	};

	$(".confirm-block-wrapper").click(function() {
		$(".confirm-block-wrapper, .confirm-block").css("display", "none");
		$(".confirm-block").removeClass("bounceIn");
		$("footer, #content, .main-h1, header").removeClass("blur");
	});

	$(document).on("click", ".manage-button.trash", function(){
		$(".confirm-block-wrapper.v1, .confirm-block.v1").css("display", "block");
		$(".confirm-block.v1").addClass("bounceIn");
		$("footer, #content, .main-h1, header").addClass("blur");
		$("#yes").attr("data-model-id", $(this).attr("data-model-id"));
		$("#yes").attr("data-model-type", $(this).attr("data-model-type"));
		return false;
	});

	$(document).on("click", "#yes", function(){
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		var modelId = $(this).attr("data-model-id");
		var modelType = $(this).attr("data-model-type");

		fd.append("model_id", modelId);
		fd.append("type", modelType);

		xhr.onload = function(){

			$(".confirm-block-wrapper, .confirm-block").css("display", "none");
			$("footer, #content, .main-h1, header").removeClass("blur");

			var response = JSON.parse(xhr.response);
			if(response['result'] == true){
				var serviceHoliday = $(".service-holiday[data-model-id='"+modelId+"'][data-model-type='"+modelType+"']");
				if(modelType == "HolidayService") {
					var categoryId = serviceHoliday.attr("data-category-id");
					var subCategoryId = serviceHoliday.attr("data-subcategory-id");

					if ($(".service-holiday[data-category-id='" + categoryId + "'][data-subcategory-id='" + subCategoryId + "']").length == 1) {
						$(response['render']).insertAfter(serviceHoliday);
					}
				}

				if(modelType == "Specialist"){
					if($(".service-holiday[data-model-type='Specialist']").length == 1)
						$(".category-seperator[data-type='specialists']").css("display", "none");
				}
				serviceHoliday.remove();
				refreshCategoryIcons();
				changeSummary();


			}
		};

		xhr.open("POST", '/profile/holiday/deleteservice');
		xhr.send(fd);

		return false;

	});

	$("#no").click(function(){
		$(".confirm-block-wrapper").click();
		return false;
	});

	function number_format(number, decimals, dec_point, thousands_sep) {
				number = (number + '')
			.replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function(n, prec) {
				var k = Math.pow(10, prec);
				return '' + (Math.round(n * k) / k)
						.toFixed(prec);
			};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
			.split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '')
				.length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1)
				.join('0');
		}
		return s.join(dec);
	}

	$(".manage-button.send-request").click(function(){
		$(".confirm-block-wrapper.v2, .confirm-block.v2").css("display", "block");
		$(".confirm-block.v2").addClass("bounceIn");
		$("footer, #content, .main-h1, header").addClass("blur");
		$(".v2 #yes2").attr("href", $(this).attr("href"));
		$(".v2 #yes2").attr("data-service-id", $(this).attr("data-service-id"));
		$(".v2 #yes2").attr("data-holiday-id", $(this).attr("data-holiday-id"));
		$(".v2 #yes2").attr("data-reciever-id", $(this).attr("data-reciever-id"));
		$(".v2 #yes2").attr("data-holiday-service-id", $(this).attr("data-holiday-service-id"));
		return false;
	});

	$(".confirm-block-wrapper.v2").click(function() {
		$(".confirm-block-wrapper, .confirm-block").css("display", "none");
		$(".confirm-block").removeClass("bounceIn");
		$("footer, #content, .main-h1, header").removeClass("blur");
	});

	$(document).on("click", "#yes2", function(){
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		var recieverId = $(this).attr("data-reciever-id");
		var holidayId = $(this).attr("data-holiday-id");
		var serviceId = $(this).attr("data-service-id");
		var hSId = $(this).attr("data-holiday-service-id");
		var message = $(".message").val();
		var phone = $(".phone-number").val();
		var postUrl = $(this).attr("href");


		fd.append("reciever_id", recieverId);
		fd.append("holiday_id", holidayId);
		fd.append("service_id", serviceId);
		fd.append("holiday_service_id", hSId);
		fd.append("message", message);
		fd.append("phone", phone);


		xhr.onload = function(){
			var response = JSON.parse(xhr.response);
			eval(response['action']);
		};

		xhr.open("POST", postUrl);
		xhr.send(fd);

		return false;

	});

	$("#no2").click(function(){
		$(".confirm-block-wrapper").click();
		return false;
	});


</script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.error-title {
		position: absolute;
		width: 100%;
		margin-top: -35px;
		text-align: center;
		color: rgb(255,40,40);
		text-transform: uppercase;
	}

	input[type='submit']:disabled {
		opacity: 0.4;
	}

	.abort_image_loading {
		font-size: 14px;
		color:black;
		position: absolute;
		width:110px;
		height:110px;
		line-height:110px;
		text-align: center;
	}

	.abort_image_loading:hover {
		cursor: pointer;
	}

	.percent {
		position: absolute;
		left: 0;
		right: 0;
		margin-top: 31px;
	}

	.manage-button.trash { background-color: #d6224a; }

    .category-seperator {
		border-bottom: 1px solid;
		margin-left: 80px;
		margin-bottom: 10px;
		margin-top: 25px;
		padding-bottom: 4px;
		text-transform: uppercase;
		color: rgb(80,80,80);
		font-weight: 600;
		font-size: 18px;
		width: 800px;
    }

</style>


