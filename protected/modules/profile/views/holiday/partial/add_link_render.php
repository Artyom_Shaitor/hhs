<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 04.04.16
 * Time: 12:40
 * @var $category Category
 * @var $model Holiday
 * @var $priceService PriceService
 */

?>

<div class="service-holiday add-link">
<div class="category-menu" data-category="<? echo $category->getParentCategory()->tag; ?>">
<!--    --><?// if($currentCategoryLoop == $category->getParentCategory()->tag && $currentCategoryLoopCount == 0) : ?>
    <?
        $get = ($currentCategoryLoop == $category->getParentCategory()->tag && $currentCategoryLoopCount == 0);
    ?>
        <div class="holiday-author-avatar" style="background-image: url(<? echo $category->getCategoryIcon() ?>); opacity: <? echo ($currentCategoryLoop == $category->getParentCategory()->tag && $currentCategoryLoopCount == 0)? "1" : "0" ?>"></div>
<!--    --><?// endif; ?>
</div>
<a style="display: inline-block;" class="service-content category-<? echo $category->getParentCategory()->tag; ?>" href="<? echo $category->buildCatalogQuery($model) ?>">
    <div class="service-info">
        <div class="add-link-title" style=""><? echo $category->name ?></div>
    </div>
    <div class="note">
        <? echo CHtml::textArea('note', "", array("Placeholder" => "Ваши заметки", "disabled" => true)) ?>
    </div>
    <div class="holiday-price" style="font-size: 16px;">
        <div class="holiday-price-view">
            <span class="blr"><? echo $priceService->convert(0, "BLR", "BLR", true, false); ?></span> &nbsp;~&nbsp;
            <span class="usd"><? echo $priceService->convert(0, "USD", "USD", true, false); ?></span>
<!--            <span class="change-price-title">Изменить цену</span>-->
        </div>
    </div>
</a>

<div class="category-menu except">
    <a class="manage-button catalog-add-holiday-button" data-alt="Добавить услугу из каталога" style="width:35px; height: 35px; background-color: #ff5c00" href="<? echo $category->buildCatalogQuery($model) ?>"></a>
</div>

</div>