<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.03.16
 * Time: 12:04
 * @var $service Service
 * @var $holidayService HolidayService
 * @var $priceService PriceService
 */

?>

<div class="service-holiday" data-category-id="<? echo $service->category_id ?>" data-subcategory-id="<? echo $service->sub_category_id ?>" data-model-id="<? echo $holidayService->id ?>" data-model-type="HolidayService">
    <div class="category-menu" data-category="<? echo $service->category->tag; ?>">
        <div class="holiday-author-avatar" style="background-image: url(<? echo $service->category->getCategoryIcon() ?>); width:40px; height:40px; opacity: <? echo ($currentCategoryLoop == $service->category->tag && $currentCategoryLoopCount == 0)? "1" : "0" ?>"></div>
    </div>
    <div class="service-content category-<? echo $service->category->tag; ?>">
        <div class="service-info">

            <a class="title" href="<? echo UrlService::performServiceUrl($service); ?>"><? echo $service->title; ?></a>
            <div class="subCategory"><? echo $service->subCategory->name; ?></div>
            <div class="author-number"><? echo PhoneService::formatToStr($service->author->phone); ?></div>
        </div>
        <div class="note">
            <? echo CHtml::textArea('note', $holidayService->getNote(), array("Placeholder" => "Ваши заметки", "data-model-id" => $holidayService->id, "data-model-type" => "HolidayService")) ?>
        </div>
        <div class="holiday-price">
            <div class="holiday-price-view">
                <span class="blr" data-model-type="HolidayService" data-model-id="<? echo $holidayService->id ?>"><? echo $priceService->convert($holidayService->price, $holidayService->currency->name, "BLR", true, true, true); ?></span> &nbsp;~&nbsp;
                <span class="usd" data-model-type="HolidayService" data-model-id="<? echo $holidayService->id ?>"><? echo $priceService->convert($holidayService->price, $holidayService->currency->name, "USD", true, true, true); ?></span>
                <span class="change-price-title">Изменить цену</span>
            </div>
        </div>
    </div>
    <div class="category-menu except">
        <div class="holiday-author-avatar" style="background-image: url(<? echo ImageService::performImageUrl($service->avatar_url, ImageType::AVATAR) ?>)"></div>
        <a class="manage-button trash" data-alt="Удалить услугу из праздника" data-model-id="<? echo $holidayService->id ?>" data-model-type="HolidayService" href="/"></a><br/>
        <?
            switch($holidayService->status){
                case HolidayService::SIMPLE_SERVICE :
                    echo "<a href='/profile/request/send'
                             class='manage-button request send-request'
                             data-alt='Отправить запрос'
                             data-service-id='{$service->id}'
                             data-holiday-id='{$holidayService->holiday_id}'
                             data-holiday-service-id='{$holidayService->id}'
                             data-reciever-id='{$service->author_id}'></a>";
                    break;
            }
        ?>
    </div>
</div>

