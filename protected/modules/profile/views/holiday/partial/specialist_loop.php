<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.03.16
 * Time: 12:04
 * @var $specialist Specialist
 * @var $priceService PriceService
 */
?>

<div class="service-holiday" data-model-id="<? echo $specialist->id ?>" data-model-type="Specialist">
    <div class="category-menu except">
    </div>
    <div class="service-content">
        <div class="service-info" data-model-id="<? echo $specialist->id ?>">
            <input type="text" class="title" onclick="document.execCommand('selectAll',false,null)" placeholder="Введите имя специалиста" value="<? echo $specialist->username; ?>">
            <input type="text" class="subCategory" onclick="document.execCommand('selectAll',false,null)" placeholder="Введите специализацию" value="<? echo $specialist->category_name; ?>">
            <input type="text" class="author-number" onclick="document.execCommand('selectAll',false,null)" placeholder="Введите номер телефона" value="<? echo $specialist->phone; ?>">
        </div>
        <div class="note">
            <? echo CHtml::textArea('note', $specialist->getNote(), array("Placeholder" => "Ваши заметки", "data-model-id" => $specialist->id, "data-model-type" => "Specialist")) ?>
        </div>
        <div class="holiday-price">
            <div class="holiday-price-view">
                <span class="blr" data-model-type="Specialist" data-model-id="<? echo $specialist->id ?>"><? echo $priceService->convert($specialist->price, $specialist->currency->name, "BLR", true, false, true); ?></span> &nbsp;~&nbsp;
                <span class="usd" data-model-type="Specialist" data-model-id="<? echo $specialist->id ?>"><? echo $priceService->convert($specialist->price, $specialist->currency->name, "USD", true, false, true); ?></span>
<!--                <input type="text" class="blr-edit" data-for="blr" value="--><?// echo $priceService->convertToCurrencyNumber($specialist->price, $specialist->currency->name, "BLR") ?><!--"/>-->
<!--                <input type="text" class="usd-edit" data-for="usd" value="--><?// echo $priceService->convertToCurrencyNumber($specialist->price, $specialist->currency->name, "USD") ?><!--"/>-->
                <span class="change-price-title">Изменить цену</span>
            </div>
        </div>
    </div>
    <div class="category-menu except">
        <a class="manage-button trash" data-alt="Удалить специалиста из праздника" data-model-id="<? echo $specialist->id ?>" data-model-type="Specialist" href="/"></a>
    </div>
</div>

