<?php
/**
 * @var $this DefaultController
 * @var $user User
 * @var $model Holiday
 * @var $form CActiveForm
 * @var $type string
 */

?>


<style>
    .form {
        width:100%;
    }

	.add-service {
		width:100% !important;
		margin-top: 30px;
	}

	.add-button[disabled] {
		opacity: 1;
	}

	.add-button[disabled]:hover {
		border: 1px solid rgba(0,150,0,0.4);
		color: rgba(0,150,0,0.6);
		cursor: auto;
	}

	.add-button {
		-webkit-appearance: none;
		border: 1px solid rgba(0,150,0,0.4);
		background-color: white;
		width: 300px;
		display: block;
		margin: 10px 0;
		position: relative;
		text-align: center;
		font-size: 14px;
		color: rgba(0,150,0,0.6);
		text-decoration: none;
		padding: 11px 10px 12px 10px;
	}

	.add-button:hover {
		border: 1px solid rgba(0,145,0,0.8);
		color: rgba(0,145,0,1);
		cursor: pointer;
	}

	#photo_1Upload,
	#photo_2Upload,
	.bg-black {
		border-radius: 100%;
	}

	.wedding-table {
		width:100%;
	}

	.wedding-table td {
		width:50%;
	}

</style>

<link rel="stylesheet" href="/css/pickmeup/pickmeup.css"/>

<?php if($model->hasErrors()) : ?>
	<div class="flash-container">
		<div class="failed-flash">Заполните все неоходимые поля</div>
		<script>
			setTimeout(function(){
				$(".failed-flash").animate({opacity : 0}, 600, function(){
					$(".failed-flash").css("display", "none");
					$('.flash-container').remove();
				})
			}, 4000)
		</script>
	</div>
<?php endif; ?>

<h1 class="main-h1"><? echo $user->username; ?></h1>

<div id="content" class="container960">
<div class="col-span-3">
	<? $this->renderPartial('application.views.site._leftMenu', array(
		"active" => null,
		"model" => $model->author
	)); ?>
</div><div class="col-span-9">

	<?php if( $model->author_id == Yii::app()->user->id) : ?>

			<div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'holiday-form',
				'enableAjaxValidation'=>false,
				'errorMessageCssClass' => "add-service-error-message",
				"htmlOptions" => array(
					"encoding" => "multipart/form-data",
				)
			)); ?>

			<div class="row buttons">
				<?php echo CHtml::submitButton($model->is_created == 0 ? 'Создать праздник' : 'Сохранить', array("class" => "add-service add-service-button", 'id' => '', 'style'=>'margin-top:0;')); ?>
			</div>

			<ol class="params-list" style="margin-top:0;">

				<? // ------ ЕСЛИ ДЕНЬ РОЖДЕНИЯ ------- ?>
				<? if($type == "birthday") : ?>

					<input type="hidden" name="scenario" value="birthday"/>

					<li>Имя именинника <?php echo $form->error($model, 'title'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'title'); ?>
					</div>

					<li>Фотография именинника <?php echo $form->error($model,'photo_1'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
<!--						<div class="sub-title">Выберите главную фотографию. Для корректного отображения используйте квадратное изображение. Максимально допустимый размер - <b>5 Мб</b></div>-->

						<div id="photo_1Upload" class="avatarUpload" <? if($model->photo_1 != "") echo "style=\"background-image: url(/images/holidayAvatars/{$model->photo_1});\""; ?> >
							<div class="bg-black">
								<div id="percent">0%</div>
								<a id="discard" href="">Отмена</a>
							</div>
						</div>
						<? echo $form->fileField($model, 'photo_1', array("accept" => "image/*", "style" => "display:none;")); ?>
						<input type="button" name="photo_1UploadButton" class="uploadButton" value="Загрузить"/>
						<input type="hidden" name="photo_1" id="photo_1Hidden" value="<?=$model->photo_1;?>"/>
						<input type="hidden" name="model_id", value="<? echo $model->id ?>"/>

					</div>

					<li>Дата проведения Дня рождения <?php echo $form->error($model, 'date'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'date'); ?>
					</div>

					<li>Место проведения <?php echo $form->error($model, 'city_id'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'style' => 'width:160px', 'class' => 'item-region') ) ?>
						<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'style' => 'width:140px', 'class' => 'item-city') ) ?>
					</div>

				<? // ------ ЕСЛИ КОРПОРАТИВ ------- ?>
				<? elseif($type == "corporate") : ?>

					<input type="hidden" name="scenario" value="corporate"/>

					<li>Название организации <?php echo $form->error($model, 'title'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'title'); ?>
					</div>

					<li>Логотип или фотография <?php echo $form->error($model,'photo_1'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<div class="sub-title">Для идеального отображения загружайте квадратную фотографию</div>

						<div id="photo_1Upload" class="avatarUpload" <? if($model->photo_1 != "") echo "style=\"background-image: url(/images/holidayAvatars/{$model->photo_1});\""; ?> >
							<div class="bg-black">
								<div id="percent">0%</div>
								<a id="discard" href="">Отмена</a>
							</div>
						</div>
						<? echo $form->fileField($model, 'photo_1', array("accept" => "image/*", "style" => "display:none;")); ?>
						<input type="button" name="photo_1UploadButton" class="uploadButton" value="Загрузить"/>
						<input type="hidden" name="photo_1" id="photo_1Hidden" value="<?=$model->photo_1;?>"/>
						<input type="hidden" name="model_id", value="<? echo $model->id ?>"/>

					</div>

					<li>Дата проведения корпоратива <?php echo $form->error($model, 'date'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'date'); ?>
					</div>

					<li>Место проведения корпоратива <?php echo $form->error($model, 'city_id'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'style' => 'width:160px', 'class' => 'item-region') ) ?>
						<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'style' => 'width:140px', 'class' => 'item-city') ) ?>
					</div>

					<? // ------ ЕСЛИ CВАДЬБА ------- ?>
				<? elseif($type == "wedding") : ?>

					<input type="hidden" name="scenario" value="wedding"/>
					<? echo $form->hiddenField($model, 'title'); ?>
					<li>Имена жениха и невесты
						<?php

							if($model->hasErrors('name_1') && $model->hasErrors('name_2'))
								echo $form->error($model, 'name_1');
							elseif($model->hasErrors('name_1'))
								echo $form->error($model, 'name_1');
							elseif($model->hasErrors('name_2'))
								echo $form->error($model, 'name_2');
						?></li>
					<div>
						<table class="wedding-table">
							<tr>
								<td><? echo $form->textField($model, 'name_1'); ?></td>
								<td><? echo $form->textField($model, 'name_2'); ?></td>
							</tr>
						</table>


					</div>

					<li>Логотип или фотография <?php echo $form->error($model,'photo_1'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<div class="sub-title">Для идеального отображения загружайте квадратную фотографию</div>
						<table class="wedding-table">
							<tr>
								<td>
									<div id="photo_1Upload" class="avatarUpload" <? if($model->photo_1 != "") echo "style=\"background-image: url(/images/holidayAvatars/{$model->photo_1});\""; ?> >
										<div class="bg-black">
											<div id="percent">0%</div>
											<a id="discard" href="">Отмена</a>
										</div>
									</div>
									<? echo $form->fileField($model, 'photo_1', array("accept" => "image/*", "style" => "display:none;")); ?>
									<input type="button" name="photo_1UploadButton" class="uploadButton" value="Загрузить"/>
									<input type="hidden" name="photo_1" id="photo_1Hidden" value="<?=$model->photo_1;?>"/>
									<input type="hidden" name="model_id", value="<? echo $model->id ?>"/>
								</td>
								<td>
									<div id="photo_2Upload" class="avatarUpload" <? if($model->photo_2 != "") echo "style=\"background-image: url(/images/holidayAvatars/{$model->photo_2});\""; ?> >
										<div class="bg-black">
											<div id="percent">0%</div>
											<a id="discard" href="">Отмена</a>
										</div>
									</div>
									<? echo $form->fileField($model, 'photo_2', array("accept" => "image/*", "style" => "display:none;")); ?>
									<input type="button" name="photo_2UploadButton" class="uploadButton" value="Загрузить"/>
									<input type="hidden" name="photo_2" id="photo_2Hidden" value="<?=$model->photo_2;?>"/>
									<input type="hidden" name="model_id", value="<? echo $model->id ?>"/>
								</td>
							</tr>
						</table>

					</div>

					<li>Дата свадьбы <?php echo $form->error($model, 'date'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'date'); ?>
					</div>

					<li>Место проведения свадьбы <?php echo $form->error($model, 'city_id'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'style' => 'width:160px', 'class' => 'item-region') ) ?>
						<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'style' => 'width:140px', 'class' => 'item-city') ) ?>
					</div>

				<? else :  ?>
					<input type="hidden" name="scenario" value="other"/>

					<li>Название праздника <?php echo $form->error($model, 'title'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'title'); ?>
					</div>

					<li>Логотип или фотография <?php echo $form->error($model,'photo_1'); echo "<a class='error-anchor'></a>"; ?></li>
					<div>
						<div class="sub-title">Для идеального отображения загружайте квадратную фотографию</div>

						<div id="photo_1Upload" class="avatarUpload" <? if($model->photo_1 != "") echo "style=\"background-image: url(/images/holidayAvatars/{$model->photo_1});\""; ?> >
							<div class="bg-black">
								<div id="percent">0%</div>
								<a id="discard" href="">Отмена</a>
							</div>
						</div>
						<? echo $form->fileField($model, 'photo_1', array("accept" => "image/*", "style" => "display:none;")); ?>
						<input type="button" name="photo_1UploadButton" class="uploadButton" value="Загрузить"/>
						<input type="hidden" name="photo_1" id="photo_1Hidden" value="<?=$model->photo_1;?>"/>
						<input type="hidden" name="model_id", value="<? echo $model->id ?>"/>

					</div>

					<li>Дата проведения праздника <?php echo $form->error($model, 'date'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<? echo $form->textField($model, 'date'); ?>
					</div>

					<li>Место проведения праздника <?php echo $form->error($model, 'city_id'); echo "<a class='error-anchor'></a>"?></li>
					<div>
						<?php echo $form->dropDownList($model, 'region_id', Region::getRegionListByCountryId(1), array("prompt" => "Выберите регион", 'style' => 'width:160px', 'class' => 'item-region') ) ?>
						<?php echo $form->dropDownList($model, 'city_id', City::getCityListByRegionId($model->region_id), array("prompt" => "Выберите город", 'style' => 'width:140px', 'class' => 'item-city') ) ?>
					</div>
				<? endif; ?>

			</ol>



			<div class="row buttons">
				<?php echo CHtml::submitButton($model->is_created == 0 ? 'Создать праздник' : 'Сохранить', array("class" => "add-service add-service-button", 'id' => '')); ?>
			</div>

			<?php $this->endWidget(); ?>

	<? else : ?>

			<h1>Ошибка прав доступа</h1>

	<? endif; ?>

</div><!-- form -->

</div>
</div>

<script src="/scripts/XMLHttpRequest.js"></script>
<script src="/scripts/service.js"></script>
<script src="/scripts/gallerymanager.js"></script>
<script src="/scripts/is.min.js"></script>
<script src="/scripts/pickmeup/jquery.pickmeup.min.js"></script>

<script>

	$("#Holiday_date").pickmeup({
		format : "Y-m-d",
		position: "bottom",
		hide_on_select : true,
		min : new Date
	});

</script>

<script>

	var photo_1XHR = new XMLHttpRequest();

	photo_1XHR.onload = function(){
		$("#photo_1Upload").css("background-image", "url(/images/holidayAvatars/"+photo_1XHR.response+")");
		$("#photo_1Hidden").val(photo_1XHR.response);
		$("input[type='submit']").prop("disabled", false);
		$("#photo_1Upload .bg-black").css("opacity", 0);
	};

	photo_1XHR.upload.onerror = function(){
		$("#photo_1Upload .bg-black #percent").html("Ошибка");
	};

	photo_1XHR.upload.onprogress = function(event){
		$("#photo_1Upload .bg-black #percent").html(Math.round(event.loaded * 100 / event.total) + "%");
	};

	photo_1XHR.before = function(){
		$("#photo_1Upload .bg-black").css("opacity", 1);
		$("input[type='submit']").prop("disabled", true);
	};


	$("input[name='photo_1UploadButton'], #photo_1Upload .bg-black").click(function(){
		$("#Holiday_photo_1").click();
	});

	$("#Holiday_photo_1").change(function(){
		var fd = new FormData();
		fd.append("photo", this.files[0]);
		fd.append("model_id", $("input[name='model_id']").val());
		fd.append("photo_id", 1);

		photo_1XHR.open("POST", "/profile/holiday/loadPhoto");
		photo_1XHR.sendWithBefore(fd);

	});

	$("#photo_1Upload a#discard").click(function(){
		photo_1XHR.abort();
		$("#photo_1Upload .bg-black").css("opacity", 0);
		return false;
	});

	<? if($type == "wedding") : ?>
		var photo_2XHR = new XMLHttpRequest();

		photo_2XHR.onload = function(){
			$("#photo_2Upload").css("background-image", "url(/images/holidayAvatars/"+photo_2XHR.response+")");
			$("#photo_2Hidden").val(photo_2XHR.response);
			$("input[type='submit']").prop("disabled", false);
			$("#photo_2Upload .bg-black").css("opacity", 0);
		};

		photo_2XHR.upload.onerror = function(){
			$("#photo_2Upload .bg-black #percent").html("Ошибка");
		};

		photo_2XHR.upload.onprogress = function(event){
			$("#photo_2Upload .bg-black #percent").html(Math.round(event.loaded * 100 / event.total) + "%");
		};

		photo_2XHR.before = function(){
			$("#photo_2Upload .bg-black").css("opacity", 1);
			$("input[type='submit']").prop("disabled", true);
		};


		$("input[name='photo_2UploadButton'], #photo_2Upload .bg-black").click(function(){
			$("#Holiday_photo_2").click();
		});

		$("#Holiday_photo_2").change(function(){
			var fd = new FormData();
			fd.append("photo", this.files[0]);
			fd.append("model_id", $("input[name='model_id']").val());
			fd.append("photo_id", 2);

			photo_2XHR.open("POST", "/profile/holiday/loadPhoto");
			photo_2XHR.sendWithBefore(fd);

		});

		$("#photo_2Upload a#discard").click(function(){
			photo_2XHR.abort();
			$("#photo_2Upload .bg-black").css("opacity", 0);
			return false;
		});
	<? endif; ?>

</script>

<style>

	header {
		box-shadow: 0 -4px 10px black;
	}

	.block .info-title {
		font-size: 14px;
	}

	.block{
		float: none;
		display: block;
		width:100%;
		margin-top: 20px;
	}

	.error-title {
		position: absolute;
		width: 100%;
		margin-top: -35px;
		text-align: center;
		color: rgb(255,40,40);
		text-transform: uppercase;
	}

	input[type='submit']:disabled {
		opacity: 0.4;
	}

	.abort_image_loading {
		font-size: 14px;
		color:black;
		position: absolute;
		width:110px;
		height:110px;
		line-height:110px;
		text-align: center;
	}

	.abort_image_loading:hover {
		cursor: pointer;
	}

	.percent {
		position: absolute;
		left: 0;
		right: 0;
		margin-top: 31px;
	}

</style>

<?php Yii::app()->clientScript->registerScript(
	"changeRadio", '

			$("body,html").on("change", "#Holiday_region_id", function(){
				var value = $(this).val();

				var xhr = new XMLHttpRequest();
				var params = "id="+value;
				var obj = this;

				xhr.open("POST", "'.CController::createUrl("/core/ajaxGetChildrenСities").'", true);

				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

				xhr.onload = function () {
					$(obj).parent().children(".item-city").children().remove();
					$(obj).parent().children(".item-city").append(xhr.response);
				};

				xhr.send(params);

			});

			$(document).on("mouseenter", ".abort_image_loading", function() {
				$(this).html(\'Отмена\');
			});

			$(document).on("mouseleave", ".abort_image_loading", function() {
				$(this).html(\'<img src="/images/icons/loading_spinner.gif" width="50" height="50" alt=""/><div class="percent"></div>\');
			});

        ', CClientScript::POS_READY
);?>

