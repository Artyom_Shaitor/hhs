<?php

class RequestController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('requests', 'accept', 'deny', 'send'),
				'roles'=>array('3')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAccept()
	{
		if(isset($_POST['service_id']) && isset($_POST['holiday_id']) && isset($_POST['request_id'])) {
			/**
			 * @type $request Request
			 */
			$request = Request::model()->findByPk($_POST['request_id']);
			if($request->author_id == Yii::app()->user->id){
				/**
				 * @type $holidayService HolidayService
				 */
				$holidayService = HolidayService::model()->find("service_id=:sid AND holiday_id=:hid", array(
					":sid" => $_POST['service_id'],
					":hid" => $_POST['holiday_id'],
				));

				$calendarDay = CalendarDay::model()->find("service_id=:sid AND author_id=:aid AND date=:date", array(
					":sid" => $_POST['service_id'],
					":aid" => Yii::app()->user->id,
					":date" => date("Y-m-d", $holidayService->holiday->date),
				));


				if($calendarDay != null) {
					$calendarDay->request = 0;
					$calendarDay->reserved = 1;
					$calendarDay->save(false);
				}

				$holidayService->status = HolidayService::ACCEPTED_SERVICE;
				$request->status = Request::ACCEPTED_REQUEST;
				if($holidayService->save(false) && $request->save(false)){
					$result = array(
						"result" => true,
						"action" => "",
					);

					echo json_encode($result);
				}else{
					$result = array(
						"result" => false
					);

					echo json_encode($result);
				}
			}
		}
	}

	public function actionSend()
	{
		if(isset($_POST['service_id']) && isset($_POST['holiday_id']) && isset($_POST['reciever_id']) && isset($_POST["holiday_service_id"])){

			$request = new Request();

			/**
			 * @type $holiday Holiday
			 */
			$holiday = Holiday::model()->findByPk($_POST['holiday_id']);

			/**
			 * @type $holidayService HolidayService
			 */
			$holidayService = HolidayService::model()->findByPk($_POST['holiday_service_id']);

			$calendarDay = new CalendarDay();

			$request->status = Request::NEW_REQUEST;
			$request->author_id = Yii::app()->user->id;
			$request->holiday_id = $_POST['holiday_id'];
			$request->reciever_id = $_POST['reciever_id'];
			$request->service_id = $_POST['service_id'];
			$request->message = $_POST['message'];
			$request->phone = PhoneService::formatToInt($_POST['phone']);

			$holidayService->status = HolidayService::WAITING_SERVICE;

			$calendarDay->service_id = $_POST['service_id'];
			$calendarDay->author_id = $_POST['reciever_id'];
			$calendarDay->date = date("Y-m-d", $holiday->date);
			$calendarDay->reserved = 0;
			$calendarDay->request = 1;
			$calendarDay->weekend = 0;

			$exist = Request::model()->count("reciever_id=:rid AND service_id=:sid AND holiday_id=:hid AND author_id=:aid", array(
				":rid" => $_POST['reciever_id'],
				":sid" => $_POST['service_id'],
				":hid" => $_POST['holiday_id'],
				":aid" => Yii::app()->user->id,
			));

			if((int)$exist > 0){
				$result = array(
					"result" => false,
					"action" => "alert('Запрос уже отправлен')",
				);
				echo json_encode($result);
				return true;
			}

			if($request->save() && $calendarDay->save(false) && $holidayService->save(false)){
				$request->sendNotification();
				$result = array(
					"result" => true,
					"action" => "console.log(true)",
				);
				echo json_encode($result);
			}else {
				$result = array(
					"result" => false,
					"action" => "console.log(false)",
				);
				echo json_encode($result);
			}

		}
	}

	public function actionDeny()
	{
		if(isset($_POST['service_id']) && isset($_POST['holiday_id']) && isset($_POST['request_id'])) {
			/**
			 * @type $request Request
			 */
			$request = Request::model()->findByPk($_POST['request_id']);

			if($request->author_id == Yii::app()->user->id){
				/**
				 * @type $holidayService HolidayService
				 */

				$holidayService = HolidayService::model()->find("service_id=:sid AND holiday_id=:hid", array(
					":sid" => $_POST['service_id'],
					":hid" => $_POST['holiday_id'],
				));

				if($holidayService != null) {
					$holidayService->status = HolidayService::SIMPLE_SERVICE;

					$calendarDay = CalendarDay::model()->deleteAll("service_id=:sid AND author_id=:aid AND date=:date",
						array(
							":sid" => $_POST['service_id'],
							":aid" => Yii::app()->user->id,
							":date" => date("Y-m-d", $holidayService->holiday->date),
						));

					$holidayService->save(false);
				}

				//TODO : отменять заявку, если я сделал выходной в календаре

				$requestId = $request->id;
				$requestStatus = $request->status;
				if($request->delete()){
					$result = array(
						"result" => true,
						"action" => 'console.log("Просто изменять кнопки")',
					);

					echo json_encode($result);
				}else{
					$result = array(
						"result" => false
					);

					echo json_encode($result);
				}
			}
		}
	}

	public function actionRequests()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);

		$requests = Request::model()->findAll("author_id=:id", array(
			":id" => Yii::app()->user->id,
		));

		$newRequests = Request::pullRequests($requests, Request::NEW_REQUEST);
		$waitingRequests = Request::pullRequests($requests, Request::WAITING_REQUEST);

		$this->render('requests', array(
			"model" => $user,
			"requests" => $requests,
			"newRequests" => $newRequests,
			"waitingRequests" => $waitingRequests
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}