<?php

class DefaultController extends Controller
{
	public $layout = 'profileLayout';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'settings', 'changePassword', 'holidays', 'addHoliday'),
				'roles'=>array('1', '2', '3')
			),
			array('allow',
				'actions' => array('calendar','services', 'saveWeekend', 'deleteWeekend', 'AjaxSetDayStateGET'),
				'roles'=>array('2', '3')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$id = Yii::app()->user->id;
		$model = User::model()->findByPk($id);
		if(!isset($model)) {
			parent::redirectError();
			return;
		}

		$this->setPageTitle($model->username." - ". Yii::app()->name);

		$services = Service::loadServicesByAuthorId($model->id);

		$this->render('user.views.default.view', array(
			"model" => $model,
			"services" => $services,
			"id" => $id,
		));

	}


	public function actionCalendar()
	{
		$model = User::model()->findByPk(Yii::app()->user->id);

		$services = Service::loadServicesByAuthorId($model->id);

		$servicesList = Service::performServicesByCategory($services);

		$this->setPageTitle("Календарь - ". Yii::app()->name);

		$this->render('calendar', array(
			"model" => $model,
			"services" => $servicesList,
		));
	}

	public function actionSaveWeekend(){
		$day_number = 0;
		$year = 0;
		$month = 0;

		if(isset($_POST['day_number']))
			$day_number = $_POST['day_number'];
		else return null;

		if(isset($_POST['month']))
			$month = $_POST['month'];
		else return null;

		if(isset($_POST['year']))
			$year = $_POST['year'];
		else return null;

		$id = Yii::app()->user->id;

        /**
         * @var $modelExist Weekend[]
         */
        $modelExist = Weekend::model()->findAllByAttributes(array(
            "author_id" => Yii::app()->user->id,
            "year" => $year,
            "day_number" => $day_number,
        ));

        $zeroMonthExist = false;
        $monthExist = false;
        foreach($modelExist as $weekend){
            if( $weekend->month_number == 0)
                $zeroMonthExist = true;
            if( $weekend->month_number == $month )
                $monthExist = true;
			if( $weekend->month_number != 0 && $month == 0) {
				$zeroMonthExist = false;
				$monthExist = false;

				$conditions = array(
					"author_id" => Yii::app()->user->id,
					"year" => $year,
					"day_number" => $day_number,
				);

				Weekend::model()->deleteAllByAttributes($conditions);
			}
        }

        if($zeroMonthExist || $monthExist){
            Yii::app()->user->setFlash('failed', 'Выходной уже задан');
            $this->redirect("calendar");
        }


        $model = new Weekend;
        $model->month_number = $month ;
        $model->year = $year;
        $model->day_number = $day_number;
        $model->service_id = 0;
        $model->author_id = Yii::app()->user->id;


        if($model->save()){

            /**
             * Удаление записей из CalendarDay
             */
            $weekendArray = array();
            if($month != 0) {
                $days_count = date("t", strtotime($year . "-" . $month . "-01"));
                for ($i = 1; $i <= $days_count; $i++) {
                    $weekend = date("N", strtotime($year."-".$month . '-' . $i));
                    if ($weekend == $day_number ) $weekendArray[] = date("Y-m-d", strtotime($year."-".$month."-".$i));
                }

                CalendarDay::model()->deleteAllByAttributes(array(
                    "author_id" => Yii::app()->user->id,
                    "date" => $weekendArray
                ));
            }


            Yii::app()->user->setFlash('success', 'Выходной успешно добавлен');
            $this->redirect("calendar");
        }

	}

	public function actionDeleteWeekend(){
		if(isset($_POST['submit'])){
			$day_number = $_POST['day_number'];
			$month_number = $_POST['month_number'];
			$year = $_POST['year'];

            $conditions = array(
                "author_id" => Yii::app()->user->id,
                "day_number" => $day_number,
                "year" => $year,
            );

            if($month_number != "all") $conditions["month_number"] = $month_number;

            $deleted_rows = Weekend::model()->deleteAllByAttributes($conditions);
			if($deleted_rows > 0){
				Yii::app()->user->setFlash('success', 'Выходной успешно удален');
				$this->redirect("calendar");
			}else{
                Yii::app()->user->setFlash('failed', 'Что-то пошло не так :(');
                $this->redirect("calendar");
            }
		}
	}

	public function actionServices(){

		/**
		 * @var $model User
		 */
		$model = User::model()->findByPk(Yii::app()->user->id);

		$criteria = new CDbCriteria();
		$criteria->compare('author_id', Yii::app()->user->id);
		$criteria->compare('is_created', 1);

		$count=Service::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->applyLimit($criteria);

		$models = Service::model()->findAll($criteria);

		$this->setPageTitle("Мои услуги - ". Yii::app()->name);

		$this->render('my_services', array(
			'models' => $models,
			'pages' => $pages,
			"model" => $model,
			'serviceCount' => $count,
		));
	}

	public function actionChangePassword(){

		$user = User::model()->findByPk(Yii::app()->user->id);
		$user->setScenario('sc_changePassword');

		if(isset($_POST['User'])){

			$user->attributes = $_POST['User'];

			if($user->validate()){

				if( $user->password == PasswordService::encode($_POST['User']['old_password']) ){
					$user->password = PasswordService::encode($_POST['User']['new_password']);
					$user->save();
					Yii::app()->user->setFlash('success', 'Пароль успешно изменен');
					$this->redirect('/profile/settings');
				}
				else
					$user->addError('old_password', 'Не верно введен старый пароль');

			}

		}

		$this->setPageTitle("Сменить пароль - ". Yii::app()->name);

		$this->render('changePassword', array(
			"model" => $user,
		));

	}


	public function actionSettings(){

		$model = User::model()->findByPk(Yii::app()->user->id);
		$model->repeat_password = $model->password;

		$model->setScenario("sc_editUser");

		if(isset($_POST["User"])){
			$model->attributes = $_POST['User'];

			$model->about = nl2br($_POST['User']['about']);

			$filename = "";

			$model->avatar_url = $_POST['avatar'];

            if(preg_match("/http:\/\/+|https:\/\/+|www.+/", $model->url) == 1)
                $model->addError('url', 'Введите правильный адрес своей страницы');

			$model->phone = PhoneService::formatToInt($model->phone);
			$model->website = (trim($model->website) == "")? "" : UrlService::performWebsiteUrl($model->website);

			if($model->validate(null, false))
				if($model->save()){
					Yii::app()->user->setFlash('success', 'Настройки сохранены');
					$this->redirect('/profile');
				}

		}

		$this->setPageTitle("Настройки профиля - ". Yii::app()->name);

		$this->render('settings', array(
			"model" => $model,
		));
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}