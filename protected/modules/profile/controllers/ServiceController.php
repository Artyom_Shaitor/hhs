<?php

class ServiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/profileLayout';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'services'),
				'roles'=>array('1', '2', '3')
			),
			array('allow',
				'actions' => array('addService', 'loadImage', 'DeleteGalleryImage', 'loadAvatar'),
				'roles'=>array('2', '3')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionDeleteGalleryImage($id)
	{
		if(isset($_POST['submit'])) {
			$model = Gallery::model()->findByPk($id);
			$link = $model->image_url;
			if ($model->delete()) {

				$filename = './images/usersGalleryImages/'.$link;
				if(file_exists($filename) && !is_dir($filename))
					unlink($filename);

				echo json_encode(array("result" => true));
			}
		}

	}


	/**
	 * Performs the AJAX validation.
	 * @param Service $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='service-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionAddService(){


//		$model = new Service('sc_addService');
		/**
		 * @type $prices Price[]
		 */
		$_price_type = 1;

		$model = Service::loadEmpty('sc_addService');

		$prices = array();
		$user = User::model()->findByPk(Yii::app()->user->id);

		$this->setPageTitle(Yii::app()->name." - Создать услугу");

		if(isset($_POST['Service']))
		{
			$model->attributes=$_POST['Service'];
			$model->author_id = Yii::app()->user->id;
			$model->is_working_around = $_POST['Service']['is_working_around'];

			$model->description = nl2br($_POST['Service']['description']);

			$post = (isset($_POST['Price']))? $_POST['Price'] : array();


			if(count($post) > 0)
			foreach( $post as $i => $postPrice ){
				if(isset($prices[$i])){
					$prices[$i]->attributes = $postPrice;
				}
				else{
					$price = new Price();
					$price->attributes = $postPrice;
					$prices[] = $price;
				}
			}

			$model->avatar_url = $_POST['avatar'];

			if(isset($_POST['Service']['Job_Region']["city"]) && count($_POST['Service']['Job_Region']["city"]) > 0){
				$citiesArray = $_POST['Service']['Job_Region']["city"];
				$cities = [];

				foreach($_POST['Service']['Job_Region']['city'] as $index => $city){
					if($city['city_id'] != null) {
						$city = City::model()->findByPk($city['city_id']);
						$cities[] = $city;
					}
				}
				$model->city = array();
				$model->city = $cities;
			}
			else {
				$model->addError('city', 'Вы должны выбрать хотя бы один город');
			}

			$model->country_id = 1;

			if(isset($_POST['Video'])){

				$deletedVideo = Video::model()->deleteAll("author_id=:id && service_id=:sid", array(
					":id" => Yii::app()->user->id,
					":sid" => $model->id,
				));

				$maxVideos = ApplicationSettings::getSettings()->max_videos;
				$videoCount = 0;
				foreach($_POST['Video'] as $video_link){
					if(!empty($video_link)) {
						$video = new Video;
						$video->video_link = $video_link;
						$video->service_id = $model->id;
						$video->author_id = Yii::app()->user->id;
						$video->video_provider = VideoService::getVideoProvider($video);
						if($video->save())
							$videoCount++;
						if($videoCount >= $maxVideos) break;
					}
				}
			}

			$model->setOccupancy();
			if($model->validate('city', false))
			if($model->save()) {


				if(count($model->city) > 0)
					foreach($model->city as $city){
						$csa = new CityServiceAssignment();
						$csa->city_id = $city->id;
						$csa->service_id = $model->id;
						$csa->save();
					}


				if(isset($_POST['MetaData']))
					if(count($_POST['MetaData']) > 0)
						foreach($_POST['MetaData'] as $attribute_id => $value){

								$md = new MetaData();
								$md->attribute_id = $attribute_id;
								if(is_array($value))
									$value = implode(',', $value);
								$md->service_id = $model->id;
								$md->value = $value;
								$md->save();

						}

				if(count($prices) > 0)
					foreach ($prices as $p) {
						$p->service_id = $model->id;
						$p->save();
					}



				Yii::app()->user->setFlash('success', 'Услуга успешно добавлена');
				$this->redirect('/profile/services');
			}

		}

		$galleries = Gallery::model()->findAll("author_id=:id AND service_id=:sid", array(
			":id" => Yii::app()->user->id,
			":sid" => $model->id,
		));

		$model->title = $user->username;

		$this->setPageTitle("Добавить услугу - ". Yii::app()->name);

		$this->render('application.modules.profile.views.default.service', array(
			"model" => $model,
			"prices" => $prices,
			"user" => $user,
			"galleries" => $galleries,
			"id" => null,
			"price_type" => $_price_type,
		));


	}

	public function actionLoadAvatar()
	{
		$model = Service::model()->findByPk($_POST['model_id']);

		if($model){
			$filename = $model->upload($_FILES['avatar']);
			echo $filename;
			return true;
		}
		return false;
	}

	public function actionLoadImage()
	{
		if ( isset($_FILES['gallery_file']) ) {

			$count = Gallery::model()->count("service_id=:sid AND author_id=:aid", array(
				":sid" => $_POST['model_id'],
				":aid" => Yii::app()->user->id
			));

			$maxPhotos = ApplicationSettings::getSettings()->max_photos;
			$service = Service::loadModel($_POST['model_id']);

			if($count < $maxPhotos || $service->pro == 1) {

				$filename = basename($_FILES['gallery_file']['name']);
				$fileType = $_FILES['gallery_file']['type'];

				$fileType = substr($filename, strlen($filename) - 3, 3);

				$filename = ImageService::setImageFilename(Yii::app()->user->id) . "." . $fileType;

				$path = YiiBase::getPathOfAlias("webroot") . '/images/usersGalleryImages/' . $filename;

				$fileContent = file_get_contents($_FILES['gallery_file']['tmp_name']);

				$writeToDisk = file_put_contents($path, $fileContent);

				$appSettings = ApplicationSettings::model()->findByPk(1);
				ImageService::resizeToWidth($path, $appSettings->gallery_image_width,
					$appSettings->gallery_image_quality);

				$model = new Gallery;
				$model->author_id = Yii::app()->user->id;
				$model->image_url = $filename;
				$model->service_id = $_POST['model_id'];

				if ($model->save()) {
					$rsp = array(
						'error' => false,
						'filename' => $filename,
						'filepath' => "/" . ImageType::GALLERY_IMAGE . "/" . $filename,
						'model_id' => $model->id,
						'post' => $_POST['model_id']
					);

				}
			}else
				$rsp = array(
					'error' => 'Максимальное количество фотографий: '.$maxPhotos
				);

			echo json_encode($rsp);
		}

	}
}
