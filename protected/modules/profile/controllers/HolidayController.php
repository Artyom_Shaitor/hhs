<?php

class HolidayController extends Controller
{
	public $layout = 'profileLayout';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'loadPhoto', 'edit', 'delete', 'addHoliday', 'holidays', 'view',
					'changeCurrency', 'changePrice', 'changeNote', 'addServiceToHoliday', 'deleteService',
					'clearSession', 'addSpecialist', 'changeSpecialistData'),
				'roles'=>array('3')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionChangeSpecialistData()
	{
		if(isset($_POST['username']) && isset($_POST['phone']) && isset($_POST['category']) && isset($_POST['model_id'])){
			$specialist = Specialist::model()->findByPk($_POST['model_id']);

			if($specialist->author_id == Yii::app()->user->id){
				$specialist->username = htmlspecialchars($_POST['username']);
				$specialist->category_name = htmlspecialchars($_POST['category']);
				$specialist->phone = htmlspecialchars($_POST['phone']);

				if($specialist->save()){
					$result = array(
						'result' => true
					);
					echo json_encode($result);
					return true;
				}
			}
		}
	}

	public function actionAddSpecialist()
	{
		if(isset($_POST['model_id'])){
			$specialist = new Specialist();
//			$specialist->username = "Введите имя специалиста";
//			$specialist->category_name = "Введите специализацию";
			$specialist->author_id = Yii::app()->user->id;
//			$specialist->phone = "Введите номер телефона";
			$specialist->holiday_id = $_POST['model_id'];

			if($specialist->save(false)){
				$priceService = new PriceService();

				$result = array(
					"result" => true,
					"render" => $this->renderPartial('partial/specialist_loop', array(
						"priceService" => $priceService,
						"specialist" => $specialist
					), true),
				);

				echo json_encode($result);
				return true;
			}
		}
	}

	public function actionClearSession(){
		SessionService::clear("holiday_id");
		$array = [
			"url" => SessionService::clear("holiday_url")
		];
		echo json_encode($array);
		return true;
	}

	public function actionChangeNote()
	{
		if(isset($_POST['model_id']) && isset($_POST['note']) && isset($_POST['type'])){
			$class = $_POST['type'];
			/**
			 * @var $model IHolidayService
			 */
			$model = $class::model()->findByPk($_POST['model_id']);
			if($model != null && $model->getAuthorId() == Yii::app()->user->id){
				$model->changeNote(htmlspecialchars($_POST['note']));
				if($model->save(false)){
					echo "true";
					return true;
				}
				else return false;
			}
			else return false;
		}else return false;
	}

	public function deleteSpecialist($model_id)
	{
		$model = Specialist::model()->findByPk($model_id);
		if($model->author_id == Yii::app()->user->id){

			if($model->delete()) {
				$result = array(
					"result" => true,
					"type" => "Specialist"
				);

				echo json_encode($result);
			}
			return;
		}

	}

	public function actionDeleteService()
	{
		if(isset($_POST['model_id']) && isset($_POST['type'])){
			$modelId = $_POST['model_id'];
			$class = $_POST['type'];

			if($class == "Specialist") {
				$this->deleteSpecialist($modelId);
				return true;
			}


			/**
			 * @var $holidayService HolidayService
			 */
			$holidayService = HolidayService::model()->findByPk($modelId);

			if($holidayService->author_id == Yii::app()->user->id) {
				$value = $holidayService->service->subCategory->name;
				$dataCategoryName = str_replace(" ", "+", $holidayService->service->category->name);
				$dataSubCategoryName = str_replace(" ", "+", $holidayService->service->subCategory->name);
				$dataCategoryId = $holidayService->service->category->id;
				$dataSubCategoryId = $holidayService->service->subCategory->id;

				$holidayServiceStatus = $holidayService->status;
				$holidayServiceDate = $holidayService->holiday->date;

				if ($holidayService->delete()) {

					if($holidayServiceStatus != HolidayService::SIMPLE_SERVICE) {

						$deleteRequest = Request::model()->deleteAll("service_id=:sid AND holiday_id=:hid AND author_id=:aid",
							array(
								":sid" => $holidayService->service_id,
								":hid" => $holidayService->holiday_id,
								":aid" => Yii::app()->user->id,
							));

						$deleteCalendarDay = CalendarDay::model()->deleteAll("service_id=:sid AND date=:date AND author_id=:aid",
							array(
								":sid" => $holidayService->service_id,
								":date" => date("Y-m-d", $holidayServiceDate),
								":aid" => Yii::app()->user->id,
							));
					}

					$result = array(
						"result" => true,
						"type" => "HolidayService",
						"value" => $value,
						"data_category_name" => $dataCategoryName,
						"data_sub_category_name" => $dataSubCategoryName,
						"data_category_id" => $dataCategoryId,
						"data_sub_category_id" => $dataSubCategoryId,
						"render" => $this->renderPartial('partial/add_link_render', array(
							"priceService" => new PriceService(),
							"model" => $holidayService->holiday,
							"category" => $holidayService->service->subCategory,
							"currentCategoryLoop" => $holidayService->service->category->tag,
							"currentCategoryLoopCount" => 0,
						), true)
					);
					echo json_encode($result);
					return;
				}
			}
		}
		echo "false";
	}

	public function actionChangeCurrency()
	{
		if(isset($_POST['model_id']) && isset($_POST['currency_id'])){
			$model = HolidayService::model()->findByPk($_POST['model_id']);
			if($model != null && $model->author_id == Yii::app()->user->id){
				$model->currency_id = $_POST['currency_id'];
				if($model->save()){
					$priceService = new PriceService();
					$array = array(
						"blr" =>  $priceService->convert($model->price, $model->currency->name, "BLR"),
						"usd" => $priceService->convert($model->price, $model->currency->name, "USD"),
					);
					echo json_encode($array);
					return true;
				}
			}
			else return false;
		}else return false;
	}

	/**
	 * @param $service Service
	 * @throws CException
	 * @return string
	 */
	public function renderServiceHoliday($service)
	{
		$priceService = new PriceService();

		return$this->renderPartial('partial/services_loop', array(
			"service" => $service,
			"priceService" => $priceService,
		), true);
	}

	public function actionAddServiceToHoliday()
	{
		if(isset($_POST['service_id']) && isset($_POST['holiday_id'])){
			$serviceId = $_POST['service_id'];
			$holidayId = $_POST['holiday_id'];

			SessionService::clear("holiday_id");

			if(count(HolidayService::model()->findAll("service_id=:sid AND holiday_id=:hid AND author_id=:uid", array(
				":sid" => $serviceId,
				":hid" => $holidayId,
				":uid" => Yii::app()->user->id,
			))) > 0){
				$result = array(
					"result" => false,
					"message" => "Этот сервис уже добавлен в Ваш праздник"
				);
				echo json_encode($result);
				return true;
			}

			$model = new HolidayService();
			$model->author_id = Yii::app()->user->id;
			$model->holiday_id = $holidayId;
			$model->service_id = $serviceId;
			$model->status = HolidayService::SIMPLE_SERVICE;

			//TODO : Добавить условие, если передано сообщение для автора услуги

			if($model->save()){
				$service = $model->service;
				$service->holidayService = $model;
				$service->holidayService->price = (int)$service->price_value;
				$service->holidayService->currency_id = (int)$service->currency_id;

				$result = array(
					"result" => true,
					"message" => "",
					"title" => $service->title,
					"category" => $service->category->name,
					"category_id" => $service->category->id,
					"sub_category" => $service->subCategory->name,
					"sub_category_id" => $service->subCategory->id,
					"avatar_url" => ImageService::performImageUrl($service->avatar_url, ImageType::AVATAR),
					"phone" => PhoneService::formatToStr($service->author->phone),
					"price" => ((int)$service->price_value != 0)? (int)$service->price_value : $model->price,
					"currency_id" => (int)$service->currency_id,
					"currency_name" => PriceService::DEFAULT_CURRENCY,
//					"render" => $this->renderServiceHoliday($service),
				);
				echo json_encode($result);
				return true;
			}
			return false;

		}
		return false;
	}

	public function actionChangePrice()
	{
		if(isset($_POST['model_id']) && isset($_POST['price']) && isset($_POST['currency']) && isset($_POST['type'])){
			$class = $_POST['type'];
			/**
			 * @var $model IHolidayService|CActiveRecord
			 */
			$_POST['price'] = ($_POST['price'] == "NaN")? 0 : $_POST['price'];
			$model = $class::model()->findByPk($_POST['model_id']);
			if($model != null && $model->getAuthorId() == Yii::app()->user->id){
//				$model->price = $_POST['price'];
				$model->setPrice((int)$_POST['price']);
				$currencyId = Currencies::getCurrencyIdByName($_POST['currency']);
				if(!$currencyId)
					return false;
//				$model->currency_id = $currencyId;
				$model->setCurrency($currencyId);
				if($model->save()){
					$priceService = new PriceService();
					$array = array(
						"blr" =>  $priceService->convert($model->getPrice(), $model->getCurrency()->name, "BLR"),
						"usd" => $priceService->convert($model->getPrice(), $model->getCurrency()->name, "USD"),
					);
					echo json_encode($array);
					return true;
				}
			}
			else return false;
		}else return false;
	}

	public function actionLoadPhoto()
	{
		$model = Holiday::model()->findByPk($_POST['model_id']);
		$photoId = $_POST['photo_id'];

		if($model->author_id == Yii::app()->user->id){

			if($model){
				$filename = $model->upload($_FILES['photo'], $photoId);
				echo $filename;
				return true;
			}
			return false;
		}

		return false;

	}

	public function actionEdit($id)
	{
		$model = Holiday::model()->findByPk($id);
		$type = HolidayManager::getTypeString($model->type);
		$user = User::model()->findByPk(Yii::app()->user->id);

		if(!empty($model->date)) $model->date = date("Y-m-d", $model->date);

		if(isset($_POST['Holiday'])){
			$model->setScenario($_POST['scenario']);

			$model->title = $_POST['Holiday']['title'];
			$model->date = $_POST['Holiday']['date'];
			$model->region_id = $_POST['Holiday']['region_id'];
			$model->city_id = $_POST['Holiday']['city_id'];
			if(isset($_POST['Holiday']['name_1'])) $model->name_1 = $_POST['Holiday']['name_1'];
			if(isset($_POST['Holiday']['name_2'])) $model->name_2 = $_POST['Holiday']['name_2'];

//			$model->attributes = $_POST['Holiday'];

			if($model->save()){
				Yii::app()->user->setFlash('success', 'Праздник успешно изменён');
				$this->redirect("/profile/holidays");
			}
		}

		$this->render('holiday_edit', array(
			"user" => $user,
			"model" => $model,
			"type" => $type
		));
	}

	/**
	 * @param null|string $type
	 */
	public function actionAddHoliday($type = null)
	{
		$model = Holiday::createHoliday(HolidayManager::$_types[$type]);
		$user = User::model()->findByPk(Yii::app()->user->id);

		if(!empty($model->date)) $model->date = date("Y-m-d", $model->date); else $model->date = "";

		$model->title = $user->username;

		if(isset($_POST['Holiday'])){
			$model->setScenario($_POST['scenario']);

			$model->title = $_POST['Holiday']['title'];
			$model->date = $_POST['Holiday']['date'];
			$model->region_id = $_POST['Holiday']['region_id'];
			$model->city_id = $_POST['Holiday']['city_id'];
			if(isset($_POST['Holiday']['name_1'])) $model->name_1 = $_POST['Holiday']['name_1'];
			if(isset($_POST['Holiday']['name_2'])) $model->name_2 = $_POST['Holiday']['name_2'];

//			$model->attributes = $_POST['Holiday'];

			if($model->save()){
				Yii::app()->user->setFlash('success', 'Праздник успешно создан');
				$this->redirect("/profile/holidays");
			}
		}

		$this->render('holiday_edit', array(
			"user" => $user,
			"model" => $model,
			"type" => $type
		));
	}


	public function actionDelete($id)
	{
		$model = Holiday::model()->find("id=:id AND author_id=:aid", array(
			":id" => $id,
			":aid" => Yii::app()->user->id,
		));

		if($model == null)
			throw new CException("Такого праздника не существует");

		if($model->author_id == Yii::app()->user->id){
			$deleted = Holiday::model()->deleteByPk($id);

			if($deleted > 0){
				Yii::app()->user->setFlash('success', 'Праздник успешно удалён');
				$this->redirect("/profile/holidays");
			}
		}
		else
			throw new CException("Ошибка прав доступа");

	}

	public function actionHolidays()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		$models = Holiday::model()->findAll("author_id=:aid AND is_created=1", array(
			":aid" => Yii::app()->user->id
		));

		$this->render('my_holidays', array(
			"model" => $user,
			"models" => $models
		));
	}

	public function actionView($id)
	{
		$user = User::model()->findByPk(Yii::app()->user->id);

		/**
		 * @var $holidayService HolidayService[]
		 */
		$holidayServices = HolidayService::model()->findAll("holiday_id=:hid AND author_id=:aid", array(
			":hid" => $id,
			":aid" => Yii::app()->user->id
		));

		$model = (count($holidayServices) > 0)? $holidayServices[0]->holiday : $model = Holiday::model()->findByPk($id);



		if(isset($model)){
			$type = HolidayManager::getTypeString($model->type);
			$baseSubCategoriesList = Category::getCategoriesListByHolidayType($type);
            $baseCategoriesList = Category::model()->findAll('parent_cat_id = 0');
			$baseCategoriesList = Category::sortBaseCategoriesByIds($baseSubCategoriesList, $baseCategoriesList);

            $servicesArray = array();

            foreach($model->services as $service){
                $servicesArray[$service->category->name][] = $service;
            }

			$this->render('holiday_view', array(
				"model" => $model,
				"holidayServices" => $holidayServices,
				"user"  => $user,
				"type"  => $type,
				"baseSubCategoriesList" => $baseSubCategoriesList,
                "baseCategoriesList" => $baseCategoriesList,
                "servicesArray" => $servicesArray,
			));
		}else
			throw new CException("Праздник не найден");


	}
}