<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 08.10.15
 * Time: 17:56
 */

class FilterService {

    public static $TYPES = array(
        'number' => array(
            'capacity' => array(
                'from' => array( "value" => true, 'title' => 'от' ),
//                'to' => array( "value" => true, 'title' => 'до' ),
                'is' => array( "value" => false, 'title' => '' ),
            )
        ),
        'select' => array(
            'brand' => array(
                'prompt' => "Выберите марку авто"
            ),
            'color' => array(
                'prompt' => 'Выберите цвет'
            ),
        ),
        'checkbox' => array(
            'two_same_auto'
        ),
    );

    /**
     * Представляет input для заполнения при создании услуги
     * @param $model Service
     * @param $metaName string
     * @return bool|null
     * @throws CException
     */
    public static function performInput($model, $metaName){

        $attribute = Attributes::model()->with('metadata')->find("name=:n AND service_id=:sid", array(
            ":n" => $metaName,
            ":sid" => $model->id,
        ));

        $metaData = MetaData::model()->find("attribute_id=:aid", array(":aid" => $attribute->id));

        if( $attribute->type == 'text' ) {
            echo CHtml::textField($attribute->name, $metaData->value);
            return true;
        }

        $filterData = FilterData::model()->findAll("filter_data_id=:id ORDER BY value DESC", array(":id" => $attribute->filter_data_id));

        $inputType = ( !empty($attribute) ) ? $attribute->type : null;
        if ($inputType == null) return null;

        $activeValue = $metaData->value;
        $echo = "";

        switch($inputType){
            case 'select' : $echo = CHtml::dropDownList($attribute->name, $activeValue, CHtml::listData($filterData, 'id', 'value')); break;
            case 'number' : $echo = CHtml::textField($attribute->name, $activeValue); break;
            case 'radio' : $echo = CHtml::radioButtonList($attribute->name, $activeValue, CHtml::listData($filterData, 'id', 'value')); break;
            case 'checkbox' :
                $activeValue = explode(',',$metaData->value);
                $echo = CHtml::checkBoxList($attribute->name, $activeValue, CHtml::listData($filterData, 'id', 'value'));
            break;
            default : throw new CException("Фильтр не поддерживает тип ".$metaName); break;
        }

        echo $echo;
        return true;
    }

    /**
     * Представляет массив данных для построение таблицы при создании услуги
     * @param $category_id
     * @param $sub_category_id
     * @param null|Service $model
     * @return array
     * @throws CException
     */
    public static function performInputsInTable($category_id, $sub_category_id, $model=null){

        $resultArray = array();

        /**
         * @type $attributes Attributes[]
         */
        $attributes = Attributes::model()->findAll("category_id=:cid AND sub_category_id=:scid", array(
            ":cid" => $category_id,
            ":scid" => $sub_category_id,
        ));

        if(count($attributes) == 0) {
            /**
             * @type $category Category
             * @type $sub_category Category
             */
            return array();
        }

        foreach($attributes as $index => $attribute){

            $select = null;
            if($model != null)
                foreach ($model->metadata as $meta) {
                    if ($meta->attribute_id == $attribute->id) $select = $meta->value;
                }

//            $name = "MetaData[".$attribute->id."][".$attribute->name."]";
            $name = "MetaData[".$attribute->id."]";

            if( $attribute->type == 'text' || $attribute->type == 'number' ) {
                $resultArray[$attribute->title] = CHtml::textField($name, $select);
                continue;
            }


            $filterData = FilterData::model()->findAll("filter_data_id=:id", array(":id" => $attribute->filter_data_id));

            $inputType = ( !empty($attribute) ) ? $attribute->type : null;

            $echo = "";

            switch($inputType){
                case 'select' : $echo = CHtml::dropDownList($name, $select, CHtml::listData($filterData, 'id', 'value'), array('prompt' => self::$TYPES['select'][$attribute->name]['prompt'])); break;
                case 'radio' : $echo = CHtml::radioButtonList($name, $select, CHtml::listData($filterData, 'id', 'value')); break;
                case 'checkbox' :
                    $select = explode(',', $select);
                    $echo = CHtml::checkBoxList($name, $select, CHtml::listData($filterData, 'id', 'value'));
                    break;
                default : throw new CException("Фильтр не поддерживает тип "); break;
            }

            if($attribute->name == self::$TYPES['checkbox'][0])
                $resultArray[" "] = $echo;
            else
                $resultArray[$attribute->title] = $echo;

        }
        return $resultArray;

    }

    /**
     * @param $model Search
     * @return string pattern : MetaData[attribute_id][type] ([param])
     */
    public static function performInputsForFilterCatalog($model){
        $category_id = $model->category_id;
        $sub_category_id = $model->sub_category_id;

        $CSSClass = "meta-data-input";

        /**
         * @type $attributes Attributes[]
         */
        $attributes = Attributes::model()->findAll("category_id=:cid AND sub_category_id=:sid", array(
            ":cid" => $category_id,
            ":sid" => $sub_category_id
        ));

        $result = "";

        foreach($attributes as $attribute){
            if($attribute->type != 'checkbox') {
                $result .= $attribute->title . "<br>";
                $result .= '<div class="padding">';
            }
            switch($attribute->type){
                case 'number' :
                    if(isset(self::$TYPES['number'][$attribute->name]))
                        foreach(self::$TYPES['number'][$attribute->name] as $param => $value){
                            if($value['value'] == true)
                                $result .= $value['title']." ".CHtml::numberField('MetaData['.$attribute->id.'][number]['.$param.']', (isset($_GET['MetaData'][$attribute->id]['number'][$param])) ? $_GET['MetaData'][$attribute->id]['number'][$param] : "", array('class' => $CSSClass))."<br>";
                        }
                break;
                case 'select' :
                    $result .= CHtml::dropDownList('MetaData['.$attribute->id.'][select]', (isset($_GET['MetaData'][$attribute->id]['select'])) ? $_GET['MetaData'][$attribute->id]['select'] : 0, $attribute->getFilterToDataArray(), array('prompt' => self::$TYPES['select'][$attribute->name]['prompt'], 'class' => $CSSClass))."<br>";
                break;
                case 'checkbox' :
                    if($attribute->name == 'two_same_auto'){ //(isset($_GET['MetaData'][$attribute->id]['checkbox'])) ? $_GET['MetaData'][$attribute->id]['checkbox'] : false
                        $result .= CHtml::checkBox('MetaData['.$attribute->id.'][checkbox][]',(isset($_GET['MetaData'][$attribute->id]['checkbox']))? 1 : 0 , array('style' => 'margin-bottom:10px;', 'value' => $attribute->getValueIdInfFilter(), 'class' => $CSSClass))." ".$attribute->title."<br>";
                    }else{
                        $result .= "<table><tr><td>".$attribute->title."</td><td>".CHtml::checkBoxList('MetaData['.$attribute->id.'][checkbox][]',0,$attribute->getFilterToDataArray(), array('class' => $CSSClass))."</td></tr></table>";
                    }

                break;
            }
            if($attribute->type != 'checkbox') $result .= '</div>';
        }

        return $result;
    }


//    public static function find($attributeName, $attributeValue){
//
//        $model = Service::model()->with('metadata')->with('attribute')->findAll("attribute.name=:attr_name", array(
//            ":attr_name" => $attributeName
//        ));
//
//    }

//    public static function performInputForAdding($category_id, $sub_category_id, $name, $model=null){
//
//
//        if($model == null) {
//
//            /**
//             * @type $attribute Attributes
//             */
//            $attribute = Attributes::model()->find("name=:n AND category_id=:cid AND sub_category_id=:scid", array(
//                ":n" => $name,
//                ":cid" => $category_id,
//                ":scid" => $sub_category_id,
//            ));
//
//            if($attribute == null) {
//                /**
//                 * @type $category Category
//                 * @type $sub_category Category
//                 */
//                $category = Category::model()->findByPk($category_id);
//                $sub_category = Category::model()->findByPk($sub_category_id);
//                throw new CException("В подкатегории \"".$sub_category->name."\"(id=".$sub_category_id.") категории \"".$category->name."\"(id=".$category_id.") нет фильтра ".$name);
//            }
//
//            if( $attribute->type == 'text' || $attribute->type == 'number' ) {
//                echo CHtml::textField($attribute->name);
//                return true;
//            }
//
//            $filterData = FilterData::model()->findAll("filter_data_id=:id", array(":id" => $attribute->filter_data_id));
//
//            $inputType = ( !empty($attribute) ) ? $attribute->type : null;
//            if ($inputType == null) return null;
//
//            $echo = "";
//
//            switch($inputType){
//                case 'select' : $echo = CHtml::dropDownList($attribute->name, 0, CHtml::listData($filterData, 'id', 'value')); break;
//                case 'radio' : $echo = CHtml::radioButtonList($attribute->name, 0, CHtml::listData($filterData, 'id', 'value')); break;
//                case 'checkbox' :
//                    $echo = CHtml::checkBoxList($attribute->name, array(), CHtml::listData($filterData, 'id', 'value'));
//                    break;
//                default : throw new CException("Фильтр не поддерживает тип ".$name); break;
//            }
//
//            echo $echo;
//            return true;
//        }
//
//
//    }

}