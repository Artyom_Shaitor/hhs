<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 05.10.15
 * Time: 14:25
 */


class VideoService extends VideoThumbnailProvider{

    private static $providers  = array( 'youtube', 'vimeo' );

    private static $mask = array(
        'youtube' => "youtu.be",
    );

//    /**
//     * @param $model Video
//     */
//    public static function getVideoThumbnail($model){
//        $provider = $model->video_provider;
//        $method = "get".$provider."thumbnail";
//        return VideoThumbnailProvider::$method($model->video_link);
//    }

//    /**
//     * @param $model Video
//     */
//    public static function getVideoId($model){
//        $provider = $model->video_provider;
//        $method = "get".$provider."videoid";
//        return VideoThumbnailProvider::$method($model->video_link);
//    }

    /**
     * @param $model Video
     * @return mixed
     */
    public static  function getVideoPreview($model){
        $provider = $model->video_provider;
        $method = "get".$provider."video";
        return VideoThumbnailProvider::$method($model->video_link);
    }

    public static function getVideoThumbnail($model){
        $provider = $model->video_provider;
        $method = "get".$provider."thumbnail";
        return VideoThumbnailProvider::$method($model->video_link);
    }

    /**
     * @param $model Video
     * @return bool
     */
    public static function getVideoProvider($model){
        foreach(self::$providers as $provider){
            if( strpos($model->video_link, $provider) != false || strpos($model->video_link, self::$mask[$provider])){
                return $provider;
            }
        }
        return false;
    }
//    /**
//     * @param $model Video
//     */
//    public static function getVideoTitle($model){
//        $provider = $model->video_provider;
//        $method = "get".$provider."videotitle";
//        return VideoThumbnailProvider::$method($model->video_link);
//    }

}


class VideoThumbnailProvider {

    protected static function getyoutubevideo($url){
        $id = self::getyoutubevideoid($url);
        echo '<iframe width="641" height="400" src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
    }

    protected static function getyoutubevideoid($url){
        $id = str_replace("https://youtu.be/", '', $url);
        $id = str_replace("https://www.youtube.com/watch?v=", '', $id);
        return $id;
    }

    protected static  function getyoutubethumbnail($url){
        $id = self::getyoutubevideoid($url);
        return "http://img.youtube.com/vi/".$id."/hqdefault.jpg";
    }

    protected static  function getvimeothumbnail($url){
        $id = self::getvimeovideoid($url);

        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));

        return $hash[0]['thumbnail_large'];
    }


    protected static function getvimeovideo($url){
        $id = self::getvimeovideoid($url);
        echo '<iframe src="https://player.vimeo.com/video/'.$id.'" width="641" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }

    protected static function getvimeovideoid($url){
//        $url = substr($url,0,strpos($url, '?'));
        $id = preg_replace("/\D/", "", $url);
//        $array = explode('/', $url);
//        return (int) $array[count($array)-1];
        return $id;
    }

}