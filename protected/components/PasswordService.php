<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 25.08.15
 * Time: 12:03
 */

class PasswordService {

    private static $salt = "evb8rey7os";

    private static $UPPERLETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static $LOWERLETTERS = "abcdefghijklmnopqrstuvwzyz";
    private static $NUMBERS = "0123456789";

    const GENERATED_PASSWORD_LENGTH = 8;

    public static function encode($password){
        return md5($password.self::$salt).sha1($password);
    }

    public static function generatePassword($length = self::GENERATED_PASSWORD_LENGTH){

        $password = "";
        while(strlen($password) != $length){
            $symbolType = rand(0,2);

            switch($symbolType){
                case 0 :
                    $index = rand(0,strlen(self::$UPPERLETTERS)-1);
                    $password .= self::$UPPERLETTERS[$index];
                break;
                case 1 :
                    $index = rand(0,strlen(self::$LOWERLETTERS)-1);
                    $password .= self::$LOWERLETTERS[$index];
                break;
                case 2 :
                    $index = rand(0,strlen(self::$NUMBERS)-1);
                    $password .= self::$NUMBERS[$index];
                break;
            }
        }

        return $password;

    }
}