<?php

class WebUser extends CWebUser {
    private $_model = null;
//    private $_pro = null;
    private $_avatar_url = null;

    public function getAvatarUrl()
    {
        if($this->_avatar_url == null){
            $this->_avatar_url = User::model()->findByPk($this->id)->avatar_url;
        }
        return $this->_avatar_url;
    }

    function getRole() {
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->role;
        }
    }

    /**
     * @return integer
     */
    public function getNewRequests()
    {
        if(Yii::app()->user->getRole() >= 2) {
            $i = Yii::app()->user->id;
            $requests = Request::model()->count("reciever_id=:rid AND status=:status", array(
                ":rid" => Yii::app()->user->id,
                ":status" => Request::NEW_REQUEST
            ));
            return $requests;
        }
        return 0;
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

//    public function isPro(){
//        if($this->_pro == null) {
//            $this->_pro = User::model()->findByPk($this->id)->pro;
//            $this->_pro = ($this->_pro == 1)? $this->_pro = true : $this->_pro = false;
//        }
//
//        return $this->_pro;
//    }


}