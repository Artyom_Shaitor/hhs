<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 14.09.15
 * Time: 19:12
 */

class PhoneService {

    private static $pattern = '~.*(\d{3})[^\d]{0,7}(\d{2})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{2})[^\d]{0,7}(\d{2}).*~';

    /**
     * Perform phone number in +XXX(YY)NNN-NN-NN
     * @param $phone
     * @return string
     */
    public static function formatToStr($phone){

        if( preg_match( self::$pattern, $phone, $matches ) ){
            $result = "+" . $matches[1] . '(' .$matches[2] . ')' . $matches[3] . '-' . $matches[4] . '-' . $matches[5];
            return $result;
        }
    }

    /**
     * Perform phone number in XXXYYNNNNNNN
     * @param $phone
     * @return mixed
     */
    public static function formatToInt($phone){
        $phone = preg_replace("/\D/", "", $phone);

        $length = strlen($phone);
        $number_length = 9; # ** *** ** ** - 9 numbers

        $start = $length - $number_length;

        $phone = substr($phone, $start);

        return "375".$phone;
    }

}