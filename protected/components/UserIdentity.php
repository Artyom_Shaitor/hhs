<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 * @var $user User
 */

class UserIdentity extends CUserIdentity
{
	// Будем хранить id.
	protected $_id;
	protected $pro;
	protected $avatar_url;


	// Данный метод вызывается один раз при аутентификации пользователя.
	public function authenticate(){
		// Производим стандартную аутентификацию, описанную в руководстве.

		if($this->provider == null && $this->uid == null) {

			$user = User::model()->find('LOWER(email)=:email AND provider IS NULL ', array(":email" => strtolower($this->email)));
			if (($user === null) || (PasswordService::encode($this->password) !== $user->password)) {
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			} else {

				if ($user->ban == 1) throw new CHttpException(403, "Доступ запрещен");
				if ($user->is_confirmed == 0) throw new CHttpException(403, "Доступ запрещен. Вы должны подтвердить регистрацию");

				// В качестве идентификатора будем использовать id, а не username,
				// как это определено по умолчанию. Обязательно нужно переопределить
				// метод getId(см. ниже).
				$this->_id = $user->id;
				$this->pro = $user->pro;
				$this->avatar_url = $user->avatar_url;

				// Далее логин нам не понадобится, зато имя может пригодится
				// в самом приложении. Используется как Yii::app()->user->name.
				// realName есть в нашей модели. У вас это может быть name, firstName
				// или что-либо ещё.
//			$this->username = $user->username;

				$this->errorCode = self::ERROR_NONE;
			}

		}
		else{

			$user = User::model()->findByAttributes(array(
				'provider' => $this->provider,
				'uid' => $this->uid
			));

			if($user === null){
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			}
			else{
				if ($user->ban == 1) throw new CHttpException(403, "Доступ запрещен");

				$this->_id = $user->id;
				$this->errorCode = self::ERROR_NONE;
			}

		}
		return !$this->errorCode;
	}

	public function getId(){
		return $this->_id;
	}

	public function getPro(){
		return $this->pro;
	}

}