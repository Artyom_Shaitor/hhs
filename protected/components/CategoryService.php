<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 21.09.15
 * Time: 18:26
 */

class CategoryService {

    /**
     * @param $model Service
     * @return string
     */
    public static function performCategory($model){

        $category = $model->category->name;
        $subCategory = $model->subCategory->name;

        return $category.", ".$subCategory;

    }

    /**
     * @param $model Service
     * @return bool
     */
    public static function getCategoryIcon($model){
        $tag = $model->category->tag;
        return "/images/icons/category/".$tag.".png";
    }

}