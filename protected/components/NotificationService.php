<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 15.04.16
 * Time: 9:52
 */

class NotificationService {

    private static $rocketSMSLogin = 993017764;
    private static $rocketSMSPassword = "e8K3N4zw";

    public static function sendSMS($phone) {
		$curl = curl_init();

        $params = array(
            "username" => self::$rocketSMSLogin,
            "password" => self::$rocketSMSPassword,
            "phone" => PhoneService::formatToInt($phone),
            "text" => "Смска успешно доставлена"
        );

		curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));

		$result = @json_decode(curl_exec($curl), true);

		if ($result && isset($result['id'])) {
			return "Message has been sent. MessageID=" . $result['id'];
		} elseif ($result && isset($result['error'])) {
            return "Error occurred while sending message. ErrorID=" . $result['error'];
        } else {
            return "Service error";
        }
	}

}