<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 15.09.15
 * Time: 17:47
 */

class DescriptionService {

    const DESCRIPTIONMAXLENGTH = 350;
    const NAMEMAXLENGTH = 32;

    public static function performText($text, $max_length = self::DESCRIPTIONMAXLENGTH){
        if(mb_strlen($text, "UTF-8") > $max_length) {
            $text = str_replace("<br />", "<br>", $text);

            $text = mb_substr($text,0,$max_length, "utf-8");
            $lastSpacePosition = mb_strripos($text, " ", null, "utf-8");
            $text = mb_substr($text,0,$lastSpacePosition, "utf-8");
            return trim($text)."...";
        }
        else return $text;
    }

    public static function performUsername($text, $max_length = self::NAMEMAXLENGTH){
        if(mb_strlen($text, "UTF-8") > $max_length) {
            $text = mb_substr($text,0,$max_length, "utf-8");
//            $lastSpacePosition = mb_strripos($text, " ", null, "utf-8");
//            $text = mb_substr($text,0,$lastSpacePosition, "utf-8");

            return trim($text)."...";
        }
        else return $text;
    }



}