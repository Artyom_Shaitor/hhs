<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 11.03.16
 * Time: 16:52
 */

class HolidayManager {

    public static $_types = array(
        "wedding" => 0,
        "corporate" => 1,
        "birthday" => 2,
        "" => 3,
    );

    private static $_categories_list = array(
        "wedding" => array(
            72, 32, 33, 13, 44, 78, 25, 35, 39, /*Свадебный костюм*/
        ),
        "corporate" => array(
            72, 32, 33, 13, 46, 35
        ),
        "birthday" => array(
            32, 33, 13, 46
        ),
        "other" => array()

    );

    /**
     * @param $type string
     * @return bool
     */
    public static function isTypeExist($type)
    {
        return isset(self::$_categories_list[$type]);
    }

    /**
     * Get Categories list by holiday's type
     * @param $type string Holiday Type
     * @return bool|array Returns id's array if $type exist. Returns false otherwise
     */
    public static function getCategoriesListByHolidayType($type)
    {
        if(self::isTypeExist($type))
            return self::$_categories_list[$type];
        return false;
    }

    private static $_typesRU = array(
        0 => "Свадьба",
        1 => "Корпоратив",
        2 => "День рождения",
        3 => "Праздник",
    );

    public static function getTypeString($index)
    {
        foreach(self::$_types as $value => $numb)
            if($index == $numb) return $value;

        return false;
    }

    public static function getTypeStringRU($index)
    {
        foreach(self::$_typesRU as $numb => $value)
            if($index == $numb) return $value;

        return false;
    }

}