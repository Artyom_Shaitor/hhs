<?php


class ShareButtonsManager
{
    /**
     * @var SocialShareProvider[]
     */
    private $providers = array();

    public function __construct()
    {
        $this->providers = array(
            new VkSocialShareProvider(),
            new FacebookSocialShareProvider(),
            new TwitterSocialShareProvider(),
            new OdnoklassnikiSocialShareProvider(),
            new MailruSocialShareProvider()
        );

        $script = "Share = { ";

        /**
         * @var $provider SocialShareProvider
         */
        foreach($this->providers as $provider){
            $script .= $provider->getFunctionText()." {
             ".$provider->buildUrlJS()."
            },
            ";
        }

        $script .= "popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};";

        Yii::app()->clientScript->registerScript('social_sharing', $script);
    }

    public function buildButtons(IKeyWordsAdapter $model, $linkClassCss = "share-link")
    {
        foreach($this->providers as $provider){
            echo "<a class='{$linkClassCss}' onclick=\"{$provider->getOnClickFunction($model)}\">{$provider->getLinkInnerHtml()}</a>";
        }
    }

}

abstract class SocialShareProvider
{
    abstract public function getOnClickFunction(IKeyWordsAdapter $model);
    abstract public function getLinkInnerHtml();
    abstract public function buildUrlJS();
    abstract public function getFunctionText();
}

class VkSocialShareProvider extends SocialShareProvider
{

    public function getOnClickFunction(IKeyWordsAdapter $model)
    {
        $array = $model->getMetaTagArray();
        $desc = str_replace("\r\n", " ", $array['description']);
        $desc = str_replace("\n", " ", $desc);
        $desc = str_replace("<br>", " ", $desc);
        $desc = htmlspecialchars($desc);
        $questionSymbolPosition = strpos($array['url'], "?");

        if($questionSymbolPosition !== false){
            $array['url'] = substr($array['url'], $questionSymbolPosition);
        }

        return "Share.vkontakte('{$array['url']}', '{$array['title']}', '{$array['image']}', '".$desc."')";
    }


    public function getLinkInnerHtml()
    {
        return "VK";
    }

    public function buildUrlJS()
    {
        return
            "url  = 'http://vkontakte.ru/share.php?';
            url += 'url='          + encodeURIComponent(purl);
            url += '&title='       + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image='       + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);";
    }

    public function getFunctionText()
    {
        return "vkontakte: function(purl, ptitle, pimg, text)";
    }
}

class FacebookSocialShareProvider extends SocialShareProvider
{

    public function getOnClickFunction(IKeyWordsAdapter $model)
    {
        $array = $model->getMetaTagArray();
        $desc = str_replace("\r\n", " ", $array['description']);
        $desc = str_replace("\n", " ", $desc);
        $desc = str_replace("<br>", " ", $desc);
        $desc = htmlspecialchars($desc);

        $questionSymbolPosition = strpos($array['url'], "?");

        if($questionSymbolPosition !== false){
            $array['url'] = substr($array['url'], $questionSymbolPosition);
        }


        return "Share.facebook('{$array['url']}', '{$array['title']}', '{$array['image']}', '".$desc."')";
    }


    public function getLinkInnerHtml()
    {
        return "Facebook";
    }

    public function buildUrlJS()
    {
        return
            "url  = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]='     + encodeURIComponent(ptitle);
            url += '&p[summary]='   + encodeURIComponent(text);
            url += '&p[url]='       + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);";
    }

    public function getFunctionText()
    {
        return "facebook: function(purl, ptitle, pimg, text)";
    }
}

class MailruSocialShareProvider extends SocialShareProvider
{

    public function getOnClickFunction(IKeyWordsAdapter $model)
    {
        $array = $model->getMetaTagArray();
        $desc = str_replace("\r\n", " ", $array['description']);
        $desc = str_replace("\n", " ", $desc);
        $desc = str_replace("<br>", " ", $desc);
        $desc = htmlspecialchars($desc);

        $questionSymbolPosition = strpos($array['url'], "?");

        if($questionSymbolPosition !== false){
            $array['url'] = substr($array['url'], $questionSymbolPosition);
        }

        return "Share.mailru('{$array['url']}', '{$array['title']}', '{$array['image']}', '".$desc."')";
    }


    public function getLinkInnerHtml()
    {
        return "Mail.ru";
    }

    public function buildUrlJS()
    {
        return
            "url  = 'http://connect.mail.ru/share?';
            url += 'url='          + encodeURIComponent(purl);
            url += '&title='       + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl='    + encodeURIComponent(pimg);
            Share.popup(url);";
    }

    public function getFunctionText()
    {
        return "mailru: function(purl, ptitle, pimg, text)";
    }
}

class TwitterSocialShareProvider extends SocialShareProvider
{

    public function getOnClickFunction(IKeyWordsAdapter $model)
    {
        $array = $model->getMetaTagArray();

        $questionSymbolPosition = strpos($array['url'], "?");

        if($questionSymbolPosition !== false){
            $array['url'] = substr($array['url'], $questionSymbolPosition);
        }

        return "Share.twitter('{$array['url']}', '{$array['title']}')";
    }


    public function getLinkInnerHtml()
    {
        return "Twitter";
    }

    public function buildUrlJS()
    {
        return
            "url  = 'http://twitter.com/share?';
            url += 'text='      + encodeURIComponent(ptitle);
            url += '&url='      + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);";
    }

    public function getFunctionText()
    {
        return "twitter: function(purl, ptitle)";
    }
}

class OdnoklassnikiSocialShareProvider extends SocialShareProvider
{

    public function getOnClickFunction(IKeyWordsAdapter $model)
    {
        $array = $model->getMetaTagArray();
        $desc = str_replace("\r\n", " ", $array['description']);
        $desc = str_replace("\n", " ", $desc);
        $desc = str_replace("<br>", " ", $desc);
        $desc = htmlspecialchars($desc);

        $questionSymbolPosition = strpos($array['url'], "?");

        if($questionSymbolPosition !== false){
            $array['url'] = substr($array['url'], $questionSymbolPosition);
        }

        return "Share.odnoklassniki('{$array['url']}', '".$desc."')";
    }


    public function getLinkInnerHtml()
    {
        return "Одноклассники";
    }

    public function buildUrlJS()
    {
        return
            "url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl='    + encodeURIComponent(purl);
            Share.popup(url);";
    }

    public function getFunctionText()
    {
        return "odnoklassniki: function(purl, text)";
    }
}

