<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 06.04.16
 * Time: 11:08
 */

interface IHolidayService {

    /**
     * Change note of the model
     * @param $value
     * @return mixed
     */
    public function changeNote($value);

    /**
     * Get note
     * @return string
     */
    public function getNote();

    /**
     * Get Author's id
     * @return integer
     */
    public function getAuthorId();

    /**
     * Get Price
     * @return integer
     */
    public function getPrice();

    /**
     * Set Price
     * @param $value integer
     */
    public function setPrice($value);

    /**
     * Get Currency
     * @return Currencies
     */
    public function getCurrency();

    /**
     * Set Currency
     * @param $id integer
     */
    public function setCurrency($id);

}