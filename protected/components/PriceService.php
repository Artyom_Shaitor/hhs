<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.09.15
 * Time: 14:16
 */


class PriceService {

    const DEFAULT_CURRENCY = "BLR";
//    const XML_URL = "http://www.nbrb.by/Services/XmlExRates.aspx";
    const LAST_CURRENCIES_DATA = "last_currencies_data/last.xml";

    private $CURRENCY_TITLES = array(
        "BLR" => " бел.руб.",
        "USD" => "$",
//        "EUR" => "€",
//        "RUB" => "₽",
    );

    private $xml = null;
    private $currencies = array();
    private $priceTypes = array(
        0 => "",
        1 => "/час",
        2 => "/чел.",
        3 => "/сутки",
    );

    private $priceTypesTitles = array(
        0 => 'За услугу',
        1 => 'В час',
        2 => 'За человека',
        3 => "В сутки",
    );

    public static $priceTypesTitlesForCatalog = array(
        0 => 'за услугу',
        1 => 'за час',
        2 => 'за человека',
        3 => "за сутки",
    );

    private function getLastCurrenciesXML()
    {
//        try {
//            $file_get_contents_result = file_get_contents(self::XML_URL);
//            $this->xml = new SimpleXMLElement($file_get_contents_result);
//        }catch (Exception $e){
            $file_get_contents_result = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/".self::LAST_CURRENCIES_DATA);
            $this->xml = new SimpleXMLElement($file_get_contents_result);
//        }


    }

    public function __construct(){

        $this->getLastCurrenciesXML();
        $this->init();

    }

    public function getCurrenciesArray(){
        return $this->CURRENCY_TITLES;
    }

    public function getCurrenciesIDArray(){
        $currencies = Currencies::getAllCurrencies();
        $result = array();
        foreach($currencies as $id => $currency)
            $result[$currency] = $id;
        return $result;
    }

    public function getCurrencyTitle($currency){
        return $this->CURRENCY_TITLES[$currency];
    }

    /**
     * Метод, инициализирующий поле currencies, которое представляет собой массив коеффициентов
     */
    private function init(){

        #echo $xml->Currency[0]->Name;

        $currencies = Currencies::model()->findAll();

        $tempCurrArray = array();

        for($i = 0; $i < count($this->xml->Currency); $i++){

            foreach($currencies as $currency) {
                if($currency->name == $this->xml->Currency[$i]->CharCode){
                    $tempCurrArray[$currency->name] = $this->xml->Currency[$i]->Rate;
                }
            }

        }

        $tempCurrArray["BLR"] = 1;

        foreach($tempCurrArray as $i => $value){

            foreach($tempCurrArray as $j => $value1){
                $this->currencies[$i][$j] = $value/$value1;
            }

        }

    }

    /**
     * Конверсия в другую валюту дополнительных стоимостей. По умолчанию конвертирует в валюту DEFAULT_CURRENCY
     * @param $model Service
     * @return double price
     */
    public function convertToCurrencyWithAdditionalPrices($model, $index, $currency = self::DEFAULT_CURRENCY, $delimiter = " "){
        if( count($model->prices) > 0 )
            if(isset($model->prices[$index])) {
                $modelPrice = $model->prices[$index]->price_value;
                $modelCurrency = $model->prices[$index]->currency->name;

                $k = $this->currencies[$modelCurrency][$currency];


                $priceValue = number_format($modelPrice * $k, 0, '.', ' ');
                $priceType = $this->priceTypes[$model->prices[$index]->price_type];
                return "<span class='price-value'>$priceValue</span>".$delimiter."<span class='price-currency'>" . $this->getCurrencyTitle($currency) . "</span><span class='price-type'>$priceType</span>";
            }else return null;
        else return null;
    }

    /**
     * @param $value int
     * @param $currencyFrom string
     * @param $currencyTo string
     * @return int
     */
    public function convert($value, $currencyFrom, $currencyTo, $withoutDescription = false, $contentedtiable = false, $inInput = false)
    {
        $k = $this->currencies[$currencyFrom][$currencyTo];
        if($withoutDescription){
            $value = number_format($value*$k, 0, '.', ' ');
            if($inInput)
                $result = "<input class='price-value' value='".$value."' size='".(mb_strlen($value, "UTF-8")+1)."'>";
            else
                $result = "<div class='price-value' ".(($contentedtiable)? 'contenteditable="true"' : "" ).">".$value."</div>";
            $result .= "<span class='price-currency'>".$this->getCurrencyTitle($currencyTo)."</span>";
            return $result ;
        }
        else
            return number_format($value*$k, 0, '.', ' ');
    }


    /**
     * Конверсия в другую валюту. По умолчанию конвертирует в валюту DEFAULT_CURRENCY
     * @param $model Service|integer
     * @param string $currency Currency
     * @param $withoutDescription bool if true, result will be returned without currency and price type
     * @return float price
     */
    public function convertToCurrency($model, $currency = self::DEFAULT_CURRENCY, $withoutDescription = false){
        $modelPrice =  $model->price_value;

        $modelCurrency = $model->currency->name;

        $k = $this->currencies[$modelCurrency][$currency];

        $priceValue = number_format($modelPrice*$k, 0, '.', ' ');
        $priceType = $this->priceTypes[$model->price_type];

        $result = "<span class='price-value'>$priceValue</span>";
        if(!$withoutDescription) $result .= "<span class='price-currency'>".$this->getCurrencyTitle($currency)."</span><span class='price-type'>$priceType</span>";
        return $result ;
    }

    public function convertToCurrencyNumber($value, $fromCurrency, $toCurrency = self::DEFAULT_CURRENCY){
        $k = $this->currencies[$fromCurrency][$toCurrency];
        $priceValue = number_format($value*$k, 0, '.', '');
        return $priceValue ;
    }

    /**
     * Конверсия максимальной цены услуги в другую валюту. По умолчанию конвертирует в валюту DEFAULT_CURRENCY
     * @param $model Service|integer
     * @return double price
     */
    public function convertToCurrencyMaxPrice($model, $currency = self::DEFAULT_CURRENCY, $withoutDescription = false){
        $modelPrice =  $model->max_price_value;

        $modelCurrency = $model->currency->name;

        $k = $this->currencies[$modelCurrency][$currency];

        $priceValue = number_format($modelPrice*$k, 0, '.', ' ');
        $priceType = $this->priceTypes[$model->price_type];
        $result = "<span class='price-value'>$priceValue</span>";
        if(!$withoutDescription) $result .= "<span class='price-currency'>".$this->getCurrencyTitle($currency)."</span><span class='price-type'>$priceType</span>";
        return $result ;
    }

    /**
     * Конверсия минимальной цены услуги в другую валюту. По умолчанию конвертирует в валюту DEFAULT_CURRENCY
     * @param $model Service|integer
     * @return double price
     */
    public function convertToCurrencyMinPrice($model, $currency = self::DEFAULT_CURRENCY, $withoutDescription = false){
        $modelPrice =  $model->min_price_value;

        $modelCurrency = $model->currency->name;

        $k = $this->currencies[$modelCurrency][$currency];

        $priceValue = number_format($modelPrice*$k, 0, '.', ' ');
        $priceType = $this->priceTypes[$model->price_type];
        $result = "<span class='price-value'>$priceValue</span>";
        if(!$withoutDescription) $result .= "<span class='price-currency'>".$this->getCurrencyTitle($currency)."</span><span class='price-type'>$priceType</span>";
        return $result ;
    }

    public function perform($model, $currency = self::DEFAULT_CURRENCY, $styles = false){
        if(!$styles){
            $modelPrice = $model->price_value;
            $modelCurrency = $model->currency->name;

            $k = $this->currencies[$modelCurrency][$currency];

            $priceValue = number_format($modelPrice*$k, 0, '.', ' ');
            $priceType = $this->priceTypes[$model->price_type];
            return $priceValue." ".$this->getCurrencyTitle($currency).$priceType;
        }else{
            return $this->convertToCurrency($model, $currency);
        }
    }

    /**
     * @param $model Service
     * @param string $currency
     * @return string
     */
    public function performPricesFromTo($model, $currency = self::DEFAULT_CURRENCY){
        $result = "";
        $spaceFlag = false;

        if($model->min_price_value != 0 && $model->min_price_value != null){
            $result .= "от ".$this->convertToCurrencyMinPrice($model, $currency, ($model->max_price_value != 0 && $model->max_price_value != null) );
            $spaceFlag = true;
        }

        if($model->max_price_value != 0 && $model->max_price_value != null){
            $result .= ($spaceFlag)? " " : "";
            $result .= "до ".$this->convertToCurrencyMaxPrice($model, $currency);
        }


        return $result;
    }

    /**
     * Блок смены курса
     */
    public function changeCurrencyWidget(){

    }

    public function getPriceTypes($withTitles = false){
        if($withTitles) return $this->priceTypesTitles;
        return $this->priceTypes;
    }

    public function optionsInHtml($withTitles = false){

        $listData = $this->getPriceTypes($withTitles);

        $htmlOptions = array('encode'=>true);
        $result = CHtml::listOptions('0', $listData, $htmlOptions);
        $result = str_replace("\n", "", $result);

        return $result;

    }

    public function initJavaScript(){
        return "
            var currencies = ".json_encode($this->currencies).";

            $('.currency-change-block a').click(function(){
                var targetCurrency = $(this).html();

                $('.currency-change-block a.active').removeClass('active');
                $(this).addClass('active');

                $('.price').each(function(){

                    price_value = parseInt($(this).children('.price-value').html().replace(/\s*/g,''));
                    price_currency = $(this).children('.price-currency').html();

                    k = currencies[price_currency][targetCurrency];

                    price_value = price_value*k;
                    $(this).children('.price-value').html(price_value);
                    $(this).children('.price-currency').html(targetCurrency);
                });

                return false;
            })
        ";
    }


}