<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.04.16
 * Time: 13:33
 */

class SendMail {

    public static $DEFAULT_FIELDS = array(
        'username' => 'Имя пользователя',
        'email' => 'E-mail',
        'phone' => 'Телефон пользователя'
    );

    const DEFAULT_SEPERATOR = '%';

    public static function send($email, $subject, $message, $user)
    {
        $message = self::refactorMessage($message);
        $message = self::replaceDefaultFields($message, $user);

        $message = "
<html>
	<head>
		<title>Happy Holiday Service</title>
	</head>
	<body>
		{$message} <br/><br/>

С уважением, команда <a href=\"http://hhs.by\">hhs.by</a> ;) <br/><br/>

Тел.: <a href=\"tel:+375292441199\">+375(29)244-11-99</a> <br>
		E-mail: <a href=\"mailto:info@hhs.by\">info@hhs.by</a> <br>
		<a href=\"http://hhs.by\">http://hhs.by</a> <br>

	</body>";


        $from = "noreply@hhs.by";

        $headers="From: HHS.BY <noreply@hhs.by>\r\n".
            "Reply-To: noreply@hhs.by\r\n".
            "MIME-Version: 1.0\r\n".
            "Date:".date("d-n-Y")."\r\n".
            "Content-Type: text/html; charset=UTF-8";

        return mail($email,$subject,$message,$headers, "-f$from");
    }

    /**
     * Replace %field% on $user->field
     * @param $message string
     * @param $user User
     * @param string $seperator
     * @return mixed|string
     */
    private static function replaceDefaultFields($message, $user, $seperator = self::DEFAULT_SEPERATOR)
    {
        $pattern = "/\\".$seperator."([\w]+)\\".$seperator."/i";
        $replacement = '${1}';

        preg_match_all($pattern, $message, $matches);

        foreach($matches[0] as $index => $pattern){
            $message = preg_replace("/{$pattern}/i", $user->$matches[1][$index], $message);
        }

        return $message;
    }

    private static function refactorMessage($message)
    {
        $message = nl2br($message);
//        $message = str_replace('\r\n', '<br><br>', $message);
//        $message = str_replace('\\n', '<br><br>', $message);
        return $message;
    }

}