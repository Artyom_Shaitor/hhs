<?php


class ImageService {

    public static $source = "images/avatars";


    /**
     * Определяет, лежит ли изображение на сайте или является ссылкой на сторонний ресурс
     * @param string $url адрес изображения
     * @param ImageType $type
     * @return string
     */
    public static function performImageUrl($url, $type){
        $q = strpos($url, "http://");
        $q = strpos($url, "https://");
        if(strpos($url, "http://") === 0 || strpos($url, "https://") === 0 )
            return $url;
        if(trim($url) != "" )
            return "/".$type."/".trim($url);
        else {
            return "http://placehold.it/200x200";
        }
    }

    public static function performHolidayAvatarImage($url, $type)
    {
        $q = strpos($url, "http://");
        $q = strpos($url, "https://");
        if(strpos($url, "http://") === 0 || strpos($url, "https://") === 0 )
            return $url;
        if(trim($url) != "" )
            return "/".$type."/".trim($url);
        else {
            return "";
        }
    }

    /**
     * Метод проверяет, является ли ссылка на изображение внешней
     * @param string $url адрес изображения
     * @return bool
     */
    private static function isExternal($url){
        $pos = strpos(trim($url), "http://") || strpos(trim($url), "https://");
        if($pos === 0) return true;
        else return false;
    }


    /**
     * Метод выводит во view аватар услуги. Если аватар не найден, то поместит placeholder
     * @param $url
     * @param int $width ширина placeholder'a
     * @param int $height высота placeholder'a
     * @param array $styles
     * @return string
     */
    public static function appendImage($url, $imageType, $width = 0, $height = 0, $styles = array()){

        $style = " ";
        foreach($styles as $name => $value){
            $style .= " $name=\"$value\"";
        }

        $wxh = $width."x".$height;

        if(!self::isExternal($url)){
            $source = mb_substr(self::performImageUrl($url, $imageType), 1);

            if(file_exists($source))
                return "<div $style style=\"background-image: url(/$source)\"></div>";
            else {

                return "<div $style style=\"background-image: url(http://placehold.it/$wxh)\"></div>";
            }

        }
        else{
            //TODO : Возможно стоит убрать эту проверку
            $headers = @get_headers($url);
            $return = "";

            if (preg_match("/(200 OK)$/", $headers[0])) {
                $return = "<div $style style=\"background-image: url($url)\"></div>";
            } else {
                $return = "<div $style style=\"background-image: url(http://placehold.it/$wxh)\"></div>";
            }

            return $return;
        }


    }

    /**
     * Method generates a gallery image's name by author_id and time() function
     * @param int $id Author ID
     * @return string
     */
    public static function setImageFilename($id){
        return md5($id.time()).md5(rand(0, 999999));
    }

    /**
     * Method generates an user avatar image's name by author_id
     * @param int $id Author ID
     * @return string
     */
    public static function setUserAvatarFilename($id){
        return "avatar_".md5(time().$id);
    }

    /**
     * Method generates an service avatar image's name by author_id and time() function
     * @param int $id Author ID
     * @return string
     */
    public static function setServiceAvatarFilename($id){
        return "service_".md5(time().$id);
    }

    /**
     * Method generates a holiday avatar image's name by author_id and time() function
     * @param int $id Author ID
     * @return string
     */
    public static function setHolidayAvatarFilename($id){
        return "holiday_".md5(time().$id);
    }

    public static function resizeToWidth($loadFrom, $width, $compression = 75){
        $simpleImage = new SimpleImage($loadFrom);
        $simpleImage->resizeToWidth($width);
        if($compression == 75)
            $simpleImage->save($loadFrom);
        else
            $simpleImage->save($loadFrom, IMAGETYPE_JPEG, $compression);
    }

}

/**
 * Class ImageEnum
 * const int AVATAR
 */
class ImageType {
    const AVATAR = "images/avatars";
    const GALLERY_IMAGE = "images/usersGalleryImages";
    const HOLIDAY_IMAGE = "images/holidayAvatars";
}


/**
 * Class SimpleImage
 */
class SimpleImage {

    var $image;
    var $image_type;

    public function __construct($filename){
        $this->load($filename);

    }

    function load($filename) {
        $image_info = getimagesize($filename);

        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
    function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }
    function output($image_type=IMAGETYPE_JPEG) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image);
        }
    }
    function getWidth() {
        return imagesx($this->image);
    }
    function getHeight() {
        return imagesy($this->image);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }
    function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }
}

