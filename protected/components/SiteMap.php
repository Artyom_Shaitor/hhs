<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.04.16
 * Time: 15:39
 */

class SiteMap {

    const ALWAYS = 'always';
    const HOURLY = 'hourly';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
    const NEVER = 'never';

    private $items = [];

    public function render($return = false)
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $urlset = $dom->createElement('urlset');
        $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
        foreach($this->items as $item)
        {
            $url = $dom->createElement('url');

            foreach ($item as $key=>$value)
            {
                $elem = $dom->createElement($key);
                $elem->appendChild($dom->createTextNode($value));
                $url->appendChild($elem);
            }

            $urlset->appendChild($url);
        }
        $dom->appendChild($urlset);

        if($return) {
            return $dom->saveXML();
        }
        else {
            $file=fopen('sitemap.xml','w');
            fputs($file,$dom->saveXML());
            fclose($file);
        }
    }

    /**
     * @param $models CActiveRecord[]|ISiteMap[]
     * @param string $frequency
     * @param float $priority
     */
    public function addModule($models, $frequency = self::MONTHLY, $priority = 0.5)
    {
        foreach($models as $model){
            $item = array(
                "loc" => $model->getUrl(),
                "changefreq" => $frequency,
                "priority" => $priority
            );

            $this->items[] = $item;
        }
    }

    public function addUrl($url, $frequency = self::MONTHLY, $priority = 0.5)
    {
        $item = null;
        if(!is_array($url)) {
            $item = array(
                "loc" => Yii::app()->request->hostInfo . $url,
                "changefreq" => $frequency,
                "priority" => $priority
            );
            $this->items[] = $item;
        }
        elseif(count($url) > 0) {
            foreach($url as $link) {
                $item = array(
                    "loc" => Yii::app()->request->hostInfo . $link,
                    "changefreq" => $frequency,
                    "priority" => $priority
                );
                $this->items[] = $item;
            }

        }

    }


}