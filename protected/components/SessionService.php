<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 04.04.16
 * Time: 13:16
 */

class SessionService {
    const SESSION_KEY_PREFIX = "hhs.by_session_67gKDq5_";

    public static function set($key, $value)
    {
        Yii::app()->session[self::SESSION_KEY_PREFIX.$key] = $value;
    }

    public static function get($key)
    {
        return Yii::app()->session[self::SESSION_KEY_PREFIX.$key];
    }

    public static function clear($key )
    {
        $return = Yii::app()->session[self::SESSION_KEY_PREFIX.$key];
        unset(Yii::app()->session[self::SESSION_KEY_PREFIX.$key]);
        return $return;
    }

    public static function isEmpty($key)
    {
        if(!isset(Yii::app()->session[self::SESSION_KEY_PREFIX.$key]))
            return true;
        return false;
    }

}