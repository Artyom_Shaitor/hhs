<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 21.09.15
 * Time: 11:20
 * @var Weekend[] $additionalArray
 */

class DateService {

    public static $weekendsDayNames = array(
        "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"
    );

    public static $monthNames = array(
        "Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек."
    );

    /**
     * Метод получает название дня недели
     * @param int $day_number индекс дня недели
     * @param bool $index выводить ли день недели вместе с индексом. По умолчанию false
     * @return null|string
     */
    private static function getWeekendDayName($day_number, $index = false){
        if($day_number >= 1 && $day_number <= 7)
            return ($index)? $day_number."_".self::$weekendsDayNames[$day_number-1] : self::$weekendsDayNames[$day_number-1];
        return null;
    }

    private static function getMonthName($month_number){
        return self::$monthNames[$month_number-1];
    }



    /**
     * @param CalendarDay[] $model
     * @var $additionalArray Weekend[]
     * @return array
     */
    public static function datesToFullWeekendsList($model, $id){
        $resultArray = array();

        /**
         * Получить все записи из CalendarDay
         */


        foreach($model as $row){

            $year = date('Y', strtotime($row->date));
            $month = date('n', strtotime($row->date));
            $day = date('j', strtotime($row->date));

            if (!isset($resultArray[$year]))
                $resultArray[$year] = array();

            if(!isset($resultArray[$year][$month]))
                $resultArray[$year][$month] = array();

            if(!isset($resultArray[$year][$month][$day]))
                $resultArray[$year][$month][$day] = array(
                    "reserved" => ($row->reserved == 1)? true : false,
                    "request" => ($row->request == 1)? true : false,
                    "weekend" => ($row->weekend == 1)? true : false,
                    "global_weekend" => false,
                );
            else{
                if($resultArray[$year][$month][$day]["reserved"] == false && $row->reserved == 1) $resultArray[$year][$month][$day]["reserved"] = true;
                if($resultArray[$year][$month][$day]["request"] == false && $row->request == 1) $resultArray[$year][$month][$day]["request"] = true;
                if($resultArray[$year][$month][$day]["weekend"] == false && $row->weekend == 1) $resultArray[$year][$month][$day]["weekend"] = true;
            }


        }

        /**
         * Добавление в массив глобальных выходных
         * @var Weekend[] $additionalArray
         */
        $additionalArray = Weekend::model()->findAll("author_id=:id", array(":id" => $id));

        foreach($additionalArray as $weekendRow){
            $year = $weekendRow->year;
            $month = $weekendRow->month_number;
            $day_number = $weekendRow->day_number;

            if (!isset($resultArray[$year]))
                $resultArray[$year] = array();

            $monthsArray = ($month != 0)? array(0 => $month) : array(1,2,3,4,5,6,7,8,9,10,11,12);;

            foreach($monthsArray as $month) {

                $year_month = $year . "-" . $month;

                $weekendsArray = array();

                $days_count = date("t", strtotime($year_month . "-01"));
                for ($i = 1; $i <= $days_count; $i++) {
                    $weekend = date("N", strtotime($year_month . '-' . $i));
                    if ($weekend == $day_number && ( !isset($resultArray[$year][$month][$i]) || $resultArray[$year][$month][$i]['weekend'] == true) ) $weekendsArray[] = date("j", strtotime($year_month . '-' . $i));
                }

                if(!isset($resultArray[$year][$month]))
                    $resultArray[$year][$month] = array();

                foreach($weekendsArray as $day){
                    if(!isset($resultArray[$year][$month][$day]))
                        $resultArray[$year][$month][$day] = array(
                            "reserved" =>  false,
                            "request" =>  false,
                            "weekend" =>  false,
                            "global_weekend" => true,
                        );
                    else{
                        $resultArray[$year][$month][$day]["reserved"] = false;
                        $resultArray[$year][$month][$day]["request"] = false;
                        $resultArray[$year][$month][$day]["weekend"] = false;
                        $resultArray[$year][$month][$day]["global_weekend"] = true;
                    }
                }
            }



        }

        return $resultArray;
    }

    /**
     * @param User $model
     */
    public static function performWeekendDays($model, $year, $isIndex = false, $returnParams = false){
        $weekends = $model->weekends;
        $resultArray = array();


        if($weekends == null) return null;



        foreach($weekends as $weekend){

            if($weekend->year == $year) {
                $day_name = self::getWeekendDayName($weekend->day_number, $isIndex);
                if (!isset($resultArray[$day_name])) {
                    $resultArray[$day_name] = array();
                    $iter = 0;
                }
                if(!$returnParams)
                    $resultArray[$day_name][] = ($weekend->month_number == 0) ? "Весь год (".$year.")" : self::getMonthName($weekend->month_number);
                else {
                    $resultArray[$day_name][$iter] = array();
                    $resultArray[$day_name][$iter]['year_month'] = ($weekend->month_number == 0) ? "Весь год (".$year.")" : self::getMonthName($weekend->month_number);
                    $resultArray[$day_name][$iter]['month_number'] = $weekend->month_number;
                    $resultArray[$day_name][$iter]['day_number'] = $weekend->day_number;
                    $resultArray[$day_name][$iter]['year'] = $weekend->year;

                    $iter++;
                }
            }

        }

        return $resultArray;
    }



}



class Date {
    private $day;
    private $month;
    private $year;

    public function __construct($dateString){
        $timestamp = strtotime($dateString);

        $this->day = date("Y", $timestamp);
        $this->month = date("n", $timestamp);
        $this->year = date("j", $timestamp);
    }

    public function getDay(){
        return $this->day;
    }

    public function getMonth(){
        return $this->month;
    }

    public function getYear(){
        return $this->year;
    }
}