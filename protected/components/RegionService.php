<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 16.09.15
 * Time: 13:42
 */

class RegionService {

    /**
     * @param $model Service
     * @return string
     */
    public static function performRegionAddressForService($model, $withStyle = false){
        $address = "";

        if(count($model->city) > 0){

            $cities = array();
            foreach ($model->city as $city) {
                $cities[] = $city->name;

            };

            if($withStyle) {
                $address = '<div class="work">Работаю в:</div>';
                $address .= '<div class="region-address">';

                $address .= implode(', ', $cities);
                $address .= "</div>";
            }
            else $address = implode(', ', $cities);

            if($model->is_working_around == 1){
                $address .= "<div class='working-around'>Работаю на выезд</div>";
            }

        }

        return $address;
    }

    /**
     * @param $model User
     * @return string
     */
    public static function performRegionAddressForUser($model){
        $address = "";
        if(isset($model->region)) $address .= $model->region->name;
        if(isset($model->city)) $address .= ", ".$model->city->name;

        return $address;
    }

    /**
     * @param $model Holiday
     * @return string
     */
    public static function performRegionAddressForHoliday($model){
        $address = "";
        if(isset($model->region)) $address .= $model->region->name;
        if(isset($model->city)) $address .= ", ".$model->city->name;

        return $address;
    }

    /**
     * @param $model Request
     * @return string
     */
    public static function performRegionAddressForRequest($model){
        $address = "";
        if(isset($model->holiday->city)) $address .= $model->holiday->city->name;
        return $address;
    }

}