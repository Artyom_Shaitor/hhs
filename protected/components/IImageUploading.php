<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 18.01.16
 * Time: 14:49
 */

interface IImageUploading {
    /**
     * Uploading image
     * @param $file string
     * @return string|bool
     */
    public function upload($file);
}