<?php

/**
 * Interface IKeyWordsAdapter
 * Общий интерфейс для службы добавления метатегов
 */
interface IKeyWordsProvider
{
    public static function put(IKeyWordsAdapter $model);
}

/**
 * Interface IKeyWordsAdapter
 * Общий интерфейс для классов
 */


interface IKeyWordsAdapter
{
    /**
     * Interface IKeyWordsAdapter
     * Метод, возвращающий ассоциативный массив name и content (для создания метатегов)
     * @return array
     */
    public function getMetaTagArray();
}

/**
 * Class KeyWordsProvider
 * Класс, реализующий интерфейс IKeyWordsProvider
 * @package MetaTag
 */
class KeyWordsProvider implements IKeyWordsProvider
{
    /**
     * @var $socialProvider SocialProviderManager
     */
    public static $socialProvider;

    /**
     * Метод создает метатеги, соотвествующие полям, вызванные $model->getMetaTagArray(), а также подбирает
     * name для соц.сетей
     * @param IKeyWordsAdapter $model
     */
    public static function put(IKeyWordsAdapter $model)
    {

        if(!self::$socialProvider) {
            self::$socialProvider = new SocialProviderManager(array(
                new VkSocialMetaProvider(),
                new FacebookSocialMetaProvider()
            ));
        }

        self::$socialProvider->putModel($model);

    }

    public static function putData($name, $content)
    {
        if(!self::$socialProvider) {
            self::$socialProvider = new SocialProviderManager(array(
//                new VkSocialMetaProvider(),
                new FacebookSocialMetaProvider()
            ));
        }

        self::$socialProvider->putData($name, $content);
    }
}

class SocialProvider
{
    protected $prefix = "";

    public function getPrefix()
    {
        return ($this->prefix != "")? $this->prefix.":" : "";
    }

    public function putData($name, $content)
    {
        Yii::app()->clientScript->registerMetaTag($content, $this->getPrefix().$name);
    }
}

/**
 * Менеджер создания метатегов
 * Class SocialProviderManager
 */
class SocialProviderManager extends SocialProvider
{
    private $tagsArray = array();

    /**
     * @var array|SocialProvider[]
     */
    private $providers = array();

    /**
     * @param $providers SocialProvider[]
     */
    public function __construct($providers)
    {
        $this->providers = $providers;
        array_push($this->providers, new SocialProvider());
    }

    /**
     * @param IKeyWordsAdapter $model
     */
    public function putModel(IKeyWordsAdapter $model)
    {
        $tagsArray = $model->getMetaTagArray();
        foreach($this->providers as $provider){
            foreach($tagsArray as $name => $content){
                if(!isset($this->tagsArray[$provider->getPrefix().$name])) {
//                    Yii::app()->clientScript->registerMetaTag($content, $provider->getPrefix() . $name);
                    $provider->putData($name, $content);
                    $this->tagsArray[$provider->getPrefix().$name] = $content;
                }
            }
        }
    }


}

class VkSocialMetaProvider extends SocialProvider
{
    protected $prefix = "vk";
}

class FacebookSocialMetaProvider extends SocialProvider
{
    protected $prefix = "og";

    public function putData($name, $content){
        Yii::app()->clientScript->registerMetaTag($content, null, null, array("property" => $this->getPrefix().$name));
    }
}