<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 15.09.15
 * Time: 18:18
 */

class UrlService {

    /**
     * Задать url для Service
     * @param $model Service
     * @return string url
     */
    public static function performServiceUrl($model){
        if($model->author->url != "")
            return "/u/{$model->author->url}/".$model->id;
        else
            return "/service/view/".$model->id;
    }

    /**
     * Задать url для редактирования Service
     * @param $model Service
     * @return string url
     */
    public static function performEditServiceUrl($model){
        return "/service/edit/".$model->id;
    }

    /**
     * Задать url для удаления Service
     * @param $model Service
     * @return string url
     */
    public static function performDeleteServiceUrl($model){
        return "/service/delete/".$model->id;
    }

    /**
     * Задать url для User
     * @param $model User
     * @return string url
     */
    public static function performUserUrl($model){
        if($model->url == "")
            return "/user/view/".$model->id;
        else
            return "/u/".$model->url;
    }

    public static function performHolidayViewUrl($model)
    {
        return "/profile/holiday/view/".$model->id;
    }

    public static function performHolidayEditUrl($model)
    {
        return "/profile/holiday/edit/".$model->id;
    }

    public static function performHolidayDeleteUrl($model)
    {
        return "/profile/holiday/delete/".$model->id;
    }

    /**
     * @param $url
     * @return Website
     */
    public static function performWebsiteUrl($url){
        $httpPos = strpos($url, "http");

        $result = $url;

        if($httpPos === false){
            $result = 'http://'.$result;
        }

        $website = new Website($result);

        return $website->getUrl();
    }

    public static function addHttpToWebsiteUrl($url){
        $httpPos = strpos($url, "http://");
        if($httpPos === false)
            return "http://".$url;
        return $url;
    }
}

class Website {
    private $url;
    private $title;

    public function __construct($url) {
        $this->url = $url;
        $this->title = $this->processing_url($url);
    }

    private function processing_url($url)
    {
        if(strcasecmp(substr($url,0,7), 'http://') == 0) $url = substr($url, 7);
        if(strcasecmp(substr($url,0,7), 'https://') == 0) $url = substr($url, 8);
        if(strcasecmp(substr($url,0,4), 'www.') == 0) $url = substr($url, 4);
        $url_len = strlen($url)-1;
        if(strcasecmp(substr($url, $url_len), '/') == 0) $url = substr($url, 0, $url_len);
        return $url;
    }

    public function getUrl(){
        return $this->url;
    }

    public function getTitle(){
        return $this->title;
    }
}