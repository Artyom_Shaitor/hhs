<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.08.15
 * Time: 17:59
 */

class CheckPage {

    /**
     * Проверяет, совпадает ли параметр $page с адресом текущей страницы
     * @param $page String
     * @return bool
     */
    static function isPage($page){
        $urlArray = explode('/', $page);

        if(count($urlArray) == 2){
            $controller = Yii::app()->controller->getId();
            $action = Yii::app()->controller->action->getId();

            if( $page == $controller.'/'.$action ) return true;
            return false;
        }
        elseif(count($urlArray) == 3) {
            $controller = Yii::app()->controller->getId();
            $action = Yii::app()->controller->action->getId();
            $module = (isset(Yii::app()->controller->module->id))? Yii::app()->controller->module->id."/" : "";

            if( $page == $module.$controller.'/'.$action ) return true;
            return false;
        }

    }

    /**
     * Проверяет, совпадает ли с адресом текущей страницы хоть один элемент массива
     * @param $pages array
     * @return bool
     */
    static function isPages($pages){

        foreach($pages as $page){
            if( self::isPage($page) ) return true;
        }
        return false;

    }
}