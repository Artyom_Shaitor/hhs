<?php

/**
 * This is the model class for table "holiday".
 *
 * The followings are the available columns in table 'holiday':
 * @property integer $id
 * @property integer $author_id
 * @property string $title
 * @property integer $date
 * @property integer $type
 * @property integer $region_id
 * @property integer $city_id
 * @property City $city
 * @property integer $is_created
 * @property string $name_1
 * @property string $name_2
 * @property string $photo_1
 * @property string $photo_2
 * @property Service[] $services
 * @property User $author
 * @property Specialist[] $specialists
 *
 * types = [
 * 		0 : wedding
 * 		1 : corporate
 * 		2 : birthday
 * 		3 : other
 * ]
 *
 *
 */
class Holiday extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'holiday';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('author_id, type, region_id, city_id', 'numerical', 'integerOnly'=>true),
			array('title, name_1, name_2', 'length', 'max'=>256),
			array('photo_1, photo_2', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, author_id, title, date, type, region_id, city_id, name_1, name_2, photo_1, photo_2', 'safe', 'on'=>'search'),

			array('title', 'required', "on" => "birthday", "message" => "Необходимо указать имя именинника"),
			array('date', 'required', "on" => "birthday", "message" => "Необходимо указать дату проведения дня рождения"),
			array('city_id', 'required', "on" => "birthday", "message" => "Необходимо указать место проведения дня рождения"),

			array('title', 'required', "on" => "corporate", "message" => "Необходимо указать название организации"),
			array('date', 'required', "on" => "corporate", "message" => "Необходимо указать дату проведения корпоратива"),
			array('city_id', 'required', "on" => "corporate", "message" => "Необходимо указать место проведения корпоратива"),

			array('name_1, name_2', 'required', "on" => "wedding", "message" => "Необходимо указать имена жениха и невесты"),
			array('date', 'required', "on" => "wedding", "message" => "Необходимо указать дату проведения корпоратива"),
			array('city_id', 'required', "on" => "wedding", "message" => "Необходимо указать место проведения корпоратива"),

			array('title', 'required', "on" => "other", "message" => "Необходимо указать название праздника"),
			array('date', 'required', "on" => "other", "message" => "Необходимо указать дату проведения праздника"),
			array('city_id', 'required', "on" => "other", "message" => "Необходимо указать место проведения праздника"),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'services' => array(self::MANY_MANY, 'Service', 'holiday_service(holiday_id, service_id)', 'with'=>'category', 'order' => 'category.id ASC' ),
			'specialists' => array(self::HAS_MANY, 'Specialist', 'holiday_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'author_id' => 'Author',
			'title' => 'Title',
			'date' => 'Date',
			'type' => 'Type',
			'region_id' => 'Region',
			'city_id' => 'City',
			'name_1' => 'Name 1',
			'name_2' => 'Name 2',
			'photo_1' => 'Photo 1',
			'photo_2' => 'Photo 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date);
		$criteria->compare('type',$this->type);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('name_1',$this->name_1,true);
		$criteria->compare('name_2',$this->name_2,true);
		$criteria->compare('photo_1',$this->photo_1,true);
		$criteria->compare('photo_2',$this->photo_2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave(){
		$this->date = strtotime($this->date);
		if($this->name_1 != "" && $this->name_2 != "")
			$this->title = $this->name_1. " & ".$this->name_2;
		if($this->title != "") $this->is_created = 1;
		return parent::beforeSave();
	}

	public function beforeDelete(){
		if(isset($this->photo_1)) unlink('./images/holidayAvatars/'.$this->photo_1);
		if(isset($this->photo_2)) unlink('./images/holidayAvatars/'.$this->photo_2);

		return parent::beforeDelete();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Holiday the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Return new empty "Holiday" model
	 * @param $type
	 * @return Holiday|null
	 */
	public static function createHoliday($type)
	{
		$emptyHolidayCount = Holiday::model()->count("is_created=0 && author_id=:id", array(
			":id" => Yii::app()->user->id,
		));

		if($emptyHolidayCount > 0) {
			$model = Holiday::model()->find("is_created=0 && author_id=:id", array(
				":id" => Yii::app()->user->id,
			));

			$model->type = $type;
			$model->is_created = 0;
			if($model->save(false))
				return $model;
		}

		$model = new Holiday();
		$model->type = $type;
		$model->author_id = Yii::app()->user->id;
		$model->is_created = 0;

		if($model->save(false))
			return $model;
		return null;
	}

	/**
	 * Uploading image
	 * @param $file string
	 * @return string|bool
	 */
	public function upload($file, $photoId)
	{
		$filename = basename($file['name']);
		$fileType = $file['type'];
		$fileType = substr($fileType, 6);
		$filename = ImageService::setHolidayAvatarFilename(Yii::app()->user->id).".".$fileType;
		$path = YiiBase::getPathOfAlias("webroot") . '/images/holidayAvatars/' . $filename;
		$fileContent = file_get_contents($file['tmp_name']);

		$defaultAvatar = ($photoId == 1)? YiiBase::getPathOfAlias("webroot") . '/images/holidayAvatars/' . $this->photo_1 : YiiBase::getPathOfAlias("webroot") . '/images/holidayAvatars/' . $this->photo_2;

		if(file_exists($defaultAvatar) && !is_dir($defaultAvatar))
			unlink($defaultAvatar);

		$writeToDisk = file_put_contents($path, $fileContent);
		$appSettings = ApplicationSettings::model()->findByPk(1);
		ImageService::resizeToWidth($path, $appSettings->avatar_width, $appSettings->gallery_image_quality);

		switch($photoId){
			case "1" : $this->photo_1 = $filename; break;
			case "2" : $this->photo_2 = $filename; break;
			default : return false; break;
		}

		$this->save(false);

		return $filename;
	}
}
