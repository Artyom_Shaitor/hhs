<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 10.11.15
 * Time: 13:33
 */

/**
 * Class Search
 * @property $metaCondition CDbCriteria
 */
class Search extends CFormModel{

    public $max_price_value;
    public $price_type;
    public $category_id;
    public $old_category_id;
    public $sub_category_id;
    public $date;
    public $currency_id;
    public $region_id;
    public $city_id;
//    public $metaData;


    public function attributeNames()
    {
        // TODO: Implement attributeNames() method.
    }

    public function refactorDate($date)
    {
        $day = date("d",strtotime($this->date));
        $month = date("m", strtotime($this->date));
        $year = date("Y", strtotime($this->date));

        return "$year-$month-$day";
    }
}