<?php
/**
 * Created by PhpStorm.
 * User: artyomshaitor
 * Date: 20.04.16
 * Time: 16:06
 */

class Catalog {

    public static function getAllCatalogUrls()
    {
        $items = [];
        $items[] = '/catalog';
        /**
         * @type $subCategories Category[]
         */
        $subCategories = Category::model()->sub()->findAll();
        if(count($subCategories) > 0){
            foreach($subCategories as $subCategory){
                $subCatName = str_replace(" ", "+", $subCategory->name);
                $сatName = str_replace(" ", "+", $subCategory->parentCategory->name);
                $items[] = "/catalog/{$сatName}/{$subCatName}";
            }
        }

        return $items;
    }

}