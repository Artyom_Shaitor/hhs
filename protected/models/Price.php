<?php

/**
 * This is the model class for table "price".
 *
 * The followings are the available columns in table 'price':
 * @property integer $id
 * @property integer $service_id
 * @property integer $price_type
 * @property double $price_value
 * @property integer $currency_id
 * @property string $description
 */
class Price extends CActiveRecord
{

	private static $TYPES = array(
		'0' => "Обычная",
		'1' => "В час",
		'2' => "человек"
	);
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'price';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('service_id, price_type, currency_id', 'numerical', 'integerOnly'=>true),
			array('description, price_value', 'required'),
			array('description', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, service_id, price_type, price_value, currency_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'currency' => array(self::BELONGS_TO, 'Currencies', 'currency_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'service_id' => 'Service',
			'price_type' => 'Price Type',
			'price_value' => 'Price Value',
			'currency_id' => 'Currency',
			'description' => 'Описание подуслуги',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('service_id',$this->service_id);
		$criteria->compare('price_type',$this->price_type);
		$criteria->compare('price_value',$this->price_value);
		$criteria->compare('currency_id',$this->currency_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Price the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTypes(){
		return self::$TYPES;
	}

	public static function optionsInHtml(){

		$listData = self::getTypes();

		$htmlOptions = array('encode'=>true);
		$result = CHtml::listOptions('0', $listData, $htmlOptions);
		$result = str_replace("\n", "", $result);

		return $result;

	}

}
