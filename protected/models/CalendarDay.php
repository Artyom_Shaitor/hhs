<?php

/**
 * This is the model class for table "calendar_day".
 *
 * The followings are the available columns in table 'calendar_day':
 * @property integer $id
 * @property integer $service_id
 * @property $date
 * @property integer $reserved
 * @property integer $request
 * @property integer $weekend
 */
class CalendarDay extends CActiveRecord
{
	public $author_id;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calendar_day';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, service_id, reserved, request, weekend, author_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, service_id, date, reserved, request, weekend, author_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'service_id' => 'Service',
			'date' => 'Date',
			'reserved' => 'Reserved',
			'request' => 'Request',
			'weekend' => 'Weekend',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('service_id',$this->service_id);
		$criteria->compare('date',$this->date, true);
		$criteria->compare('reserved',$this->reserved);
		$criteria->compare('request',$this->request);
		$criteria->compare('weekend',$this->weekend);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CalendarDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
