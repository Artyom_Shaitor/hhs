<?php

/**
 * This is the model class for table "application_settings".
 *
 * The followings are the available columns in table 'application_settings':
 * @property integer $id
 * @property integer $avatar_width
 * @property integer $gallery_image_width
 * @property integer $gallery_image_quality
 * @property integer $max_photos
 * @property integer $max_videos
 * @property integer $default_currency
 */
class ApplicationSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'application_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('avatar_width, gallery_image_width, gallery_image_quality', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, avatar_width, gallery_image_width, gallery_image_quality', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'avatar_width' => 'Avatar Width',
			'gallery_image_width' => 'Gallery Image Width',
			'gallery_image_quality' => 'Gallery Image Quality',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('avatar_width',$this->avatar_width);
		$criteria->compare('gallery_image_width',$this->gallery_image_width);
		$criteria->compare('gallery_image_quality',$this->gallery_image_quality);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ApplicationSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return ApplicationSettings
	 */
	public static function getSettings(){
		return self::model()->findByPk(1);
	}
}
