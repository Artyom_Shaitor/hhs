<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property integer $parent_cat_id
 * @property Category parentCategory
 * @property string $tag
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_cat_id', 'numerical', 'integerOnly'=>true),
			array('name, tag', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent_cat_id, tag', 'safe', 'on'=>'search'),
		);
	}

	public function scopes()
	{
		return array(
			'sub'=>array(
				'condition' => 'parent_cat_id IS NOT NULL AND parent_cat_id != 0',
			),
		);
	}

	/**
	 * @param $holiday Holiday
	 * @return string
	 */
	public function buildCatalogQuery($holiday)
	{
//		$query = "?Search[city_id]={$holiday->city_id}&Search[region_id]={$holiday->region_id}&Search[date]=".date("d-m-Y", $holiday->date)."&holiday_id={$holiday->id}";
		$query = "?Search[date]=".date("d-m-Y", $holiday->date)."&holiday_id={$holiday->id}";
		if($this->parentCategory == null){
			$query = "/catalog/".str_replace(" ", "+", $this->name)."/{$query}";
		}
		else
			$query = "/catalog/".str_replace(" ", "+", $this->parentCategory->name)."/".str_replace(" ", "+", $this->name).$query;
		return $query;

	}

	/**
	 * @return $this Category
	 */
	public function sort()
	{
//		return array(
//			'sorted' => array(
//				'condition' => 'order by name ASC'
//			),
//		);
		$this->getDbCriteria()->mergeWith(array(
			'order' => 'name ASC'
		));

		return $this;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parentCategory' => array(self::BELONGS_TO, 'Category', 'parent_cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_cat_id' => 'Parent Cat',
			'tag' => "Ярлык"
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_cat_id',$this->parent_cat_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getAllParentCategories($excludeCategories = null){
		$condition = "";
		if(count($excludeCategories) > 0){
			$condition = "AND id NOT IN (".implode(',', $excludeCategories).")";
		}
		$array = Category::model()->findAll("parent_cat_id IS NULL OR parent_cat_id=0 $condition");

		return CHtml::listData($array, 'id', 'name');

	}

	public function getParentCategory()
	{
		if($this->parentCategory == null || $this->parent_cat_id == 0)
			return $this;
		return $this->parentCategory;
	}


	/**
	 * @param $array Category[]
	 * @return Category[]
	 */
	public static function refactorSubCategoriesList(&$array)
	{
		if(count($array) != 0) {
			$i = 0;
			while ($i < count($array) && $array[$i]->name != 'Другое' ) {
				$i++;
			}
			if (count($array) == $i) {
				return $array;
			} else {
				$category = $array[$i];
				unset($array[$i]);
				$array[] = $category;
			}
		}
		return $array;
	}

	/**
	 * @param $baseSubCategoriesList Category[]
	 * @param $baseCategoriesList Category[]
	 * @return Category[]
	 */
	public static function sortBaseCategoriesByIds($baseSubCategoriesList, $baseCategoriesList)
	{
		$idsArray = [];
		foreach($baseSubCategoriesList as $subCategory){
			if(!in_array($subCategory->getParentCategory(), $idsArray)) {
				$idsArray[] = $subCategory->getParentCategory();
			}
		}
		$resultArray = [];
		foreach($idsArray as $key1 => $category)
			foreach($baseCategoriesList as $key2 => $baseCategory){
				if($baseCategory == $category){
					$temp = $baseCategoriesList[$key2];
					$baseCategoriesList[$key2] = $baseCategoriesList[$key1];
					$baseCategoriesList[$key1] = $temp;
				}
			}
		return $baseCategoriesList;
	}

	/**
	 * Get Categories list by holiday's type
	 * @param $type string Holiday Type
	 * @return Category[]|array Returns category array if $type exist. Returns false otherwise
	 */
	public static function getCategoriesListByHolidayType($type)
	{
		if($categoriesIdArray = HolidayManager::getCategoriesListByHolidayType($type) ){
			$criteria = new CDbCriteria();
			$criteria->order = 'FIELD(id, '.implode(',', $categoriesIdArray).')';
			$criteria->addInCondition('id', $categoriesIdArray);
			$query = $criteria->condition;
			return Category::model()->findAll($criteria);
		}
		return array();
	}

	/**
	 * @return bool
	 */
	public function getCategoryIcon(){
		$tag = ($this->tag == "")? $this->getParentCategory()->tag : $this->tag;
		return "/images/icons/category/".$tag.".png";
	}

	public static function getAllChildrenCategories($parent_id){
		if($parent_id == 0 || $parent_id == null) $array = array();
		else {
			$array = Category::model()->sort()->findAll("parent_cat_id=:id", array(
				":id" => $parent_id,
			));
			self::refactorSubCategoriesList($array);
		}


		if(empty($array)) return array();
		return CHtml::listData($array, 'id', 'name');
	}

	public static function getAllChildrenCategoriesInUL($category, $start = 0, $limit = 0){

		$result = '<div class="categories-list-block">';
		if($start == 0)
			$result .= '<a href="/catalog/'.str_replace(" ", "+", $category->name).'">'.$category->name.'</a>';

		$array = Category::model()->sort()->findAll("parent_cat_id=:id", array(
			":id" => $category->id,
		));

		self::refactorSubCategoriesList($array);
		$result .= "<ul>";

		$i = 0;
		foreach($array as $index => $subCategory){
			if($index >= $start && ($limit == 0 || $limit >= $i)) {
				$result .= '<li><a href="/catalog/' . str_replace(" ", "+", $category->name) . '/'.str_replace(" ", "+", $subCategory->name).'">' . $subCategory->name . '</a></li>';
				$i++;
			}
		}
		
//		return CHtml::listData($array, 'id', 'name');

		$result .= '</ul></div>';
		return $result;
	}
}
