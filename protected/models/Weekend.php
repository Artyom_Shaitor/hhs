<?php

/**
 * This is the model class for table "weekend".
 *
 * The followings are the available columns in table 'weekend':
 * @property integer $id
 * @property integer $day_number
 * @property integer $month_number
 * @property integer $year
 * @property integer $service_id
 * @property integer $author_id
 */
class Weekend extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'weekend';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('day_number, month_number, year, service_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, day_number, month_number, year, service_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'day_number' => 'Day Number',
			'month_number' => 'Month Number',
			'year' => 'Year',
			'service_id' => 'Service',
			'author_id' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('day_number',$this->day_number);
		$criteria->compare('month_number',$this->month_number);
		$criteria->compare('year',$this->year);
		$criteria->compare('service_id',$this->service_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Weekend the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Проверяет, является ли выбранный день глобальным выходным
	 * @param $date
	 * @return bool
	 */
	public static function isGlobalWeekend($date){
		$year = date("Y", strtotime($date));
		$month = date("n", strtotime($date));
		$day = date("j", strtotime($date));

		$day_number = date("N", strtotime($date));

		$model = Weekend::model()->findAll("day_number=:dn AND ( month_number=:mn OR month_number=0 ) AND year=:y", array(
			":dn" => $day_number,
			":mn" => $month,
			":y" => $year
		));

		if(count($model) > 0) return true;
		return false;

	}

}
