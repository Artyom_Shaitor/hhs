<?php

/**
 * This is the model class for table "specialist".
 *
 * The followings are the available columns in table 'specialist':
 * @property integer $id
 * @property string $username
 * @property string $category_name
 * @property string $phone
 * @property string $note
 * @property integer $holiday_id
 * @property integer $author_id
 * @property integer $price
 * @property integer $currency_id
 * @property User $author
 * @property Holiday $holiday
 * @property Currencies $currency
 */
class Specialist extends CActiveRecord implements IHolidayService
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'specialist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('holiday_id, author_id, price, currency_id', 'numerical', 'integerOnly'=>true),
			array('username, category_name', 'length', 'max'=>128),
			array('phone', 'length', 'max'=>25),
			array('note', 'length', 'max'=>8000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, category_name, phone, note, holiday_id, author_id', 'safe', 'on'=>'search'),
		);
	}

	protected function beforeSave(){
		if($this->isNewRecord) {
			$this->price = 0;
			$this->currency_id = 1;
		}
		return parent::beforeSave();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			"author" => array(self::BELONGS_TO, "User", "author_id"),
			"holiday" => array(self::BELONGS_TO, "Holiday", "holiday_id"),
			"currency" => array(self::BELONGS_TO, "Currencies", "currency_id"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'category_name' => 'Category Name',
			'phone' => 'Phone',
			'note' => 'Note',
			'holiday_id' => 'Holiday',
			'author_id' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('category_name',$this->category_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('holiday_id',$this->holiday_id);
		$criteria->compare('author_id',$this->author_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Specialist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Change note of the model
	 * @param $value
	 * @return mixed
	 */
	public function changeNote($value)
	{
		$this->note = $value;
	}

	/**
	 * Get Author's id
	 * @return integer
	 */
	public function getAuthorId()
	{
		return $this->author_id;
	}

	/**
	 * Get Price
	 * @return integer
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * Set Price
	 * @param $value integer
	 */
	public function setPrice($value)
	{
		$this->price = $value;
	}

	/**
	 * Get Currency
	 * @return Currencies
	 */
	public function getCurrency()
	{
		return $this->currency;
	}

	/**
	 * Set Currency
	 * @param $id integer
	 */
	public function setCurrency($id)
	{
		$this->currency_id = $id;
	}

	/**
	 * Get note
	 * @return string
	 */
	public function getNote()
	{
		$note = $this->note;
		$note = htmlspecialchars_decode($note);
		$note = str_replace("<br />", "", $note);
		$note = str_replace("<br/>", "", $note);
		$note = str_replace("<br>", "", $note);

		return $note;
	}
}
