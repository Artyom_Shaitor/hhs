<?php

Yii::import('application.extensions.Validators.userExistence');

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $is_confirmed
 * @property integer $ban
 * @property string $username
 * @property string $url
 * @property integer $created
 * @property integer $last_paid_date
 * @property string $password
 * @property integer $role
 * @property string $phone
 * @property string $avatar_url
 * @property string $about
 * @property string $email
 * @property bool $terms
 * @property Country $country
 * @property Region $region
 * @property City $city
 * @property int $country_id
 * @property int $pro
 * @property int $region_id
 * @property int $city_id
 * @property CalendarDay[] calendarDay
 * @property Weekend[] weekends
 * @property Service[] service
 */
class User extends CActiveRecord implements IImageUploading, IKeyWordsAdapter, ISiteMap
{
	const ROLE_ADMIN = 'administrator';
	const ROLE_CONTRACTOR = 'contractor';
	const ROLE_USER = 'user';
	const ROLE_BANNED = 'banned';

	public
		$role,
		$terms,
		$old_password,
		$new_password,
		$repeat_password;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	public function scopes()
	{
		return array(
			'published'=>array(
				'condition' => 'is_confirmed = 1 AND ban = 0',
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(


			array('username, password, email', 'required', 'on' => 'registration'),

			array('username, email, password, city_id, region_id', 'required', 'on' => 'catalogRegistration'),
			array('phone', 'required', 'message' => 'Необходимо указать телефон', 'on' => 'catalogRegistration'),
//			array('phone', 'unique', "attributeName" => "phone", "message" => "Номер <b>+{value}</b> уже используется в системе", "on" => 'catalogRegistration, sc_editUser, sc_addUser'),

			array('country_id, about, email, phone', 'required', 'on' => 'sc_editUser'),
			array('region_id, city_id', 'required', 'on' => 'sc_editUser', "message" => "\"{attribute}\""),
			array('username', 'required', 'on' => 'sc_editUser'),

			array('new_password', 'compare', 'compareAttribute'=>'repeat_password', 'on' => 'sc_changePassword'),
			array('old_password, new_password, repeat_password', 'required', 'on' => 'sc_changePassword'),

			array('terms', 'compare', 'compareValue' => true, 'message' => 'Вы должны принять условия <a href="/user-agreement.pdf" target="_blank">HHS.BY</a>', 'on' => 'registration, catalogRegistration'),
			array('created, role', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>128),
			array('password', 'length', 'max'=>256),
			array('phone', 'length', 'max'=>20),
			array('avatar_url', 'length', 'max'=>512),
			array('avatar_url', 'required', 'on' => 'sc_editUser', 'message' => 'Необходимо выбрать изображение'),
			array('about', 'length', 'max'=>1500),
			array('email, website', 'length', 'max'=>45),
			array('created', 'length', 'max' => 9999999999),
			array('email', 'userExistence', "on" => "catalogRegistration, registration"),

			array('url', 'unique', "attributeName" => "url", "message" => "Адрес <b>{value}</b> уже используется в системе"),
			array('email', 'email', 'message' => 'Ваш e-mail не является правильным e-mail адресом'),



			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, created, password, ban, url, is_confirmed, role, pro, provider, phone, avatar_url, about, last_paid_date, email, city_id, region_id, country_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'service' => array(self::HAS_MANY, 'Service', 'author_id'),
			'weekends' => array(self::HAS_MANY, 'Weekend', 'author_id', 'order' => 'year ASC, day_number ASC, month_number ASC'),
			'calendarDay' => array(self::HAS_MANY, 'CalendarDay', 'author_id', 'order' => 'date ASC, month ASC, day ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Имя пользователя',
			'created' => 'Дата регистрации',
			'last_paid_date' => 'Последняя оплата',
			'password' => 'Пароль',
			'role' => 'Роль',
			'website' => 'Сайт',
			'phone' => '+375 ** *** ** **',
			'avatar_url' => 'Аватар',
			'about' => 'О вас',
			'email' => 'E-mail',
			'is_confirmed' => 'Подтвержден',
			'ban' => 'Бан',
			'city_id' => 'Город',
			'region_id' => 'Область',
			'country_id' => 'Страна',
			'terms' => 'Я принимаю условия <a class="user-agreement-link" href="/user-agreement.pdf" target="_blank">HHS.BY</a>',
			'new_password' => 'Новый пароль',
			'old_password' => 'Старый пароль',
			'repeat_password' => 'Повторите пароль'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id, true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('role',$this->role, true);
		$criteria->compare('phone',PhoneService::formatToInt($this->phone),true);
		$criteria->compare('ban',$this->ban, true);
		$criteria->compare('pro',$this->pro, true);
		$criteria->compare('is_confirmed',$this->is_confirmed, true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('provider',$this->provider,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord && !isset($this->provider)) {
			$this->password = PasswordService::encode($this->password);
			$this->created = time();
//			$this->pro = 1;
			$this->last_paid_date = ($this->pro == 1)? $this->created : 0;

		}
		$this->phone = PhoneService::formatToInt($this->phone);
		$this->country_id = 1;
		return parent::beforeSave();
	}

	/**
	 * Returns User model by its email
	 *
	 * @param string $email
	 * @access public
	 * @return User
	 */
	public function findByEmail($email)
	{
		return self::model()->findByAttributes(array('email' => $email));
	}

	public function hasRegion(){
		if($this->country_id != 0 || $this->country_id != null) return true;
		if($this->region_id != 0 || $this->region_id != null) return true;
		if($this->city_id != 0 || $this->city_id != null) return true;
		return false;
	}


	/**
	 * Uploading image
	 * @param $file string
	 * @return string|bool
	 */
	public function upload($file)
	{
		$filename = basename($file['name']);
		$fileType = $file['type'];
		$fileType = substr($fileType, 6);
		$filename = ImageService::setUserAvatarFilename(Yii::app()->user->id).".".$fileType;
		$path = YiiBase::getPathOfAlias("webroot") . '/images/avatars/' . $filename;
		$fileContent = file_get_contents($file['tmp_name']);

		$defaultAvatar = YiiBase::getPathOfAlias("webroot") . '/images/avatars/' . $this->avatar_url;

		if(file_exists($defaultAvatar) && !is_dir($defaultAvatar))
			unlink($defaultAvatar);

		$writeToDisk = file_put_contents($path, $fileContent);
		$appSettings = ApplicationSettings::model()->findByPk(1);
		ImageService::resizeToWidth($path, $appSettings->avatar_width, $appSettings->gallery_image_quality);

		$this->avatar_url = $filename;

		$this->save(false);

		return $filename;
	}

    /**
     * Interface IKeyWordsAdapter
     * Метод, возвращающий ассоциативный массив name и content (для создания метатегов)
     * @return array
     */
    public function getMetaTagArray()
    {
        $imageUrl = Yii::app()->getBaseUrl(true).ImageService::performImageUrl($this->avatar_url, ImageType::AVATAR);
        $width = getimagesize($imageUrl)[0];
        $height = getimagesize($imageUrl)[1];

        return array(
            'title' => $this->username." - Happy Holiday Service",
            'image' => Yii::app()->getBaseUrl(true).ImageService::performImageUrl($this->avatar_url, ImageType::AVATAR),
            'image:width' => $width,
            'image:height' => $height,
            'description' => str_replace("<br>", "", DescriptionService::performText($this->about)),
            'url' => Yii::app()->getBaseUrl(true).UrlService::performUserUrl($this),
        );
    }

	public function getUrl()
	{
		return Yii::app()->request->hostInfo.UrlService::performUserUrl($this);
	}
}
