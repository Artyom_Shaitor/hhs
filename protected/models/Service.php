<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property integer $price_type
 * @property integer $is_created
 * @property integer $pro
 * @property integer $is_working_around
 * @property integer $price_value
 * @property integer $occupancy
 * @property string $avatar_url
 * @property CalendarDay[] $calendarDay
 * @property Gallery[] $gallery
 * @property Category $category
 * @property Category $subCategory
 * @property City[] $city
 * @property Video[] $video
 * @property integer $currency_id
 * @property integer $author_id
 * @property string $author_name
 * @property MetaData[] $metadata
 * @property Price[] $prices
 * @property Country $country
 * @property Region $region
 * @property integer $region_id
 * @property integer $created
 * @property integer $max_price_value
 * @property integer $min_price_value
 * @property string $note
 * @property HolidayService $holidayService
 */
class Service extends CActiveRecord implements IKeyWordsAdapter, IImageUploading, ISiteMap
{
	/**
	 * Cache duration time in seconds
	 */
	const CACHE_DURATION = 60;
	/**
	 * Dependency query
	 */
	const DEPENDENCY_QUERY = 'SELECT COUNT(id) from service WHERE is_created=1';
	/**
	 * @return string the associated database table name
	 */

	public $author_name;

	public function tableName()
	{
		return 'service';
	}

	public function scopes()
	{
		return array(
			'published'=>array(
				'condition' => 'is_created = 1',
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, price_type, currency_id', 'numerical', 'integerOnly'=>true),
			array('price_value', 'numerical', 'integerOnly'=>true, "message" => "Цена должна быть целым числом"),
			array('description', 'length', 'max'=>1500, "message" => "Слишком длинное описание"),
			array('avatar_url', 'length', 'max'=>128),
			array('max_price_value, min_price_value', 'length', 'min' => 0),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, author_id, author_name, is_created', 'safe', 'on'=>'search'),
			array('id, title, description, author_id, author_name, is_created', 'safe'),
			array('description, price_type, currency_id', 'required', 'on' => 'sc_addService, sc_editService'),
			array('title', 'required', 'on' => 'sc_addService, sc_editService'),
			array('avatar_url', 'required', 'on' => 'sc_addService, sc_editService', 'message' => 'Необходимо выбрать изображение'),
			array('category_id, sub_category_id', 'required', 'message' => '"{attribute}"', 'on' => 'sc_addService, sc_editService'),
//			array('price_value', 'required', 'message' => 'Необходимо указать цену', 'on' => 'sc_addService, sc_editService')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'city' => array(self::MANY_MANY, 'City', 'city_service_assignment(service_id, city_id)'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'currency' => array(self::BELONGS_TO, 'Currencies', 'currency_id'),
			'gallery' => array(self::HAS_MANY, 'Gallery', 'service_id'),
			'metadata' => array(self::HAS_MANY, 'MetaData', 'service_id'),
			'video' => array(self::HAS_MANY, 'Video', 'service_id'),
			'calendarDay' => array(self::HAS_MANY, 'CalendarDay', 'service_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'subCategory' => array(self::BELONGS_TO, 'Category', 'sub_category_id'),
			'prices' => array(self::HAS_MANY, 'Price', 'service_id'),

			'holidayService' => array(self::HAS_MANY, 'HolidayService', 'service_id'),
			'holidays' => array(self::MANY_MANY, 'Holiday', 'holiday_service(service_id, holiday_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название услуги',
			'description' => 'Описание',
			'category_id' => 'Категория',
			'sub_category_id' => 'Подкатегория',
			'price_type' => 'Тип цены',
			'price_value' => 'Значение цены',
			'avatar_url' => 'Обложка',
			'author_name' => 'Имя автора'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('is_created',$this->is_created);
		$criteria->compare('author_id',$this->author_id);
		$v = $this->author_name;
		if(isset($_GET['Service']['author_name'])){
			$criteria->with = array('author');
			$criteria->compare('username', $this->author_name, true);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave(){
		if($this->isNewRecord) {
			$this->is_created = 0;
			$this->created = time();
			$this->author_id = Yii::app()->user->id;
		}else{
			//TODO : Проверка, создана ли услуга полностью
			$this->setScenario("sc_editService");
			if($this->validate()) $this->is_created = 1;
		}
		return parent::beforeSave();
	}

	/**
	 * Создание пустой модели
	 * @return bool
	 */
	private static function saveEmpty($scenario = null){
		$model = ($scenario == null)? new Service : new Service($scenario);
		$model->author_id = Yii::app()->user->id;
		$model->is_created = 0;
		if($model->save(false))
			return $model;
		else
			return false;
	}

	/**
	 * Загрузка пустой модели
	 * @return null|Service
	 */
	public static function loadEmpty($scenario = null){

		$criteria = new CDbCriteria();
		$criteria->compare('is_created', 0);
		$criteria->compare('author_id', Yii::app()->user->id);
		$criteria->limit = 1;

		$model = Service::model()->find($criteria);
		if(count($model) > 0){
			$model->setScenario($scenario);
			return $model;
		}
		else {
			$model = Service::saveEmpty($scenario);
			return $model;
		}
	}

	public function isCreated(){
		return ($this->is_created)? true : false;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function loadServicesByAuthorId($id){
		$model = Service::model()->findAll("author_id=:id AND is_created=1", array(":id" => $id));
		if(isset($model))
			return $model;
		return null;
	}

	public static function getCalendarDaysByServiceID($id){
		$model = CalendarDay::model()->findAll("service_id=:id", array(":id" => $id));
	}

	public function beforeDelete(){

		foreach($this->gallery as $gallery){
			unlink('./images/usersGalleryImages/'.$gallery->image_url);
		}

		return parent::beforeDelete();
	}

	/**
	 * @param Service[] $model
	 * @return array
	 */
	public static function performServicesByCategory($model){

		$resultArray = array();

		foreach($model as $row){

			if (!isset($resultArray[$row->category->name]))
				$resultArray[$row->category->name] = array();

			$resultArray[$row->category->name][] = $row;

		}

		return $resultArray;
	}

	public function getPrices(){
		return $this->prices;
	}

	public static function loadModel($id){
		return self::model()->findByPk($id);
	}

	/**
	 * Interface IKeyWordsAdapter
	 * Метод, возвращающий ассоциативный массив name и content (для создания метатегов)
	 * @return array
	 */
	public function getMetaTagArray()
	{
		$imageUrl = Yii::app()->getBaseUrl(true).ImageService::performImageUrl($this->avatar_url, ImageType::AVATAR);
		$width = getimagesize($imageUrl)[0];
		$height = getimagesize($imageUrl)[1];

		$subCategory = ($this->subCategory->name != "Другое")? $this->subCategory->name : $this->category->name;

		return array(
			'title' => $this->title." ".$subCategory,
			'image' => Yii::app()->getBaseUrl(true).ImageService::performImageUrl($this->avatar_url, ImageType::AVATAR),
			'image:width' => $width,
			'image:height' => $height,
			'description' => str_replace("<br>", "", DescriptionService::performText($this->description)),
			'url' => Yii::app()->getBaseUrl(true).UrlService::performServiceUrl($this),
		);
	}

	/**
	 * Uploading image
	 * @param $file string
	 * @return string|bool
	 */
	public function upload($file)
	{
		$filename = basename($file['name']);
		$fileType = $file['type'];
		$fileType = substr($fileType, 6);
		$filename = ImageService::setServiceAvatarFilename(Yii::app()->user->id).".".$fileType;
		$path = YiiBase::getPathOfAlias("webroot") . '/images/avatars/' . $filename;
		$fileContent = file_get_contents($file['tmp_name']);

		$defaultAvatar = YiiBase::getPathOfAlias("webroot") . '/images/avatars/' . $this->avatar_url;

		if(file_exists($defaultAvatar) && !is_dir($defaultAvatar))
			unlink($defaultAvatar);

		$writeToDisk = file_put_contents($path, $fileContent);
		$appSettings = ApplicationSettings::model()->findByPk(1);
		ImageService::resizeToWidth($path, $appSettings->avatar_width, $appSettings->gallery_image_quality);

		$this->avatar_url = $filename;

		$this->save(false);

		return $filename;
	}

	public function setOccupancy(){
		$summary = 0;
		$length = mb_strlen($this->description);
		$photo = count($this->gallery);
		$video = count($this->video);

		$summary += ($length > 750)? 750 : $length;
		$summary += $photo*100;
		$summary += $video*300;

		$this->occupancy = $summary;
	}

	public function getUrl()
	{
		return Yii::app()->request->hostInfo.UrlService::performServiceUrl($this);
	}
}
