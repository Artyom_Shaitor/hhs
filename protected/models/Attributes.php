<?php

/**
 * This is the model class for table "attributes".
 *
 * The followings are the available columns in table 'attributes':
 * @property integer $id
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property Category $category
 * @property Category $subCategory
 * @property string $name
 * @property string $title
 * @property string $type
 * @property integer $filter_data_id
 * @property FilterData $filterData
 */
class Attributes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'attributes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, sub_category_id, filter_data_id', 'numerical', 'integerOnly'=>true),
			array('name, title', 'length', 'max'=>45),
			array('type', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, sub_category_id, name, title, type, filter_data_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'subCategory' => array(self::BELONGS_TO, 'Category', 'sub_category_id'),
			'metadata' => array(self::HAS_MANY, 'MetaData', 'attribute_id'),
			'filterData' => array(self::HAS_MANY, 'FilterData', 'filter_data_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'sub_category_id' => 'Sub Category',
			'name' => 'Name',
			'title' => 'Title',
			'type' => 'Type',
			'filter_data_id' => 'Filter Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('sub_category_id',$this->sub_category_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('filter_data_id',$this->filter_data_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getValueIdInfFilter(){
		$filterData = FilterData::model()->findAll("filter_data_id=:fid", array(
			":fid" => $this->filter_data_id,
		));

		return $filterData[0]->id;
	}

	/**
	 * @return FilterData[]|null
	 */
	public function getFilterData(){
		$filterData = FilterData::model()->findAll("filter_data_id=:fid ORDER by value ASC", array(
			":fid" => $this->filter_data_id,
		));

		return $filterData;
	}

	public function getFilterToDataArray(){
		$data = $this->getFilterData();
		return CHtml::listData($data, 'id', 'value');
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attributes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
