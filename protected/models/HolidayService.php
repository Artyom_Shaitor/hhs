<?php

/**
 * This is the model class for table "holiday_service".
 *
 * The followings are the available columns in table 'holiday_service':
 * @property integer $id
 * @property integer $author_id
 * @property integer $holiday_id
 * @property integer $service_id
 * @property integer $price
 * @property integer $status
 * @property integer $currency_id
 * @property Currencies $currency
 * @property string $note
 * @property City $city
 * @property Service $service
 * @property User $author
 * @property Holiday $holiday
 */
class HolidayService extends CActiveRecord implements IHolidayService
{
	const SIMPLE_SERVICE = 0;
	const WAITING_SERVICE = 1;
	const ACCEPTED_SERVICE = 2;
	const DENIED_SERVICE = 3;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'holiday_service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('holiday_id, service_id, currency_id, status, price', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, holiday_id, service_id, currency_id, price, status, note', 'safe', 'on'=>'search'),
		);
	}

	public function getNote()
	{
		$note = $this->note;
		$note = htmlspecialchars_decode($note);
		$note = str_replace("<br />", "", $note);
		$note = str_replace("<br/>", "", $note);
		$note = str_replace("<br>", "", $note);

		return $note;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'holiday' => array(self::BELONGS_TO, 'Holiday', 'holiday_id'),
			'service' => array(self::BELONGS_TO, 'Service', 'service_id'),
			'currency' => array(self::BELONGS_TO, 'Currencies', 'currency_id'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id')
		);
	}

	protected function beforeSave(){
		if($this->isNewRecord) {
			$this->price = (int)$this->service->price_value;
			$this->currency_id = (int)$this->service->currency_id;
		}
		return parent::beforeSave();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'holiday_id' => 'Holiday',
			'service_id' => 'Service',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('holiday_id',$this->holiday_id);
		$criteria->compare('note',$this->note);
		$criteria->compare('service_id',$this->service_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HolidayService the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Change note of the model
	 * @param $value
	 * @return mixed
	 */
	public function changeNote($value)
	{
		$this->note = $value;
	}

	/**
	 * Get Author's id
	 * @return integer
	 */
	public function getAuthorId()
	{
		return $this->author_id;
	}

	/**
	 * Get Price
	 * @return integer
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * Set Price
	 * @param $value integer
	 */
	public function setPrice($value)
	{
		$this->price = $value;
	}

	/**
	 * Get Currency
	 * @return Currencies
	 */
	public function getCurrency()
	{
		return $this->currency;
	}

	/**
	 * Set Currency
	 * @param $id integer
	 */
	public function setCurrency($id)
	{
		$this->currency_id = $id;
	}
}
