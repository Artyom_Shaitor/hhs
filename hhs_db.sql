-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: hhs_db
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application_settings`
--

DROP TABLE IF EXISTS `application_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_width` int(11) DEFAULT NULL,
  `gallery_image_width` int(11) DEFAULT NULL,
  `gallery_image_quality` int(11) DEFAULT NULL,
  `max_photos` int(11) DEFAULT NULL,
  `max_videos` int(11) DEFAULT NULL,
  `default_currency` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_settings`
--

LOCK TABLES `application_settings` WRITE;
/*!40000 ALTER TABLE `application_settings` DISABLE KEYS */;
INSERT INTO `application_settings` VALUES (1,500,1000,72,30,10,1);
/*!40000 ALTER TABLE `application_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `filter_data_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attributes_category1_idx` (`category_id`),
  KEY `fk_attributes_category2_idx` (`sub_category_id`),
  CONSTRAINT `fk_attributes_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_attributes_category2` FOREIGN KEY (`sub_category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` VALUES (5,1,8,'capacity','Вместимость','number',NULL),(6,1,9,'capacity','Вместимость','number',NULL),(7,1,10,'capacity','Вместимость','number',NULL),(8,1,11,'capacity','Вместимость','number',NULL),(9,5,35,'two_same_auto','Два одинаковых авто','checkbox',1),(17,5,38,'capacity','Вместимость','number',NULL),(18,5,35,'brand','Марка','select',2),(19,5,35,'capacity','Вместимость','number',NULL),(20,5,36,'brand','Марка','select',2),(21,5,37,'brand','Марка','select',2),(22,5,38,'brand','Марка','select',2),(23,5,36,'capacity','Вместимость','number',NULL),(24,5,37,'capacity','Вместимость','number',NULL);
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_day`
--

DROP TABLE IF EXISTS `calendar_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reserved` tinyint(1) DEFAULT NULL,
  `request` tinyint(1) DEFAULT NULL,
  `weekend` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calendar_day_user1_idx` (`author_id`),
  CONSTRAINT `fk_calendar_day_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_day`
--

LOCK TABLES `calendar_day` WRITE;
/*!40000 ALTER TABLE `calendar_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `parent_cat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Место проведения','place',0),(2,'Шоу программа','show',0),(3,'Декорирование','decorating',0),(4,'Фото и Видео','photo-video',0),(5,'Транспорт','transport',0),(6,'Свадебный образ','fashion',0),(7,'Свадебные атрибуты','attributes',0),(8,'Кафе и рестораны',NULL,1),(9,'Кейтеринг',NULL,1),(10,'Выездная роспись',NULL,1),(11,'Усадьбы',NULL,1),(13,'Музыкальные группы',NULL,2),(14,'Музыканты-инструменталисты',NULL,2),(15,'Певцы',NULL,2),(16,'Диджеи',NULL,2),(17,'Шоу-балет',NULL,2),(18,'Иллюзионисты',NULL,2),(19,'Шоу мыльных пузырей',NULL,2),(20,'Бармен-шоу',NULL,2),(21,'Фаер-шоу',NULL,2),(22,'Световое и лазерное шоу',NULL,2),(23,'Постановка свадебного танца',NULL,2),(24,'Другое',NULL,2),(25,'Стол молодых',NULL,3),(26,'Флористика',NULL,3),(27,'Украшение столов и стульев',NULL,3),(28,'Кэнди-бар',NULL,3),(29,'Стол для дарения',NULL,3),(30,'Фотозона',NULL,3),(31,'Другое',NULL,3),(32,'Фотограф',NULL,4),(33,'Видеограф',NULL,4),(34,'Фотоальбом',NULL,4),(35,'Автомобили',NULL,5),(36,'Ретроавтомобили',NULL,5),(37,'Лимузины',NULL,5),(38,'Автобусы',NULL,5),(39,'Свадебное платье',NULL,6),(40,'Свадебный костюм',NULL,6),(41,'Кольца',NULL,6),(42,'Прическа',NULL,6),(43,'Маникюр',NULL,6),(44,'Букет невесты',NULL,7),(45,'Пригласительные',NULL,7),(46,'Торт',NULL,7),(47,'Приглашения',NULL,7),(48,'Подушечки для колец',NULL,7),(49,'Сундучки для денег',NULL,7),(50,'Бокалы для молодожёнов',NULL,7),(51,'Банкетные карточки',NULL,7),(52,'Нумерация столов',NULL,7),(53,'Книги пожеланий',NULL,7),(54,'Аксессуары для фотосессии',NULL,7),(55,'Свечи и домашний очаг',NULL,7),(56,'Бонбоньерка',NULL,7),(57,'Украшения автомобилей',NULL,7),(58,'Аксессуары для конкурсов',NULL,7),(59,'План рассадки гостей',NULL,7),(60,'Фигурки на торт',NULL,7),(61,'Свадебные наборы',NULL,7),(62,'Приборы для торта',NULL,7),(63,'Плакаты и наклейки',NULL,7),(64,'Украшения для бокалов и шампанского',NULL,7),(65,'Обложки для свидетельства',NULL,7),(66,'Рушники',NULL,7),(67,'Корзины',NULL,7),(68,'Свадебные замочки',NULL,7),(69,'Другое',NULL,7),(70,'Организаторы и ведущие','leading',0),(71,'Организатор',NULL,70),(72,'Ведущий',NULL,70),(73,'Регистратор',NULL,70),(74,'Другое',NULL,1),(75,'Другое',NULL,4),(76,'Другое',NULL,5),(77,'Другое',NULL,6),(78,'Макияж',NULL,6),(79,'Фотобудка',NULL,4),(80,'Аренда звука и света',NULL,2),(81,'Галстук и бабочка',NULL,7),(82,'Аренда платьев',NULL,6),(83,'Аренда пеньюаров',NULL,6);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_region_idx` (`regionid`),
  CONSTRAINT `fk_city_region` FOREIGN KEY (`regionid`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Брест',1),(2,'Барановичи',1),(3,'Берёза',1),(4,'Ганцевичи',1),(5,'Дрогичин',1),(6,'Жабинка',1),(7,'Иваново',1),(8,'Ивацевичи',1),(9,'Каменец',1),(10,'Кобрин',1),(11,'Лунинец',1),(12,'Ляховичи',1),(13,'Малорита',1),(14,'Пинск',1),(15,'Пружаны',1),(16,'Столин',1),(17,'Минск',2),(18,'Березино',2),(19,'Борисов',2),(20,'Вилейка',2),(21,'Воложин',2),(22,'Дзержинск',2),(23,'Жодино',2),(24,'Клецк',2),(25,'Копыль',2),(26,'Крупки',2),(27,'Логойск',2),(28,'Любань',2),(29,'Марьина Горка',2),(30,'Молодечно',2),(31,'Мядель',2),(32,'Несвиж',2),(33,'Плещеницы',2),(34,'Пуховичи',2),(35,'Слуцк',2),(36,'Смолевичи',2),(37,'Солигорск',2),(38,'Старые Дороги',2),(39,'Столбцы',2),(40,'Узда',2),(41,'Червень',2),(42,'Витебск',3),(43,'Браслав',3),(44,'Верхнедвинск',3),(45,'Глубокое',3),(46,'Городок',3),(47,'Докшицы',3),(48,'Дубровно',3),(49,'Лепель',3),(50,'Лиозно',3),(51,'Миоры',3),(52,'Новополоцк',3),(53,'Орша',3),(54,'Полоцк',3),(55,'Поставы',3),(56,'Россоны',3),(57,'Сенно',3),(58,'Толочин',3),(59,'Ушачи',3),(60,'Чашники',3),(61,'Шарковщина',3),(62,'Шумилино',3),(63,'Гродно',4),(64,'Берестовица',4),(65,'Волковыск',4),(66,'Вороново',4),(67,'Дятлово',4),(68,'Зельева',4),(69,'Ивье',4),(70,'Кореличи',4),(71,'Лида',4),(72,'Мосты',4),(73,'Новогрудок',4),(74,'Островец',4),(75,'Ошмяны',4),(76,'Свислочь',4),(77,'Слоним',4),(78,'Сморгонь',4),(79,'Щучин',4),(80,'Гомель',5),(81,'Брагин',5),(82,'Буда-Кошелево',5),(83,'Ветка',5),(84,'Добруш',5),(85,'Ельск',5),(86,'Житковичи',5),(87,'Жлобин',5),(88,'Калинковичи',5),(89,'Корма',5),(90,'Лельчицы',5),(91,'Лоев',5),(92,'Мозырь',5),(93,'Наровля',5),(94,'Октябрьский',5),(95,'Петриков',5),(96,'Речица',5),(97,'Рогачев',5),(98,'Светлогорск',5),(99,'Хойники',5),(100,'Чечерск',5),(101,'Могилёв',6),(102,'Белыничи',6),(103,'Бобруйск',6),(104,'Быхов',6),(105,'Глуск',6),(106,'Горки',6),(107,'Дрибин',6),(108,'Кировск',6),(109,'Климовичи',6),(110,'Кличев',6),(111,'Костюковичи',6),(112,'Краснополье',6),(113,'Кричев',6),(114,'Круглое',6),(115,'Мстиславль',6),(116,'Осиповичи',6),(117,'Славгород',6),(118,'Хотимск',6),(119,'Чаусы',6),(120,'Чериков',6),(121,'Шклов',6);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_service_assignment`
--

DROP TABLE IF EXISTS `city_service_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_service_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_service_assignment_service1_idx` (`service_id`),
  KEY `fk_city_service_assignment_city1_idx` (`city_id`),
  CONSTRAINT `fk_city_service_assignment_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_city_service_assignment_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_service_assignment`
--

LOCK TABLES `city_service_assignment` WRITE;
/*!40000 ALTER TABLE `city_service_assignment` DISABLE KEYS */;
INSERT INTO `city_service_assignment` VALUES (51,1,6),(53,1,6),(55,17,6),(56,1,6),(57,17,6);
/*!40000 ALTER TABLE `city_service_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Беларусь');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (1,'BLR'),(2,'USD');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter_data`
--

DROP TABLE IF EXISTS `filter_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_data_id` int(11) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_filter_data_attributes1_idx` (`filter_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter_data`
--

LOCK TABLES `filter_data` WRITE;
/*!40000 ALTER TABLE `filter_data` DISABLE KEYS */;
INSERT INTO `filter_data` VALUES (7,1,'Два одинаковых авто'),(8,2,'Acura'),(9,2,'Alfa Romeo'),(10,2,'Audi'),(11,2,'BMW'),(15,2,'Chevrolet'),(16,2,'Chrysler'),(17,2,'Citroen'),(18,2,'Dodge'),(19,2,'Fiat'),(20,2,'Ford'),(21,2,'Honda'),(22,2,'Hyndai'),(23,2,'Infiniti'),(24,2,'Jeep'),(25,2,'KIA'),(26,2,'Lada (ВАЗ)'),(27,2,'Land Rover'),(28,2,'Lexus'),(29,2,'Mazda'),(30,2,'Mercedes-Benz'),(31,2,'Mini'),(32,2,'Mitsubishi'),(33,2,'Nissan'),(34,2,'Opel'),(35,2,'Peugeot'),(36,2,'Porsche'),(37,2,'Renault'),(38,2,'Rover'),(39,2,'Skoda'),(40,2,'Toyota'),(41,2,'Volkswagen'),(42,2,'Volvo'),(43,2,'Jaguar');
/*!40000 ALTER TABLE `filter_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `image_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gallery_service1_idx` (`service_id`),
  KEY `fk_gallery_user1_idx` (`author_id`),
  CONSTRAINT `fk_gallery_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_gallery_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holiday`
--

DROP TABLE IF EXISTS `holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `name_1` varchar(256) DEFAULT NULL,
  `name_2` varchar(256) DEFAULT NULL,
  `photo_1` varchar(500) DEFAULT NULL,
  `photo_2` varchar(500) DEFAULT NULL,
  `is_created` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_holiday_user1_idx` (`author_id`),
  CONSTRAINT `fk_holiday_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holiday`
--

LOCK TABLES `holiday` WRITE;
/*!40000 ALTER TABLE `holiday` DISABLE KEYS */;
INSERT INTO `holiday` VALUES (1,6,'Артем Шайтор & Мария Заворотняя',1458162000,0,1,1,'Артем Шайтор','Мария Заворотняя','holiday_f977914313ba58130981fb389dbb8f05.jpeg','holiday_0f1fddfeb39c4753f3cf357895d2671c.jpeg',1);
/*!40000 ALTER TABLE `holiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holiday_service`
--

DROP TABLE IF EXISTS `holiday_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `note` varchar(8000) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_holiday_service_holiday1_idx` (`holiday_id`),
  KEY `fk_holiday_service_service1_idx` (`service_id`),
  CONSTRAINT `fk_holiday_service_holiday1` FOREIGN KEY (`holiday_id`) REFERENCES `holiday` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_holiday_service_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holiday_service`
--

LOCK TABLES `holiday_service` WRITE;
/*!40000 ALTER TABLE `holiday_service` DISABLE KEYS */;
INSERT INTO `holiday_service` VALUES (3,1,6,'',120,2,6);
/*!40000 ALTER TABLE `holiday_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_data`
--

DROP TABLE IF EXISTS `meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_meta_data_service1_idx` (`service_id`),
  KEY `fk_meta_data_attributes1_idx` (`attribute_id`),
  CONSTRAINT `fk_meta_data_attributes1` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meta_data_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_data`
--

LOCK TABLES `meta_data` WRITE;
/*!40000 ALTER TABLE `meta_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `price_type` tinyint(1) DEFAULT NULL,
  `price_value` double DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_price_service1_idx` (`service_id`),
  KEY `fk_price_currencies1_idx` (`currency_id`),
  CONSTRAINT `fk_price_currencies1` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_price_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_region_country1_idx` (`country_id`),
  CONSTRAINT `fk_region_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Брестская область',1),(2,'Минская область',1),(3,'Витебская область',1),(4,'Гродненская область',1),(5,'Гомельская область',1),(6,'Могилёвская область',1);
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(3500) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `price_type` tinyint(1) DEFAULT NULL,
  `price_value` int(11) DEFAULT NULL,
  `avatar_url` varchar(128) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `max_price_value` int(11) DEFAULT NULL,
  `min_price_value` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `is_created` tinyint(1) DEFAULT NULL,
  `is_working_around` tinyint(1) DEFAULT NULL,
  `occupancy` int(11) DEFAULT NULL,
  `pro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_user1_idx` (`author_id`),
  CONSTRAINT `fk_service_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (6,'Артем Шайтор Сергеевич Батькович','adsfasdf ',4,75,0,100,'service_7862e349ef77800d40585ab7144753be.jpeg',NULL,NULL,1,6,2,0,0,1458207250,1,0,9,NULL);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `role` tinyint(2) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `avatar_url` varchar(512) DEFAULT NULL,
  `about` varchar(3500) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ban` tinyint(1) DEFAULT NULL,
  `is_confirmed` tinyint(1) DEFAULT NULL,
  `provider` varchar(45) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `pro` tinyint(1) DEFAULT NULL,
  `last_paid_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'Артем Шайтор Сергеевич Батькович','artyomshaitor',1450267465,'42b7a5240e4bc0430c727d39732dae077c4a8d09ca3762af61e59520943dc26494f8941b',3,375259680876,'avatar_43086497e354df35f9fe983a5dd1dead.png','О нас','artyomshaitor@gmail.com',0,1,NULL,NULL,1,1,0,'http://kasatkatest.ml',1,1450267465),(7,'Big cake photo',NULL,1450261970,'32a23cf8854fc281ba47481cda79a18a796875871992f9a7b53e2cc9f1207890d988c6c0',3,NULL,'avatar_db6906c3e20f9ef76d6b07b813bb5491.jpg',NULL,'Fallder13@gmail.com',0,1,NULL,NULL,1,1,1,NULL,1,1450261970),(8,'Кристофер Нолан Оливерович Первый Старший',NULL,1450262186,'fdc2a0bb6f2a9bc6960d5477c3f5e1d691032ad7bbcb6cf72875e8e8207dcfba80173f7c',3,NULL,'avatar_973ad76380ac24cd9c1524e0bebaa126.jpg',NULL,'olegthefraiz@gmail.com',0,1,NULL,NULL,1,1,1,NULL,1,1450262186),(9,'bigCAKE films',NULL,1450262709,'76263440574af5391ba8c4b4e7895617957a9205416a5ab6770035a0a71fce3747ebda4d',3,NULL,'avatar_1e658c4bbe1a83e69103f3fe53f3894b.jpg',NULL,'pavellldanilyuk@gmail.com',0,1,NULL,NULL,1,1,1,NULL,1,1450262709);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_link` varchar(256) DEFAULT NULL,
  `video_provider` varchar(45) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_video_service1_idx` (`service_id`),
  KEY `fk_video_user1_idx` (`author_id`),
  CONSTRAINT `fk_video_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_video_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weekend`
--

DROP TABLE IF EXISTS `weekend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_number` int(11) DEFAULT NULL,
  `month_number` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_weekend_user1_idx` (`author_id`),
  CONSTRAINT `fk_weekend_user1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weekend`
--

LOCK TABLES `weekend` WRITE;
/*!40000 ALTER TABLE `weekend` DISABLE KEYS */;
/*!40000 ALTER TABLE `weekend` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-18 15:40:12
