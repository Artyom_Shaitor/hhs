/**
 * Before method
 */
XMLHttpRequest.prototype.before = function(){};

/**
 * Send data after "Before" function
 * @param data : String|ArrayBuffer|Blob|HTMLDocument|FormData, optional
 */
XMLHttpRequest.prototype.sendWithBefore = function(data){
    this.before();
    this.send(data);
};