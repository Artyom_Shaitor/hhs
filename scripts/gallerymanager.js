
//var xhr = new XMLHttpRequest();

const MAX_FILES = 10;

var queue = [];
var queueIterator = 0;
var files = [];
var imagesCountDOM = $("div#images-count");
var imagesCountNumberDOM = imagesCountDOM.children("span");

$("input[name='load_gallery_pic']").on('click', function() {
    $("input[name='gallery_file[]']").click();
});

$("input[name='gallery_file[]']").on('change', function(event){
    files = event.target.files;
    if(files.length > MAX_FILES){
        alert("Вы можете загружать не более 10 изображений за раз");
        return false;
    }

    imagesCountNumberDOM.html(files.length);
    imagesCountDOM.css("opacity", "1");

    upload(queueIterator);

});

function upload(iterator)
{
    if(iterator >= files.length){
        imagesCountNumberDOM.html("");
        imagesCountDOM.css("opacity", "0");

        queue = [];
        queueIterator = 0;
        files = [];

        return true;
    }

    var file = files[iterator];
    var xhr = new XMLHttpRequest();
    var fd = new FormData();

    var abort ;
    var galleryItem ;

    xhr.before = function(){

        abort = $('<div class="abort_image_loading"><img src="/images/icons/loading_spinner.gif" width="50" height="50" alt=""/><div class="percent"></div></div>');
        galleryItem = $('<div class="gallery-item"></div>');

        galleryItem.append(abort);
        galleryItem.insertBefore( $("input[name='load_gallery_pic']") );

        abort.click(function(){
            xhr.abort();
            $(this).parent().remove();
            queueIterator++;
            imagesCountNumberDOM.html(files.length - queueIterator);
            upload(queueIterator);
        });
    };


    xhr.upload.onprogress = function(event)
    {
        var percent = abort.children(".percent");
        if(percent.length > 0) {
            percent.html(Math.round(event.loaded * 100 / event.total) + "%");
        }
    };

    xhr.onload = function()
    {
        var response = JSON.parse(xhr.response);
        if(response.error != false){
            alert(response.error);
            xhr.abort();
            abort.parent().remove();
            imagesCountNumberDOM.html("");
            imagesCountDOM.css("opacity", "0");
            return;
        }
        var delete_a = $('<a href="#" name="delete" id="'+response.model_id+'">Удалить</a>');

        var tmppath = (URL.createObjectURL(file) != "" || URL.createObjectURL(file) != null)? URL.createObjectURL(file) : response.filepath;
        galleryItem.css("background-image", 'url('+tmppath+')');

        if(tmppath == URL.createObjectURL(file)){
            var image = new Image();
            image.src = response.filepath;
        }
        galleryItem.html("").append(delete_a);
        queueIterator++;
        imagesCountNumberDOM.html(files.length - queueIterator);
        upload(queueIterator)
    };

    fd.append('gallery_file', file);
    fd.append('model_id', $("input[name='model_id']").val());

    xhr.open('POST', '/profile/service/loadImage', true);
    xhr.sendWithBefore(fd);

}

$('body').on('click', ".gallery-item a", function(){
    var xhr = new XMLHttpRequest();
    var fd = new FormData();

    var obj = this;

    xhr.onload = function()
    {
        var response = JSON.parse(xhr.response);
        if(response.result == true){
            $(obj).parent().remove();
        }
        return false;
    };

    fd.append('submit', $(this).attr("name"));
    xhr.open('POST', '/profile/service/deleteGalleryImage/'+$(this).attr("id"), true);
    xhr.send(fd);

    return false;
});


