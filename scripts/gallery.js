(function( $ ) {

    $.fn.slider = function(options) {

        var settings = $.extend({
            galleryContainer : ".list",
            item : ".li",
            checkBoxes : "checkBoxes",
            checkboxClass : 'checkbox',
            timer : 2000,
            speed : 600
        },options);

        var count = this.children(settings.galleryContainer).children(settings.itemClass).length;
        var iterator = 1;

        var k = 0.9;

        $(settings.galleryContainer).css("-webkit-transition-duration", settings.speed+"ms");

        var next = function(){

            var item = iterator%count;

            $("."+settings.checkBoxes+" ."+settings.checkboxClass+".active").removeClass("active");
            $(".checkbox[item='"+item+"']").addClass("active");
            $(settings.galleryContainer).css("margin-left", -item*100+"%");

            iterator++;
        };

        var interval = setInterval(next, settings.timer+settings.speed);

        this.append("<div class='"+settings.checkBoxes+"'></div>");

        for(i = 0; i<count; i++) {
            if (i == 0)
                this.children("."+settings.checkBoxes).append("<div class=\""+settings.checkboxClass+" active\" item=\"0\"></div>");
            else
                this.children("."+settings.checkBoxes).append("<div class=\""+settings.checkboxClass+"\" item=\""+i+"\"></div>");
        }

        $("."+settings.checkBoxes+" ."+settings.checkboxClass).click(function(){
            clearInterval(interval);
            iterator = $(this).attr("item");
            next();
            interval = setInterval(next, settings.timer+settings.speed);

        });

        $(window).on("scroll", function(){
            var scrollTop = $(this).scrollTop();

            var offset = scrollTop*k;

            $(settings.galleryContainer+" "+settings.item).css("background-position-y", offset+"px");

        });

        return this;
    };



})(jQuery);