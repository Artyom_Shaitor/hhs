

function div(x, y){
    return (x-x%y)/y;
}

/**
 * ------------------------------------------------------------------------------------------------------------------------------------
 */

var CalendarService = function(year, month, calendarContainerDOM){
    this.year = year;
    this.month = month;
    this.calendarContainer = calendarContainerDOM;
    this.monthBlock = this.getMonthBlock();

    this.isYearChanged = false;
    /**
     * Active Day
     * @type {ActiveDay}
     */
    this.activeDay = new ActiveDay(null, null, null);

    this.CALENDAR_MONTH_CLASS = "calendar-month";
    this.CALENDAR_DATES_CLASS = "calendar-dates";
    this.month_names_rod_pad = ['января', 'февраля', "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
    /**
     *
     * @type {{reserved: string[], request: string[]}}
     */
    this.STATE_TYPE_TITLES = { reserved : ["Отметить заказанной", "Отменить"], request : ["", "Заявка"] };

    this.activeDayToString = function(){
        return this.activeDay.day+" "+this.month_names_rod_pad[this.activeDay.month-1]+" "+this.year+" года";
    };

    this.rewrite = function(year, month){
        this.clear().changeDate(year,month).render();
        return this;
    };

    this.activeDayStateRender = function(){

        if(this.activeDay.isNull()){
            $("#services").css("display", "none");
        }else {
            $("#services #active-day-string").html(this.activeDayToString());
            $(".day-state-button").removeClass("active");
            if(this.activeDay.dayState == 0){
                $("#day-state-work").addClass("active");
            }else {
                $("#day-state-weekend").addClass("active");
            }

            if(this.activeDay.dayState == 0) $("#services #services-container").css("display", "block");
            else $("#services #services-container").css("display", "none");
            $("#services").css("display", "inline-block");
        }

        return this;
    };

    /**
     * Сбрасывает настройки сервисов
     * @returns {CalendarService}
     */
    this.clearServicesList = function(){
        $("#services-container .service a.reserved-button").attr("data-state", 0).html( this.STATE_TYPE_TITLES.reserved[0] );
        $("#services-container .service a.request-button").attr("data-state", 0).html(this.STATE_TYPE_TITLES.request[0]).css("display", "none");
        return this;
    };

    this.clear = function(){
        $(this.calendarContainer).children(".calendar-container").children().remove();
        return this;
    };

    this.loadServicesData = function(){
        var self = this;
        $.ajax({
            url : window.location.origin+"/core/ajaxgetservicesdataincalendar",
            dataType : 'json',
            type : "POST",
            data : {day : self.activeDay.day, month : self.activeDay.month, year : self.activeDay.year},
            success : function(data){

                $("#services-container .service a.reserved-button").attr('data-state', 0).html(self.STATE_TYPE_TITLES.reserved[0]);
                $("#services-container .service a.request-button").attr('data-state', 0).html(self.STATE_TYPE_TITLES.request[1]);

                for(var calendarDayIndexID in data){
                    if(data[calendarDayIndexID].reserved == 1) {
                        $("#services-container .service#service_" + calendarDayIndexID + " a.reserved-button").attr("data-state", 1).html(self.STATE_TYPE_TITLES.reserved[1])
                    }
                    if(data[calendarDayIndexID].request == 1) {
                        $("#services-container .service#service_" + calendarDayIndexID + " a.reserved-button").attr("data-state", 1).html(self.STATE_TYPE_TITLES.request[1]);
                    }
                }


                var td = selectTd(calendarService.activeDay.day, calendarService.activeDay.month, calendarService.activeDay.year);
                if( $("#services-container .service a.request-button[data-state='1']").length == 0 ){
                    //td.children(".status-container").children(".request-day").remove();
                }else {
                    if( td.children(".status-container").children(".request-day").length == 0 )
                        td.children(".status-container").append('<div class="request-day"></div>');
                }
            }
        });

        return this;
    };

    this.loadData = function(id){

        var self = this;
        /* LOAD DATA */
        $.ajax({
            url : window.location.origin+"/core/ajaxgetalluserscalendardayswithweekends",
            dataType : 'json',
            async: false,
            type : "POST",
            data : { id : id, year : this.year, month : this.month, isYC : this.isYearChanged},
            success : function(data){
                var fullWeekendsList = data['fullWeekendsList'];
                var weekendsManageList = data['weekendsManageList'];
                var manageListCount = data['manageListCount'];


                for(var year in fullWeekendsList) {
                    for (var month in fullWeekendsList[year]) {

                        for (var day in fullWeekendsList[year][month]) {
                            var attribs = fullWeekendsList[year][month][day];


                            td = selectTd(day, month, year);


                            if (attribs.request || attribs.reserved || attribs.weekend || attribs.global_weekend) {


                                var statusContainer = td.children(".status-container");


                                if (attribs.reserved) {
                                    if (!(statusContainer.children('.reserved-day').length > 0)) {
                                        $(statusContainer).append("<div class='reserved-day'></div>");
                                    }
                                }

                                if (attribs.request) {
                                    if (!(statusContainer.children('.request-day').length > 0)) {
                                        $(statusContainer).append("<div class='request-day'></div>");
                                    }
                                }

                                if (attribs.weekend) {
                                    if (!(statusContainer.children('.weekend-day').length > 0)) {
                                        td.addClass('disabled-button');
                                    }
                                }

                                if (attribs.global_weekend) {
                                    if (!(statusContainer.children('.weekend-day').length > 0)) {
                                        td.addClass('disabled-button');
                                    }
                                }

                            }



                        }


                    }
                }

                if(self.isYearChanged) {
                    if(manageListCount > 0) {
                        $(".weekends-list").children().remove();
                        $(".weekends-list").append("<table></table>");
                        for (var day_with_index in weekendsManageList) {
                            var day = day_with_index.substr(2);
                            var day_number = day_with_index.substr(0, 1);
                            var tr = $("<tr></tr>");

                            var first_td = $("<td>" + day + "</td>");
                            var second_td = $("<td></td>");

                            for (var element in weekendsManageList[day_with_index]) {
                                var form = $('<form class="inline-form" action="deleteWeekend" method="post"></form>');

                                var list = weekendsManageList[day_with_index];
                                second_td.append(list[element]['year_month']);
                                form.append('<input type="hidden" name="month_number" value="' + list[element]['month_number'] + '">');
                                form.append('<input type="hidden" name="day_number" value="' + list[element]['day_number'] + '">');
                                form.append('<input type="hidden" name="year" value="' + list[element]['year'] + '">');
                                form.append('<input type="submit" name="submit" value="✕"></form>');

                                second_td.append(form);
                            }

                            var third_td = $("<td></td>");

                            var form2 = $('<form class="inline-form" action="deleteWeekend" method="post"></form>');
                            form2.append('<input type="hidden" name="month_number" value="all">');
                            form2.append('<input type="hidden" name="day_number" value="' + day_number + '">');
                            form2.append('<input type="hidden" name="year" value="2015">');
                            form2.append('<input type="submit" name="submit" value="✕">');

                            third_td.append(form2);

                            tr.append(first_td);
                            tr.append(second_td);
                            tr.append(third_td);

                            $(".weekends-list table").append(tr);

                        }
                    }else{
                        $(".weekends-list").children().remove();
                    }

                    self.isYearChanged = false;

                }
            }
        });

        return this;
    };

    this.changeDate = function(year, month){
        this.year = year;
        this.month = month;
        this.monthBlock = this.getMonthBlock();

        return this;
    };

    this.changeYear = function(year){
        this.year = year;
        this.isYearChanged = true;

        return this;
    };

    this.render = function() {

        $("#calendar-year span#year").html(this.year);
        $("input[name=year]").val(this.year);
        $("#weekend-info-block .year").html(this.year);

        for (var i in this.monthBlock) {
            var calendarI = '<div id="calendar-' + i + '" class="calendar"><div class="' + this.CALENDAR_MONTH_CLASS + '"></div><div class="' + this.CALENDAR_DATES_CLASS + '"></div></div>';
            $(this.calendarContainer+" .calendar-container").append(calendarI);

            $("#calendar-" + i + " ." + this.CALENDAR_MONTH_CLASS).html(Calendar.month_name_ru[i]);
            $("#calendar-" + i + " ." + this.CALENDAR_DATES_CLASS).html((new Calendar()).getCalendar(this.monthBlock[i], this.year, this.activeDay));
        }

        return this;
    }

};




CalendarService.prototype.getMonthBlock = function(){
    var month = this.month;
    var monthBlock = div(month,3);

    var monthBlockArray = [];
    monthBlockArray[3*monthBlock] = Calendar.month_name[3*monthBlock];
    monthBlockArray[3*monthBlock+1] = Calendar.month_name[3*monthBlock+1];
    monthBlockArray[3*monthBlock+2] = Calendar.month_name[3*monthBlock+2];

    return monthBlockArray;
};





/**
 * ------------------------------------------------------------------------------------------------------------------------------------
 */


var Calendar = function(){

    this.month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.month_name_ru = ['Январь', 'Февраль', "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
    this.days_names = "ПнВтСрЧтПтСбВс";
    this.days_indexes = [1,2,3,4,5,6,0];

    this.getDayName = function(index){
        if(index >= 0 && index < 7)
            return this.days_names[index*2] + this.days_names[index*2+1];
        else return undefined;
    };

    this.getMonthName = function(){

    };

    /**
     *
     * @param monthName {string}
     * @param year {int}
     * @param activeDay {ActiveDay}
     * @returns {*}
     */
    this.getCalendar = function(monthName, year, activeDay){
        var d = new Date();

        monthName = (monthName == null)? this.month_name[d.getMonth()] : monthName;
        year = (year == null)? d.getFullYear() : year;

        var first_date = monthName + " " + 1 + " " + year;


        var month = new Date(first_date).getMonth();

        var tmp = new Date(first_date).toDateString(); // Tue September 01 2015
        var first_day = tmp.substring(0,3); // Tue
        var day_name = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var day_no = day_name.indexOf(first_day); // 2

        var days = new Date(year, month+1, 0).getDate(); // 30

        var calendar = this.build(day_no, days, month+1, year, activeDay);

        return calendar;
    };

    this.addWeekendFormRender = function(){



        return this;
    };

    /**
     *
     * @param day_no
     * @param days
     * @param month
     * @param year
     * @param activeDay {ActiveDay}
     * @returns {HTMLElement}
     */
    this.build = function(day_no, days, month, year, activeDay){
        var table = document.createElement('table');

        var disabled = false;

        var tr = document.createElement('tr');
        $(tr).addClass('days-names');

        for(c = 0; c < 7; c++ ){
            var td = document.createElement('td');
            td.innerHTML = this.getDayName(c);
            $(td).addClass('disabled');
            if(c >= 5) $(td).addClass('weekend-day-red');
            tr.appendChild(td);
        }

        table.appendChild(tr);

        tr = document.createElement('tr');
        var c = 0;
        for(c; c < 7; c++ ){
            if(day_no == this.days_indexes[c]){
                break;
            }
            var td = document.createElement('td');
            $(td).addClass('disabled');
            if(c >= 5) $(td).addClass('weekend-day-red');
            td.innerHTML = "";
            tr.appendChild(td);

        }

        var today = new Date();

        var count = 1;
        for(c; c < 7; c++){

            disabled = (new Date(year, month-1, count+1) < today)? true : false;
            var td = performTd(c, count, month, year, disabled, activeDay);
            count++;
            tr.appendChild(td);
        }

        table.appendChild(tr);

        for(var r=3; r<=7; r++){
            tr = document.createElement('tr');
            for(c = 0; c < 7; c++ ){
                if(count > days){
                    table.appendChild(tr);

                    return table;
                }
                disabled = (new Date(year, month-1, count+1) < today)? true : false;
                var td = performTd(c, count, month, year, disabled, activeDay);
                count++;
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
    }

};

var ActiveDay = function(day, month, year){
    this.day = parseInt(day);
    this.month = parseInt(month);
    this.year = parseInt(year);
    this.dayState = null;

    this.getDate = function(){
        return this.year+"-"+this.month+"-"+this.year;
    };

    this.isNull = function(){
        return (this.day == null && this.month == null && this.year == null);
    };

    this.setNull = function(){
        this.day = null;
        this.month = null;
        this.year = null;
        this.dayState = null;
    };

    this.setActiveDay = function(day, month, year, state) {
        this.day = parseInt(day);
        this.month = parseInt(month);
        this.year = parseInt(year);
        this.dayState = state;
    };

    this.isEqual = function(day, month, year){
        day = parseInt(day);
        month = parseInt(month);
        year = parseInt(year);

        return (day == this.day && month == this.month && year == this.year)
    };
};

/**
 *
 * @param c
 * @param count
 * @param month
 * @param year
 * @param disabled
 * @param activeDay {ActiveDay}
 * @returns {HTMLElement}
 */
var performTd = function(c, count, month, year, disabled, activeDay){
    var td = document.createElement('td');
    td.innerHTML = "<span>"+count+"</span>";
    var day = (count < 10) ? "0"+count : count;
    month = (month < 10)? "0"+month : month;
    $(td).attr('data-day', day);
    $(td).attr('data-month', month);
    $(td).attr('data-year', year);
    $(td).append('<div class="status-container"></div>');
    if(disabled) $(td).addClass("disabled");
    if(c >= 5) $(td).addClass('weekend-day-red');
    if(activeDay.isEqual(count, month, year))
        $(td).addClass("active-day");

    return td;
};

var selectTd = function(day, month, year){

    day = (day < 10)? "0"+day : day;
    month = (month < 10)? "0"+month : month;

    return $("td[data-day='"+day+"'][data-month='"+month+"'][data-year='"+year+"']");

};

Calendar.month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
Calendar.month_name_ru = ['Январь', 'Февраль', "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
Calendar.days_names = "ПнВтСрЧтПтСбВс";
Calendar.days_indexes = [1,2,3,4,5,6,0];




