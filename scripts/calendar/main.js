
$(".button").click(function(){

    if($(this).hasClass("disabled-button")) return false;

    /**
     * ЕСЛИ ЭТИ КНОПКИ ОТНОСЯТСЯ К ГОДУ
     */
    if($(this).attr("for") == 'year') {

        if ($(this).hasClass('left'))
            if(year - 1>= MIN_YEAR) { calendarService.changeYear(--year);  } else return false;
        if ($(this).hasClass('right'))
            calendarService.changeYear(++year);

    }

    /**
     * ЕСЛИ ЭТИ КНОПКИ ОТНОСЯТСЯ К МЕСЯЦАМ
     */
    if($(this).attr("for") == 'months') {

        if($(this).hasClass("disabled")) return false;
        if ($(this).hasClass('left'))
            if(month-3 < 0){
                month = 11;
                calendarService.changeYear(--year);
            }else month-=3;

        if ($(this).hasClass('right'))
            if(month+3 > 11){
                month = 0;
                calendarService.changeYear(++year);
            }else month+=3;

    }

    if(year > MIN_YEAR) $("#calendar-year .left").removeClass("disabled");
    else {
        month = (month > MIN_MONTH) ? month : MIN_MONTH;
        $("#calendar-year .left").addClass("disabled");
    }

    if(month > MIN_MONTH || year > MIN_YEAR) $("#change-months-container .left").removeClass("disabled");
    else $("#change-months-container .left").addClass("disabled");

    calendarService.rewrite(year, month).loadData(id).loadServicesData();

    return false;
});




$("#day-state-work, #day-state-weekend").click(function(){

    var obj = this;

    var activeDayTd = $("td.active-day");
    var reservedDay = (activeDayTd.children(".status-container").children(".reserved-day").length > 0);

    if(reservedDay){
        $(".confirm-block-wrapper, .confirm-block").css("display", "block");
        $(".confirm-block").addClass("bounceIn");
        $("footer, #content, .main-h1, header").addClass("blur");
        $("#yes").attr("href", $(this).attr("href"));
        return false;
    }

    changeDayState(this);

});

var changeDayState = function(object){

    if(!$(object).hasClass("active")){
        $(".day-state-button").removeClass("active");
        var state = $(object).attr("data-value");

        console.log(4);

        $.ajax({
            url : window.location.origin+"/core/ajaxsetdaystate",
            type : "POST",
            data : {state : state, day : calendarService.activeDay.day, month : calendarService.activeDay.month, year : calendarService.activeDay.year},
            success : function(status){
                var td = selectTd(calendarService.activeDay.day, calendarService.activeDay.month, calendarService.activeDay.year);

                switch(status){
                    case 'weekend' : $("#day-state-selector").attr("data-value", 0).html("Сделать рабочим"); td.addClass("disabled-button"); td.children(".status-container").children().remove(); calendarService.clearServicesList(); $("#services-container").css("display", "none"); break;
                    case 'workday' : $("#day-state-selector").attr("data-value", 1).html("Сделать выходным"); td.removeClass("disabled-button"); $("#services-container").css("display", "block"); break;
                }

                $(".day-state-button").removeClass("active");
                $(object).addClass("active");

            }
        })
    }
};


$("body").on("click", ".state-button", function(){

    var state = $(this).attr("data-state");
    var stateType = $(this).attr("data-state-type");
    var serviceID = $(this).attr("data-service-id");

    var self = this;

    if(state == 1){
        $(this).attr("href", "/profile/requests");
        return true;
    }

    $.ajax({
        url : window.location.origin+"/core/ajaxsetservicestate",
        type : "POST",
        dataType : 'json',
        data : {serviceID : serviceID, state : state, state_type : stateType, day : calendarService.activeDay.day, month : calendarService.activeDay.month, year : calendarService.activeDay.year},
        /**
         * @param result {{service_id: number, state_type: string, state: number}}
         */
        success : function(result){
            //console.log(result);
            var title = calendarService.STATE_TYPE_TITLES[result.state_type][result.state];

            $(self).html(title).attr("data-state", result.state);

            if( $("#services-container .service a.reserved-button[data-state='1']").length == 0 ){
                var td = selectTd(calendarService.activeDay.day, calendarService.activeDay.month, calendarService.activeDay.year);
                td.children(".status-container").children(".reserved-day").remove();
            }else {
                var td = selectTd(calendarService.activeDay.day, calendarService.activeDay.month, calendarService.activeDay.year);
                if( td.children(".status-container").children(".reserved-day").length == 0 )
                    td.children(".status-container").append('<div class="reserved-day"></div>');
            }

        }
    });

    return false;
});


