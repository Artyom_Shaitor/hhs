$(document).ready(function(){

    if($("#desc-textarea").length > 0)
        changeTextAreaIndicator("#desc-textarea");

    $("#photo-gallery .image").click(function(){
        var image = $(this).css("background-image");
        $("#photo-gallery #main-image").css("background-image", image).attr("data-image", $(this).attr("data-image")).attr("data-image-index", $(this).attr("data-image-index"));

    });



    $("#photo-gallery #main-image").click(function(){
        $(".wrapper").css("display", "block");
        $(".image-container").css("display", "block").css("background-image", "url("+$(this).attr("data-image")+")").attr("data-image-index", $(this).attr("data-image-index"));
    });

    $(".wrapper").click(function(){
        $(this).css("display", "none");
        $(".image-container").css("display", "none");
    });

    $(".image-container").click(function(){
        var dataIndex = parseInt($(this).attr("data-image-index"));
        var _this = this;
        var nextImg = getImgByIndex(dataIndex+1);

        if(!isset(nextImg)){
            nextImg = getImgByIndex(0);
            dataIndex = -1;
        }

        var url = nextImg.attr("data-image");
        $(_this).css("background-image", "url("+url+")").attr("data-image-index", dataIndex+1 );

        //console.log(isset($("#photo-gallery .image-list div[data-image-index='"+(dataIndex+1)+"']")));
    });

    $("#desc-textarea").on('input', function(){
        changeTextAreaIndicator(this);
    })

});

var changeTextAreaIndicator = function(obj){
    if($(obj).length > 0) {
        var maxlength = parseInt($(obj).attr("maxlength"));
        var currentLength = $(obj).val().length;

        var percent = currentLength / maxlength;
        var redColor = 20 + Math.floor(235 * Math.pow(percent, 3));
        var greenColor = 255 - redColor;


        $("[data-for='data-textarea']").css("color", 'rgb(' + redColor + ', ' + greenColor + ', 0)').html(maxlength - currentLength);
    }
}

/**
 *
 * @param element
 * @return bool
 */
var isset = function(element){
    if(element.length>0) return true;
    return false;
};

var getImgByIndex = function(index){
    return $("#photo-gallery #image-list div[data-image-index='"+index+"']");
};

