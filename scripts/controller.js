/**
 * Created by artyomshaitor on 14.08.15.
 */
$(document).ready(function(){

    $("[data-pop-up=true]").click(function(){
        var popupID = $(this).attr("for");

        $("#"+popupID).parent().css("display", "block");
        $("body, html").css("overflow", "hidden");
        return false;
    });

    $(".close-pop-up").click(function(){
        $(this).parent().parent().css("display", "none");
        $("body, html").css("overflow", "auto");
        return false;
    });

    //$(".manage-button").hover(function(){
    //    var alt = $(this).attr("alt");
    //    console.log($(this).css("background-color"));
    //});
});