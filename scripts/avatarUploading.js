var avatarXHR = new XMLHttpRequest();

avatarXHR.onload = function(){
    $("#avatarUpload").css("background-image", "url(/images/avatars/"+avatarXHR.response+")");
    $("#avatarHidden").val(avatarXHR.response);
    $("input[type='submit']").prop("disabled", false);
    $("#avatarUpload .bg-black").css("opacity", 0);
};

avatarXHR.upload.onerror = function(){
    $(".bg-black #percent").html("Ошибка");
};

avatarXHR.upload.onprogress = function(event){
    $(".bg-black #percent").html(Math.round(event.loaded * 100 / event.total) + "%");
};

avatarXHR.before = function(){
    $("#avatarUpload .bg-black").css("opacity", 1);
    $("input[type='submit']").prop("disabled", true);
};


$("input[name='avatarUploadButton'], #avatarUpload .bg-black").click(function(){
    if($("#Service_avatar_url").length > 0) $("#Service_avatar_url").click();
    if($("#User_avatar_url").length > 0) $("#User_avatar_url").click();
});

$("#Service_avatar_url").change(function(){
    var fd = new FormData();
    fd.append("avatar", this.files[0]);
    fd.append("model_id", $("input[name='model_id']").val());

    avatarXHR.open("POST", "/profile/service/loadAvatar");
    avatarXHR.sendWithBefore(fd);

});

$("#User_avatar_url").change(function(){
    var fd = new FormData();
    fd.append("avatar", this.files[0]);
    fd.append("model_id", $("input[name='model_id']").val());


    avatarXHR.open("POST", "/user/loadAvatar");
    avatarXHR.sendWithBefore(fd);

});


$("#avatarUpload a#discard").click(function(){
    avatarXHR.abort();
    $("#avatarUpload .bg-black").css("opacity", 0);
    return false;
});